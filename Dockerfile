FROM gitlab-registry.cern.ch/linuxsupport/alma9-base:latest

EXPOSE 8080

# The user id must be a generic one, since it is used in the OpenShift cluster
ENV USER_ID 1001
ENV WORKING_DIR /opt/app-root/src

RUN yum -y install epel-release
RUN yum -y install https://mirrors.rpmfusion.org/free/el/rpmfusion-free-release-$(rpm -E %rhel).noarch.rpm

# pcre is needed in order to have the UWSGI rules working
# CERN-CA-certs are required in order to make requests to sites with CERN certificates
RUN yum -y update
RUN yum -y install \
  gcc \
  git \
  python \
  python-devel \
  python-pip \
  openssl-devel \
  mysql-devel \
  pcre \
  pcre-devel \
  CERN-CA-certs \ 
  ffmpeg \
  ffmpeg-libs

RUN yum clean -y all

# Use the system certificates for requests. This includes the CERN's ones
ENV REQUESTS_CA_BUNDLE /etc/ssl/certs/ca-bundle.crt
ENV PYTHONPATH ${WORKING_DIR}
ENV FLASK_APP ${WORKING_DIR}/wsgi.py
ENV PYTHONUNBUFFERED 1
# The following environment variables are used by Poetry to install dependencies
ENV POETRY_VERSION 1.5.0
ENV POETRY_HOME /opt/poetry
ENV POETRY_VIRTUALENVS_IN_PROJECT true
ENV POETRY_CACHE_DIR ${WORKING_DIR}/.cache
ENV VIRTUAL_ENVIRONMENT_PATH ${WORKING_DIR}/.venv

# Adding the virtual environment to PATH in order to "activate" it.
# https://docs.python.org/3/library/venv.html#how-venvs-work
ENV PATH="$VIRTUAL_ENVIRONMENT_PATH/bin:$PATH"


RUN python --version
RUN python -m ensurepip --upgrade
RUN python -m pip --version

RUN mkdir -p ${WORKING_DIR}
# Set folder permissions
RUN chgrp -R 0 ${WORKING_DIR} && \
  chmod -R g=u ${WORKING_DIR}

WORKDIR ${WORKING_DIR}

# Install Poetry and dependencies
COPY pyproject.toml ./
COPY poetry.lock ./

# Using Poetry to install dependencies without requiring the project main files to be present
RUN pip install poetry==${POETRY_VERSION} && poetry install --only main --no-root --no-directory

COPY etc ./etc
COPY server ./server
COPY wsgi.py ./
COPY celery_app.py ./

COPY app ./app
COPY migrations ./migrations

CMD ["sh", "/opt/app-root/src/etc/entrypoint.sh"]