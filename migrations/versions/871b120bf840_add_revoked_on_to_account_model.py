"""Add revoked_on to Account model

Revision ID: 871b120bf840
Revises: 18719dc8f5b9
Create Date: 2022-04-04 09:58:43.861281

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '871b120bf840'
down_revision = '18719dc8f5b9'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('account', sa.Column('revoked_on', sa.DateTime(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('account', 'revoked_on')
    # ### end Alembic commands ###
