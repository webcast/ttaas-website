"""Add start_time and end_time to Upload

Revision ID: 624b6cfa349d
Revises: 7a45316ae251
Create Date: 2024-08-12 16:06:08.756252

"""
from alembic import op
import sqlalchemy as sa
import sqlalchemy_utils


# revision identifiers, used by Alembic.
revision = '624b6cfa349d'
down_revision = '7a45316ae251'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('upload', sa.Column('start_time', sa.String(length=255), nullable=True))
    op.add_column('upload', sa.Column('end_time', sa.String(length=255), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('upload', 'end_time')
    op.drop_column('upload', 'start_time')
    # ### end Alembic commands ###
