from sqlalchemy import extract

from app.models.accounts import Account
from app.models.token_blacklist import ApiToken
from app.models.uploads import TaskStates, Upload
from app.models.users import User


def get_users_metrics(year: int) -> dict:
    """Get the users metrics for the given year.

    Args:
        year (int): The year to get the metrics for.

    Returns:
        dict: A dict with the metrics.
    """
    new_users_count = (
        User.query.filter_by(active=True)
        .filter(extract("year", User.confirmed_at) == year)
        .count()
    )

    active_users_count = (
        User.query.filter_by(active=True)
        .filter(extract("year", User.last_login) == year)
        .count()
    )

    revoked_users_count = User.query.filter(
        extract("year", User.revoked_on) == year
    ).count()

    return {
        "new_users_count": new_users_count,
        "active_users_count": active_users_count,
        "revoked_users_count": revoked_users_count,
    }


def get_accounts_metrics(year: int):
    new_accounts = Account.query.filter(
        extract("year", Account.created_on) == year
    ).count()

    revoked_accounts = Account.query.filter(
        extract("year", Account.revoked_on) == year
    ).count()

    active_accounts = Account.query.filter(
        extract("year", Account.last_used) == year
    ).count()

    return {
        "new_accounts_count": new_accounts,
        "revoked_accounts_count": revoked_accounts,
        "active_accounts_count": active_accounts,
    }


def get_transcriptions_metrics(year: int):

    requested_transcriptions_count = Upload.query.filter(
        extract("year", Upload.created_on) == year
    ).count()

    completed_transcriptions_count = (
        Upload.query.filter(
            (Upload.state == TaskStates.COMPLETED)
            | (Upload.state == TaskStates.COMPLETED_CALLBACK_FAILED)
            | (Upload.state == TaskStates.COMPLETED_EMAIL_FAILED)
        )
        .filter(extract("year", Upload.created_on) == year)
        .count()
    )

    errored_transcriptions_count = (
        Upload.query.filter(extract("year", Upload.created_on) == year)
        .filter(
            (Upload.state == TaskStates.ERROR_RUNNING_TRANSCRIPTIONS)
            | (Upload.state == TaskStates.ERROR_RUNNING_TRANSLATIONS)
            | (Upload.state == TaskStates.ERROR_SUBMITTING)
            | (Upload.state == TaskStates.ERROR_UPLOADING)
            | (Upload.state == TaskStates.ERROR_CALLBACK_FAILED)
            | (Upload.state == TaskStates.ERROR_EMAIL_FAILED)
            | (Upload.state == TaskStates.ERROR_NOTIFIED)
            | (Upload.state == TaskStates.ERROR)
            | (Upload.state == TaskStates.ERROR_PREPARING_UPLOAD)
            | (Upload.state == TaskStates.ERROR)
        )
        .count()
    )

    return {
        "requested_transcriptions_count": requested_transcriptions_count,
        "completed_transcriptions_count": completed_transcriptions_count,
        "errored_transcriptions_count": errored_transcriptions_count,
    }


def get_api_tokens_metrics(year: int):
    new_api_tokens_count = ApiToken.query.filter(
        extract("year", ApiToken.created_on) == year
    ).count()

    active_api_tokens_count = ApiToken.query.filter(
        extract("year", ApiToken.last_used) == year
    ).count()

    revoked_api_tokens_count = (
        ApiToken.query.filter(extract("year", ApiToken.revoked_on) == year)
        .filter_by(revoked=True)
        .count()
    )

    return {
        "new_api_tokens_count": new_api_tokens_count,
        "active_api_tokens_count": active_api_tokens_count,
        "revoked_api_tokens_count": revoked_api_tokens_count,
    }


def get_metrics(year: int):

    users_metrics = get_users_metrics(year)
    accounts_metrics = get_accounts_metrics(year)
    transcriptions_metrics = get_transcriptions_metrics(year)
    api_tokens_metrics = get_api_tokens_metrics(year)

    return {
        "users": users_metrics,
        "accounts": accounts_metrics,
        "transcriptions": transcriptions_metrics,
        "api_tokens": api_tokens_metrics,
    }
