import logging

from flask_sqlalchemy.query import Query

from app.models.pre_uploads import PreUpload, PreUploadStates

logger = logging.getLogger("webapp.utils")


def build_sort_query(base_query, sort_by, direction) -> Query:
    if sort_by is not None:
        if direction == "asc":
            return base_query.order_by(getattr(PreUpload, sort_by).asc())
        if direction == "desc":
            return base_query.order_by(getattr(PreUpload, sort_by).desc())
    return base_query


def build_pre_uploads_query(
    sort_by, direction, account_id=None, username=None
) -> Query:
    base_query = PreUpload.query
    if account_id:
        base_query = PreUpload.query.filter_by(account_id=account_id)
    if username:
        base_query = PreUpload.query.filter(PreUpload.username == username)

    query = base_query.filter(
        PreUpload.state.not_in(
            [PreUploadStates.COMPLETED, PreUploadStates.ALREADY_EXISTS]
        )
    )
    sort_query = build_sort_query(query, sort_by, direction)
    return sort_query
