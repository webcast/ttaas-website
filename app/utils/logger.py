import logging
import sys
from datetime import datetime

from pytz import timezone, utc


def zurich_time(*args):
    """
    Convert the given date to Geneva/Zurich timezone
    :param args:
    :return:
    """
    utc_dt = utc.localize(datetime.utcnow())
    my_tz = timezone("Europe/Zurich")
    converted = utc_dt.astimezone(my_tz)
    return converted.timetuple()


def setup_logs(app, logger_name, to_stdout=True, to_file=False, to_mail=False):
    """
    Configure the application loggers

    :param app:
    :param logger_name:
    :param to_stdout:
    :param to_file:
    :return:
    """
    logger = logging.getLogger(logger_name)

    if app.config["LOG_LEVEL"] == "DEV":
        logger.setLevel(logging.DEBUG)

    if app.config["LOG_LEVEL"] == "PROD":
        logger.setLevel(logging.INFO)

    formatter = logging.Formatter(
        "%(asctime)s | %(levelname)s | %(name)s | %(message)s | %(pathname)s:%(lineno)d | %(funcName)s()"
    )

    logging.Formatter.converter = zurich_time

    if to_stdout:
        configure_stdout_logging(
            logger=logger, formatter=formatter, log_level=app.config["LOG_LEVEL"]
        )

    if to_file:
        configure_file_logging(logger=logger, formatter=formatter)

    if to_mail:
        configure_email_logging(logger=logger, app=app)


def configure_stdout_logging(logger=None, formatter=None, log_level="DEV"):
    stream_handler = logging.StreamHandler(stream=sys.stdout)

    stream_handler.setFormatter(formatter)
    if log_level == "DEV":
        stream_handler.setLevel(logging.DEBUG)
    if log_level == "PROD":
        stream_handler.setLevel(logging.INFO)

    logger.addHandler(stream_handler)
    print(f"Logging {str(logger)} to stdout -> True")


def configure_file_logging(logger=None, formatter=None):
    handler = logging.FileHandler("/logs/webapp.log")

    handler.setFormatter(formatter)
    handler.setLevel(logging.DEBUG)

    logger.addHandler(handler)
    print(f"Logging {str(logger)} to file -> True")


def configure_email_logging(logger=None, app=None):
    fmt_email = logging.Formatter(
        """
            Message type:  %(levelname)s
            Name:          %(name)s
            Location:      %(pathname)s:%(lineno)d
            Module:        %(module)s/%(filename)s
            Function:      %(funcName)s
            Time:          %(asctime)s
            Message:

            %(message)s

            Stack trace:

            %(stack_info)s
        """
    )

    prefix = ""
    if app.config.get("IS_DEV", False):
        prefix = "-TEST"
    subject = "[TTAAS-WEB" + prefix + "] Application error"

    # email in case of errors
    mail_handler = logging.handlers.SMTPHandler(
        app.config.get("LOG_MAIL_HOSTNAME"),
        app.config.get("LOG_MAIL_FROM"),
        app.config.get("LOG_MAIL_TO"),
        subject,
    )
    mail_handler.setLevel(logging.ERROR)
    mail_handler.setFormatter(fmt_email)
    logger.addHandler(mail_handler)
    print(f"Logging {str(logger)} to email -> True")
