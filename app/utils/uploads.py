import datetime
import logging
import os
import uuid
from typing import Optional, Union

from flask import current_app
from flask_sqlalchemy.query import Query
from werkzeug.datastructures import FileStorage
from werkzeug.utils import secure_filename

from app.api.api_config import ALLOWED_MEDIA_FILES, ALLOWED_UPLOAD_EXTENSIONS
from app.models.uploads import TaskStates, Upload

logger = logging.getLogger("webapp.utils")


def is_file_allowed(
    filename: str, allowed_files_list: Optional[list[str]] = None
) -> bool:
    """Check if the file extension is allowed

    Args:
        filename (str): The name of the file
        allowed_files_list (list[str]) (optional): List of file extensions to verify the the file against

    Returns:
        bool: Whether it is allowed or not
    """
    allowed_extensions = (
        allowed_files_list if allowed_files_list else ALLOWED_UPLOAD_EXTENSIONS
    )
    return "." in filename and filename.rsplit(".", 1)[1].lower() in allowed_extensions


def save_uploaded_files(
    account_name: str, media_file: FileStorage
) -> Union[str, None,]:
    """Save the media file

    Args:
        account_name (int): The name of the account used for the upload
        media_file (str): Media file path

    Returns:
        str: The path of the media file
    """
    media_file_path = None

    if (
        media_file
        and media_file.filename
        and is_file_allowed(media_file.filename, ALLOWED_MEDIA_FILES)
    ):
        media_file_path = save_uploaded_file(account_name, media_file)
        if media_file_path:
            return media_file_path
    return None


def save_uploaded_file(file_prefix: str, file: FileStorage) -> Union[str, None]:
    """Save the file to the uploads folder

    Args:
        file_prefix (str): Prefix of the file
        file (object): File to be saved

    Returns:
        str: Path of the saved file
    """
    new_file_name = generate_temp_file_name(file_prefix, file)
    if new_file_name:
        filename = secure_filename(new_file_name)
        file_path = os.path.join(current_app.config["UPLOADS_FOLDER"], filename)
        file.save(file_path)
        return file_path
    return None


def generate_temp_file_name(file_prefix: str, file: FileStorage) -> Union[str, None]:
    """Generates a temporary file name with a random UUID and the current date

    Args:
        file_prefix (str): The prefix of the file
        file (object): The file

    Returns:
        str: The new name of the file
    """
    if file and file.filename:
        current_date_time = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
        file_name = f"{file_prefix}_{current_date_time}_{uuid.uuid4()}"
        file_extension = f"{file.filename.rsplit('.', 1)[1]}"
        new_file_name = f"{file_name}.{file_extension}"
        return new_file_name
    return None


def build_sort_query(base_query, sort_by, direction) -> Query:
    if sort_by is not None:
        if direction == "asc":
            return base_query.order_by(getattr(Upload, sort_by).asc())
        if direction == "desc":
            return base_query.order_by(getattr(Upload, sort_by).desc())
    return base_query


def build_uploads_query(sort_by, direction, account_id=None, username=None) -> Query:
    base_query = Upload.query
    if account_id:
        base_query = Upload.query.filter_by(account_id=account_id)
    if username:
        base_query = Upload.query.filter(Upload.username == username)

    query = base_query.filter(Upload.state != TaskStates.DELETED)
    sort_query = build_sort_query(query, sort_by, direction)
    return sort_query
