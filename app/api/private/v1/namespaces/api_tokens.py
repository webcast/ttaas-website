import logging

from flask_restx import Namespace, Resource, abort, fields

from app.api.api_authorizations import authorizations
from app.daos.accounts import AccountDAO
from app.daos.api_tokens import ApiTokenDAO
from app.utils.auth.client_oidc import oidc_validate

logger = logging.getLogger("webapp.api.services")

namespace = Namespace(
    "api-tokens",
    description="API Tokens related operations",
    authorizations=authorizations,
    security=["Bearer Token"],
)


api_token_form = namespace.model(
    "ApiTokenForm",
    {
        "name": fields.String(
            required=True, description="The API token name", attribute="name"
        ),
        "resource": fields.String(
            required=True, description="The allowed resource", attribute="resource"
        ),
        "account": fields.Integer(
            description="Associated token account", attribute="account"
        ),
    },
)

api_token_list_model = namespace.model(
    "ApiTokenModelList",
    {
        "id": fields.String(
            readOnly=True, description="The api token unique identifier", attribute="id"
        ),
        "name": fields.String(
            required=True, description="The API token name", attribute="name"
        ),
        "resource": fields.String(
            required=True,
            description="The allowed resource",
            attribute="resource.value",
        ),
        "lastUsed": fields.DateTime(
            description="The last time the token was used", attribute="last_used"
        ),
    },
)

full_token_model = namespace.model(
    "FullApiTokenModel",
    {
        "id": fields.String(
            readOnly=True, description="The api token unique identifier", attribute="id"
        ),
        "name": fields.String(
            required=True, description="The API token name", attribute="name"
        ),
        "resource": fields.String(
            required=True,
            description="The allowed resource",
            attribute="resource.value",
        ),
        "accessToken": fields.String(
            description="The api key access token", attribute="access_token"
        ),
        "lastUsed": fields.DateTime(
            description="The last time the token was used", attribute="last_used"
        ),
    },
)


@namespace.doc(security=["Bearer Token"])
@namespace.route("/account/<int:account_id>/")
class ApiTokenListEndpoint(Resource):
    decorators = [oidc_validate]

    @namespace.doc("list_api_tokens")
    @namespace.marshal_list_with(api_token_list_model)
    def get(self, account_id):
        """Get all tokens from a given account"""
        logger.info("API Private GET API tokens")

        if AccountDAO.has_access_current_user_to_account(account_id):
            accounts = ApiTokenDAO.get_all(account_id)
            return accounts, 200

        abort(400, message="You don't have access to this account")

    @namespace.doc("create_api_token")
    @namespace.expect(
        api_token_form, description="The model that is expected to receive"
    )
    @namespace.marshal_with(full_token_model, code=201)
    def post(self, account_id):
        """Create a new api token"""
        args = namespace.payload
        instance = ApiTokenDAO.create(account_id, args)
        return instance, 201


@namespace.doc(security=["Bearer Token"])
@namespace.route("/<string:token_id>")
class ApiTokenDetailsEndpoint(Resource):
    decorators = [oidc_validate]

    @namespace.doc("token_details")
    @namespace.marshal_with(full_token_model)
    def put(self, token_id):
        """Update a token by its id"""
        logger.info("API Private refresh API token by ID")
        token = ApiTokenDAO.refresh_by_id(token_id)
        return token

    @namespace.doc("token_delete")
    def delete(self, token_id):
        """Revoke a token by id"""
        logger.info("API Private DELETE API token")

        result = ApiTokenDAO.delete_by_id(token_id)
        if result is True:
            return "", 204

        return abort(400)
