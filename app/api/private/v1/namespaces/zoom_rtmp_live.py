import logging
import time
from typing import Tuple

from flask import Response, current_app, request
from flask_restx import Namespace, Resource, fields
from requests import HTTPError

from app.api.api_authorizations import authorizations
from app.daos.accounts import AccountDAO
from app.services.zoom_rtmp_live.zoom_rmtp_live_service import (
    ZoomMeetingNotStartedException,
    ZoomRtmpLiveService,
    ZoomRtmpLiveServiceException,
)
from app.utils.auth.client_oidc import oidc_validate

logger = logging.getLogger("webapp.api")

namespace = Namespace(
    "zoom-rtmp-live",
    description="Accounts operations",
    authorizations=authorizations,
    security=["Bearer Token"],
)

account_model = namespace.model(
    "AccountModel",
    {
        "id": fields.Integer(required=True, description="The account ID"),
        "accountName": fields.String(
            required=True, description="The user first name", attribute="account_name"
        ),
        "isGuestAccount": fields.Boolean(
            required=True, default=False, attribute="is_guest_account"
        ),
    },
)


@namespace.doc(security=["Bearer Token"])
@namespace.route("/meeting/<int:meeting_id>/")
class ZoomLivemeetingEndpoint(Resource):
    method_decorators = [oidc_validate]

    @namespace.doc("accounts_endpoint")
    # @namespace.marshal_with(account_model, as_list=True)
    def get(self, meeting_id) -> Tuple[dict, int]:
        """Get the current user accounts

        Returns:
            Response: Flask response object
        """

        user_accounts = AccountDAO.get_current_user_accounts()
        zoom_live_account = AccountDAO.get_account_by_id(
            current_app.config["ZOOM_LIVE_ACCOUNT_INTERNAL_ID"]
        )
        result = None
        try:
            if zoom_live_account not in user_accounts:
                return {
                    "error": True,
                    "message": "You don't have access to the live asr",
                }, 400

            is_live = False
            service = ZoomRtmpLiveService()
            result = service.get_zoom_meeting_status(meeting_id)

            if result == "started":
                is_live = True
        except ZoomMeetingNotStartedException as error:
            logger.warning(f"Meeting {meeting_id} is not started yet ({str(error)})")
            is_live = False

        logger.debug(result)
        return {"meetingId": meeting_id, "isLive": is_live}, 200

    @namespace.doc("accounts_endpoint")
    # @namespace.marshal_with(account_model, as_list=True)
    def put(self, meeting_id) -> Tuple[dict, int]:
        """Configure the meeting live stream and start it

        Returns:
            Response: Flask response object
        """

        user_accounts = AccountDAO.get_current_user_accounts()
        zoom_live_account = AccountDAO.get_account_by_id(
            current_app.config["ZOOM_LIVE_ACCOUNT_INTERNAL_ID"]
        )
        result = None
        if zoom_live_account not in user_accounts:
            return {
                "error": True,
                "message": "You don't have access to the live asr",
            }, 400

        service = ZoomRtmpLiveService()
        result = service.set_zoom_meeting_live_stream_settings(meeting_id)
        if result:
            return {"meetingId": meeting_id, "started": True}, 200
        return {"meetingId": meeting_id, "started": False}, 400


@namespace.doc(security=["Bearer Token"])
@namespace.route("/sessions/count/")
class AsrStreamingSessionsCountEndpoint(Resource):
    method_decorators = [oidc_validate]

    @namespace.doc("accounts_endpoint")
    # @namespace.marshal_with(account_model, as_list=True)
    def get(self) -> Tuple[dict, int]:
        """Get the current active streaming sessions

        Returns:
            Response: Flask response object
        """
        user_accounts = AccountDAO.get_current_user_accounts()
        logger.debug(user_accounts)
        service = ZoomRtmpLiveService()
        result = service.list_streaming_sessions()
        logger.debug(result)
        # TODO Bloquear si el meeting id ya esta en uso en la lista de sesiones
        if not result["error"]:
            return {
                "error": False,
                "activeSessions": len(result["meetingsInfo"]),
                "maxSessions": current_app.config[
                    "LIVESTREAMING_MAX_PARALLEL_SESSIONS"
                ],
            }, 200
        return {"error": True, "activeSessions": "Unknown"}, 400


@namespace.doc(security=["Bearer Token"])
@namespace.route("/sessions/")
class AsrStreamingSessionsEndpoint(Resource):
    method_decorators = [oidc_validate]

    @namespace.doc("accounts_endpoint")
    # @namespace.marshal_with(account_model, as_list=True)
    def get(self) -> Response:
        """Get the current active streaming sessions

        Returns:
            Response: Flask response object
        """
        user_accounts = AccountDAO.get_current_user_accounts()
        logger.debug(user_accounts)
        service = ZoomRtmpLiveService()
        result = service.list_streaming_sessions()
        logger.debug(result)

        return result


@namespace.doc(security=["Bearer Token"])
@namespace.route("/sessions/<int:meeting_id>/")
class StartAsrStreamingSessionsEndpoint(Resource):
    method_decorators = [oidc_validate]

    @namespace.doc("start_session_endpoint")
    # @namespace.marshal_with(account_model, as_list=True)
    def post(self, meeting_id) -> Tuple[dict, int]:
        """Start a live asr session with the given meeting id

        Returns:
            Response: Flask response object
        """
        user_accounts = AccountDAO.get_current_user_accounts()
        logger.debug(user_accounts)

        # Get live meeting
        zoom_live_account = AccountDAO.get_account_by_id(
            current_app.config["ZOOM_LIVE_ACCOUNT_INTERNAL_ID"]
        )
        result_live = None

        if zoom_live_account not in user_accounts:
            return {
                "meetingId": meeting_id,
                "isLive": False,
                "configured": False,
                "livestreamStarted": False,
                "asrStarted": False,
                "error": True,
                "message": "You don't have access to the live asr",
            }, 200

        service = ZoomRtmpLiveService()
        is_live = False
        try:
            result_live = service.get_zoom_meeting_status(meeting_id)
            if result_live == "started":
                is_live = True
        except ZoomMeetingNotStartedException as error:
            logger.warning(f"Meeting {meeting_id} is not started yet ({str(error)})")
            is_live = False
            return {
                "meetingId": meeting_id,
                "isLive": is_live,
                "configured": False,
                "livestreamStarted": False,
                "asrStarted": False,
                "error": True,
                "message": "Meeting is not live",
            }, 200

        time.sleep(1)
        # Configure live stream
        configure_result = service.set_zoom_meeting_live_stream_settings(meeting_id)
        logger.debug(f"Configure result: {configure_result}")
        if not configure_result:
            return {
                "meetingId": meeting_id,
                "configured": False,
                "liveStreamStarted": False,
                "isLive": is_live,
                "asrStarted": False,
                "error": True,
                "message": "Error configuring meeting",
            }, 200

        time.sleep(1)
        try:
            start_livestream = service.start_meeting_live_streaming(meeting_id)
            logger.debug(f"Start livestream result: {start_livestream}")
            if not start_livestream:
                return {
                    "meetingId": meeting_id,
                    "isLive": is_live,
                    "configured": True,
                    "livestreamStarted": False,
                    "asrStarted": False,
                    "error": True,
                    "message": "Livestream was not started. Please, try again in a few seconds.",
                }, 200

        except HTTPError as error:
            logger.error(f"Error starting livestream: {str(error)}")
            return {
                "meetingId": meeting_id,
                "isLive": is_live,
                "configured": True,
                "livestreamStarted": False,
                "asrStarted": False,
                "error": True,
                "message": "Error starting livestream. Please verify it is not already started.",
            }, 200

        time.sleep(1)
        # Start RTMP live
        try:
            # Get channel and password from query string
            channel = request.args.get("channel")
            password = request.args.get("password")
            start_result = service.connect_to_live_asr(
                meeting_id, channel=channel, password=password
            )
            logger.debug(f"Start result: {start_result}")
            if start_result:
                return {
                    "meetingId": meeting_id,
                    "started": True,
                    "isLive": is_live,
                    "configured": True,
                    "livestreamStarted": True,
                    "asrStarted": True,
                    "error": False,
                    "message": "Zoom Live ASR started",
                }, 200
        except ZoomRtmpLiveServiceException as error:
            logger.error(f"Error starting live asr: {str(error)}")
            return {
                "meetingId": meeting_id,
                "isLive": is_live,
                "configured": True,
                "livestreamStarted": True,
                "asrStarted": False,
                "error": True,
                "message": "Error from the Zoom API during the ASR RTMP start",
            }, 200

        except HTTPError as error:
            logger.error(f"Error connecting to live asr: {str(error)}")
            return {
                "meetingId": meeting_id,
                "isLive": is_live,
                "configured": True,
                "livestreamStarted": True,
                "asrStarted": False,
                "error": True,
                "message": "Error connecting to Live ASR RMTP server",
            }, 200

        return {
            "meetingId": meeting_id,
            "isLive": is_live,
            "configured": True,
            "livestreamStarted": True,
            "asrStarted": False,
            "error": True,
            "message": "Unable to start Zoom Live ASR",
        }, 200
