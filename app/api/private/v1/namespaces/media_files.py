import io
import json
import logging
import os
import tempfile
from multiprocessing import Process
from typing import Any, Dict, Tuple, Union

import flask
from flask import Response
from flask_login import current_user
from flask_restx import Namespace, Resource, fields, reqparse

from app.api.api_authorizations import authorizations
from app.daos.accounts import AccountDAO
from app.services.mllp.api_client import ApiMLLPMetadataStatus, ApiSpeechClientMLLP
from app.utils.auth.client_oidc import oidc_validate

logger = logging.getLogger("webapp.api")

namespace = Namespace(
    "transcriptions",
    description="Files operations",
    authorizations=authorizations,
    security=["Bearer Token"],
)

speaker = namespace.model("speaker", {"name": fields.String()})

mediainfo = namespace.model(
    "mediainfo",
    {
        "language": fields.String(),
        "title": fields.String(),
        "date": fields.String(),
        "category": fields.String(),
        "duration": fields.Integer(),
        "speakers": fields.List(fields.Nested(speaker)),
    },
)

media = namespace.model(
    "media",
    {
        "is_url": fields.Boolean(),
        "type_code": fields.Integer(),
        "media_format": fields.String(),
        "location": fields.String(),
    },
)

audiotracks = namespace.model(
    "audiotracks",
    {
        "lang": fields.String(),
        "voice_type": fields.Integer(),
        "id": fields.Integer(),
        "location": fields.String(),
        "media_format": fields.String(),
        "audio_type": fields.String(),
        "is_url": fields.Boolean(),
        "sub_type": fields.Integer(),
        "description": fields.String(),
    },
)

media_model_metadata = namespace.model(
    "Media Model Metadata",
    {
        "rcode": fields.Integer(
            required=True,
            description="Description",
        ),
        "rcode_description": fields.String(
            required=True, description="The description of the rcode"
        ),
        "audiotracks": fields.Nested(audiotracks),
        "media": fields.List(fields.Nested(media)),
        "mediainfo": fields.Nested(mediainfo),
    },
)

caption = namespace.model(
    "caption",
    {
        "lang_code": fields.String(),
        "lang_name": fields.String(),
        "last_updated": fields.String(),
    },
)

media_model_list_item = namespace.model(
    "Media Model List Item",
    {
        "id": fields.String(),
        "media": fields.List(fields.Nested(media)),
        "metadata": fields.Nested(mediainfo),
    },
)

media_list = namespace.model(
    "Media list",
    {
        "total": fields.Integer(
            required=True,
            description="Total number of media",
        ),
        "objects": fields.List(fields.Nested(media_model_list_item)),
    },
)


@namespace.doc(security=["Bearer Token"])
@namespace.route("/account/<int:account_id>/media/")
class MediaEndpoint(Resource):

    method_decorators = [oidc_validate]

    @namespace.doc("List all media")
    @namespace.marshal_with(media_list, as_list=False)
    @namespace.doc(
        params={"limit": "Number of results to return per page. Defaults to 10"}
    )
    @namespace.doc(params={"page": "Page number of results to return. Defaults to 1"})
    @namespace.doc(params={"title": "Title of the media to filter"})
    @namespace.doc(params={"external_id": "External ID of the media to filter"})
    @namespace.doc(params={"sort": "Field to sort by. Defaults to date"})
    @namespace.doc(params={"from": "Date from to filter"})
    @namespace.doc(params={"to": "Date to to filter"})
    def get(
        self, account_id
    ) -> Union[Tuple[Dict[str, str], int], Tuple[Union[str, bytes], int]]:
        """Get the current user media files

        Returns:
            Response: Flask response object
        """
        if AccountDAO.has_access_current_user_to_account(account_id):
            parser = reqparse.RequestParser()
            parser.add_argument("limit", type=int)
            parser.add_argument("page", type=int)
            parser.add_argument("title", type=str)
            parser.add_argument("sort", type=str)
            parser.add_argument("from", type=str)
            parser.add_argument("to", type=str)
            parser.add_argument("external_id", type=str)

            items_per_page = parser.parse_args()["limit"]
            page_number = parser.parse_args()["page"]
            title = parser.parse_args()["title"]
            sort_by = parser.parse_args()["sort"]
            date_from = parser.parse_args()["from"]
            date_to = parser.parse_args()["to"]
            external_id = parser.parse_args()["external_id"]

            account = AccountDAO.get_account_by_id(account_id)
            api_client = ApiSpeechClientMLLP(substitute_user=account.su_account)
            files = api_client.get_media_full_list(
                items_per_page,
                page_number,
                title=title,
                external_id=external_id,
                sort_by=sort_by,
                date_from=date_from,
                date_to=date_to,
            )
            files = json.loads(files)
            return files, 200
        return {"error": "Not authorized"}, 403


@namespace.doc(security=["Bearer Token"])
@namespace.route("/account/<int:account_id>/media/<string:media_id>/")
class MediaDetailsEndpoint(Resource):

    method_decorators = [oidc_validate]

    @namespace.doc("Get media details")
    @namespace.marshal_with(media_model_metadata, as_list=False)
    def get(self, account_id, media_id) -> Tuple[Dict[str, str], int]:
        """Get the current media metadata

        Returns:
            Response: Flask response object
        """
        if AccountDAO.has_access_current_user_to_account(account_id):
            account = AccountDAO.get_account_by_id(account_id)
            api_client = ApiSpeechClientMLLP(substitute_user=account.su_account)
            file = api_client.get_media_metadata(media_id)
            file = json.loads(file)

            if file["rcode"] == ApiMLLPMetadataStatus.DOESNT_EXIST.value:
                return {"error": "Requested media doesn't exist"}, 400

            return file, 200
        return {"error": "Not authorized"}, 403


lang_model_list_item = namespace.model(
    "Lang Model List Item",
    {
        "audiotracks": fields.Nested(audiotracks),
        "system_time": fields.String(),
        "sup_status": fields.Integer(),
        "sup_ratio": fields.Float(),
        "rev_rtf": fields.Float(),
        "system_rtf": fields.String(),
        "lang_code": fields.String(),
        "rev_time": fields.Integer(),
        "last_updated": fields.String(),
        "lang_name": fields.String(),
    },
)

langs_model_list = namespace.model(
    "Langs list",
    {
        "media_lang": fields.String(),
        "rcode_description": fields.String(),
        "rcode": fields.Integer(),
        "subs_locked": fields.Boolean(),
        "langs": fields.List(fields.Nested(lang_model_list_item)),
    },
)


@namespace.doc(security=["Bearer Token"])
@namespace.route("/account/<int:account_id>/media/<string:media_id>/languages/")
class LanguagesEndpoint(Resource):

    method_decorators = [oidc_validate]

    @namespace.doc("Get media languages")
    @namespace.marshal_with(langs_model_list, as_list=False)
    def get(self, account_id, media_id) -> Tuple[Dict[str, str], int]:
        """Get the current media languages

        Returns:
            Response: Flask response object
        """
        logger.debug("Getting media languages")
        if AccountDAO.has_access_current_user_to_account(account_id):
            account = AccountDAO.get_account_by_id(account_id)
            api_client = ApiSpeechClientMLLP(substitute_user=account.su_account)
            file = api_client.get_media_langs(media_id)
            file = json.loads(file)

            return file, 200
        return {"error": "Not authorized"}, 403


@namespace.doc(security=["Bearer Token"])
@namespace.route(
    "/account/<int:account_id>/media/<string:media_id>/languages/<string:lang>/"
)
class LanguagesDownloadEndpoint(Resource):

    method_decorators = [oidc_validate]

    @namespace.doc("Download a media language")
    def get(
        self, account_id, media_id, lang
    ) -> Union[Tuple[Dict[str, str], int], Response]:
        """Get the current media languages

        Returns:
            Response: Flask response object
        """
        logger.debug("Download media language")
        if AccountDAO.has_access_current_user_to_account(account_id):
            account = AccountDAO.get_account_by_id(account_id)
            api_client = ApiSpeechClientMLLP(substitute_user=account.su_account)
            downloaded_file = api_client.get_media_download(media_id, lang)

            with tempfile.TemporaryDirectory() as temp_dir:
                file_path = os.path.join(temp_dir, f"{media_id}_{lang}.vtt")
                with open(file_path, "w", encoding="utf-8") as file:
                    file.write(downloaded_file)
                return self.send_temp_file(file_path)
        return {"error": "Not authorized"}, 403

    def background_job(self, callback):
        task = Process(target=callback())
        task.start()

    def send_temp_file(
        self,
        file_path: str,
    ) -> Response:
        """_summary_

        Args:
            file_path (str): Path of the file that is being generated

        Returns:
            Response: Flask response object
        """
        with open(file_path, "rb") as file:
            content = io.BytesIO(file.read())

        return flask.send_file(
            content,
            as_attachment=True,
            download_name=os.path.split(file_path)[1],
            mimetype="application/octet-stream",
        )


@namespace.doc(security=["Bearer Token"])
@namespace.route("/account/<int:account_id>/media/<string:media_id>/editor/")
class EditorLinksEndpoint(Resource):

    method_decorators = [oidc_validate]

    @namespace.doc("Get the editor link")
    # @namespace.marshal_with(media_model_list_item, as_list=True)
    def get(self, account_id, media_id) -> Tuple[Dict[Any, Any], int]:
        """Get the current media languages

        Returns:
            Response: Flask response object
        """
        logger.debug("Getting editor links")
        if AccountDAO.has_access_current_user_to_account(account_id):

            account = AccountDAO.get_account_by_id(account_id)
            tlp_api_client = ApiSpeechClientMLLP(
                api_user=account.su_account, api_key=account.api_secret_key
            )
            editor_urls = tlp_api_client.get_editor_urls(
                media_id, current_user.first_name, current_user.last_name
            )
            return editor_urls, 200
        return {"error": "Not authorized"}, 403
