import logging

from flask_restx import Namespace, Resource, fields

from app.api.api_authorizations import authorizations
from app.utils.auth.client_oidc import oidc_validate

logger = logging.getLogger("webapp.api")

namespace = Namespace(
    "test",
    description="Test related operations",
    authorizations=authorizations,
    security=["Bearer Token"],
)


test_model = namespace.model(
    "TestModel",
    {
        "result": fields.String(required=True, description="The test result"),
    },
)


@namespace.doc(security=["Bearer Token"])
@namespace.route("/")
class TestEndpoint(Resource):

    method_decorators = [oidc_validate]

    @namespace.doc("test_endpoint")
    @namespace.marshal_with(test_model)
    def get(self):
        """
        Test response with secured endpoint

        This endpoint can be used to check the API connectivity.

        """
        result = {"result": "ok"}
        return result
