import json
import logging
import os
import uuid
from typing import Any, Dict, Tuple, Union

from flask import current_app, request
from flask_login import current_user
from flask_restx import Namespace, Resource, fields, reqparse

from app.api.api_authorizations import authorizations
from app.api.api_config import UPLOAD_DIRECTION_KEYS, UPLOAD_SORT_KEYS
from app.daos.accounts import AccountDAO
from app.daos.pre_uploads import PreUploadDAO, PreUploadDAOException
from app.daos.state_log import StateLogDAO
from app.daos.uploads import UploadDAO
from app.models.pre_uploads import PreUpload
from app.models.uploads import NotificationMethods, UploadTypes
from app.services.mllp.api_client import ApiSpeechClientMLLP
from app.tasks.ingest import ingest_task
from app.utils.auth.client_oidc import oidc_validate
from app.utils.uploads import build_uploads_query, save_uploaded_files

logger = logging.getLogger("webapp.api")

namespace = Namespace(
    "uploads",
    description="Uploads operations",
    authorizations=authorizations,
    security=["Bearer Token"],
)

upload_model = namespace.model(
    "Upload",
    {
        "id": fields.Integer(required=True, description="The file ID"),
        "mediaId": fields.String(
            required=True, description="The media ID", attribute="media_id"
        ),
        "language": fields.String(
            required=True, description="The media language", attribute="language.value"
        ),
        "title": fields.String(required=True, description="The media title"),
        "state": fields.String(
            description="Current upload status", attribute="state.value"
        ),
        "account": fields.String(
            description="Used account", attribute="account.account_name"
        ),
        "comments": fields.String(description="Comments"),
        "notificationEmail": fields.String(
            required=True,
            description="Notification email address",
            attribute="notification_email",
        ),
        "createdOn": fields.DateTime(
            required=True,
            description="The upload creation date",
            attribute="created_on",
        ),
        "updatedOn": fields.DateTime(
            required=True, description="The upload update date", attribute="updated_on"
        ),
        "startTime": fields.String(
            required=False, description="The upload start time", attribute="start_time"
        ),
        "endTime": fields.String(
            required=False, description="The upload end time", attribute="end_time"
        ),
    },
)


uploads_list = namespace.model(
    "Uploads list",
    {
        "total": fields.Integer(
            required=True,
            description="Total number of uploads",
        ),
        "objects": fields.List(fields.Nested(upload_model)),
    },
)


@namespace.doc(security=["Bearer Token"])
@namespace.route("/account/<int:account_id>/")
class UploadsEndpoint(Resource):
    method_decorators = [oidc_validate]

    parser = reqparse.RequestParser()
    parser.add_argument(
        "sortBy",
        type=str,
        choices=UPLOAD_SORT_KEYS,
        help=f"Sort keys for uploads. Valid values: {', '.join(UPLOAD_SORT_KEYS)}. Defaults to created_on",
    )
    parser.add_argument(
        "direction",
        type=str,
        choices=UPLOAD_DIRECTION_KEYS,
        help=(
            "Direction for uploads sorting. "
            f"Valid values: {', '.join(UPLOAD_DIRECTION_KEYS)}. Defaults to desc"
        ),
    )
    parser.add_argument(
        "limit", type=int, help="Number of items per page. Defaults to 10"
    )
    parser.add_argument(
        "page", type=int, help="Page of items to display. Defaults to 1"
    )

    @namespace.doc("Get uploads")
    @namespace.expect(parser)
    @namespace.marshal_with(uploads_list, as_list=False)
    def get(self, account_id) -> Tuple[Dict[str, Any], int]:
        """Get the uploads from the account

        Returns:
            Response: Flask response object
        """
        logger.debug("Get uploads")
        args = self.parser.parse_args()
        sort_by = args.get("sortBy", "created_on")
        direction = args.get("direction", "desc")
        items_per_page = args.get("limit", 10)
        page_number = args.get("page", 1)

        if page_number < 1:
            page_number = 1

        if AccountDAO.has_access_current_user_to_account(account_id):
            query = build_uploads_query(sort_by, direction, account_id=account_id)
            record_query = query.paginate(
                page=page_number, per_page=items_per_page, count=True
            )
            total = record_query.total
            record_items = record_query.items

            return {"total": total, "objects": record_items}, 200
        return {"error": "Unauthorized"}, 401


@namespace.doc(security=["Bearer Token"])
@namespace.route("/media/account/<int:account_id>/<string:media_id>/status/")
class MediaStatusEndpoint(Resource):
    method_decorators = [oidc_validate]

    @namespace.doc("Get media status")
    # @namespace.marshal_with(media_model_list_item, as_list=True)
    def get(self, account_id, media_id) -> Tuple[Dict[str, str], int]:
        """Get the current upload status

        Returns:
            Response: Flask response object
        """
        if AccountDAO.has_access_current_user_to_account(account_id):
            account = AccountDAO.get_account_by_id(account_id)
            api_client = ApiSpeechClientMLLP(substitute_user=account.su_account)
            file = api_client.get_upload_status(media_id)
            file = json.loads(file)

            return file, 200
        return {"error": "Unauthorized"}, 401


@namespace.route("/user/")
class UserUploadsEndpoint(Resource):
    method_decorators = [oidc_validate]

    parser = reqparse.RequestParser()
    parser.add_argument(
        "sortBy",
        type=str,
        choices=UPLOAD_SORT_KEYS,
        help=f"Sort keys for uploads. Valid values: {', '.join(UPLOAD_SORT_KEYS)}. Defaults to created",
    )
    parser.add_argument(
        "direction",
        type=str,
        choices=UPLOAD_DIRECTION_KEYS,
        help=(
            "Direction for uploads sorting. "
            f"Valid values: {', '.join(UPLOAD_DIRECTION_KEYS)}. Defaults to desc"
        ),
    )
    parser.add_argument(
        "limit", type=int, help="Number of items per page. Defaults to 10"
    )
    parser.add_argument(
        "page", type=int, help="Page of items to display. Defaults to 1"
    )

    @namespace.doc("User uploads")
    @namespace.expect(parser)
    @namespace.marshal_with(uploads_list, as_list=True)
    def get(self) -> Tuple[Dict[str, Any], int]:
        """Retrieve the user uploads assigned to the current user

        Returns:
            Response: Flask response object
        """
        logger.debug("Get Other uploads")

        args = self.parser.parse_args()
        sort_by = args.get("sortBy", "created_on")
        direction = args.get("direction", "desc")
        items_per_page = args.get("limit", 10)
        page_number = args.get("page", 1)

        query = build_uploads_query(sort_by, direction, username=current_user.username)  # type: ignore
        record_query = query.paginate(page=page_number, per_page=items_per_page)

        total = record_query.total
        record_items = record_query.items

        return {"total": total, "objects": record_items}, 200


@namespace.doc(security=["Bearer Token"])
@namespace.route("/account/<int:account_id>/ingest/")
class IngestEndpoint(Resource):
    method_decorators = [oidc_validate]

    @namespace.doc("Ingest media files")
    @namespace.marshal_with(upload_model)
    def post(
        self, account_id
    ) -> Union[Tuple[PreUpload, int], Tuple[Dict[str, str], int]]:
        """Ingest

        Returns:
            Response: Flask response object
        """

        if AccountDAO.has_access_current_user_to_account(account_id):
            account = AccountDAO.get_account_by_id(account_id)

            media_file = request.files.get("mediaFile")
            title = request.form.get("title")
            language = request.form.get("language")
            # terms = request.form.get("termsOfService") TODO?
            comments = request.form.get("comments", "")
            username = request.form.get("username", "")
            notification_email = request.form.get("notificationEmail", None)
            media_id = f"{account.su_account}_{uuid.uuid4()}"
            transcription_is_public = request.form.get("isPublic", False)
            start_time = request.form.get("startTime", None)
            end_time = request.form.get("endTime", None)

            if isinstance(transcription_is_public, str):
                transcription_is_public = transcription_is_public.lower() == "true"

            logger.debug(request.form)
            logger.debug(request.files)

            upload_info = {
                "account_id": account_id,
                "acl_read_groups": "",
                "acl_read_users": "",
                "comments": comments,
                "language": language,
                "notification_email": notification_email,
                "notification_method": NotificationMethods.EMAIL.value,
                "notification_url": "",
                "reference_url": "",
                "su_account": account.su_account,
                "title": title,
                "transcription_is_public": transcription_is_public,
                "upload_type": UploadTypes.WEB.value,
                "username": username,
                "start_time": start_time,
                "end_time": end_time,
            }

            logger.debug(upload_info)
            try:
                if not media_file:
                    return {"error": "Media file is required"}, 400
                if not language:
                    return {"error": "Language is required"}, 400
                if not title:
                    return {"error": "Title is required"}, 400

                media_file_path = save_uploaded_files(
                    account.su_account,
                    media_file,
                )

                upload_info["media_file_path"] = media_file_path
                try:
                    pre_upload = PreUploadDAO.create_pre_upload(
                        account_id,
                        username,
                        media_id,
                        title,
                        notification_method=upload_info["notification_method"],
                        notification_email=notification_email,
                        notification_url="",
                    )

                    ingest_task.apply_async(
                        queue=current_app.config["CELERY_INGEST_QUEUE"],
                        args=[upload_info, media_id],
                    )

                    return pre_upload, 201

                except PreUploadDAOException as error:
                    logger.warning(error)
                    return {"error": str(error)}, 400
            except OSError as error:
                logger.error(
                    f"Disk quota exceded ({error}). Please, empty the uploads volume."
                )
                return {"error": "Server error"}, 500
        return {"error": "Unauthorized"}, 401


@namespace.doc(security=["Bearer Token"])
@namespace.route("/account/<int:account_id>/uploads/<int:upload_id>/media-file/")
class MediaFileEndpoint(Resource):
    method_decorators = [oidc_validate]

    @namespace.doc("Get media file existence")
    # @namespace.marshal_with(media_model_list_item, as_list=True)
    def get(
        self, account_id, upload_id
    ) -> Union[Tuple[Dict[str, bool], int], Tuple[Dict[str, str], int]]:
        if AccountDAO.has_access_current_user_to_account(account_id):
            upload = UploadDAO.get_upload_by_id(upload_id)
            logger.debug(upload)

            # Check if file exists
            logger.debug(upload.media_file_path)
            file_exists = os.path.exists(upload.media_file_path)

            result = {
                "exists": file_exists,
            }

            return result, 200
        return {"error": "Unauthorized"}, 401


@namespace.doc(security=["Bearer Token"])
@namespace.route("/account/<int:account_id>/uploads/<int:upload_id>/retry-ingest/")
class RetryIngestEndpoint(Resource):
    method_decorators = [oidc_validate]

    @namespace.doc("Retry ingest for a given upload")
    @namespace.marshal_with(upload_model)
    def post(
        self, account_id, upload_id
    ) -> Union[Tuple[Dict[str, bool], int], Tuple[Dict[str, str], int]]:
        logger.info(f"Retry ingest for upload {upload_id} (account {account_id})")
        if AccountDAO.has_access_current_user_to_account(account_id):
            account = AccountDAO.get_account_by_id(account_id)

            upload = UploadDAO.get_upload_by_id(upload_id)

            media_id = f"{account.su_account}_{uuid.uuid4()}"

            upload_info = {
                "account_id": account_id,
                "acl_read_groups": upload.cern_search_settings.read_groups,
                "acl_read_users": upload.cern_search_settings.read_users,
                "comments": upload.comments,
                "language": upload.language.value,
                "notification_email": upload.notification_email,
                "notification_method": upload.notification_method.value,
                "notification_url": upload.notification_url,
                "reference_url": upload.reference_url,
                "title": upload.title,
                "transcription_is_public": upload.cern_search_settings.is_public,
                "upload_type": upload.upload_type.value,
                "username": upload.username,
                "media_file_path": upload.media_file_path,
                "su_account": account.su_account,
            }
            logger.debug(f"Retry ingest info: {upload_info}")
            try:
                logger.debug(f"Creating pre-upload for {media_id}...")
                pre_upload = PreUploadDAO.create_pre_upload(
                    account_id,
                    upload.username,
                    media_id,
                    upload.title,
                    notification_method=upload_info["notification_method"],
                    notification_email=upload.notification_email,
                    notification_url=upload.notification_url,
                )
                logger.debug(f"Calling ingest task for {media_id}...")
                ingest_task.apply_async(
                    queue=current_app.config["CELERY_INGEST_QUEUE"],
                    args=[upload_info, media_id],
                )
                logger.debug(f"Deleting upload {upload_id}...")
                UploadDAO.delete_upload_by_id(upload_id)
                StateLogDAO.create(
                    upload_id, f"Upload retried with media_id {media_id}"
                )

                return pre_upload, 201

            except PreUploadDAOException as error:
                logger.warning(error)
                return {"error": str(error)}, 400

        return {"error": "Unauthorized"}, 401
