import logging
from typing import Dict, Tuple

import requests
from flask import current_app
from flask_restx import Namespace, Resource, fields, reqparse

from app.api.api_authorizations import authorizations
from app.services.cern_search.cern_search_service import CernSearchService

# from app.utils.auth.client_oidc import oidc_validate

logger = logging.getLogger("webapp.api")

namespace = Namespace(
    "search-weblectures",
    description="Search for media in CERN Search",
    authorizations=authorizations,
    security=["Bearer Token"],
)

weblecture_search_model_list_item = namespace.model(
    "CERN Search Weblecture Item",
    {
        "id": fields.String(attribute="control_number"),
        "url": fields.String(),
        "language": fields.String(),
        "event_date": fields.String(attribute="eventdate"),
        "title": fields.String(),
        "ttpmediaid": fields.String(attribute="ttpmediaid"),
        "times": fields.Integer(),
    },
)

weblecture_search_list = namespace.model(
    "Weblecture Search List",
    {
        "total": fields.Integer(
            required=True,
            description="Total number of weblectures in the search",
        ),
        "objects": fields.List(fields.Nested(weblecture_search_model_list_item)),
    },
)


def build_elastic_search_date(event_date_from, event_date_to):
    date_range = None
    if event_date_from or event_date_to:
        if event_date_from and event_date_to:
            # Format the date to be using elastic search dsl format
            date_range = f"[{event_date_from} TO {event_date_to}]"
        elif event_date_from and not event_date_to:
            date_range = f"[{event_date_from} TO *]"
        elif not event_date_from and event_date_to:
            date_range = f"[* TO {event_date_to}]"

        date_range = f"eventdate={date_range}"
    return date_range


@namespace.doc(security=["Bearer Token"])
@namespace.route("/")
class SearchWeblecturesEndpoint(Resource):
    # method_decorators = [oidc_validate]

    @namespace.doc("Search for public weblectures")
    @namespace.marshal_with(weblecture_search_list, as_list=False)
    # @namespace.doc(
    #     params={"limit": "Number of results to return per page. Defaults to 10"}
    # )
    # @namespace.doc(params={"page": "Page number of results to return. Defaults to 1"})
    @namespace.doc(params={"title": "Title of the media to filter"})
    # @namespace.doc(params={"external_id": "External ID of the media to filter"})
    # @namespace.doc(params={"sort": "Field to sort by. Defaults to date"})
    @namespace.doc(params={"from": "Date from to filter"})
    @namespace.doc(params={"to": "Date to to filter"})
    @namespace.doc(params={"text": "Text of the transcript"})
    # @namespace.doc(params={"language": "Language of the transcript"})
    def get(
        self,
    ):
        """Get the current user media files

        Returns:
            Response: Flask response object
        """
        parser = reqparse.RequestParser()
        # parser.add_argument("limit", type=int)
        # parser.add_argument("page", type=int)
        parser.add_argument("title", type=str)
        # # parser.add_argument("sort", type=str)
        parser.add_argument("from", type=str)
        parser.add_argument("to", type=str)
        parser.add_argument("text", type=str)
        # parser.add_argument("language", type=str)

        # items_per_page = parser.parse_args()["limit"]
        # page_number = parser.parse_args()["page"]
        title = parser.parse_args()["title"]
        # # sort_by = parser.parse_args()["sort"]
        date_from = parser.parse_args()["from"]
        date_to = parser.parse_args()["to"]
        text = parser.parse_args()["text"]
        # language = parser.parse_args()["language"]
        files = {"total": 0, "objects": []}

        # logger.debug(
        #     (
        #         f"Searching for weblectures in {language} with title {title} and text {text} "
        #         f"from {date_from} to {date_to}. Page {page_number}"
        #     )
        # )

        # if not text:
        #     return files, 200

        date_range = build_elastic_search_date(date_from, date_to)

        other_filters = []

        if date_range:
            other_filters.append(date_range)

        # if language:
        #     language = f"language={language}"
        #     other_filters.append(language)

        if title:
            title = f"title={title}"
            other_filters.append(title)

        # if not items_per_page:
        #     items_per_page = 20

        # Make a call to the CERN Search API to retrieve the weblectures
        search_service = CernSearchService(logger)
        search_results = search_service.get_documents_ready_for_player(
            text, [], 1000, -1, other_filters
        )

        formatted_results = []
        count = 0
        # Iterate the results dictionary
        for result in search_results:
            result_content = search_results[result]
            # Generate the url by appending the first 50 results of the ttpmediaid array to the url
            player_url = result_content["metadata"]["url"]
            first_50_results = result_content["ttpmediaid"][:50]
            concatenated_times = ",".join(first_50_results)
            final_url = f"{player_url}&time={concatenated_times}"

            formatted_results.append(
                {
                    "ttpmediaid": result,
                    "title": result_content["metadata"]["title"],
                    "eventdate": result_content["metadata"]["eventdate"],
                    "language": result_content["metadata"]["language"],
                    "url": final_url,
                    "times": len(result_content["ttpmediaid"]),
                }
            )
            count += 1

        sorted_by_time_array = sorted(
            formatted_results, key=lambda x: x["times"], reverse=True
        )

        files["objects"] = sorted_by_time_array
        files["total"] = count

        # logger.debug(f"Search results: {search_results}")

        # lectures = search_service.group_weblectures_by_ttpid(search_results["objects"])

        # metadatas = []
        # total_results = search_results["total"]

        # # logger.debug(lectures[0])

        # for weblecture in lectures:
        #     metadatas.append(weblecture["metadata"])

        # files["objects"] = metadatas
        # files["total"] = total_results

        return files, 200


@namespace.doc(security=["Bearer Token"])
@namespace.route("/<string:ttp_media_id>")
class SearchWeblectureDetailsEndpoint(Resource):
    # method_decorators = [oidc_validate]

    @namespace.doc("Search for public weblecture details")
    @namespace.doc(params={"text": "Text of the transcript"})
    def get(self, ttp_media_id: str) -> Tuple[Dict[str, object], int]:
        """Get the current user media files

        Returns:
            Response: Flask response object
        """
        parser = reqparse.RequestParser()
        parser.add_argument("text", type=str)

        # sort_by = parser.parse_args()["sort"]
        text = parser.parse_args()["text"]

        # Make a call to the CERN Search API to retrieve the weblectures
        search_service = CernSearchService(logger)
        document_details = search_service.search_document_by_id(ttp_media_id, all=False)
        search_results = search_service.search_text_in_document(text, ttp_media_id)

        logger.debug(f"Document details: {document_details}")

        # logger.debug(f"Search results: {search_results}")

        # lectures = search_service.group_weblectures_by_ttpid(search_results["objects"])

        total_results = search_results["total"]

        # logger.debug(lectures[0])
        files = {
            "total": 0,
            "title": "",
            "eventdate": "",
            "language": "",
            "url": "",
        }
        current_item = 0

        files["title"] = document_details["objects"][0]["metadata"]["title"]
        files["eventdate"] = document_details["objects"][0]["metadata"]["eventdate"]
        files["language"] = document_details["objects"][0]["metadata"]["language"]

        sorted_by_url = sorted(
            search_results["objects"], key=lambda k: k["metadata"]["url"]
        )

        for weblecture in sorted_by_url:
            logger.debug(weblecture["metadata"]["url"])
            begin_hour = (
                f"{weblecture['metadata']['begin_hour']})h"
                if int(weblecture["metadata"]["begin_hour"]) > 0
                else ""
            )
            begin_min = (
                f"{weblecture['metadata']['begin_min']}m"
                if int(weblecture["metadata"]["begin_min"]) > 0
                else ""
            )
            begin_sec = (
                f"{weblecture['metadata']['begin_sec']}s"
                if int(weblecture["metadata"]["begin_sec"]) > 0
                else ""
            )

            if current_item == 0:
                files["url"] = weblecture["metadata"]["url"]
            else:
                logger.debug(f"begin_hour: {begin_hour}{begin_min}{begin_sec}")
                files["url"] = (
                    str(files["url"]) + f",{begin_hour}{begin_min}{begin_sec}"
                )

            current_item += 1
            if current_item >= 50:
                break

            # metadatas.append(weblecture["metadata"])

        files["total"] = total_results

        return files, 200


@namespace.doc(security=["Bearer Token"])
@namespace.route("/test")
class SearchWeblectureTestEndpoint(Resource):
    # method_decorators = [oidc_validate]

    @namespace.doc("Search for public weblecture details")
    @namespace.doc(params={"text": "Text of the transcript"})
    def get(self):
        """Get the current user media files

        Returns:
            Response: Flask response object
        """

        def get_documents_with_distinct_ttpmediaid():
            url = current_app.config["CERNSEARCH_INSTANCE"] + "/api/records/"
            headers = {"Content-Type": "application/json"}
            payload = {
                "size": 0,
                "aggs": {
                    "distinct_ttpmediaid": {
                        "terms": {"field": "ttpmediaid.keyword", "size": 10000}
                    }
                },
            }

            try:
                response = requests.get(url, headers=headers, json=payload)
                response_json = response.json()
                logger.debug(response_json)
                distinct_ttpmediaids = [
                    bucket["key"]
                    for bucket in response_json["aggregations"]["distinct_ttpmediaid"][
                        "buckets"
                    ]
                ]
                return distinct_ttpmediaids
            except requests.exceptions.RequestException as e:
                print("Error retrieving documents:", e)
                return []

        distinct_ttpmediaids = get_documents_with_distinct_ttpmediaid()
        logger.debug(distinct_ttpmediaids)

        return distinct_ttpmediaids, 200
