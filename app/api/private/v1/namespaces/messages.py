import logging

from flask_restx import Namespace, Resource, fields

from app.api.api_authorizations import authorizations
from app.models.messages import Message

logger = logging.getLogger("webapp.api")

namespace = Namespace(
    "messages",
    description="Messages related operations",
    authorizations=authorizations,
    security=["Bearer Token"],
)


test_model = namespace.model(
    "Message",
    {
        "id": fields.Integer(required=True, description="The id of the message"),
        "title": fields.String(required=True, description="The title of the message"),
        "body": fields.String(required=True, description="The content of the message"),
        "category": fields.String(
            required=True,
            description="The message category",
            attribute="category.value",
        ),
        "createdOn": fields.DateTime(
            required=True, description="Creation date", attribute="created_on"
        ),
        "updatedOn": fields.DateTime(description="Update date", attibute="updated_on"),
    },
)


@namespace.doc(security=["Bearer Token"])
@namespace.route("/")
class MessagesEndpoint(Resource):
    @namespace.doc("messages_endpoint")
    @namespace.marshal_with(test_model, as_list=True)
    def get(self):
        """
        Test response with secured endpoint

        This endpoint can be used to check the API connectivity.

        """
        messages = Message.query.filter_by(active=True).all()
        return messages
