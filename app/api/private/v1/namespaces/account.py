import logging

from flask import Response
from flask_restx import Namespace, Resource, fields

from app.api.api_authorizations import authorizations
from app.daos.accounts import AccountDAO
from app.models.accounts import Account
from app.utils.auth.client_oidc import oidc_validate

logger = logging.getLogger("webapp.api")

namespace = Namespace(
    "accounts",
    description="Accounts operations",
    authorizations=authorizations,
    security=["Bearer Token"],
)

account_model = namespace.model(
    "AccountModel",
    {
        "id": fields.Integer(required=True, description="The account ID"),
        "accountName": fields.String(
            required=True, description="The user first name", attribute="account_name"
        ),
        "isGuestAccount": fields.Boolean(
            required=True, default=False, attribute="is_guest_account"
        ),
    },
)


@namespace.doc(security=["Bearer Token"])
@namespace.route("/")
class AccountEndpoint(Resource):

    method_decorators = [oidc_validate]

    @namespace.doc("accounts_endpoint")
    @namespace.marshal_with(account_model, as_list=True)
    def get(self) -> Response:
        """Get the current user accounts

        Returns:
            Response: Flask response object
        """
        user_accounts = AccountDAO.get_current_user_accounts()
        guest_account = Account(id=0, account_name="Guest")
        guest_account.is_guest_account = True
        user_accounts = [guest_account] + user_accounts
        logger.debug(user_accounts)
        return user_accounts
