import logging

from flask import Response
from flask_login import current_user
from flask_restx import Namespace, Resource, fields

from app.api.api_authorizations import authorizations
from app.utils.auth.client_oidc import oidc_validate

logger = logging.getLogger("webapp.api")

namespace = Namespace(
    "user",
    description="Test related operations",
    authorizations=authorizations,
    security=["Bearer Token"],
)

user_model = namespace.model(
    "UserModel",
    {
        "username": fields.String(required=True, description="The user UID"),
        "firstName": fields.String(
            required=True, description="The user first name", attribute="first_name"
        ),
        "lastName": fields.String(
            required=True, description="The user last name", attribute="last_name"
        ),
        "email": fields.String(required=True, description="The user email"),
        "roles": fields.List(
            fields.String, required=True, description="The user roles"
        ),
    },
)


@namespace.doc(security=["Bearer Token"])
@namespace.route("/")
class UserEndpoint(Resource):
    method_decorators = [oidc_validate]

    @namespace.doc("user_endpoint")
    @namespace.marshal_with(user_model)
    def get(self) -> Response:
        """Get the current user details

        Returns:
            Response: Flask response object
        """
        result = current_user
        return result
