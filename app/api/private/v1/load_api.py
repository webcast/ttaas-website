import logging

from flask import Blueprint
from flask_restx import Api

from app.api.private.v1.namespaces.account import namespace as account_namespace
from app.api.private.v1.namespaces.api_tokens import namespace as api_tokens_namespace
from app.api.private.v1.namespaces.cern_search import namespace as cern_search_namespace
from app.api.private.v1.namespaces.media_files import (
    namespace as transcriptions_namespace,
)
from app.api.private.v1.namespaces.messages import namespace as messages_namespace
from app.api.private.v1.namespaces.pre_uploads import namespace as pre_uploads_namespace
from app.api.private.v1.namespaces.test import namespace as test_namespace
from app.api.private.v1.namespaces.uploads import namespace as uploads_namespace
from app.api.private.v1.namespaces.user import namespace as user_namespace
from app.api.private.v1.namespaces.zoom_rtmp_live import (
    namespace as zoom_rtmp_live_namespace,
)

logger = logging.getLogger("webapp")


def load_api_namespaces() -> Blueprint:
    """Loadd all the namespaces from the private API.
    Private API is the API that is only accessible to
    the application internal use fro CRUD operations.

    Returns:
        Blueprint: The blueprint for the API
    """
    blueprint = Blueprint("private_api", __name__)
    api = Api(blueprint)
    api.add_namespace(test_namespace)
    api.add_namespace(user_namespace)
    api.add_namespace(api_tokens_namespace)
    api.add_namespace(account_namespace)
    api.add_namespace(transcriptions_namespace)
    api.add_namespace(uploads_namespace)
    api.add_namespace(messages_namespace)
    api.add_namespace(pre_uploads_namespace)
    api.add_namespace(zoom_rtmp_live_namespace)
    api.add_namespace(cern_search_namespace)

    return blueprint
