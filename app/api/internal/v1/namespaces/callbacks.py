import enum
import logging
from typing import Tuple, Union

from flask import request
from flask_restx import Namespace, Resource

from app.api.api_authorizations import authorizations
from app.daos.captions_edits import CaptionEditDAO
from app.daos.uploads import UploadDAO
from app.models.captions_edits import CaptionEdit
from app.models.upload_states import NotificationMethods, RunningStates
from app.models.uploads import Upload
from app.tasks.email_notifications import send_single_caption_update_email
from app.tasks.transcriptions import update_cern_search_documents
from app.tasks.url_notifications import send_single_caption_callback

logger = logging.getLogger("webapp.api")

namespace = Namespace(
    "callbacks",
    description="Callbacks related operations",
    authorizations=authorizations,
    security=["Bearer Token"],
)


class AsrCallbackSource(enum.Enum):
    INGEST = "ingest-service"
    POST_PROCESSOR = "postprocessor"


def handle_post_processor_callback():
    logger.debug(
        (
            f"Handling post processor callback: "
            f"external_id: {request.form['external_id']} language: {request.form['language']}"
        )
    )
    caption = create_caption_and_update_cern_search_docs()

    if caption:
        logger.debug(
            f"Caption created for media_id: {caption.media_id}. Timestamp: {caption.timestamp}"
        )
        if caption.upload.notification_method == NotificationMethods.EMAIL:
            send_single_caption_update_email.apply_async(
                queue="callbacks",
                args=[caption.upload_id, caption.language, caption.timestamp],
            )
            logger.info(
                f"Finished post processor callback handled with email: media_id: {caption.media_id}"
            )
        else:
            send_single_caption_callback.apply_async(
                queue="callbacks",
                args=[caption.upload_id, caption.language, caption.timestamp],
            )
            logger.info(
                f"Finished post processor callback handled with callback: media_id: {request.form['external_id']}"
            )


def handle_notification_ingest_callback() -> bool:
    media_id = request.form["external_id"]
    logger.debug(f"Handling notification ingest callback: external_id: {media_id}")
    upload = UploadDAO.get_upload_by_media_id(media_id)
    if upload:
        new_status = request.form["internal_status_code"]
        UploadDAO.update_running_state(upload.id, new_status)
        logger.info(f"Finished ingest callback handle: media_id: {media_id}")
        if (
            new_status == RunningStates.COMPLETED.value
            or new_status == RunningStates.MEDIA_UPDATED.value
        ):
            logger.debug(
                f"Creating caption for media_id: {media_id} and updating CERN Search"
            )
            update_cern_search_docs(upload)
            logger.debug(f"Caption created for media_id: {media_id}")
        return True
    else:
        logger.warning(
            f"Unable to handle ingest notification for media_id: {media_id} (Not found)"
        )
    return False


@namespace.route("/")
class CallbackEndpoint(Resource):
    @namespace.doc("Callback for notifications after ingest")
    def post(self) -> Union[Tuple[str, int], Tuple[dict, int]]:
        """Notify the status change of a media

        Returns:
            Response: Flask response object
        """
        logger.info("ASR Callback Notification received")
        logger.debug(f"Received payload: {request.form}")

        callback_type = request.form["source"]

        if callback_type == AsrCallbackSource.INGEST.value:
            handled = handle_notification_ingest_callback()
            if handled:
                logger.info("ASR INGEST Callback handled")
            else:
                return {"error": "The requested upload was not found"}, 400

        if callback_type == AsrCallbackSource.POST_PROCESSOR.value:
            handle_post_processor_callback()
            logger.info("ASR POST_PROCESSOR Callback Notification handled")

        return "", 200


def handle_ingest_callback():
    logger.debug(
        (
            "Handling ingest callback: "
            f"media_id: {request.form['media_id']} "
            f"ingest_id: {request.form['upload_id']} nonce: {request.form['nonce']}"
        )
    )

    upload = UploadDAO.get_upload_by_media_id_and_nonce(
        request.form["media_id"], request.form["nonce"]
    )

    if not upload:
        return None
    new_status = request.form["internal_status_code"]
    # UploadDAO.update_running_state(upload.id, new_status)
    UploadDAO.update_running_state(upload.id, new_status)

    if (
        new_status == RunningStates.COMPLETED.value
        or new_status == RunningStates.MEDIA_UPDATED.value
    ):
        create_caption_and_update_cern_search_docs()

    logger.info(
        f"Finished ingest callback handle: media_id: {request.form['media_id']}"
    )
    return upload


def create_caption_and_update_cern_search_docs() -> Union[CaptionEdit, None]:
    try:
        caption = CaptionEditDAO.create(request.form)
        su_account = caption.upload.account.su_account
        language = request.form["language"]
        update_cern_search_documents.apply_async(
            args=[
                caption.media_id,
                language,
                su_account,
                caption.upload.cern_search_settings.is_public,
                caption.upload.account.id,
            ]
        )
        return caption
    except Exception as error:
        logger.error(f"Error creating caption: {error}", exc_info=True)
    return None


def update_cern_search_docs(upload: Upload) -> bool:
    try:
        su_account = upload.account.su_account
        language = upload.language.value
        update_cern_search_documents.apply_async(
            args=[
                upload.media_id,
                language,
                su_account,
                upload.cern_search_settings.is_public,
                upload.account.id,
            ]
        )
        return True
    except Exception as error:
        logger.error(f"Error creating caption: {error}", exc_info=True)
    return False


@namespace.route("/ingest/")
class CallbackIngestEndpoint(Resource):
    @namespace.doc("Callback for ingest statuses")
    def post(self) -> Union[Tuple[str, int], Tuple[dict, int]]:
        """Notify the status change of a media

        Returns:
            Response: Flask response object
        """
        logger.info("INGEST Callback received")
        logger.debug(request.form)

        callback_type = request.form["source"]
        logger.debug(callback_type)
        if callback_type == AsrCallbackSource.INGEST.value:
            result = handle_ingest_callback()
            if not result:
                return {"error": f"Unknow upload: {request.form['media_id']}"}, 400
            logger.info("INGEST Callback handled")
        else:
            return {"error": f"Unknow Callback type: {callback_type}"}, 400

        return "", 200
