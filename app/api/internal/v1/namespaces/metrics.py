import logging
from datetime import datetime

from flask_restx import Namespace, Resource, fields, reqparse

from app.api.api_authorizations import authorizations
from app.api.decorators import jwt_resource_required
from app.utils.metrics import get_metrics

logger = logging.getLogger("webapp.api")

namespace = Namespace(
    "metrics",
    description="Metrics related operations",
    authorizations=authorizations,
    security=["Bearer Token"],
)

user_model = namespace.model(
    "User Model",
    {
        "newUsersCount": fields.Integer(
            required=True,
            description="Number of users registered on the selected year",
            attribute="new_users_count",
        ),
        "activeUsersCount": fields.Integer(
            required=True,
            description="Number of users logged in on the selected year",
            attribute="active_users_count",
        ),
        "revokedUsersCount": fields.Integer(
            required=True,
            description="Number of users revoked on the selected year",
            attribute="revoked_users_count",
        ),
    },
)

account_model = namespace.model(
    "Account Model",
    {
        "newAccountsCount": fields.Integer(
            required=True,
            description="Number of accounts created on the selected year",
            attribute="new_accounts_count",
        ),
        "activeAccountsCount": fields.Integer(
            required=True,
            description="Number of accounts used on the selected year",
            attribute="active_accounts_count",
        ),
        "revokedAccountsCount": fields.Integer(
            required=True,
            description="Number of revoked accounts on the selected year",
            attribute="revoked_accounts_count",
        ),
    },
)

api_token_model = namespace.model(
    "API Token Model",
    {
        "newApiTokensCount": fields.Integer(
            required=True,
            description="Number of API tokens created the selected year",
            attribute="new_api_tokens_count",
        ),
        "activeApiTokensCount": fields.Integer(
            required=True,
            description="Number of API tokens used on the selected year",
            attribute="active_api_tokens_count",
        ),
        "revokedApiTokensCount": fields.Integer(
            required=True,
            description="Number of API tokens revoked on the selected year",
            attribute="revoked_api_tokens_count",
        ),
    },
)

transcription_model = namespace.model(
    "Transcription Model",
    {
        "newTranscriptionsCount": fields.Integer(
            required=True,
            description="Number of transcriptions requested on the selected year",
            attribute="requested_transcriptions_count",
        ),
        "activeTranscriptionsCount": fields.Integer(
            required=True,
            description="Number of transcriptions completed in on the selected year",
            attribute="completed_transcriptions_count",
        ),
        "erroredTranscriptionsCount": fields.Integer(
            required=True,
            description="Number of transcriptions errored on the selected year",
            attribute="errored_transcriptions_count",
        ),
    },
)

metrics = namespace.model(
    "Metrics Model",
    {
        "users": fields.Nested(user_model),
        "accounts": fields.Nested(account_model),
        "apiTokens": fields.Nested(api_token_model, attribute="api_tokens"),
        "transcriptions": fields.Nested(transcription_model),
        "datetime": fields.String(),
    },
)

parser = reqparse.RequestParser()
parser.add_argument("year", type=int, help="Year of the data")


@namespace.doc(security=["Bearer Token"])
@namespace.route("/")
class MetricsEndpoint(Resource):

    method_decorators = [jwt_resource_required("metrics")]

    @namespace.doc("metrics_endpoint")
    @namespace.expect(parser)
    @namespace.marshal_with(metrics)
    def get(self):
        """
        Return metrics for a given year

        This endpoint can be used to retrieve the statistics for a given year.

        """
        args = parser.parse_args()
        year = args["year"]
        if not year:
            year = datetime.today().year

        result = get_metrics(year)
        result["datetime"] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        return result, 200
