import logging

from flask import Blueprint
from flask_restx import Api

from app.api.internal.v1.namespaces.callbacks import namespace as callbacks_namespace
from app.api.internal.v1.namespaces.cern_search import (
    namespace as cern_search_namespace,
)
from app.api.internal.v1.namespaces.metrics import namespace as metrics_namespace
from app.api.internal.v1.namespaces.test import namespace as test_namespace

logger = logging.getLogger("webapp")


def load_api_namespaces() -> Blueprint:
    """Loadd all the namespaces from the internal API.
    Private API is the API that is only accessible to
    the application internal use fro CRUD operations.

    Returns:
        Blueprint: The blueprint for the API
    """
    bp = Blueprint("internal_api", __name__)
    api = Api(
        bp,
        version="1.0",
        title="Transcription and Translation Service Internal API",
        description="Internal operations for the Transcription and Translation Service",
        contact="Weblecture Service",
        contact_url="https://webcast.docs.cern.ch/",
    )
    api.add_namespace(test_namespace)
    api.add_namespace(metrics_namespace)
    api.add_namespace(cern_search_namespace)
    api.add_namespace(callbacks_namespace)

    return bp
