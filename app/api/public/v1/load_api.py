import logging

from flask import Blueprint
from flask_restx import Api

from app.api.public.v1.namespaces.editor import namespace as editor_namespace
from app.api.public.v1.namespaces.media_files import namespace as media_files_namespace
from app.api.public.v1.namespaces.test import namespace as test_namespace
from app.api.public.v1.namespaces.uploads import namespace as uploads_namespace

logger = logging.getLogger("webapp")


def load_api_namespaces() -> Blueprint:
    """Loadd all the namespaces from the public API.
    Private API is the API that is only accessible to
    the application internal use fro CRUD operations.

    Returns:
        Blueprint: The blueprint for the API
    """
    bp = Blueprint("public_api", __name__)
    api = Api(
        bp,
        version="1.0",
        title="Transcription and Translation Service Public API",
        description="Common operations for the Transcription and Translation Service API (media transcriptions)",
        contact="Weblecture Service",
        contact_url="https://webcast.docs.cern.ch/",
    )
    api.add_namespace(test_namespace)
    api.add_namespace(uploads_namespace)
    api.add_namespace(media_files_namespace)
    api.add_namespace(editor_namespace)

    return bp
