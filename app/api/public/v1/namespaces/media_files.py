import io
import json
import logging
import os
import tempfile
from multiprocessing import Process
from typing import Dict, Tuple, Union

import flask
from flask import Response, g
from flask_restx import Namespace, Resource, fields, reqparse

from app.api.api_authorizations import authorizations
from app.api.decorators import jwt_resource_required
from app.daos.accounts import AccountDAO
from app.models.uploads import Upload
from app.services.mllp.api_client import ApiSpeechClientMLLP

logger = logging.getLogger("webapp.api")

namespace = Namespace(
    "media-files",
    description="Media files operations",
    authorizations=authorizations,
    security=["Bearer Token"],
)

speaker = namespace.model("speaker", {"name": fields.String()})

mediainfo = namespace.model(
    "mediainfo",
    {
        "language": fields.String(),
        "title": fields.String(),
        "category": fields.String(),
        "duration": fields.Integer(),
        "speakers": fields.List(fields.Nested(speaker)),
    },
)

media = namespace.model(
    "media",
    {
        "is_url": fields.Boolean(),
        "type_code": fields.Integer(),
        "media_format": fields.String(),
        "location": fields.String(),
    },
)

media_model_list_item = namespace.model(
    "Media Model List Item",
    {
        "id": fields.String(),
        "media": fields.List(fields.Nested(media)),
        "metadata": fields.Nested(mediainfo),
    },
)

media_model_list = namespace.model(
    "Media Model List Item",
    {
        "total": fields.Integer(
            required=True,
            description="Total number of media",
        ),
        "objects": fields.List(fields.Nested(media_model_list_item)),
    },
)

speaker = namespace.model("speaker", {"name": fields.String()})

mediainfo = namespace.model(
    "mediainfo",
    {
        "language": fields.String(),
        "title": fields.String(),
        "category": fields.String(),
        "duration": fields.Integer(),
        "speakers": fields.List(fields.Nested(speaker)),
    },
)

media = namespace.model(
    "media",
    {
        "is_url": fields.Boolean(),
        "type_code": fields.Integer(),
        "media_format": fields.String(),
        "location": fields.String(),
    },
)

audiotracks = namespace.model(
    "audiotracks",
    {
        "lang": fields.String(),
        "voice_type": fields.Integer(),
        "id": fields.Integer(),
        "location": fields.String(),
        "media_format": fields.String(),
        "audio_type": fields.String(),
        "is_url": fields.Boolean(),
        "sub_type": fields.Integer(),
        "description": fields.String(),
    },
)

media_model_metadata = namespace.model(
    "Media Model Metadata",
    {
        "rcode": fields.Integer(
            required=True,
            description="Description",
        ),
        "rcode_description": fields.String(
            required=True, description="The description of the rcode"
        ),
        "audiotracks": fields.Nested(audiotracks),
        "media": fields.List(fields.Nested(media)),
        "mediainfo": fields.Nested(mediainfo),
    },
)


@namespace.doc(security=["Bearer Token"])
@namespace.route("/")
class MediaListEndpoint(Resource):

    method_decorators = [jwt_resource_required("all")]

    @namespace.doc("Get media files")
    @namespace.marshal_with(media_model_list, as_list=False)
    @namespace.doc(
        params={"limit": "Number of results to return per page. Defaults to 10"}
    )
    @namespace.doc(params={"page": "Page number of results to return. Defaults to 1"})
    def get(self) -> Tuple[Union[str, bytes], int]:
        """Get the media files uploaded with the current account"""

        parser = reqparse.RequestParser()
        parser.add_argument("limit", type=int)
        parser.add_argument("page", type=int)

        args = parser.parse_args()
        items_per_page = args.get("limit", 10)
        page_number = args.get("page", 1)

        account_id = g.account_id
        account = AccountDAO.get_account_by_id(account_id)
        api_client = ApiSpeechClientMLLP(substitute_user=account.su_account)
        files = api_client.get_media_full_list(items_per_page, page_number)
        files = json.loads(files)
        logger.debug(len(files))

        return files, 200


speaker = namespace.model("speaker", {"name": fields.String()})

mediainfo = namespace.model(
    "mediainfo",
    {
        "language": fields.String(),
        "title": fields.String(),
        "category": fields.String(),
        "duration": fields.Integer(),
        "speakers": fields.List(fields.Nested(speaker)),
    },
)

media = namespace.model(
    "media",
    {
        "is_url": fields.Boolean(),
        "type_code": fields.Integer(),
        "media_format": fields.String(),
        "location": fields.String(),
    },
)

audiotracks = namespace.model(
    "audiotracks",
    {
        "lang": fields.String(),
        "voice_type": fields.Integer(),
        "id": fields.Integer(),
        "location": fields.String(),
        "media_format": fields.String(),
        "audio_type": fields.String(),
        "is_url": fields.Boolean(),
        "sub_type": fields.Integer(),
        "description": fields.String(),
    },
)

caption = namespace.model(
    "caption",
    {
        "lang_code": fields.String(),
        "lang_name": fields.String(),
        "last_updated": fields.String(),
    },
)

media_model_metadata = namespace.model(
    "Media Model Metadata",
    {
        "rcode": fields.Integer(
            required=True,
            description="Description",
        ),
        "rcode_description": fields.String(
            required=True, description="The description of the rcode"
        ),
        "audiotracks": fields.Nested(audiotracks),
        "media": fields.List(fields.Nested(media)),
        "mediainfo": fields.Nested(mediainfo),
        "captions": fields.List(fields.Nested(caption)),
    },
)


@namespace.doc(security=["Bearer Token"])
@namespace.route("/<string:media_id>/")
class MediaDetailsEndpoint(Resource):

    method_decorators = [jwt_resource_required("all")]

    @namespace.doc("Get media file details")
    @namespace.marshal_with(media_model_metadata, as_list=False)
    def get(self, media_id) -> Response:
        """Get the details of a media element"""

        account_id = g.account_id
        account = AccountDAO.get_account_by_id(account_id)
        api_client = ApiSpeechClientMLLP(substitute_user=account.su_account)
        file = api_client.get_media_metadata(media_id)
        file = json.loads(file)

        logger.debug(file)

        return file


audio_track_model = namespace.model(
    "Audio track",
    {
        "aid": fields.Integer(
            required=True,
            description="Audiotrack ID.",
        ),
        "voice_gender": fields.Integer(
            required=True,
            description="""Voice gender.
            - m → Male voice.
            - f → Female voice.""",
        ),
        "voice_type": fields.Integer(
            required=True,
            description="""
            Voice type.
            - tts → Text-To-Speech, synthesized audiotrack
            - rec → Human-recorded audiotrack.""",
        ),
    },
)

language = namespace.model(
    "Language",
    {
        "lang_code": fields.String(
            required=True,
            description="Subtitle language code (ISO-639-1).",
        ),
        "lang_name": fields.String(required=True, description="Local language name."),
        "sup_status": fields.Integer(
            required=True,
            description="""Subtitle supervision status code. Defines the level of human supervision of the subtitles.
            - 0 → Fully Automatic: The whole subtitles are automatic.
            - 1 → Partially Human: The subtitles have been supervised, but not for the whole video.
            - 2 → Fully Human: The subtitles have been fully supervised by humans.""",
        ),
        "sup_ratio": fields.Float(
            required=True, description="Percentage of supervised segments."
        ),
        "rev_time": fields.Integer(
            required=True,
            description="Time spent by the user while editing the subtitles.",
        ),
        "rev_rtf": fields.Float(
            required=True,
            description="RTF spent by the user while editing the subtitles.",
        ),
        "system_time": fields.Integer(
            required=True,
            description="Time spent by the ASR/MT system to generate the subtitles.",
        ),
        "system_rtf": fields.Float(
            required=True,
            description="RTF spent by the ASR/MT system to generate the subtitles.",
        ),
        "audiotracks": fields.List(
            fields.Nested(audio_track_model),
            description="List of audiotracks available in the corresponding language.",
        ),
    },
)

language_list_metadata = namespace.model(
    "Language Metadata",
    {
        "rcode": fields.Integer(
            required=True,
            description="Return code of the WS call. 0: exists. 1: not exists",
        ),
        "rcode_description": fields.String(
            required=True, description="Description of the return code"
        ),
        "media_lang": fields.String(
            required=True,
            description="Language code (ISO-639-1) of the media’s original audio language.",
        ),
        "locked": fields.Boolean(
            required=True,
            description="Lock status of subtitles. If true, only authors are allowed to send subtitle modifications.",
        ),
        "langs": fields.List(fields.Nested(language)),
    },
)


@namespace.doc(security=["Bearer Token"])
@namespace.route("/<string:media_id>/languages/")
class LanguagesEndpoint(Resource):

    method_decorators = [jwt_resource_required("all")]

    @namespace.doc("Get media languages")
    @namespace.marshal_with(language_list_metadata, as_list=False)
    def get(self, media_id) -> Response:
        """Get the current media languages

        Returns:
            Response: Flask response object
        """
        account_id = g.account_id
        account = AccountDAO.get_account_by_id(account_id)
        api_client = ApiSpeechClientMLLP(substitute_user=account.su_account)
        file = api_client.get_media_langs(media_id)
        file = json.loads(file)
        return file


@namespace.doc(security=["Bearer Token"])
@namespace.route("/<string:media_id>/languages/<string:lang>/")
class LanguagesDownloadEndpoint(Resource):

    method_decorators = [jwt_resource_required("all")]

    @namespace.doc("Download transcription from a media id")
    def get(self, media_id, lang):
        """Get the download trascription file of a given media id"""
        account_id = g.account_id
        account = AccountDAO.get_account_by_id(account_id)
        api_client = ApiSpeechClientMLLP(substitute_user=account.su_account)
        downloaded_file = api_client.get_media_download(media_id, lang)
        with tempfile.TemporaryDirectory() as temp_dir:
            file_path = os.path.join(temp_dir, f"{media_id}_{lang}.vtt")
            with open(file_path, "w", encoding="utf-8") as file:
                file.write(downloaded_file)
            return self.send_temp_file(file_path)

    def background_job(self, callback):
        task = Process(target=callback())
        task.start()

    def send_temp_file(self, file_path: str):
        """_summary_

        Args:
            file_path (str): Path of the file that is being generated
            temp_dir (tempfile.TemporaryDirectory): Temporary directory of the created file

        Returns:
            Response: Flask response object
        """
        with open(file_path, "rb") as file:
            content = io.BytesIO(file.read())

        return flask.send_file(
            content,
            as_attachment=True,
            download_name=os.path.split(file_path)[1],
            mimetype="application/octet-stream",
        )


already_exists_model = namespace.model(
    "Already Exists",
    {
        "already_exists": fields.Boolean(
            required=True,
            description="Whether or not a media file was uploaded to the given account and has the same checksum",
        ),
    },
)


@namespace.doc(security=["Bearer Token"])
@namespace.route("/checksum/<string:checksum>/")
class MediaFileChecksumExistsEndpoint(Resource):

    method_decorators = [jwt_resource_required("all")]

    @namespace.doc("Check if a media file was already uploaded")
    @namespace.marshal_with(already_exists_model, as_list=False)
    def get(self, checksum) -> Tuple[Dict[str, bool], int]:
        """Check if a media file already exists providing it's checksum.

        To verify the cheksum of a media file, we use the following code.
        You can use the following code to generate the checksum client side and then make the API
        call to verify it.

        ```python
        import hashlib

        def get_file_checksum(file_path: str) -> str:
            with open(file_path, "rb") as file:
                file_hash = hashlib.md5()
                while chunk := file.read(
                    8192
                ):
                    file_hash.update(chunk)
                return file_hash.hexdigest()
        """

        account_id = g.account_id
        media_files = Upload.query.filter_by(
            media_file_checksum=checksum, account_id=account_id
        ).all()
        if len(media_files) > 0:
            return {"already_exists": True}, 200
        return {"already_exists": False}, 200
