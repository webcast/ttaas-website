import logging

from flask_restx import Namespace, Resource, fields

from app.api.api_authorizations import authorizations
from app.api.decorators import jwt_resource_required

logger = logging.getLogger("webapp.api")

namespace = Namespace(
    "test",
    description="Test related operations",
    authorizations=authorizations,
    security=["Bearer Token"],
)


test_model = namespace.model(
    "Test Model",
    {
        "result": fields.String(required=True, description="The test result"),
    },
)


@namespace.doc(security=["Bearer Token"])
@namespace.route("/")
class TestEndpoint(Resource):

    method_decorators = [jwt_resource_required("all")]

    @namespace.doc("test_endpoint")
    @namespace.marshal_with(test_model)
    def get(self):
        """
        Test response with secured endpoint

        This endpoint can be used to check the API connectivity.

        """
        result = {"result": "ok"}
        return result


@namespace.route("/ping/")
class TestPublicEndpoint(Resource):
    @namespace.doc("test_public_endpoint")
    @namespace.marshal_with(test_model)
    def get(self):
        """
        Test response with secured endpoint

        This endpoint can be used to check the API connectivity.

        """
        result = {"result": "ok"}
        return result
