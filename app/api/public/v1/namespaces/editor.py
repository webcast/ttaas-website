import logging

from flask import Response
from flask_login import current_user
from flask_restx import Namespace, Resource, fields

from app.api.api_authorizations import authorizations
from app.api.decorators import jwt_resource_required
from app.services.mllp.api_client import ApiSpeechClientMLLP

logger = logging.getLogger("webapp.api")

namespace = Namespace(
    "editor",
    description="Editor links operations",
    authorizations=authorizations,
    security=["Bearer Token"],
)

editor_metadata = namespace.model(
    "Editor links",
    {
        "editUrl": fields.String(
            description="The link to the transcription editor with Read and Write rights",
        ),
        "viewUrl": fields.String(
            description="The link to the transcription editor with View rights"
        ),
    },
)


@namespace.doc(security=["Bearer Token"])
@namespace.route("/<string:media_id>/")
class EditorLinksEndpoint(Resource):

    method_decorators = [jwt_resource_required("all")]

    @namespace.doc("Get the editor link")
    @namespace.marshal_with(editor_metadata, as_list=False)
    def get(self, media_id) -> Response:
        """Get the editor links for the given media"""
        api_client = ApiSpeechClientMLLP()
        tlp = api_client.get_tlp_client()
        edit_url = tlp.generate_player_url(
            media_id,
            current_user.username,
            100,
            author_name=f"{current_user.first_name} {current_user.last_name}",
        )
        view_url = tlp.generate_player_url(
            media_id,
            None,
            100,
            author_name=f"{current_user.first_name} {current_user.last_name}",
        )
        return {"editUrl": edit_url, "viewUrl": view_url}, 200
