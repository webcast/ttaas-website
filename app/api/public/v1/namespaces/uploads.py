import logging
import uuid
from typing import Any, Dict, Tuple, Union

from flask import current_app, g, request
from flask_restx import Namespace, Resource, fields, reqparse
from werkzeug.datastructures import FileStorage

from app.api.api_authorizations import authorizations
from app.api.api_config import (
    ALLOWED_NOTIFICATION_METHODS,
    ALLOWED_UPLOAD_EXTENSIONS,
    ALLOWED_UPLOAD_LANGUAGES,
    UPLOAD_DIRECTION_KEYS,
    UPLOAD_SORT_KEYS,
)
from app.api.decorators import jwt_resource_required
from app.daos.accounts import AccountDAO
from app.daos.pre_uploads import PreUploadDAO, PreUploadDAOException
from app.daos.state_log import StateLogDAO
from app.daos.uploads import UploadDAO
from app.extensions import db
from app.models.uploads import NotificationMethods, TaskStates, UploadTypes
from app.services.mllp.api_client import ApiSpeechClientMLLP
from app.tasks.ingest import ingest_task
from app.utils.uploads import build_uploads_query, save_uploaded_files

logger = logging.getLogger("webapp.api")

namespace = Namespace(
    "uploads",
    description="Uploads operations",
    authorizations=authorizations,
    security=["Bearer Token"],
)

upload = namespace.model(
    "Upload",
    {
        "id": fields.Integer(required=True, description="The upload ID"),
        "mediaId": fields.String(
            required=True, description="The media ID", attribute="media_id"
        ),
        "existingMediaId": fields.String(
            required=False,
            description="The existing media ID if it's a duplicate",
            attribute="existing_media_id",
        ),
        "language": fields.String(
            required=False, description="The media language", attribute="language.value"
        ),
        "title": fields.String(required=False, description="The media title"),
        "state": fields.String(
            required=True, description="Current upload status", attribute="state.value"
        ),
        "comments": fields.String(required=False, description="Comments"),
        "username": fields.String(
            required=False, description="The username of the uploader"
        ),
        "notificationEmail": fields.String(
            required=False,
            description="Notification email address",
            attribute="notification_email",
        ),
        "referenceUrl": fields.String(
            required=False,
            description="URL to reference the media",
            attribute="reference_url",
        ),
        "createdOn": fields.DateTime(
            required=True,
            description="The upload creation date",
            attribute="created_on",
        ),
        "updatedOn": fields.DateTime(
            required=True, description="The upload update date", attribute="updated_on"
        ),
        "uploadType": fields.String(
            required=False,
            description="The type of the upload (api or web)",
            attribute="upload_type.value",
        ),
        "searchIsPublic": fields.Boolean(
            attribute="cern_search_settings.is_public",
            required=False,
        ),
        "startTime": fields.String(
            required=False, description="The upload start time", attribute="start_time"
        ),
        "endTime": fields.String(
            required=False, description="The upload end time", attribute="end_time"
        ),
    },
)


uploads_list = namespace.model(
    "Uploads list",
    {
        "total": fields.Integer(
            required=True,
            description="Total number of uploads",
        ),
        "objects": fields.List(fields.Nested(upload)),
    },
)


@namespace.doc(security=["Bearer Token"])
@namespace.route("/")
class UploadsEndpoint(Resource):
    method_decorators = [jwt_resource_required("all")]

    parser = reqparse.RequestParser()
    parser.add_argument(
        "sortBy",
        type=str,
        choices=UPLOAD_SORT_KEYS,
        help=f"Sort keys for uploads. Valid values: {', '.join(UPLOAD_SORT_KEYS)}. Defaults to created",
    )
    parser.add_argument(
        "direction",
        type=str,
        choices=UPLOAD_DIRECTION_KEYS,
        help=(
            "Direction for uploads sorting. "
            f"Valid values: {', '.join(UPLOAD_DIRECTION_KEYS)}. Defaults to desc"
        ),
    )
    parser.add_argument(
        "limit", type=int, help="Number of items per page. Defaults to 10"
    )
    parser.add_argument(
        "page", type=int, help="Page of items to display. Defaults to 1"
    )

    @namespace.doc("Get uploads")
    @namespace.marshal_with(uploads_list, as_list=False)
    @namespace.expect(parser)
    def get(self) -> Tuple[Dict[str, Any], int]:
        """Get the current uploads and their states"""
        logger.debug("Get uploads")
        args = self.parser.parse_args()
        sort_by = args.get("sortBy", "created_on")
        direction = args.get("direction", "desc")
        items_per_page = args.get("limit", 10)
        page_number = args.get("page", 1)

        account_id = g.account_id

        logger.debug("Building uploads query")
        query = build_uploads_query(sort_by, direction, account_id=account_id)
        record_query = query.paginate(page=page_number, per_page=items_per_page)

        total = record_query.total
        record_items = record_query.items
        return {"total": total, "objects": record_items}, 200


@namespace.doc(security=["Bearer Token"])
@namespace.route("/<string:media_id>/")
class UploadDetailsEndpoint(Resource):
    method_decorators = [jwt_resource_required("all")]

    update_parser = namespace.parser()
    update_parser.add_argument(
        "title",
        location="form",
        type=str,
        required=False,
        help="Title of the uploaded media",
    )
    update_parser.add_argument(
        "username",
        location="form",
        type=str,
        required=False,
        help="Username of the uploader. Used to allow the user to identify his files",
    )
    update_parser.add_argument(
        "referenceUrl",
        location="form",
        type=str,
        required=False,
        help="URL to reference the media",
    )
    update_parser.add_argument(
        "notificationUrl",
        location="form",
        type=str,
        required=False,
        help="URL for the notifications",
    )
    update_parser.add_argument(
        "notificationEmail",
        location="form",
        type=str,
        required=False,
        help="Email address for the notifications",
    )
    update_parser.add_argument(
        "searchIsPublic",
        location="form",
        type=bool,
        required=False,
        help="Whether or not the transcription in the Search is public or not",
    )
    update_parser.add_argument(
        "startTime",
        location="form",
        type=str,
        required=False,
        help="Format: HH:MM:SS. The start time of the media",
    )
    update_parser.add_argument(
        "endTime",
        location="form",
        type=str,
        required=False,
        help="Format: HH:MM:SS. The end time of the media",
    )

    @namespace.doc("Get upload details")
    @namespace.response(404, "Upload not found")
    @namespace.marshal_with(upload, as_list=False)
    def get(self, media_id) -> Tuple[Any, int]:
        """Get the details of an upload or pre upload"""
        logger.info(
            f"Calling get upload details endpoint for media id {media_id}. Account ID: {g.account_id}"
        )
        account_id = g.account_id
        upload_item = UploadDAO.get_upload_by_media_id_for_account(media_id, account_id)

        if not upload_item:
            upload_item = PreUploadDAO.get_pre_upload_by_media_id_for_account(
                media_id, account_id
            )

        if upload_item:
            return upload_item, 200

        return {"message": "Upload not found"}, 404

    @namespace.doc("Update upload details")
    @namespace.expect(update_parser)
    @namespace.response(404, "Upload not found")
    @namespace.marshal_with(upload, as_list=False)
    def patch(self, media_id) -> Tuple[Any, int]:
        """Update the details of an upload"""
        logger.info(
            f"Calling update upload endpoint for media id {media_id}. Account ID: {g.account_id}"
        )
        account_id = g.account_id
        upload_item = UploadDAO.get_upload_by_media_id_for_account(media_id, account_id)

        if upload_item:
            username = request.form.get("username", None)
            title = request.form.get("title", None)
            reference_url = request.form.get("referenceUrl", None)
            notification_url = request.form.get("notificationUrl", None)
            search_is_public = request.form.get("searchIsPublic", False)
            start_time = request.form.get("startTime", None)
            end_time = request.form.get("endTime", None)

            # convert search_is_public from string to boolean
            if isinstance(search_is_public, str):
                search_is_public = search_is_public.lower() == "true"

            logger.debug(search_is_public)
            if username and username != upload_item.username:
                logger.debug(f"Updating username to {username} for {media_id}")
                upload_item.username = username
            if title and title != upload_item.title:
                logger.debug(f"Updating title to {title} for {media_id}")
                upload_item.title = title
            if reference_url and reference_url != upload_item.reference_url:
                logger.debug(
                    f"Updating reference url to {reference_url} for {media_id}"
                )
                upload_item.reference_url = reference_url
            if notification_url and notification_url != upload_item.notification_url:
                logger.debug(
                    f"Updating notification url to {reference_url} for {media_id}"
                )
                upload_item.notification_url = notification_url
            logger.debug(upload_item.cern_search_settings.is_public)
            if search_is_public != upload_item.cern_search_settings.is_public:
                logger.debug(
                    f"Updating searchIsPublic to {search_is_public} for {media_id}"
                )
                upload_item.cern_search_settings.is_public = search_is_public

            if start_time and start_time != upload_item.start_time:
                logger.debug(f"Updating startTime to {start_time} for {media_id}")
                upload_item.start_time = start_time

            if end_time and end_time != upload_item.end_time:
                logger.debug(f"Updating endTime to {end_time} for {media_id}")
                upload_item.end_time = end_time

            db.session.commit()

            upload_item = UploadDAO.get_upload_by_media_id_for_account(
                media_id, account_id
            )

            logger.debug(upload_item.cern_search_settings)

            return upload_item, 200
        return {"message": "Upload not found"}, 404

    @namespace.doc("Delete upload")
    @namespace.marshal_with(upload, as_list=False)
    @namespace.response(404, "Upload not found")
    def delete(self, media_id) -> Union[Tuple[str, int], Tuple[Dict[str, str], int]]:
        """Delete the details of an upload"""
        logger.info(
            f"Delete update upload endpoint for media id {media_id}. Account ID: {g.account_id}"
        )
        account_id = g.account_id
        account = AccountDAO.get_account_by_id(account_id)

        upload_item = UploadDAO.get_upload_by_media_id_for_account(media_id, account_id)

        if upload_item:
            UploadDAO.delete_upload_by_id(upload_item.id)
            logger.info(
                f"Successfully deleted media_id: {media_id} from account: {account_id}"
            )
            logger.info("Trying to delete media from TLP")
            tlp_client = ApiSpeechClientMLLP(substitute_user=account.su_account)
            upload_id = tlp_client.delete_media(media_id)
            if upload_id:
                logger.info(
                    f"Media {media_id} deleted from TLP. Return value: {upload_id}"
                )

            return "", 204
        return {"message": "Upload not found"}, 404


pre_upload_model = namespace.model(
    "PreUpload",
    {
        "id": fields.Integer(required=True, description="The upload ID"),
        "mediaId": fields.String(
            required=True, description="The media ID", attribute="media_id"
        ),
        "existingMediaId": fields.String(
            required=False,
            description="The existing media ID if it's a duplicate",
            attribute="existing_media_id",
        ),
        "username": fields.String(
            required=True,
            description="The username that owns the media",
            attribute="username",
        ),
        "title": fields.String(
            required=True, description="The title of the media", attribute="title"
        ),
        "state": fields.String(
            required=True, description="Current upload status", attribute="state.value"
        ),
        "createdOn": fields.DateTime(
            required=True,
            description="The upload creation date",
            attribute="created_on",
        ),
        "updatedOn": fields.DateTime(
            required=True, description="The upload update date", attribute="updated_on"
        ),
    },
)


@namespace.doc(security=["Bearer Token"])
@namespace.route("/ingest/")
class IngestEndpoint(Resource):
    method_decorators = [jwt_resource_required("all")]

    upload_parser = namespace.parser()
    upload_parser.add_argument(
        "mediaFile",
        location="files",
        type=FileStorage,
        required=True,
        help="Media file to be uploaded. Maximum size is 2GB",
    )
    upload_parser.add_argument(
        "title",
        location="form",
        type=str,
        required=True,
        help="Title of the uploaded media",
    )
    upload_parser.add_argument(
        "language",
        location="form",
        type=str,
        required=True,
        choices=ALLOWED_UPLOAD_LANGUAGES,
        help="Language of the uploaded media",
    )
    upload_parser.add_argument(
        "username",
        location="form",
        type=str,
        required=True,
        help="Username of the uploader. Used to allow the user to identify his files",
    )
    upload_parser.add_argument(
        "notificationMethod",
        location="form",
        type=str,
        required=True,
        choices=ALLOWED_NOTIFICATION_METHODS,
        help="""Method that will be used to notify the user once the transcription is
        complete if notification method is set to email""",
    )
    upload_parser.add_argument(
        "notificationEmail",
        location="form",
        type=str,
        required=False,
        help="""Email that will be used to notify the user once the transcription
        is complete if notification method is set to callback""",
    )
    upload_parser.add_argument(
        "notificationUrl",
        location="form",
        type=str,
        required=False,
        help="""URL that will be used to notify the user once the transcription is complete.
        POST method will be used. You can pass query parameters as well.""",
    )
    upload_parser.add_argument(
        "comments",
        location="form",
        type=str,
        required=False,
        help="Additional comments about the media",
    )
    upload_parser.add_argument(
        "searchReadGroups",
        location="form",
        type=str,
        required=False,
        help="CERN Search: List of comma separated groups that will have access to the transcription",
    )
    upload_parser.add_argument(
        "searchReadUsers",
        location="form",
        type=str,
        required=False,
        help="CERN Search: List of comma separated usernames that will have access to the transcription",
    )
    upload_parser.add_argument(
        "searchTranscriptionIsPublic",
        location="form",
        type=bool,
        required=False,
        help="CERN Search: Whether or not the transcription must be public",
    )
    upload_parser.add_argument(
        "searchDateTime",
        location="form",
        type=str,
        required=False,
        help="CERN Search: Date and time of the media. Format: YYYY-MM-DD, HH:MM:SS",
    )
    upload_parser.add_argument(
        "referenceUrl",
        location="form",
        type=str,
        required=False,
        help="URL that will be used to reference the media",
    )
    upload_parser.add_argument(
        "startTime",
        location="form",
        type=str,
        required=False,
        help="Format: HH:MM:SS. The start time of the media",
    )
    upload_parser.add_argument(
        "endTime",
        location="form",
        type=str,
        required=False,
        help="Format: HH:MM:SS. The end time of the media",
    )

    def allowed_file(self, filename):
        return (
            "." in filename
            and filename.rsplit(".", 1)[1].lower() in ALLOWED_UPLOAD_EXTENSIONS
        )

    @namespace.doc("Ingest media files")
    @namespace.expect(upload_parser)
    @namespace.response(500, "Volume quota exceeded")
    @namespace.response(201, "Success", pre_upload_model)
    @namespace.marshal_with(pre_upload_model, as_list=False)
    def post(self) -> Tuple[Dict[str, str], int]:
        """
        Ingest media files to generate a transcription and a translation either to English or French.

        When the transcription and/or translation is generated, it will be indexed on a CERN Search instance to allow
        the search of terms there. This can be configured using some parameters during the ingest.

        When media files are ingested, an upload will be created. This upload will go through the following states.

        | Step | State | Description |
        |---| ---------| ----------- |
        | 1 | CREATED | The upload has been created |
        | 2 | SUBMITTED | The upload has been submitted succesfully |
        | 3 | PREPARING_UPLOAD | The upload is being prepared to be sent to the ASR service |
        | 4 | UPLOADING | The upload is being uploaded to the ASR service |
        | 4a | WAITING_TRANSCRIPTIONS | The media has been uploaded but it didn't start yet transcribing |
        | 5a | RUNNING_TRANSCRIPTIONS | The ASR service is generating the transcriptions and translations |
        | 6a | TRANSCRIPTION_FINISHED | The ASR service has finished generating the transcription |
        | 4b | WAITING_TRANSLATIONS | The media has been uploaded but it didn't start yet translating |
        | 5b | RUNNING_TRANSLATIONS | The ASR service is generating the transcriptions and translations |
        | 6b | TRANSLATION_FINISHED | The ASR service has finished generating the translation |
        | 7 | USER_NOTIFIED | The user has been notified that the transcriptions/translations have been generated |
        | 8 | COMPLETED | The upload is finished transcribing/translating |

        If the notification fails, but the transcription/translation went trought,
        the state will be one of the following, but the upload should be considered as completed:

        | Step | Description |
        |---| ----------- |
        | COMPLETED_CALLBACK_FAILED | Unable to notify the user via callback that the task is completed |
        | COMPLETED_EMAIL_FAILED | Unable to notify the user via email that the task is completed|

        The following errors must be considered as failed:

        | Step | Description |
        |---| ----------- |
        | ERROR_SUBMITTING | Error during the submission |
        | ERROR_PREPARING_UPLOAD | Error during upload preparation |
        | ERROR_UPLOADING | Error during the upload to the ASR service |
        | ERROR_RUNNING_TRANSCRIPTIONS | Error generating the transcriptions |
        | ERROR_RUNNING_TRANSLATIONS | Error generating the translations |

        ### Callbacks

        The query parameters included in the notification URL will be
        returned as part of the callback body as a JSON object.

        Additionally, the response will always include the `mediaId` and the `callbackType`.

        Callback types can be the following:

        | callbackType | Description |
        |---| ---------|
        | FINISHED | Transcription process is finished. |
        | ERROR | An error happened during the upload or transcription. |
        | ALREADY_EXISTS | You tried to upload a file that was already uploaded to your account. |
        | | In this case, the field `existingMediaId` will reference the already existing upload. |
        | | Another field included in this callback is the `state` of |
        | | the upload, that can take one of the values specified above. |
        | CAPTION_UPDATED | Will notify when the transcription has been updated. |

        """
        account_id = g.account_id
        account = AccountDAO.get_account_by_id(account_id)
        media_file = request.files.get("mediaFile", None)
        title = request.form.get("title", None)
        title = title if title != "" else None
        language = request.form.get("language", None)
        reference_url = request.form.get("referenceUrl", None)
        comments = request.form.get("comments", "")
        read_groups = request.form.get("searchReadGroups", "")
        read_users = request.form.get("searchReadUsers", "")
        media_datetime = request.form.get("searchDateTime", None)
        start_time = request.form.get("startTime", None)
        end_time = request.form.get("endTime", None)

        logger.debug(
            f"Calling ingest endpoint for account {account.account_name} and media {title}"
        )

        transcription_is_public = request.form.get("searchTranscriptionIsPublic", False)
        if isinstance(transcription_is_public, str):
            transcription_is_public = transcription_is_public.lower() == "true"

        username = request.form.get("username", "")
        notification_email = request.form.get("notificationEmail", None)
        notification_url = request.form.get("notificationUrl", None)
        notification_method = request.form.get("notificationMethod", None)
        media_id = f"{account.su_account}_{uuid.uuid4()}"

        notification_method = (
            notification_method.upper()
            if notification_method
            else NotificationMethods.NONE.value
        )

        upload_info = {
            "account_id": account_id,
            "acl_read_groups": read_groups,
            "acl_read_users": read_users,
            "comments": comments,
            "language": language,
            "notification_email": notification_email,
            "notification_method": notification_method,
            "notification_url": notification_url,
            "reference_url": reference_url,
            "su_account": account.su_account,
            "title": title,
            "transcription_is_public": transcription_is_public,
            "upload_type": UploadTypes.API.value,
            "username": username,
            "media_datetime": media_datetime,
            "start_time": start_time,
            "end_time": end_time,
        }

        try:
            if not media_file:
                return {"error": "Media file is required"}, 400

            media_file_path = save_uploaded_files(account.su_account, media_file)

            upload_info["media_file_path"] = media_file_path
            try:
                pre_upload = PreUploadDAO.create_pre_upload(
                    account_id,
                    username,
                    media_id,
                    title,
                    notification_method=notification_method,
                    notification_email=notification_email,
                    notification_url=notification_url,
                )

                logger.debug(f"Pre-upload created: {pre_upload.media_id}")
                logger.debug(
                    f"Calling ingest task for media id {media_id}: {upload_info}"
                )
                ingest_task.apply_async(
                    queue=current_app.config["CELERY_INGEST_QUEUE"],
                    args=[upload_info, media_id],
                )
                return pre_upload, 201
            except PreUploadDAOException as error:
                logger.warning(error)
                return {"error": str(error)}, 400
        except OSError as error:
            logger.error(
                f"Disk quota exceded ({error}). Please, empty the uploads volume."
            )
            return {"error": "Server error"}, 500


translation_model = namespace.model(
    "Translation",
    {
        "languageFrom": fields.String(
            required=True,
            description="The original language of the Upload",
            attribute="language_from",
        ),
        "languageTo": fields.String(
            required=True,
            description="The destination language of the Upload",
            attribute="language_to",
        ),
        "mediaId": fields.String(
            required=True, description="The original media ID", attribute="media_id"
        ),
        "uploadId": fields.String(
            required=True, description="The returned media ID", attribute="upload_id"
        ),
    },
)


@namespace.doc(security=["Bearer Token"])
@namespace.route("/translation/<string:media_id>/")
class TranslationDetailsEndpoint(Resource):
    method_decorators = [jwt_resource_required("all")]

    update_parser = namespace.parser()

    @namespace.doc("Request a translation for an upload")
    @namespace.response(404, "Upload not found")
    @namespace.marshal_with(translation_model, as_list=False)
    def patch(self, media_id) -> Tuple[Dict[str, Any], int]:
        """Request the translation for an upload"""
        logger.info(
            f"Calling request translation endpoint for media id {media_id}. Account ID: {g.account_id}"
        )
        account_id = g.account_id
        account = AccountDAO.get_account_by_id(account_id)
        upload_item = UploadDAO.get_upload_by_media_id_for_account(media_id, account_id)

        if upload_item:
            StateLogDAO.create(upload_item.id, "Requested translation")
            api_client = ApiSpeechClientMLLP(substitute_user=account.su_account)
            upload_data = api_client.request_translation(
                media_id, upload_item.language.value, upload_item.nonce
            )
            logger.debug(
                f"Translation requested successfully for media id {media_id}. Response: {upload_data}"
            )
            if upload_data and upload_data["upload_id"]:
                UploadDAO.update_ingest_id(
                    upload_item.id,
                    upload_data["upload_id"],
                    TaskStates.WAITING_TRANSLATIONS,
                )
                StateLogDAO.create(upload_item.id, "Waiting for translation")
                return upload_data, 200
        return {"message": "Upload not found"}, 404
