#
UPLOAD_SORT_KEYS = [
    "id",
    "created_on",
    "title",
    "state",
    "updated_on",
]
UPLOAD_DIRECTION_KEYS = [
    "asc",
    "desc",
]
# List of allowed file extensions for the ingest
# Values: list of strings (Default: ["txt", "pdf", "png", "jpg", "jpeg", "mp4", "avi"])
ALLOWED_MEDIA_FILES = [
    "mp4",
    "m4v",
    "ogv",
    "wmv",
    "avi",
    "mpg",
    "flv",
    "ogg",
    "wav",
    "mp3",
    "oga",
    "flac",
    "aac",
]
ALLOWED_SUBTITLE_FILES = ["dfxp", "srt", "trs"]
ALLOWED_SLIDES_FILES = [
    "ppt",
    "pptx",
]
ALLOWED_DOCUMENT_FILES = ["txt", "doc", "docx", "pdf"]
ALLOWED_UPLOAD_EXTENSIONS = set().union(
    ALLOWED_SUBTITLE_FILES, ALLOWED_DOCUMENT_FILES, ALLOWED_SLIDES_FILES
)
#
#
ALLOWED_UPLOAD_LANGUAGES = ["EN", "FR"]
#
#
ALLOWED_NOTIFICATION_METHODS = [
    "NONE",
    "EMAIL",
    "CALLBACK",
]
