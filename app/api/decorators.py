import logging

from flask import current_app, g
from flask_jwt_extended import get_jwt_identity, verify_jwt_in_request
from sqlalchemy.orm.exc import NoResultFound

from app.daos.accounts import AccountDAO
from app.daos.api_tokens import ApiTokenDAO
from app.utils.auth.client_oidc import set_dummy_api_key

logger = logging.getLogger("webapp.require_token")


def jwt_resource_required(resource):
    """
    Custom decorator that verifies the JWT is present in
    the request, as well as insuring that this user has access to the given resource.
    This way, the access token will only work with the selected API endpoints

    :param resource: Name of the resource (all, test)
    :return:
    """

    def decorator(func):
        def wrapper(*args, **kwargs):
            if current_app.config["LOGIN_DISABLED"]:
                set_dummy_api_key()
                return func(*args, **kwargs)
            verify_jwt_in_request()
            name = get_jwt_identity()
            try:
                api_key = ApiTokenDAO.get_by_name(name)
                g.account_id = api_key.account_id
                if api_key.resource.value != resource:
                    return "Forbidden", 401
                ApiTokenDAO.update_last_used(api_key.id)
                AccountDAO.set_account_last_used(api_key.account_id)
            except NoResultFound:
                return "Forbidden", 401
            else:
                return func(*args, **kwargs)

        return wrapper

    return decorator
