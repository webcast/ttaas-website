import logging
import os
from importlib import import_module
from typing import Optional

import sentry_sdk
from flask import Flask
from flask_admin import Admin
from flask_admin.menu import MenuLink
from flask_cors import CORS
from flask_login import LoginManager, current_user
from kombu import Queue
from sentry_sdk.integrations.flask import FlaskIntegration
from werkzeug.middleware.proxy_fix import ProxyFix
from zoom_python_client.utils.logger import setup_logs as setup_zoom_logs

from app.api_loader import (
    load_api_internal_blueprints,
    load_api_private_blueprints,
    load_api_public_blueprints,
)
from app.extensions import cache, celery, db, jwt, migrate
from app.models.accounts import Account
from app.models.captions_edits import CaptionEdit
from app.models.messages import Message
from app.models.pre_uploads import PreUpload
from app.models.token_blacklist import ApiToken
from app.models.uploads import Upload
from app.models.users import BackendUser, User
from app.services.cern_search.commands import (
    search_delete_document,
    search_delete_document_from_api_by_mediaid,
    search_search_document,
    search_search_document_from_api,
    search_send_document,
    search_send_document_from_api,
)
from app.services.mllp.commands import (
    delete_media,
    get_media_langs,
    get_media_list,
    get_media_metadata,
    get_systems,
    get_upload_list,
    get_upload_status,
)
from app.utils.blacklist_helpers import check_if_token_revoked
from app.utils.celery_probes import LivenessProbe
from app.utils.cern_openid import load_cern_openid
from app.utils.logger import setup_logs
from app.views.admin_account_form import AccountModelView
from app.views.admin_index_view import ProtectedAdminIndexView
from app.views.admin_preupload_list import PreUploadModelView
from app.views.admin_protected_view import AdminProtectedView
from app.views.admin_upload_form import UploadModelView
from app.views.api_token_detail_view import ApiTokenDetailView
from app.views.blueprints import all_blueprints

logger = logging.getLogger("webapp")


def create_app(
    config_filename: str = "config/config.py",
    static_url_path: str = "/static-files",
) -> Flask:
    """Factory function to create the application.

    Args:
        config_filename (str, optional): The path of the config py file
        to be used in the application.
        Defaults to "config/config.py".

        static_url_path (str, optional): The url path to the static files of the application.
    Returns:
        Flask: The flask application
    """
    app = Flask(
        __name__,
        static_url_path=static_url_path,
    )
    app.config.from_pyfile(config_filename)

    if app.config["USE_PROXY"]:
        app.wsgi_app = ProxyFix(app.wsgi_app, x_for=1, x_proto=1, x_host=1)  # type: ignore

    setup_logs(
        app,
        "webapp",
        to_file=app.config.get("LOG_FILE_ENABLED", False),
        to_mail=app.config.get("LOG_MAIL_ENABLED", False),
    )

    CORS(app)
    init_sentry(app)
    initialize_database(app)
    initialize_models(app)
    load_cern_openid(app)
    init_jwt_auth(app)
    init_flask_admin(app)
    load_private_api_v1(app)
    load_public_api_v1(app)
    load_internal_api_v1(app)
    initialize_views(app)
    load_cli(app)
    initialize_cache(app)
    init_celery(app)

    log_level = logging.DEBUG if app.config["LOG_LEVEL"] == "DEV" else logging.INFO
    setup_zoom_logs(log_level=log_level)

    @app.context_processor
    def inject_template_variables():
        is_dev = app.config["IS_DEV"]
        return dict(is_dev=is_dev, current_user=current_user)

    return app


def init_sentry(application: Flask) -> None:
    if application.config["ENABLE_SENTRY"]:
        print("Initializing Sentry... ", end="")
        # pylint: disable=abstract-class-instantiated
        sentry_sdk.init(  # type: ignore
            dsn=application.config["SENTRY_DSN"],
            integrations=[FlaskIntegration()],
            environment=application.config["SENTRY_ENVIRONMENT"],
        )
        print("OK")


def load_cli(application: Flask) -> None:
    """Load the CLI commands into the application
    Args:
        application (Flask): Application to load the CLI commands into.
    """
    logger.info("Adding the Command Line commands")
    application.cli.add_command(get_media_list)
    application.cli.add_command(get_systems)
    application.cli.add_command(get_media_langs)
    application.cli.add_command(get_media_metadata)
    application.cli.add_command(get_upload_status)
    application.cli.add_command(get_upload_list)

    application.cli.add_command(search_send_document)
    application.cli.add_command(search_search_document)
    application.cli.add_command(search_delete_document)
    application.cli.add_command(search_send_document_from_api)
    application.cli.add_command(search_delete_document_from_api_by_mediaid)
    application.cli.add_command(search_search_document_from_api)
    application.cli.add_command(delete_media)
    logger.info("Added the Command Line commands")


def load_private_api_v1(application: Flask) -> None:
    """Loads the private API v1.
    Args:
        application (Flask): The application to load the API into.
    """
    api_blueprints = load_api_private_blueprints()
    prefix = "/api"
    version = "v1"

    for blueprint in api_blueprints:
        application.register_blueprint(
            blueprint,
            url_prefix=f"{prefix}/private/{version}",
        )


def load_public_api_v1(application: Flask) -> None:
    """Loads the public API v1.
    Args:
        application (Flask): The application to load the API into.
    """
    api_blueprints = load_api_public_blueprints()
    prefix = "/api"
    version = "v1"

    for blueprint in api_blueprints:
        application.register_blueprint(
            blueprint,
            url_prefix=f"{prefix}/public/{version}",
        )


def load_internal_api_v1(application: Flask) -> None:
    """Loads the internal API v1.
    Args:
        application (Flask): The application to load the API into.
    """
    api_blueprints = load_api_internal_blueprints()
    prefix = "/api"
    version = "v1"

    for blueprint in api_blueprints:
        application.register_blueprint(
            blueprint,
            url_prefix=f"{prefix}/internal/{version}",
        )


def init_jwt_auth(application: Flask) -> None:
    """
    Initializes the JWT Authentication and sets the global configuration for the application JWT
    :param app: Application that will be set up
    :return: -
    """
    print("Initializing JWT Authentication... ", end="")
    application.config["JWT_ACCESS_TOKEN_EXPIRES"] = False
    application.config["JWT_BLACKLIST_ENABLED"] = True
    application.config["JWT_BLACKLIST_TOKEN_CHECKS"] = ["access", "refresh"]

    jwt.init_app(application)

    jwt.token_in_blocklist_loader(check_if_token_revoked)
    print("ok")


def initialize_database(application: Flask) -> None:
    """
    Initialize the database and the migrations
    """

    if application.config.get("DB_ENGINE", None) == "postgresql":
        print("Initializing Postgres Database... ", end="")
        application.config["SQLALCHEMY_DATABASE_URI"] = (
            "postgresql://"
            + f"{application.config['DB_USER']}:{application.config['DB_PASS']}"
            + f"@{application.config['DB_SERVICE_NAME']}:{application.config['DB_PORT']}"
            + f"/{application.config['DB_NAME']}"
        )

        application.config["SQLALCHEMY_RECORD_QUERIES"] = False
        application.config["SQLALCHEMY_POOL_SIZE"] = int(
            application.config.get("SQLALCHEMY_POOL_SIZE", 5)
        )
        application.config["SQLALCHEMY_POOL_TIMEOUT"] = int(
            application.config.get("SQLALCHEMY_POOL_TIMEOUT", 10)
        )
        application.config["SQLALCHEMY_POOL_RECYCLE"] = int(
            application.config.get("SQLALCHEMY_POOL_RECYCLE", 120)
        )
        application.config["SQLALCHEMY_MAX_OVERFLOW"] = int(
            application.config.get("SQLALCHEMY_MAX_OVERFLOW", 3)
        )

    elif application.config.get("DB_ENGINE", None) == "mysql":
        print("Initializing Mysql Database... ", end="")
        user = application.config["DB_USER"]
        password = application.config["DB_PASS"]
        service_name = application.config["DB_SERVICE_NAME"]
        port = application.config["DB_PORT"]
        db_name = application.config["DB_NAME"]
        application.config[
            "SQLALCHEMY_DATABASE_URI"
        ] = f"mysql+pymysql://{user}:{password}@{service_name}:{port}/{db_name}?charset=utf8mb4"

    else:
        print("Initializing Sqlite Database...", end="")
        _basedir = os.path.abspath(os.path.dirname(__file__))
        application.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///" + os.path.join(
            _basedir, "webapp.db"
        )

    application.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

    db.init_app(application)
    db.app = application
    print("ok")

    print("Initializing Database Migrations... ", end="")
    migrate.init_app(application, db)
    print("ok")


def initialize_models(application: Flask) -> None:
    """
    Load all the available models
    """
    with application.app_context():
        print("Loading models... ", end="")
        application_models = [
            "app.models.token_blacklist",
            "app.models.accounts",
            "app.models.users",
            "app.models.uploads",
            "app.models.pre_uploads",
            "app.models.messages",
            "app.models.cern_search_settings",
            "app.models.captions_edits",
            "app.models.upload_state_logs",
        ]
        for module in application_models:
            import_module(module)
        print("ok")


def init_flask_admin(application: Flask) -> None:
    """Initializes the flask admin extension.

    Args:
        application (Flask): The flask application to initialize the flask admin with.
    """
    print("Initializing Admin... ", end="")
    with application.app_context():
        admin = Admin(
            application,
            name="Transcription and Translation Service Website Administration",
            index_view=ProtectedAdminIndexView(),
            template_mode="bootstrap4",
        )
        admin.add_link(MenuLink(name="Logout", url="/logout/"))

        admin.add_view(AccountModelView(Account, db.session))
        admin.add_view(ApiTokenDetailView(ApiToken, db.session))
        admin.add_view(AdminProtectedView(Message, db.session))
        admin.add_view(UploadModelView(Upload, db.session, category="Uploads"))
        admin.add_view(AdminProtectedView(CaptionEdit, db.session, category="Uploads"))
        admin.add_view(PreUploadModelView(PreUpload, db.session, category="Uploads"))
        admin.add_view(AdminProtectedView(BackendUser, db.session, category="Users"))
        admin.add_view(AdminProtectedView(User, db.session, category="Users"))

    print("ok")

    # setup login manager
    login_manager = LoginManager()
    login_manager.login_view = "cern_openid.login"  # type: ignore


def initialize_views(application: Flask) -> None:
    """
    Initializes all the application blueprints

    :param application: Application where the blueprints will be registered
    :return:
    """
    print("initiliaze views")

    for blueprint in all_blueprints:
        import_module(blueprint.import_name)
        application.register_blueprint(blueprint)


def initialize_cache(application: Flask) -> None:
    """Initializes the cache for the application.

    Args:
        application (Flask): The Flask application to initialize the cache for.
    """
    cache_config = {"CACHE_TYPE": "null"}
    if application.config.get("CACHE_ENABLE", False):
        print("Initializing Redis Caching... ", end="")
        cache_config = {
            "CACHE_TYPE": "redis",
            "CACHE_REDIS_HOST": application.config.get("CACHE_REDIS_HOST", None),
            "CACHE_REDIS_PASSWORD": application.config.get(
                "CACHE_REDIS_PASSWORD", None
            ),
            "CACHE_REDIS_PORT": application.config.get("CACHE_REDIS_PORT", None),
        }
    else:
        print("Caching disabled... ", end="")
    cache.init_app(application, config=cache_config)
    print("ok")


def init_celery(
    app: Optional[Flask] = None,
    config_filename: str = "config/config.py",
):
    """
    Initialize the Celery tasks support.

    :param app: The current Flask application. If it's not set,
    it will be created using the config parameter
    :param config: Configuration object for the Flask application
    :return: The Celery instance
    """
    print("Initializing Celery... ", end="")
    tasks_modules = [
        "app.tasks.sample",
        "app.tasks.ingest",
        "app.tasks.upload_check",
        "app.tasks.pre_upload_check",
        "app.tasks.pre_upload_notifications",
        "app.tasks.email_notifications",
        "app.tasks.url_notifications",
        "app.tasks.files_handle",
        "app.tasks.transcriptions",
    ]

    app = app or create_app(config_filename)
    celery.app = app  # type: ignore
    celery.conf.broker_url = app.config["CELERY_BROKER_URL"]  # type: ignore
    celery.conf.result_backend = app.config["CELERY_RESULTS_BACKEND"]  # type: ignore
    celery.conf.beat_schedule = app.config["CELERY_BEAT_SCHEDULE"]
    celery.conf.include = tasks_modules
    celery.conf.task_serializer = "json"
    celery.conf.result_serializer = "json"
    # 3600 = 1 hour
    # 24 = 1 day
    # 90 = 3 months
    celery.conf.result_expires = (
        "7776000"  # 3 months to delete old results from database
    )
    celery.conf.result_extended = True

    # default_exchange = Exchange("default", type="direct")
    # media_exchange = Exchange("media", type="direct")
    # callbacks_exchange = Exchange("callbacks", type="direct")

    celery.conf.task_queues = (
        Queue("default", routing_key="default"),
        Queue("media", routing_key="media"),
        Queue("callbacks", routing_key="callbacks"),
    )
    # celery.conf.task_default_queue = "default"
    # celery.conf.task_default_exchange = "default"
    # celery.conf.task_default_routing_key = "default"

    celery.conf.task_routes = {
        "app.tasks.sample.*": {"queue": "default"},
        "app.tasks.upload_check.*": {"queue": "default"},
        "app.tasks.transcriptions.*": {"queue": "default"},
        "app.tasks.ingest.*": {"queue": "media"},
        "app.tasks.files_handle.*": {"queue": "media"},
        "app.tasks.url_notifications.*": {"queue": "callbacks"},
        "app.tasks.email_notifications.*": {"queue": "callbacks"},
        "app.tasks.pre_upload_notifications.*": {"queue": "callbacks"},
    }

    celery.conf.update(app.config)

    print("Adding Celery Liveness Probe... ", end="")
    celery.steps["worker"].add(LivenessProbe)
    print("ok")

    class ContextTask(celery.Task):
        """Make celery tasks work with Flask app context"""

        def __call__(self, *args, **kwargs):
            if app:
                with app.app_context():
                    return self.run(*args, **kwargs)

    celery.Task = ContextTask
    print("ok")
    return celery
