import os
from pathlib import Path

from celery.schedules import crontab  # noqa

# Either if it's a local development environment or a production environment
# Values: True | False
IS_DEV = True
# Logging level for the application
# Values: DEV | PROD
LOG_LEVEL = "DEV"
# Logging to file
# Values: True|False
LOG_FILE_ENABLED = False
# Logging errors by email
# Values: True|False
LOG_MAIL_ENABLED = False
# CERN email server
# Values: string (default: cernmx.cern.ch)
LOG_MAIL_HOSTNAME = "cernmx.cern.ch"
# Email address on the FROM of the error message emails
# Values: string
LOG_MAIL_FROM = "<EMAIL_ADDRESS_FROM>"
# Email address on the TO of the error message emails
# Values: string
LOG_MAIL_TO = "<EMAIL_ADDRESS_TO>"
# DATABASE
#
DB_ENGINE = "mysql"
DB_NAME = "mllp_api_client"
DB_USER = "root"
DB_PASS = "root"
DB_PORT = 3306
DB_SERVICE_NAME = "127.0.0.1"
# Enable Werkzeug proxy
#
USE_PROXY = True
# Configuration for the MLLP API
#
MLLP_CONFIG = {
    "API_USER": "<TODO>",
    "API_SECRET": "<TODO>",
    "API_BASE": "https://ttp.mllp.upv.es/api/v3",
    "EDITOR_BASE": "https://ttp.mllp.upv.es/player",
}
# Callback URL for the ingest. It will be notified by the ASR Server on state changes
# Values: string (Default: "http://localhost:5000/api/v1/ingest/callback")
MLLP_INGEST_CALLBACK_URL = "http://localhost:5000/api/internal/v1/callbacks/"
# ID for the french transcription model system in the ASR Server database
# Values: int (Default: 103)
MLLP_FRENCH_ASR_ID = 103
# ID for the english transcription model system in the ASR Server database
# Values: str (Default: 150)
MLLP_ENGLISH_ASR_ID = 150
# Will disable the application authentication and token validation
# Default: False (Other values: True)
LOGIN_DISABLED = False
# OIDC configuration for CERN's SSO. This is used to authenticate the user in the frontend
#
OIDC_CONFIG = {
    "OIDC_CLIENT_ID": "<TODO>",
    "OIDC_JWKS_URL": "https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/certs",
    "OIDC_ISSUER": "https://auth.cern.ch/auth/realms/cern",
    "OIDC_LOGOUT_URL": "https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/logout",
}
# The application's secret key
# Default: Value generated with: import os; os.urandom(24)
SECRET_KEY = "b'\xdc\x04+.\x16\xd1\xf4c\xae\xc7\xd2\x98\x1d4\x06!\xbc\xad\x81\x1aE]F,'"
# Secret key used for the JWT authorization
# Default: Value generated with: import os; os.urandom(24)
JWT_SECRET_KEY = "super-secret"
#
# This line is only required during development if not using SSL
# !! It must be removed in production
os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"
# In case it's needed, _basedir can be used to get the absolute path of the current file
#
# _basedir = os.path.abspath(os.path.dirname(__file__))
current_dir = os.path.abspath(os.path.dirname(__file__))
path = Path(current_dir)
project_dir = path.parent.absolute().parent
#
#
UPLOADS_FOLDER = os.path.join(project_dir, "uploads")
# CERN OpenID configuration for the backend
# Backend uses a different application ID and secret than the frontend since only ADMINS can access it.
#
CERN_OPENID_CLIENT_ID = "<TODO>"
CERN_OPENID_CLIENT_SECRET = "<TODO>"
OIDC_JWKS_URL = "https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/certs"
OIDC_ISSUER = "https://auth.cern.ch/auth/realms/cern"
OIDC_LOGOUT_URL = "https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/logout"
#
#
PROPAGATE_EXCEPTIONS = True
# The main MLLP account used. It will be checked on every request to avoid the use of substitute_account
#
MLLP_ADMIN_ACCOUNT = "<TODO>"
# Whether or not to perform real ingests on the MLLP API
# Values: True | False (Default: True)
MLLP_TEST_INGEST = True
# Whether or not to enable the translations on ingest.
# Values: True | False (Default: False)
MLLP_REQUEST_TRANSLATION = False
# Whether or not to enable caching
# Values: True | False (Default: False)
CACHE_ENABLE = False
# Hostname for the redis instance
# Values: string (Default: "redis")
CACHE_REDIS_HOST = "redis"
# Port to connect to the redis instance
# Values: int (Default: 6379)
CACHE_REDIS_PORT = "6379"
# Password to connect to the redis instance
# Values: string (Default: "redis")
CACHE_REDIS_PASSWORD = "<TODO>"
# Redis database identifier
# Values: string (Default: "0")
CACHE_REDIS_DB = "0"
# The scheduled tasks configuration for Celery
# Default: A dictionary with the following keys:
# "task_name": {"task": "task_module", "schedule": "crontab(minute="*/5")"}
CELERY_BEAT_SCHEDULE = {  # type: ignore
    # 'dummy_task': {
    #     'task': 'app.tasks.sample.dummy_task',
    #     'schedule': crontab(minute="*/5")
    # }
}
# The URL to the Celery's broker (redis)
# Default: 'redis://redis:6379/0'
CELERY_BROKER_URL = "redis://redis:6379/0"
# The URL to the Celery's backend (redis or MySQL)
# Default: 'redis://redis:6379/0'
CELERY_RESULTS_BACKEND = "redis://redis:6379/0"
# The name of the queue to use for the Celery ingest tasks
# Default: 'media'
CELERY_INGEST_QUEUE = "default"
# Whether or not to enable the notification emails
# Values: True|False (Default: True)
ENABLE_NOTIFICATION_EMAILS = True
# Whether or not to enable the notification URL callbacks
# Values: True|False (Default: True)
ENABLE_NOTIFICATION_CALLBACKS = True
# The hostname of the Mail server
# Values: string (Default: "cernmx.cern.ch")
MAIL_HOSTNAME = "cernmx.cern.ch"
# The email address used as FROM in the emails sent by the application
# Values: string (Default: "")
MAIL_FROM = "webcast-team@cern.ch"
# Email address used to notify the service owners
# Value: string (Default: "")
SERVICE_MAIL = "<TODO>"
# The email address used as TO by default in the emails sent by the application
# Values: list[string] (Default: [])
MAIL_TO = "<TODO>"
# Days to keep the uploaded files before deleting them
# Values: int (Default: 7)
UPLOAD_EXPIRATION_DAYS = 7
# Days to keep the uploaded files before deleting them
# Values: int (Default: 7)
UPLOAD_EXPIRATION_DAYS = 7
# Days to display the media files from an account
# Values: int (Default: 365)
MEDIA_DISPLAY_DAYS = 365
# Whether or not to enable the CERN Search integration
# Values: True|False (Default: False)
CERNSEARCH_ENABLE = False
# The base URL
# Value: string (Default: "https://asrservice-search-dev.web.cern.ch")
CERNSEARCH_INSTANCE = "<TODO>"
# This token is a personal access token generated on <CERNSEARCH_INSTANCE>/account/settings/applications/
# value: string
CERNSEARCH_TOKEN = "<TODO>"
# Whether or not to enable Sentry
# value: boolean (Deffault: False)
ENABLE_SENTRY = False
# The Sentry DSN URL
# value: string (URL)
SENTRY_DSN = "<TODO>"
#
#
SENTRY_ENVIRONMENT = "<TODO>"
#
#
UPLOAD_SORT_KEYS = [
    "created_on",
    "title",
    "state",
    "updated_on",
]
UPLOAD_DIRECTION_KEYS = [
    "asc",
    "desc",
]
#
# URL of the weblecture player to use
#
CERNSEARCH_BASIC_URL_PLAYER = "https://weblecture-player-test.web.cern.ch/"
#
# CES token to access the ACL API
#
CERNSEARCH_CES_TOKEN = "<TODO>"
#
# CES URL for the ACL API
#
CERNSEARCH_CES_URL = "https://ces-prod.web.cern.ch/api/v1/indico/acl"
#
# CES URL for the Indico API
#
CERNSEARCH_CES_URL_INDICO = "https://ces-prod.web.cern.ch/api/v1/indico/event"
#
# Schema for the CERN Search index
#
CERNSEARCH_SCHEMA = (
    "https://asrservice-search-dev.web.cern.ch/schemas/asrservice/vtt_ttaas_v1.0.0.json"
)
#
# Role for the Weblectures service media
#
WEBLECTURES_SERVICE_ROLE = "<TODO>"
#
# ZOOM API integration (server to server tokens)
#
ZOOM_ACCOUNT_ID = "<TODO>"
ZOOM_CLIENT_ID = "<TODO>"
ZOOM_CLIENT_SECRET = "<TODO>"
ZOOM_TOKEN_PATH = "/tmp/"
#
# Internal ID of the Zoom Live account in the application.
# This is the account that will be used for Zoom ASR Live events.
#
ZOOM_LIVE_ACCOUNT_INTERNAL_ID = 11
#
#
#
LIVESTREAMING_SERVER = "rtmp://wowzaqaorigin.cern.ch:1935/liveoutside"
LIVESTREAMING_RESTAPI = "https://ttaas-qa-rtmp.cern.ch/"

LIVESTREAMING_URL = "https://cern.ch"
LIVESTREAMING_RESOLUTION = "720p"
LIVESTREAMING_USER = "<TODO>"
LIVESTREAMING_APIKEY = "<TODO>"
LIVESTREAMING_SYSTEMID = 0
LIVESTREAMING_SEGMENTSIZE = 8
LIVESTREAMING_MAX_PARALLEL_SESSIONS = 2
