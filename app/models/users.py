import datetime

from flask_dance.consumer.storage.sqla import OAuthConsumerMixin
from flask_login import UserMixin
from sqlalchemy import event
from sqlalchemy.orm.attributes import get_history

from app.extensions import db


class User(db.Model, UserMixin):
    __tablename__ = "user"

    id = db.Column(db.Integer, unique=True, primary_key=True)
    first_name = db.Column(db.String(255))
    username = db.Column(db.String(255))
    last_name = db.Column(db.String(255))
    email = db.Column(db.String(255), unique=True)

    is_admin = db.Column(db.Boolean, nullable=False, default=False)
    active = db.Column(db.Boolean, default=True)
    revoked_on = db.Column(db.DateTime, nullable=True)

    confirmed_at = db.Column(db.DateTime, default=datetime.datetime.now)
    last_login = db.Column(db.DateTime, default=datetime.datetime.now)


class BackendUser(db.Model, UserMixin):
    __tablename__ = "backend_user"

    id = db.Column(db.Integer, unique=True, primary_key=True)
    first_name = db.Column(db.String(255))
    username = db.Column(db.String(255))
    last_name = db.Column(db.String(255))
    email = db.Column(db.String(255), unique=True)
    is_admin = db.Column(db.Boolean, nullable=False, default=False)

    department = db.Column(db.String(255), nullable=True)
    group = db.Column(db.String(255), nullable=True)
    section = db.Column(db.String(255), nullable=True)
    organization = db.Column(db.String(255), nullable=True)
    building = db.Column(db.String(255), nullable=True)

    active = db.Column(db.Boolean, default=True)
    confirmed_at = db.Column(db.DateTime, default=datetime.datetime.now)
    last_login = db.Column(db.DateTime, default=datetime.datetime.now)

    def __repr__(self):
        return f"<User {self.id}: {self.first_name} {self.last_name} {self.username} >"


class OAuth(db.Model, OAuthConsumerMixin):
    """
    Represents an Oauth connection in the application
    """

    __tablename__ = "flask_dance_oauth"

    user_id = db.Column(db.Integer, db.ForeignKey(BackendUser.id))
    user = db.relationship(BackendUser)


@event.listens_for(db.session, "before_flush")
def receive_before_flush_user(session, flush_context, instances):
    "listen for the 'after_flush' event"
    # pylint: disable=unused-argument
    for instance in session.dirty:
        if not isinstance(instance, User):
            continue
        previous_value = get_history(instance, "active")[2]
        if len(previous_value) > 0:
            active_value = previous_value[0]
            if instance.active != active_value:
                if instance.active:
                    instance.revoked_on = datetime.datetime.now()
                else:
                    instance.revoked_on = None
