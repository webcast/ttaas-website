import datetime
import enum
import logging
from secrets import token_hex

from sqlalchemy import TIMESTAMP
from sqlalchemy_utils.types.choice import ChoiceType

from app.extensions import db
from app.models.base import ModelBase
from app.models.upload_states import (
    AvailableLanguages,
    NotificationMethods,
    RunningStates,
    TaskStates,
    UploadTypes,
)
from app.utils.dates import get_date_as_string

logger = logging.getLogger("webapp.daos")


class MetadataRcodes(enum.Enum):
    EXISTS = 0
    NO_EXISTS = 1


class Upload(ModelBase):

    __table_args__ = (db.UniqueConstraint("account_id", "media_id"),)

    title = db.Column(db.String(255), nullable=False)
    username = db.Column(db.String(255), nullable=False)
    language = db.Column(
        ChoiceType(AvailableLanguages, impl=db.String(255)),
        nullable=False,
        default=AvailableLanguages.EN,
    )
    state = db.Column(
        ChoiceType(TaskStates, impl=db.String(255)),
        nullable=False,
        default=TaskStates.CREATED,
    )

    running_state = db.Column(
        ChoiceType(RunningStates, impl=db.String(255)), nullable=True, default=None
    )

    media_id = db.Column(db.String(255), nullable=False, unique=True)
    nonce = db.Column(db.String(255), nullable=False, default=token_hex(16))

    media_file_path = db.Column(db.String(255), nullable=False)
    media_file_checksum = db.Column(db.String(255), nullable=True)

    comments = db.Column(db.Text(), nullable=True)

    notification_method = db.Column(
        ChoiceType(NotificationMethods, impl=db.String(255)),
        nullable=False,
        default=NotificationMethods.NONE,
    )
    notification_email = db.Column(db.String(255), nullable=True)
    notification_url = db.Column(db.String(255), nullable=True)

    ingest_id = db.Column(db.String(255), nullable=True, unique=True)

    upload_type = db.Column(
        ChoiceType(UploadTypes, impl=db.String(255)),
        nullable=False,
        default=UploadTypes.WEB,
    )

    reference_url = db.Column(db.String(255), nullable=True)

    start_time = db.Column(db.String(255), nullable=True)
    end_time = db.Column(db.String(255), nullable=True)

    created_on = db.Column(db.DateTime, default=datetime.datetime.now)
    updated_on = db.Column(
        TIMESTAMP(),
        nullable=False,
        default=datetime.datetime.now,
        onupdate=datetime.datetime.now,
    )

    account_id = db.Column(db.Integer, db.ForeignKey("account.id"))

    cern_search_settings = db.relationship(
        "CernSearchSettings", backref="upload", uselist=False, cascade="all,delete"
    )

    captions_edits = db.relationship(
        "CaptionEdit", backref="upload", lazy=True, cascade="all, delete"
    )

    state_logs = db.relationship(
        "StateLog", backref="upload", lazy=True, cascade="all, delete"
    )

    def to_dict(self) -> dict:
        """Return the values of the object as a dictionary

        Returns:
            dict: The object values
        """
        created_on_str = get_date_as_string(self.created_on)
        updated_on_str = get_date_as_string(self.updated_on)

        return {
            "id": self.id,
            "title": self.title,
            "media_id": self.media_id,
            "language": self.language,
            "state": self.state,
            "comments": self.comments,
            "notification_email": self.notification_email,
            "created_on": created_on_str,
            "updated_on": updated_on_str if self.updated_on else None,
        }

    def __str__(self) -> str:
        return f"<Upload-{self.id}: language={self.language} media_id={self.media_id} state={self.state} />"
