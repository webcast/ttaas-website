import enum


class CompletedType(enum.Enum):
    TRANSLATION = "TRANSLATION"
    TRANSCRIPTION = "TRANSCRIPTION"


class NotificationMethods(enum.Enum):
    NONE = "NONE"
    EMAIL = "EMAIL"
    CALLBACK = "CALLBACK"


class UploadTypes(enum.Enum):
    API = "API"
    WEB = "WEB"


class AvailableLanguages(enum.Enum):
    EN = "en"
    FR = "fr"


class RunningStates(enum.Enum):
    INGESTED = "0"  # → Video ingested, not processed yet.
    PROCESSING = "1"  # → Processing Media Package file.
    TRANSCRIPTION_IN_PROGRESS = "2"  # → Transcription in progress.
    TRANSLATION_IN_PROGRESS = "3"  # → Translation(s) in progress.
    TTP_TRACKS_IN_PROGRESS = (
        "4"  # → Text-To-Speech audio track(s) generation in progress.
    )
    PREPARING_TLP_PLAYER = "5"  # → Preparing data for the TLP Player.
    COMPLETED = "6"  # → New media successfully processed.
    MEDIA_UPDATED = "20"  # → Media successfully updated.
    MEDIA_DELETED = "30"  # → Media successfully deleted.
    OPERATION_CANCELED = "40"  # → Cancel upload operation successfully processed.
    UPLOAD_CANCELED = "90"  # → Upload cancelled.

    ERROR = "100"  # → An unknown error occurred.
    ERROR_PROCESSING_MEDIA_PACKAGE = (
        "101"  # → An error occurred while processing the Media Package.
    )
    ERROR_TRANSCRIBING_MEDIA = (
        "102"  # → An error occurred while transcribing the media.
    )
    ERROR_TRANSLATING_MEDIA = "103"  # → An error occurred while translating the media.
    ERROR_GENERATING_TTP = (
        "104"  # → An error occurred while generating Text-To-Speech audio tracks.
    )
    ERROR_PREPARING_TLP_PLAYER = (
        "105"  # → An error occurred while preparing data for the TLP Player.
    )
    ERROR_UPDATING_DATABASE = (
        "106"  # → An error occurred while updating internal database.
    )


class TaskStates(enum.Enum):
    CREATED = "CREATED"  # Created"
    SUBMITTED = "SUBMITTED"  # "Submitted"
    PREPARING_UPLOAD = "PREPARING_UPLOAD"  # Preparing upload"
    UPLOADING = "UPLOADING"  # "Uploading"
    WAITING_TRANSCRIPTIONS = "WAITING_TRANSCRIPTIONS"  # "Waiting transcriptions"
    RUNNING_TRANSCRIPTIONS = "RUNNING_TRANSCRIPTIONS"  # "Running transcriptions"
    TRANSCRIPTION_FINISHED = "TRANSCRIPTION_FINISHED"  # Transcription finished"
    WAITING_TRANSLATIONS = "WAITING_TRANSLATIONS"  # "Waiting translations"
    RUNNING_TRANSLATIONS = "RUNNING_TRANSLATIONS"  # "Running translations"
    TRANSLATION_FINISHED = "TRANSLATION_FINISHED"  # "Translation finished"
    USER_NOTIFIED = "USER_NOTIFIED"  # Finished and user notified"
    COMPLETED = "COMPLETED"  # "Completed"

    ERROR_SUBMITTING = "ERROR_SUBMITTING"  # Error submitting"
    ERROR_PREPARING_UPLOAD = "ERROR_PREPARING_UPLOAD"  # "Error preparing upload"
    ERROR_UPLOADING = "ERROR_UPLOADING"  # "Error uploading"
    ERROR_RUNNING_TRANSCRIPTIONS = (
        "ERROR_RUNNING_TRANSCRIPTIONS"  # "Error running transcriptions"
    )
    ERROR_RUNNING_TRANSLATIONS = (
        "ERROR_RUNNING_TRANSLATIONS"  # "Error running translations"
    )
    COMPLETED_CALLBACK_FAILED = "COMPLETED_CALLBACK_FAILED"  # "Callback failed"
    COMPLETED_EMAIL_FAILED = "COMPLETED_EMAIL_FAILED"  # "Notification email failed"

    ERROR = "ERROR"
    ERROR_NOTIFIED = "ERROR_NOTIFIED"
    ERROR_CALLBACK_FAILED = "ERROR_CALLBACK_FAILED"
    ERROR_EMAIL_FAILED = "ERROR_EMAIL_FAILED"
    DELETED = "DELETED"


class CERNSearchStates(enum.Enum):
    PENDING = "PENDING"
    SUBMITTED = "SUBMITTED"
    ERROR_SUBMITTING = "ERROR_SUBMITTING"
