import datetime

from sqlalchemy import event
from sqlalchemy.orm.attributes import get_history

from app.extensions import db
from app.models.base import ModelBase


class Account(ModelBase):
    __table_args__ = (db.UniqueConstraint("role", "su_account"),)

    role = db.Column(db.String(255), nullable=False)
    su_account = db.Column(db.String(255), nullable=False)
    api_secret_key = db.Column(db.String(255), nullable=True)
    account_name = db.Column(
        db.String(255), nullable=False, default=datetime.datetime.now
    )
    always_index = db.Column(db.Boolean, nullable=True, default=False)
    created_on = db.Column(db.DateTime, default=datetime.datetime.now)
    last_used = db.Column(db.DateTime, nullable=True)
    revoked = db.Column(db.Boolean, nullable=False, default=False)
    revoked_on = db.Column(db.DateTime, nullable=True)

    disable_callbacks = db.Column(db.Boolean, nullable=True, default=False)

    api_tokens = db.relationship(
        "ApiToken", backref="account", lazy=True, cascade="all, delete"
    )
    uploads = db.relationship(
        "Upload", backref="account", lazy=True, cascade="all, delete"
    )
    pre_uploads = db.relationship(
        "PreUpload", backref="account", lazy=True, cascade="all, delete"
    )

    def __str__(self) -> str:
        return f"{self.account_name} ({self.su_account})"


@event.listens_for(db.session, "before_flush")
def receive_before_flush_account(session, flush_context, instances):
    # pylint: disable=unused-argument
    "listen for the 'after_flush' event"
    for instance in session.dirty:
        if not isinstance(instance, Account):
            continue
        previous_value = get_history(instance, "revoked")[2]
        if len(previous_value) > 0:
            revoked_value = previous_value[0]
            if instance.revoked != revoked_value:
                if instance.revoked:
                    instance.revoked_on = datetime.datetime.now()
                else:
                    instance.revoked_on = None
