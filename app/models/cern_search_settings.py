import datetime
import enum
import logging

from sqlalchemy import TIMESTAMP
from sqlalchemy_utils.types.choice import ChoiceType

from app.extensions import db
from app.models.base import ModelBase
from app.utils.dates import get_date_as_string

logger = logging.getLogger("webapp.daos")


class CernSearchStates(enum.Enum):
    PENDING = "Pending"
    SUBMITTED = "Submitted"
    ERROR_SUBMITTING = "Error submitting"


class CernSearchSettings(ModelBase):
    cern_search_state = db.Column(
        ChoiceType(CernSearchStates, impl=db.String(255)),
        nullable=False,
        default=CernSearchStates.PENDING,
    )

    is_public = db.Column(db.Boolean, nullable=False, default=False)

    read_groups = db.Column(db.Text)
    read_users = db.Column(db.Text)

    created_on = db.Column(db.DateTime, default=datetime.datetime.now)
    updated_on = db.Column(
        TIMESTAMP(),
        nullable=False,
        default=datetime.datetime.now,
        onupdate=datetime.datetime.now,
    )

    media_datetime = db.Column(db.DateTime)

    upload_id = db.Column(db.Integer, db.ForeignKey("upload.id"))

    def to_dict(self) -> dict:
        """Return the values of the object as a dictionary

        Returns:
            dict: The object values
        """
        created_on_str = get_date_as_string(self.created_on)
        updated_on_str = get_date_as_string(self.updated_on)

        return {
            "id": self.id,
            "title": self.title,
            "media_id": self.media_id,
            "language": self.language,
            "state": self.state,
            "comments": self.comments,
            "notification_email": self.notification_email,
            "created_on": created_on_str,
            "updated_on": updated_on_str if self.updated_on else None,
        }

    def __repr__(self):
        return f"<CernSearchSettings {self.id} {self.is_public} {self.read_groups} {self.read_users}>"
