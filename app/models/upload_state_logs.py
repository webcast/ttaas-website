import datetime

from app.extensions import db
from app.models.base import ModelBase


class StateLog(ModelBase):

    description = db.Column(db.String(400), nullable=False)
    upload_id = db.Column(db.Integer, db.ForeignKey("upload.id"))

    created_on = db.Column(db.DateTime, default=datetime.datetime.now)
