import datetime
import enum

from sqlalchemy import TIMESTAMP
from sqlalchemy_utils.types.choice import ChoiceType

from app.extensions import db
from app.models.base import ModelBase


class SupervisionStatuses(enum.Enum):
    """Subtitle supervision status code. Defines the level of human supervision of the subtitles."""

    FULLY_AUTOMATIC = "0"  # → Fully Automatic: The whole subtitles are automatic.
    PARTIALLY_HUMAN = "1"  # → Partially Human: The subtitles have been supervised, but not for the whole video.
    FULLY_HUMAN = (
        "2"  # → Fully Human: The subtitles have been fully supervised by humans.
    )
    UNKNOWN = ""


class CaptionEdit(ModelBase):
    language = db.Column(db.String(400), nullable=False)
    media_id = db.Column(db.String(400), nullable=False)

    supervision_status = db.Column(
        ChoiceType(SupervisionStatuses, impl=db.String(255)), nullable=True
    )
    supervision_ratio = db.Column(db.Float, nullable=True)
    info = db.Column(db.Text)
    user = db.Column(db.String(400), nullable=True)
    timestamp = db.Column(db.String(255), nullable=False)

    upload_id = db.Column(db.Integer, db.ForeignKey("upload.id"))

    created_on = db.Column(db.DateTime, default=datetime.datetime.now)
    updated_on = db.Column(
        TIMESTAMP(),
        nullable=False,
        default=datetime.datetime.now,
        onupdate=datetime.datetime.now,
    )
