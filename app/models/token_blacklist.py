import datetime
import enum
import logging

from sqlalchemy import event
from sqlalchemy.orm.attributes import get_history
from sqlalchemy_utils.types.choice import ChoiceType

from app.extensions import db
from app.models.base import ModelBase

logger = logging.getLogger("webapp.models")


class ApiTokenResources(enum.Enum):
    ALL = "all"
    TEST = "test"
    METRICS = "metrics"
    CERN_SEARCH = "cern_search"


class ApiToken(ModelBase):

    __table_args__ = (db.UniqueConstraint("account_id", "name"),)

    name = db.Column(db.String(255), nullable=False)
    access_token = db.Column(db.String(500), nullable=False)
    jti = db.Column(db.String(36), nullable=False)
    token_type = db.Column(db.String(10), nullable=False)
    user_identity = db.Column(db.String(50), nullable=False)
    revoked = db.Column(db.Boolean, nullable=False)
    revoked_on = db.Column(db.DateTime, nullable=True)
    expires = db.Column(db.DateTime, nullable=True)
    created_on = db.Column(db.DateTime, default=datetime.datetime.now)
    last_used = db.Column(db.DateTime, nullable=True)
    resource = db.Column(
        ChoiceType(ApiTokenResources, impl=db.String(255)),
        nullable=False,
        default=ApiTokenResources.TEST,
    )

    account_id = db.Column(db.Integer, db.ForeignKey("account.id"))

    def to_dict(self) -> dict:
        """Return the values of the object as a dictionary

        Returns:
            dict: The object values
        """
        return {
            "token_id": self.id,
            "jti": self.jti,
            "token_type": self.token_type,
            "user_identity": self.user_identity,
            "revoked": self.revoked,
            "expires": self.expires,
            "resource": self.resource,
            "name": self.name,
        }


@event.listens_for(db.session, "before_flush")
def receive_before_flush_api_token(session, flush_context, instances):
    "listen for the 'after_flush' event"
    # pylint: disable=unused-argument
    for instance in session.dirty:
        if not isinstance(instance, ApiToken):
            continue

        previous_value = get_history(instance, "revoked")[2]
        if len(previous_value) > 0:
            revoked_value = previous_value[0]
            if instance.revoked != revoked_value:
                if instance.revoked:
                    instance.revoked_on = datetime.datetime.now()
                else:
                    instance.revoked_on = None
