import datetime
import enum
import logging

from sqlalchemy import TIMESTAMP
from sqlalchemy_utils.types.choice import ChoiceType

from app.extensions import db
from app.models.base import ModelBase
from app.models.upload_states import NotificationMethods

logger = logging.getLogger("webapp.daos")


class PreUploadStates(enum.Enum):
    CREATED = "CREATED"
    WAITING_CHECKSUM = "WAITING_CHECKSUM"
    ALREADY_EXISTS = "ALREADY_EXISTS"
    COMPLETED = "COMPLETED"

    ERROR = "ERROR"
    ERROR_NOTIFIED = "ERROR_NOTIFIED"
    ERROR_CALLBACK_FAILED = "ERROR_CALLBACK_FAILED"
    ERROR_EMAIL_FAILED = "ERROR_EMAIL_FAILED"

    ALREADY_EXISTS_NOTIFIED = "ALREADY_EXISTS_NOTIFIED"


class PreUpload(ModelBase):
    __table_args__ = (db.UniqueConstraint("account_id", "media_id"),)

    title = db.Column(db.String(255), nullable=True)
    username = db.Column(db.String(255), nullable=False)

    state = db.Column(
        ChoiceType(PreUploadStates, impl=db.String(255)),
        nullable=False,
        default=PreUploadStates.CREATED,
    )
    # The media ID of the object
    media_id = db.Column(db.String(255), nullable=False, unique=True)
    # If there is already an Upload with the same checksum
    existing_media_id = db.Column(db.String(255), nullable=True, unique=False)

    notification_method = db.Column(
        ChoiceType(NotificationMethods, impl=db.String(255)),
        nullable=False,
        default=NotificationMethods.NONE,
    )
    notification_email = db.Column(db.String(255), nullable=True, default="")
    notification_url = db.Column(db.String(255), nullable=True, default="")

    created_on = db.Column(db.DateTime, default=datetime.datetime.now)
    updated_on = db.Column(
        TIMESTAMP(),
        nullable=False,
        default=datetime.datetime.now,
        onupdate=datetime.datetime.now,
    )

    account_id = db.Column(db.Integer, db.ForeignKey("account.id"))
