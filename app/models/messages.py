import datetime
import enum
import logging

from sqlalchemy import TIMESTAMP
from sqlalchemy_utils.types.choice import ChoiceType

from app.extensions import db
from app.models.base import ModelBase
from app.utils.dates import get_date_as_string

logger = logging.getLogger("webapp.daos")


class MessageCategories(enum.Enum):
    ERROR = "error"
    WARNING = "warning"
    INFO = "info"
    SUCCESS = "success"


class Message(ModelBase):

    title = db.Column(db.String(255), nullable=True, default="")
    category = db.Column(
        ChoiceType(MessageCategories, impl=db.String(255)),
        nullable=False,
        default=MessageCategories.INFO,
    )
    body = db.Column(db.Text(), nullable=True, default="")
    active = db.Column(db.Boolean(), nullable=False, default=False)

    created_on = db.Column(db.DateTime, default=datetime.datetime.now)
    updated_on = db.Column(
        TIMESTAMP(),
        nullable=False,
        default=datetime.datetime.now,
        onupdate=datetime.datetime.now,
    )

    def to_dict(self) -> dict:
        """Return the values of the object as a dictionary

        Returns:
            dict: The object values
        """
        created_on_str = get_date_as_string(self.created_on)
        updated_on_str = get_date_as_string(self.updated_on)

        return {
            "id": self.id,
            "title": self.title,
            "category": self.category,
            "body": self.body,
            "active": self.active,
            "created_on": created_on_str,
            "updated_on": updated_on_str if self.updated_on else None,
        }
