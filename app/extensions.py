from celery import Celery
from flask_caching import Cache
from flask_jwt_extended import JWTManager
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
migrate = Migrate()
cache = Cache(config={"CACHE_TYPE": "null"})
jwt = JWTManager()
celery = Celery()
