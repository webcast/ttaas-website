import logging
import time

from flask import current_app
from sqlalchemy import or_

from app.daos.state_log import StateLogDAO
from app.daos.uploads import UploadDAO
from app.extensions import celery
from app.models.upload_states import CompletedType, NotificationMethods, TaskStates
from app.models.uploads import Upload
from app.services.email.upload_email_service import UploadEmailService

logger = logging.getLogger("job.email_notifications")


class EmailException(Exception):
    pass


@celery.task()
def send_upload_finished_emails():
    """Background task to send emails to the user when the transcription is completed.

    Returns:
        string: "OK" when finished
    """
    logger.info(f"Start emails task : {time.ctime()}")

    if not current_app.config.get("ENABLE_NOTIFICATION_EMAILS", False):
        logger.info(f"Emails are disabled. Skipping : {time.ctime()}")

    uploads = (
        Upload.query.filter_by(state=TaskStates.TRANSCRIPTION_FINISHED)
        .filter_by(notification_method=NotificationMethods.EMAIL)
        .all()
    )

    for upload in uploads:
        send_single_email_completed_notification.apply_async(
            args=[upload.id, CompletedType.TRANSCRIPTION.value]
        )

    uploads = (
        Upload.query.filter_by(state=TaskStates.TRANSLATION_FINISHED)
        .filter_by(notification_method=NotificationMethods.EMAIL)
        .all()
    )

    for upload in uploads:
        send_single_email_completed_notification.apply_async(
            args=[upload.id, CompletedType.TRANSLATION.value]
        )

    logger.info(f"End emails task : {time.ctime()}")

    return "OK"


@celery.task(
    autoretry_for=(Exception,), retry_backoff=10, max_retries=3, retry_jitter=True
)
def send_single_email_completed_notification(upload_id: int, completed_type: str):

    logger.info(f"Start email completed notification task : {time.ctime()}")

    result = None
    upload = Upload.query.get(upload_id)

    logger.debug(
        f"Sending email completed for upload {upload.id}: {upload.notification_email}"
    )
    email_service = UploadEmailService(logger=logger)
    result = email_service.send_upload_finished_notification(upload.id, completed_type)

    if result:
        UploadDAO.update_task_state(upload.id, TaskStates.USER_NOTIFIED)
        StateLogDAO.create(
            upload.id, f"User notified via email on {upload.notification_email}"
        )
    else:
        if (
            send_single_email_completed_notification.request.retries
            == send_single_email_completed_notification.max_retries
        ):
            logger.error(
                f"Email completed notification failed for upload {upload.id}: {upload.notification_email}"
            )
            UploadDAO.update_task_state(upload.id, TaskStates.COMPLETED_EMAIL_FAILED)
            StateLogDAO.create(
                upload.id, "Unable to notify user via email. Max retries reached."
            )
        raise Exception("Unable to send email completed notification")

    logger.info(f"End email completed task : {time.ctime()}")

    return "OK"


@celery.task(
    autoretry_for=(EmailException,),
    retry_backoff=10,
    max_retries=3,
    retry_jitter=True,
)
def send_single_caption_update_email(
    upload_id: int, language: str, timestamp: str
) -> str:
    """Task that sends an email to notify the user that a caption update has been made.

    Args:
        upload_id (int): ID of the upload to send the callback for

    Raises:
        EmailException: If could not be sent

    Returns:
        str: "OK" when finished
    """
    logger.info(f"Start URL caption update email task : {time.ctime()}")

    result = None
    upload = Upload.query.get(upload_id)

    logger.debug(f"Sending email for upload {upload.id}: {upload.notification_email}")
    email_service = UploadEmailService(logger=logger)
    result = email_service.send_captions_updated_notification(
        upload.id, language, timestamp
    )

    if result:
        logger.info(f"End email captions task : {time.ctime()}")
        return "OK"
    else:
        max_retries_reached = (
            send_single_caption_update_email.request.retries
            == send_single_caption_update_email.max_retries
        )
        if max_retries_reached:
            logger.error(
                f"""Email caption notification failed for upload {upload.id}
                (Result {result}): {upload.notification_email}"""
            )
        raise EmailException("Unable to send captions update notification email")


@celery.task()
def send_upload_error_emails():
    logger.info(f"Start URL error emails task : {time.ctime()}")

    if not current_app.config.get("ENABLE_NOTIFICATION_CALLBACKS", False):
        logger.info(f"Notification callbacks are disabled. Skipping : {time.ctime()}")
        return "OK"

    uploads = (
        Upload.query.filter(
            or_(
                Upload.state == TaskStates.ERROR_PREPARING_UPLOAD,
                Upload.state == TaskStates.ERROR_RUNNING_TRANSCRIPTIONS,
                Upload.state == TaskStates.ERROR_RUNNING_TRANSLATIONS,
                Upload.state == TaskStates.ERROR_SUBMITTING,
                Upload.state == TaskStates.ERROR_UPLOADING,
            )
        )
        .filter_by(notification_method=NotificationMethods.EMAIL)
        .all()
    )

    for upload in uploads:
        send_single_upload_error_email.apply_async(queue="callbacks", args=[upload.id])

    logger.info(f"End URL error callbacks task : {time.ctime()}")

    return "OK"


@celery.task(
    autoretry_for=(EmailException,),
    retry_backoff=10,
    max_retries=3,
    retry_jitter=True,
)
def send_single_upload_error_email(upload_id: int) -> str:
    logger.info(f"Start single email error task : {time.ctime()}")

    result = None
    upload = Upload.query.get(upload_id)

    logger.debug(
        f"Sending single email error for upload {upload.id} {upload.notification_email}"
    )

    email_service = UploadEmailService(logger=logger)
    result = email_service.send_upload_error_notification(upload.id)

    if result:
        logger.info(f"End email error notification task : {time.ctime()}")
        UploadDAO.update_task_state(upload.id, TaskStates.ERROR_NOTIFIED)
        StateLogDAO.create(
            upload.id,
            f"Error email: Error notified on {upload.notification_email}",
        )
        logger.info(f"End email error task : {time.ctime()}")

        return "OK"
    else:
        max_retries_reached = (
            send_single_upload_error_email.request.retries
            == send_single_upload_error_email.max_retries
        )
        if max_retries_reached:
            logger.error(
                f"Email error notification failed for pre upload {upload.id}: {upload.notification_email}"
            )
            UploadDAO.update_task_state(upload.id, TaskStates.ERROR_EMAIL_FAILED)
            StateLogDAO.create(
                upload.id,
                f"Error email: Unable to send email notification to {upload.notification_email}. Max retries reached",
            )
        raise EmailException("Unable to send error email notification")
