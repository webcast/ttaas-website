import datetime
import logging
import time

import pytz
from sqlalchemy import and_

from app.daos.pre_uploads import PreUploadDAO
from app.extensions import celery
from app.models.pre_uploads import PreUpload, PreUploadStates

logger = logging.getLogger("job.upload_check")


@celery.task()
def set_waiting_for_checksum_as_error():
    """Background task to set the PreUpload as ERROR if they are in WAITING_CHECKSUM
    state for more than 3 hours.

    Returns:
        string: "OK" when finished
    """
    logger.info(f"Start set preparing upload as error task : {time.ctime()}")
    upload_max_time = 180  # minutes
    uploads_since = datetime.datetime.now(
        pytz.timezone("Europe/Zurich")
    ) - datetime.timedelta(minutes=upload_max_time)
    uploads = PreUpload.query.filter(
        and_(
            PreUpload.state == PreUploadStates.WAITING_CHECKSUM,
            PreUpload.updated_on < uploads_since,
        ),
    ).all()

    for pre_upload in uploads:
        PreUploadDAO.set_pre_upload_error(pre_upload.media_id)

    logger.info(f"End set preparing upload as error task : {time.ctime()}")

    return "OK"
