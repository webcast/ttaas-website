import datetime
import json
import logging
import time

import pytz
from sqlalchemy import and_, or_

from app.daos.state_log import StateLogDAO
from app.daos.uploads import UploadDAO
from app.extensions import celery
from app.models.uploads import NotificationMethods, RunningStates, TaskStates, Upload
from app.services.mllp.api_client import ApiSpeechClientMLLP

logger = logging.getLogger("job.upload_check")


@celery.task()
def dummy_upload_task():
    logger.info(f"Start dummy_upload_task : {time.ctime()}")
    logger.info(f"End dummy_upload_task : {time.ctime()}")
    return "OK"


@celery.task()
def upload_status_check():
    """Background task to ingest the upload. Will retry if there is an error

    Args:
        upload_id (int): ID of the upload to ingest

    Returns:
        string: "OK" when finished
    """
    logger.info(f"Start status check task : {time.ctime()}")

    uploads = Upload.query.filter_by(state=TaskStates.RUNNING_TRANSCRIPTIONS).all()

    for upload in uploads:
        account = upload.account

        api_client = ApiSpeechClientMLLP(substitute_user=account.su_account)
        upload_data = api_client.get_upload_status(upload.ingest_id)
        json_upload_data = json.loads(upload_data)
        logger.debug(json_upload_data)
        logger.debug(
            f"Upload with Ingest ID {upload.ingest_id} status: {str(json_upload_data['status_code'])}"
        )
        try:
            UploadDAO.update_running_state(
                upload.id, str(json_upload_data["status_code"])
            )
        except KeyError as error:
            logger.warning(
                f"Upload with Ingest ID {upload.ingest_id} not found: {error}"
            )
            UploadDAO.update_running_state(upload.id, RunningStates.ERROR.value)

    logger.info(f"End status check task : {time.ctime()}")

    return "OK"


@celery.task()
def set_uploads_completed():
    """Background task to set the uploads as completed.

    Returns:
        string: "OK" when finished
    """
    logger.info(f"Start set completed task : {time.ctime()}")

    uploads = Upload.query.filter(
        or_(
            Upload.state == TaskStates.USER_NOTIFIED,
            and_(  # If notification is set to None and transcription is finished
                Upload.state == TaskStates.TRANSCRIPTION_FINISHED,
                Upload.running_state == RunningStates.COMPLETED,
                Upload.notification_method == NotificationMethods.NONE,
            ),
        )
    ).all()

    for upload in uploads:
        UploadDAO.update_task_state(upload.id, TaskStates.COMPLETED)
        StateLogDAO.create(upload.id, "Processing completed")
    logger.info(f"End set completed task : {time.ctime()}")

    return "OK"


@celery.task()
def set_preparing_upload_as_error():
    """Background task to set the uploads as ERROR if they are in PREPARING state for more
    than 2 hour.

    Returns:
        string: "OK" when finished
    """
    logger.info(f"Start set preparing upload as error task : {time.ctime()}")
    upload_max_time = 120  # minutes
    uploads_since = datetime.datetime.now(
        pytz.timezone("Europe/Zurich")
    ) - datetime.timedelta(minutes=upload_max_time)
    uploads = Upload.query.filter(
        and_(
            Upload.state == TaskStates.PREPARING_UPLOAD,
            Upload.updated_on < uploads_since,
        ),
    ).all()

    for upload in uploads:
        UploadDAO.update_task_state(upload.id, TaskStates.ERROR_PREPARING_UPLOAD)
        StateLogDAO.create(upload.id, "Error preparing upload")

    logger.info(f"End set preparing upload as error task : {time.ctime()}")

    return "OK"


@celery.task()
def set_error_running_limit():
    """Background task to set the uploads as ERROR if they are in RUNNING_TRANSCRIPTIONS for more than 72h

    Returns:
        string: "OK" when finished
    """
    logger.info(f"Start set_error_running_limit task : {time.ctime()}")
    upload_max_time = 60 * 72  # minutes
    uploads_since = datetime.datetime.now(
        pytz.timezone("Europe/Zurich")
    ) - datetime.timedelta(minutes=upload_max_time)
    uploads = Upload.query.filter(
        and_(
            Upload.state == TaskStates.RUNNING_TRANSCRIPTIONS,
            Upload.updated_on < uploads_since,
        ),
    ).all()

    for upload in uploads:
        UploadDAO.update_task_state(upload.id, TaskStates.ERROR_RUNNING_TRANSCRIPTIONS)
        StateLogDAO.create(upload.id, "Error running transcription. Max time reached")

    uploads = Upload.query.filter(
        and_(
            Upload.state == TaskStates.RUNNING_TRANSLATIONS,
            Upload.updated_on < uploads_since,
        ),
    ).all()

    for upload in uploads:
        UploadDAO.update_task_state(upload.id, TaskStates.ERROR_RUNNING_TRANSLATIONS)
        StateLogDAO.create(upload.id, "Error running translation. Max time reached")
    logger.info(f"End set_error_running_limit task : {time.ctime()}")

    return "OK"
