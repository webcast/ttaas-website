import logging
import time

from flask import current_app

from app.daos.pre_uploads import PreUploadDAO
from app.extensions import celery
from app.models.pre_uploads import PreUpload, PreUploadStates
from app.models.upload_states import NotificationMethods
from app.services.callback.url_callback import UrlCallbackPost
from app.services.email.upload_email_service import UploadEmailService
from app.tasks.url_notifications import CallbackTypes

logger = logging.getLogger("job.pre_upload_notifications")


class PreUploadCallbackException(Exception):
    pass


class PreUploadEmailException(Exception):
    pass


@celery.task(
    autoretry_for=(PreUploadCallbackException,),
    retry_backoff=10,
    max_retries=3,
    retry_jitter=True,
)
def send_single_pre_upload_error_callback(upload_id: int) -> str:
    """Background task that makes a callback to the URL specified for the upload.

    Args:
        upload_id (int): ID of the upload to send the callback for

    Raises:
        CallbackException: If callback response is not 200

    Returns:
        str: "OK" when finished
    """
    result = None
    upload = PreUpload.query.get(upload_id)

    if upload.account.disable_callbacks:
        logger.info(
            f"Notification callbacks are disabled on account {upload.account.su_account}. Skipping"
        )
        return "OK"

    logger.debug(
        f"Sending URL error callback for pre upload {upload.id}: {upload.notification_url}"
    )
    extra_data = {"media_id": upload.media_id}
    service = UrlCallbackPost(logger=logger)
    result = service.make_callback_request(
        upload.notification_url,
        callback_type=CallbackTypes.ERROR.value,
        extra_data=extra_data,
    )

    if result and result.status_code == 200:
        PreUploadDAO.set_pre_upload_error_notified(upload.media_id)
    else:
        max_retries_reached = (
            send_single_pre_upload_error_callback.request.retries
            == send_single_pre_upload_error_callback.max_retries
        )
        if max_retries_reached:
            logger.error(
                (
                    f"Error callback: Notification failed for upload {upload.id} "
                    f"(status code {result.status_code}): {upload.notification_url}"
                )
            )
            PreUploadDAO.set_pre_upload_error_callback_failed(upload.media_id)
        raise PreUploadCallbackException(
            "Unable to send error callback notification for PreUpload"
        )

    return "OK"


@celery.task()
def send_pre_upload_error_callbacks():
    """Background task to send url callbacks to the user when there is an error with the upload or transcription.

    Returns:
        string: "OK" when finished
    """
    logger.info(f"Start URL error pre uploads emails task : {time.ctime()}")

    if not current_app.config.get("ENABLE_NOTIFICATION_CALLBACKS", False):
        logger.info(f"Notification callbacks are disabled. Skipping : {time.ctime()}")
        return "OK"

    uploads = (
        PreUpload.query.filter(
            PreUpload.state == PreUploadStates.ERROR,
        )
        .filter_by(notification_method=NotificationMethods.CALLBACK)
        .all()
    )

    for upload in uploads:
        send_single_pre_upload_error_callback.apply_async(
            queue="callbacks", args=[upload.id]
        )

    logger.info(f"End URL error pre upload callbacks task : {time.ctime()}")

    return "OK"


@celery.task()
def send_pre_upload_error_emails():
    """Background task to send url callbacks to the user when there is an error with the upload or transcription.

    Returns:
        string: "OK" when finished
    """
    logger.info(f"Start email error emails task : {time.ctime()}")

    uploads = (
        PreUpload.query.filter(
            PreUpload.state == PreUploadStates.ERROR,
        )
        .filter_by(notification_method=NotificationMethods.EMAIL)
        .all()
    )

    for upload in uploads:
        send_single_pre_upload_error_email.apply_async(
            queue="callbacks", args=[upload.id]
        )

    logger.info(f"End EMAIL error callbacks task : {time.ctime()}")

    return "OK"


@celery.task(
    autoretry_for=(PreUploadEmailException,),
    retry_backoff=10,
    max_retries=3,
    retry_jitter=True,
)
def send_single_pre_upload_error_email(upload_id: int) -> str:
    """Background task that makes a callback to the URL specified for the upload.

    Args:
        upload_id (int): ID of the upload to send the callback for

    Raises:
        CallbackException: If callback response is not 200

    Returns:
        str: "OK" when finished
    """
    logger.info(f"Start URL callback task : {time.ctime()}")

    result = None
    pre_upload = PreUpload.query.get(upload_id)

    logger.debug(
        f"Sending URL error email for pre upload {pre_upload.id}: {pre_upload.notification_email}"
    )

    email_service = UploadEmailService(logger=logger)
    result = email_service.send_pre_upload_error_notification(pre_upload.id)

    if result:
        PreUploadDAO.set_pre_upload_error_notified(pre_upload.media_id)
    else:
        max_retries_reached = (
            send_single_pre_upload_error_email.request.retries
            == send_single_pre_upload_error_email.max_retries
        )
        if max_retries_reached:
            logger.error(
                f"Email error notification failed for pre upload {pre_upload.id}: {pre_upload.notification_email}"
            )
            PreUploadDAO.set_pre_upload_error_email_failed(pre_upload.media_id)
        raise PreUploadEmailException("Unable to send error callback notification")

    logger.info(f"End pre upload error email task : {time.ctime()}")

    return "OK"


@celery.task()
def send_pre_upload_already_exist_emails():
    """Background task to send url callbacks to the user when the media already exists.

    Returns:
        string: "OK" when finished
    """
    logger.info(f"Start email already_exist emails task : {time.ctime()}")

    uploads = (
        PreUpload.query.filter(
            PreUpload.state == PreUploadStates.ALREADY_EXISTS,
        )
        .filter_by(notification_method=NotificationMethods.EMAIL)
        .all()
    )

    for upload in uploads:
        send_single_pre_upload_already_exists_email.apply_async(
            queue="callbacks", args=[upload.id]
        )

    logger.info(f"End EMAIL already_exist callbacks task : {time.ctime()}")

    return "OK"


@celery.task(
    autoretry_for=(PreUploadEmailException,),
    retry_backoff=10,
    max_retries=3,
    retry_jitter=True,
)
def send_single_pre_upload_already_exists_email(pre_upload_id: int) -> str:
    """Background task that makes a callback to the URL specified for the upload.

    Args:
        pre_upload_id (int): ID of the pre_upload to send the callback for

    Returns:
        str: "OK" when finished
    """
    logger.info(f"Start URL callback task : {time.ctime()}")

    result = None
    pre_upload = PreUpload.query.get(pre_upload_id)

    logger.debug(
        f"Sending URL error email for pre upload {pre_upload.id}: {pre_upload.notification_email}"
    )

    email_service = UploadEmailService(logger=logger)
    result = email_service.send_pre_upload_already_exists_notification(pre_upload.id)

    if result:
        PreUploadDAO.set_pre_upload_error_notified(pre_upload.media_id)
    else:
        max_retries_reached = (
            send_single_pre_upload_already_exists_email.request.retries
            == send_single_pre_upload_already_exists_email.max_retries
        )
        if max_retries_reached:
            logger.error(
                f"Email error notification failed for pre upload {pre_upload.id}: {pre_upload.notification_email}"
            )
            PreUploadDAO.set_pre_upload_already_exists_notified(pre_upload.media_id)
        raise PreUploadEmailException("Unable to send error callback notification")

    logger.info(f"End pre upload error email task : {time.ctime()}")

    return "OK"
