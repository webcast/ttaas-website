import datetime
import logging
import os
import time

from flask import current_app

from app.extensions import celery
from app.models.uploads import Upload

logger = logging.getLogger("job.files_handle")


class FilesHandleException(Exception):
    pass


@celery.task(
    autoretry_for=(FilesHandleException,),
    retry_backoff=5,
    max_retries=3,
    retry_jitter=True,
)
def remove_upload_files(upload_id: int):
    """Background task to remove the already uploaded files. Will retry if there is an error

    Args:
        upload_id (int): ID of the upload to ingest

    Returns:
        string: "OK" when finished
    """
    logger.info(f"Start remove files task : {time.ctime()}")

    upload = Upload.query.get(upload_id)
    try:
        os.remove(upload.media_file_path)
    except FileNotFoundError:
        logger.warning(
            f"Unable to remove file: {upload.media_file_path} not found at {time.ctime()}"
        )

    logger.info(f"End remove_files task : {time.ctime()}")

    return "OK"


@celery.task()
def remove_old_files():
    """Background task to remove the already uploaded files. Will retry if there is an error

    Returns:
        string: "OK" when finished
    """
    logger.info(f"Start remove files task : {time.ctime()}")

    current_time = datetime.datetime.utcnow()
    several_days_ago = current_time - datetime.timedelta(
        days=current_app.config["UPLOAD_EXPIRATION_DAYS"]
    )

    old_uploads = Upload.query.filter(Upload.created_on > several_days_ago).all()

    for upload in old_uploads:
        try:
            try:
                os.remove(upload.media_file_path)
            except FileNotFoundError:
                logger.warning(
                    f"Unable to remove file: {upload.media_file_path} not found at {time.ctime()}"
                )
        except Exception as e:
            logger.warning(f"Unable to delete the files for upload {upload.id} ({e})")

    logger.info(f"End remove_files task : {time.ctime()}")

    return "OK"
