import logging
import time
from typing import Optional

from flask import current_app

from app.extensions import celery
from app.models.accounts import Account
from app.services.cern_search.cern_search_service import CernSearchService

logger = logging.getLogger("job.transcription")


@celery.task()
def update_cern_search_documents(
    media_id: str,
    language: str,
    su_account: str,
    is_public: bool = False,
    account_id: Optional[int] = None,
):
    """Background task to update the documents in CERN Search

    Args:
        media_id (str): ID of the upload to ingest
        language (str): Language code of the document
        su_account (str): Account used for the transcription
        is_public (bool): Whether or not the transcriptions are public

    Returns:
        string: "OK" when finished
    """
    logger.info(
        f"Start update cern search document task for {media_id} : {time.ctime()}"
    )
    account = Account.query.get(account_id)
    if (
        (
            current_app.config.get("CERNSEARCH_ENABLE", False)
            and account.role == current_app.config["WEBLECTURES_SERVICE_ROLE"]
            and is_public
        )
        or current_app.config.get("CERNSEARCH_ENABLE", False)
        and account.always_index
    ):
        cern_search_service = CernSearchService(logger=logger)
        logger.info(
            (
                f"Updating search document for {media_id} : {time.ctime()}. "
                f"is_public: {is_public} always_index: {account.always_index}"
            )
        )
        cern_search_service.handle_caption_edit(media_id, language, su_account)
    else:
        logger.info(
            (
                f"Not updating search document for {media_id} "
                f"(not public or doesn't belong to weblecture service media) : {time.ctime()}"
            )
        )

    logger.info(f"End ingest task for {media_id} : {time.ctime()}")

    return "OK"
