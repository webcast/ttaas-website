import json
import logging
import time

from app.daos.accounts import AccountDAO
from app.daos.pre_uploads import PreUploadDAO
from app.daos.state_log import StateLogDAO
from app.daos.uploads import UploadDAO, UploadDAOException
from app.extensions import celery
from app.models.upload_states import NotificationMethods, TaskStates
from app.models.uploads import MetadataRcodes
from app.services.ffmpeg.ffmpeg_tools import trim_video_subprocess
from app.services.files.file_checksum import get_file_checksum
from app.services.mllp.api_client import ApiSpeechClientMLLP
from app.tasks.files_handle import remove_upload_files
from app.tasks.url_notifications import send_single_upload_already_exists_callback

logger = logging.getLogger("job.ingest")


class IngestException(Exception):
    pass


class CriticalIngestException(Exception):
    pass


def exists_in_asr(media_id: str, account_id: int) -> bool:
    logger.info(f"Checking if {media_id} exists in ASR for account {account_id}")
    # Check the media in the MLLP system to verify if it exists
    account = AccountDAO.get_account_by_id(account_id)
    api_client = ApiSpeechClientMLLP(substitute_user=account.su_account)
    file = api_client.get_media_metadata(media_id)
    file_json = json.loads(file)
    if file_json and file_json["rcode"] == MetadataRcodes.EXISTS.value:
        return True
    return False


def has_valid_split_values(start_time: str, end_time: str) -> bool:
    """Check if the camera end time is set

    Returns:
        boolean: If the split values for the camera are set
    """
    if (
        end_time is not None
        and end_time != ""
        and end_time != "00:00:00"
        and start_time is not None
        and start_time != ""
    ):
        return True
    return False


@celery.task(
    autoretry_for=(IngestException,),
    retry_backoff=5,
    max_retries=3,
    retry_jitter=True,
    queue="media",
)
def ingest_task(upload_data: dict, media_id=None):
    """Background task to ingest the upload. Will retry if there is an error

    Args:
        upload_id (int): ID of the upload to ingest

    Returns:
        string: "OK" when finished
    """
    logger.info(f"Start ingest task for {media_id} : {time.ctime()}")

    if media_id:

        # Cut the video
        if has_valid_split_values(upload_data["start_time"], upload_data["end_time"]):
            trim_video_subprocess(
                upload_data["media_file_path"],
                upload_data["start_time"],
                upload_data["end_time"],
            )

        PreUploadDAO.set_pre_upload_waiting_for_checksum(media_id)
        logger.info(f"Checking file checksum for {media_id} : {time.ctime()}")
    else:
        logger.info(f"Checking file checksum (no media_id yet) : {time.ctime()}")
    try:
        checksum = get_file_checksum(upload_data["media_file_path"])
        logger.info(f"Checking file checksum -> OK : {time.ctime()}")
    except FileNotFoundError as error:
        logger.warning(
            (
                f"Unable to get the checksum for {media_id} of "
                f"file {upload_data['media_file_path']}. File not found. ({error})"
            )
        )
        return None

    existing_upload = UploadDAO.get_upload_by_account_id_and_checksum(
        upload_data["account_id"], checksum
    )

    if existing_upload:
        already_in_asr = exists_in_asr(
            existing_upload.media_id, upload_data["account_id"]
        )
        if already_in_asr:
            logger.info(
                f"""Warning uploading media package {media_id}.
                An existing upload with ID {existing_upload.media_id}.
                A media package with the same checksum already exist in ASR."""
            )
            # If the media was already uploaded, we change its status and register it
            # to keep track of how many duplicates were sent for a given media
            PreUploadDAO.set_pre_upload_already_exists(media_id, existing_upload)
            # Send a notification with the existing upload status
            logger.debug(
                f"Notification method for {media_id}: {upload_data['notification_method']}"
            )
            if upload_data["notification_method"] == NotificationMethods.CALLBACK.value:
                send_single_upload_already_exists_callback.apply_async(
                    queue="callbacks",
                    args=[
                        upload_data["notification_url"],
                        media_id,
                        existing_upload.media_id,
                        existing_upload.state.value,
                        existing_upload.id,
                    ],
                )
            logger.info(
                f"Cleaning media package for {media_id} on {existing_upload.media_id} files."
            )
            remove_upload_files.apply_async(queue="media", args=[existing_upload.id])
            return "OK"
        # There is an upload on TTAAS website but not on MLLP.
        # This is inconsistent and we should delete the Upload
        logger.info(
            f"Deleting upload: {existing_upload.media_id}. It is present in TTAAS but not on ASR."
        )
        UploadDAO.delete_upload_by_id(existing_upload.id, hard=True)

    try:
        upload = UploadDAO.create(upload_data, media_id=media_id)

        account = upload.account
        UploadDAO.update_checksum(upload.id, checksum)
        StateLogDAO.create(upload.id, "Updating checksum")
        UploadDAO.update_task_state(upload.id, TaskStates.PREPARING_UPLOAD)
        StateLogDAO.create(upload.id, "Preparing upload")
        logger.info(
            f"Creating and sending media package {media_id} to TLP : {time.ctime()}"
        )

        # Delete the pre-upload when the upload is fully created
        if media_id and upload:
            PreUploadDAO.delete_pre_upload(media_id)

        api_client = ApiSpeechClientMLLP(substitute_user=account.su_account)

        ingest_id = api_client.create_and_upload_media_package(
            upload.title,
            upload.language.value,
            upload.media_id,
            upload.media_file_path,
            upload.nonce,
        )

        if ingest_id:
            logger.info(
                f"Creating and sending media package {media_id} to TLP -> OK : {time.ctime()}"
            )
            UploadDAO.update_ingest_id(
                upload.id, ingest_id, TaskStates.WAITING_TRANSCRIPTIONS
            )
            StateLogDAO.create(upload.id, "Waiting for transcriptions to run")
        else:
            logger.warning(
                f"Creating and sending media package {media_id} to TLP -> ERROR : {time.ctime()}"
            )

            UploadDAO.update_task_state(upload.id, TaskStates.ERROR_UPLOADING)
            StateLogDAO.create(upload.id, "Upload uploading the media package")
            logger.error(f"Error uploading media package: {upload.media_id}")
            raise CriticalIngestException("Error uploading media package")
    except UploadDAOException as error:
        PreUploadDAO.set_pre_upload_error(media_id)
        logger.error(f"Error uploading media package: {media_id} ({error})")
        raise CriticalIngestException("Error uploading media package")

    logger.info(f"End ingest task for {media_id} : {time.ctime()}")

    return "OK"
