import enum
import logging
import time
from datetime import datetime

from flask import current_app
from sqlalchemy import or_

from app.daos.state_log import StateLogDAO
from app.daos.uploads import UploadDAO
from app.extensions import celery
from app.models.uploads import NotificationMethods, TaskStates, Upload
from app.services.callback.url_callback import UrlCallbackPost

logger = logging.getLogger("job.url_notifications")


class CallbackException(Exception):
    pass


class CallbackTypes(enum.Enum):
    FINISHED = "FINISHED"
    ERROR = "ERROR"
    ALREADY_EXISTS = "ALREADY_EXISTS"
    CAPTION_UPDATED = "CAPTION_UPDATED"


@celery.task()
def send_upload_finished_callbacks():
    """Background task to send url callbacks to the user when the transcription is completed.

    Returns:
        string: "OK" when finished
    """
    logger.info(f"Start URL callbacks task : {time.ctime()}")

    if not current_app.config.get("ENABLE_NOTIFICATION_CALLBACKS", False):
        logger.info(f"Notification callbacks are disabled. Skipping : {time.ctime()}")
        return "OK"

    uploads = (
        Upload.query.filter_by(state=TaskStates.TRANSCRIPTION_FINISHED)
        .filter_by(notification_method=NotificationMethods.CALLBACK)
        .all()
    )

    for upload in uploads:
        send_single_upload_finished_callback.apply_async(
            queue="callbacks", args=[upload.id]
        )

    uploads = (
        Upload.query.filter_by(state=TaskStates.TRANSLATION_FINISHED)
        .filter_by(notification_method=NotificationMethods.CALLBACK)
        .all()
    )

    for upload in uploads:
        send_single_upload_finished_callback.apply_async(
            queue="callbacks", args=[upload.id]
        )

    logger.info(f"End URL callbacks task : {time.ctime()}")

    return "OK"


@celery.task(
    autoretry_for=(CallbackException,),
    retry_backoff=10,
    max_retries=3,
    retry_jitter=True,
)
def send_single_upload_finished_callback(upload_id: int) -> str:
    """Background task that makes a callback to the URL specified for the upload.

    Args:
        upload_id (int): ID of the upload to send the callback for

    Raises:
        CallbackException: If callback response is not 200

    Returns:
        str: "OK" when finished
    """
    logger.info(f"Start URL callback task : {time.ctime()}")

    result = None
    upload = Upload.query.get(upload_id)

    if upload.account.disable_callbacks:
        logger.info(
            f"Notification callbacks are disabled on account {upload.account.su_account}. Skipping"
        )
        return "OK"

    logger.debug(
        (
            f"Sending URL callback for upload {upload.id}: "
            f"{upload.notification_url}. Retry ({send_single_upload_finished_callback.request.retries})"
        )
    )
    extra_data = {"mediaId": upload.media_id}
    service = UrlCallbackPost(logger=logger)
    try:
        result = service.make_callback_request(
            upload.notification_url,
            callback_type=CallbackTypes.FINISHED.value,
            extra_data=extra_data,
        )
    except (ConnectionError, OSError) as error:
        logger.warning(
            f"Unable to send the callback of {upload.media_id} to {upload.notification_url} ({error})"
        )

    if result and result.status_code == 200:
        UploadDAO.update_task_state(upload.id, TaskStates.USER_NOTIFIED)
        StateLogDAO.create(
            upload.id,
            f"Finished callback: User notified via callback on {upload.notification_url}",
        )
    else:
        max_retries_reached = (
            send_single_upload_finished_callback.request.retries
            == send_single_upload_finished_callback.max_retries
        )
        if max_retries_reached:
            logger.error(
                f"Finished callback: Notification failed for upload {upload.id}: {upload.notification_url}"
            )
            UploadDAO.update_task_state(upload.id, TaskStates.COMPLETED_CALLBACK_FAILED)
            StateLogDAO.create(
                upload.id,
                (
                    f"Finished callback: Unable to notify user via callback "
                    f"on {upload.notification_url}. Max retries reached"
                ),
            )
            return "ERROR"
        raise CallbackException("Unable to send callback notification")

    logger.info(f"End callback task : {time.ctime()}")

    return "OK"


@celery.task()
def send_upload_error_callbacks():
    """Background task to send url callbacks to the user when there is an error with the upload or transcription.

    Returns:
        string: "OK" when finished
    """
    logger.info(f"Start URL error callbacks task : {time.ctime()}")

    if not current_app.config.get("ENABLE_NOTIFICATION_CALLBACKS", False):
        logger.info(f"Notification callbacks are disabled. Skipping : {time.ctime()}")
        return "OK"

    uploads = (
        Upload.query.filter(
            or_(
                Upload.state == TaskStates.ERROR_PREPARING_UPLOAD,
                Upload.state == TaskStates.ERROR_RUNNING_TRANSCRIPTIONS,
                Upload.state == TaskStates.ERROR_RUNNING_TRANSLATIONS,
                Upload.state == TaskStates.ERROR_SUBMITTING,
                Upload.state == TaskStates.ERROR_UPLOADING,
            )
        )
        .filter_by(notification_method=NotificationMethods.CALLBACK)
        .all()
    )

    for upload in uploads:
        send_single_upload_error_callback.apply_async(
            queue="callbacks", args=[upload.id]
        )

    logger.info(f"End URL error callbacks task : {time.ctime()}")

    return "OK"


@celery.task(
    autoretry_for=(CallbackException,),
    retry_backoff=10,
    max_retries=3,
    retry_jitter=True,
)
def send_single_upload_error_callback(upload_id: int) -> str:
    """Background task that makes a callback to the URL specified for the upload.

    Args:
        upload_id (int): ID of the upload to send the callback for

    Raises:
        CallbackException: If callback response is not 200

    Returns:
        str: "OK" when finished
    """
    logger.info(f"Start URL callback task : {time.ctime()}")

    result = None
    upload = Upload.query.get(upload_id)

    if upload.account.disable_callbacks:
        logger.info(
            f"Notification callbacks are disabled on account {upload.account.su_account}. Skipping"
        )
        return "OK"

    logger.debug(
        f"Sending URL error callback for upload {upload.id}: {upload.notification_url}"
    )
    extra_data = {"mediaId": upload.media_id}
    service = UrlCallbackPost(logger=logger)
    result = service.make_callback_request(
        upload.notification_url,
        callback_type=CallbackTypes.ERROR.value,
        extra_data=extra_data,
    )

    if result and result.status_code == 200:
        UploadDAO.update_task_state(upload.id, TaskStates.ERROR_NOTIFIED)
        StateLogDAO.create(
            upload.id,
            f"Error callback: Error notified on {upload.notification_url}",
        )
    else:
        max_retries_reached = (
            send_single_upload_error_callback.request.retries
            == send_single_upload_error_callback.max_retries
        )
        if max_retries_reached:
            logger.error(
                (
                    f"Error callback: Notification failed for upload {upload.id} "
                    f"(status code {result.status_code}): {upload.notification_url}"
                )
            )
            UploadDAO.update_task_state(upload.id, TaskStates.ERROR_CALLBACK_FAILED)
            StateLogDAO.create(
                upload.id,
                f"Error callback: Unable to send notification to {upload.notification_url}. Max retries reached",
            )
        raise CallbackException("Unable to send error callback notification")

    logger.info(f"End callback task : {time.ctime()}")

    return "OK"


@celery.task(
    autoretry_for=(CallbackException,),
    retry_backoff=10,
    max_retries=15,
    retry_jitter=True,
)
def send_single_caption_callback(upload_id: int, language: str, timestamp: str) -> str:
    """Background task that makes a callback to the URL to notify a caption has changed.

    Args:
        upload_id (int): ID of the upload to send the callback for

    Raises:
        CallbackException: If callback response is not 200

    Returns:
        str: "OK" when finished
    """
    logger.info(f"Start URL callback task : {time.ctime()}")

    result = None
    upload = Upload.query.get(upload_id)

    if upload.account.disable_callbacks:
        logger.info(
            f"Notification callbacks are disabled on account {upload.account.su_account}. Skipping"
        )
        return "OK"

    logger.debug(
        f"Sending Single caption URL callback for upload {upload.id}: {upload.notification_url}"
    )
    # Print the timestamp type
    logger.debug(f"Caption timestamp type: {type(timestamp)}")
    # If the timestamp is a datetime, convert it to string
    if isinstance(timestamp, datetime):
        timestamp = timestamp.strftime("%Y-%m-%d %H:%M:%S")
    extra_data = {
        "mediaId": upload.media_id,
        "language": language,
        "timestamp": timestamp,
    }
    service = UrlCallbackPost(logger=logger)
    result = service.make_callback_request(
        upload.notification_url,
        callback_type=CallbackTypes.CAPTION_UPDATED.value,
        extra_data=extra_data,
    )

    if result and result.status_code == 200:
        logger.info(f"End callback task : {time.ctime()}")
        UploadDAO.update_task_state(upload.id, TaskStates.USER_NOTIFIED)
        StateLogDAO.create(
            upload.id,
            f"Captions updated callback: User notified on {upload.notification_url}",
        )
        return "OK"
    else:
        max_retries_reached = (
            send_single_caption_callback.request.retries
            == send_single_caption_callback.max_retries
        )
        if max_retries_reached:
            logger.error(
                f"Captions updated callback:  Notification failed for upload {upload.id}: {upload.notification_url}"
            )
            UploadDAO.update_task_state(upload.id, TaskStates.COMPLETED_CALLBACK_FAILED)
            StateLogDAO.create(
                upload.id,
                (
                    "Captions updated callback: Unable to notify user "
                    f"on {upload.notification_url}. Max retries reached"
                ),
            )
        raise CallbackException("Unable to send callback notification")


@celery.task(
    autoretry_for=(CallbackException,),
    retry_backoff=10,
    max_retries=3,
    retry_jitter=True,
    queue="callbacks",
)
def send_single_upload_already_exists_callback(
    notification_url: str,
    media_id: str,
    existing_media_id: str,
    state: str,
    upload_id: int,
) -> str:
    """Sends a notification to the notification_url to inform that an ingest already exists

    Args:
        notification_url (str): The notification URL that will be used for the callback
        media_id (str): The media ID of the involved Upload
        existing_media_id (str): The media ID of the already existing upload
        state (str): The state of the given (duplicated) Upload

    Raises:
        CallbackException: If callback response is not 200

    Returns:
        str: "OK" when finished
    """
    upload = Upload.query.get(upload_id)
    if upload.account.disable_callbacks:
        logger.info(
            f"Notification callbacks are disabled on account {upload.account.su_account}. Skipping already-exists"
        )
        return "OK"

    result = None
    logger.debug(
        f"Sending already-exists URL callback for {media_id} to url {notification_url}"
    )
    extra_data = {
        "mediaId": media_id,
        "existingMediaId": existing_media_id,
        "state": state,
    }
    service = UrlCallbackPost(logger=logger)
    result = service.make_callback_request(
        notification_url,
        callback_type=CallbackTypes.ALREADY_EXISTS.value,
        extra_data=extra_data,
    )

    if not result or result.status_code != 200:
        max_retries_reached = (
            send_single_upload_already_exists_callback.request.retries
            == send_single_upload_already_exists_callback.max_retries
        )
        if max_retries_reached:
            logger.error(
                f"Callback already-exists notification failed for media_id {media_id} "
                f"(status code {result.status_code}): {notification_url}"
            )
        raise CallbackException("Unable to send callback notification")

    logger.info(f"End already-exists callback task for {media_id} : {time.ctime()}")

    return "OK"
