import logging
from typing import Optional

from app.extensions import db
from app.models.accounts import Account
from app.models.pre_uploads import PreUpload, PreUploadStates
from app.models.upload_states import NotificationMethods

logger = logging.getLogger("webapp.daos")


class PreUploadDAOException(Exception):
    pass


class PreUploadDAO:
    @staticmethod
    def create_pre_upload(
        account_id,
        username,
        media_id,
        title,
        notification_method: Optional[str] = NotificationMethods.NONE.value,
        notification_email: Optional[str] = "",
        notification_url: Optional[str] = "",
    ) -> PreUpload:
        if None in [account_id, username, media_id, title]:
            raise PreUploadDAOException(
                f"Missing parameters during the creation of a PreUpload with media_id {media_id}"
            )

        # Create the temporary upload
        pre_upload = PreUpload(
            account_id=account_id,
            media_id=media_id,
            username=username,
            title=title,
            notification_method=notification_method,
            notification_email=notification_email,
            notification_url=notification_url,
        )
        logger.debug(f"PreUpload created: {pre_upload}")

        db.session.add(pre_upload)
        account = Account.query.get(account_id)
        account.pre_uploads.append(pre_upload)
        db.session.commit()

        return pre_upload

    @staticmethod
    def get_pre_upload_by_media_id_for_account(
        media_id: str, account_id: int
    ) -> PreUpload:
        upload = PreUpload.query.filter_by(
            media_id=media_id, account_id=account_id
        ).one_or_none()
        return upload

    @staticmethod
    def delete_pre_upload(media_id):
        logger.info(f"Deleting pre-upload {media_id}")
        pre_upload = PreUpload.query.filter_by(media_id=media_id).one()
        db.session.delete(pre_upload)

    @staticmethod
    def set_pre_upload_waiting_for_checksum(media_id):
        logger.info(f"Setting pre-upload {media_id} to waiting for checksum")
        pre_upload = PreUpload.query.filter_by(media_id=media_id).one()
        pre_upload.state = PreUploadStates.WAITING_CHECKSUM
        db.session.commit()

    @staticmethod
    def set_pre_upload_already_exists(media_id, existing_upload):
        logger.info(f"Upload {media_id} already exists in ASR")
        pre_upload = PreUpload.query.filter_by(media_id=media_id).one()
        pre_upload.state = PreUploadStates.ALREADY_EXISTS
        pre_upload.existing_media_id = existing_upload.media_id
        db.session.commit()

    @staticmethod
    def set_pre_upload_error(media_id):
        logger.info(f"Setting pre-upload {media_id} to ERROR")
        pre_upload = PreUpload.query.filter_by(media_id=media_id).one()
        pre_upload.state = PreUploadStates.ERROR
        db.session.commit()

    @staticmethod
    def set_pre_upload_error_notified(media_id):
        logger.info(f"Setting pre-upload {media_id} to ERROR_NOTIFIED")
        pre_upload = PreUpload.query.filter_by(media_id=media_id).one()
        pre_upload.state = PreUploadStates.ERROR_NOTIFIED
        db.session.commit()

    @staticmethod
    def set_pre_upload_error_callback_failed(media_id):
        logger.info(f"Setting pre-upload {media_id} to ERROR_CALLBACK_FAILED")
        pre_upload = PreUpload.query.filter_by(media_id=media_id).one()
        pre_upload.state = PreUploadStates.ERROR_CALLBACK_FAILED
        db.session.commit()

    @staticmethod
    def set_pre_upload_error_email_failed(media_id):
        logger.info(f"Setting pre-upload {media_id} to ERROR_EMAIL_FAILED")
        pre_upload = PreUpload.query.filter_by(media_id=media_id).one()
        pre_upload.state = PreUploadStates.ERROR_EMAIL_FAILED
        db.session.commit()

    @staticmethod
    def set_pre_upload_already_exists_notified(media_id):
        logger.info(f"Setting pre-upload {media_id} to ALREADY_EXISTS_NOTIFIED")
        pre_upload = PreUpload.query.filter_by(media_id=media_id).one()
        pre_upload.state = PreUploadStates.ALREADY_EXISTS_NOTIFIED
        db.session.commit()
