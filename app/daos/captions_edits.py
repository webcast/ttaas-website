import logging

from sqlalchemy.orm.exc import NoResultFound

from app.extensions import db
from app.models.captions_edits import CaptionEdit, SupervisionStatuses
from app.models.uploads import Upload

logger = logging.getLogger("webapp.daos")


class CaptionEditDAOException(Exception):
    pass


class CaptionEditDAO:
    @staticmethod
    def get_by_id(edit_id: str) -> CaptionEdit:
        element = CaptionEdit.query.get(edit_id)
        return element

    @staticmethod
    def get_by_media_id(media_id: str) -> CaptionEdit:
        element = (
            CaptionEdit.query.filter_by(media_id=media_id)
            .order_by(CaptionEdit.updated_on.desc())  # pylint: disable=no-member
            .first()
        )
        return element

    @staticmethod
    def get_language_last_edit(media_id: str, language: str) -> CaptionEdit:
        element = (
            CaptionEdit.query.filter_by(media_id=media_id, language=language)
            .order_by(CaptionEdit.timestamp)
            .first_or_none()
        )
        return element

    @staticmethod
    def create_precondition(info):
        logger.debug(info)
        assert info.get("language", None)
        assert info.get("external_id", None)
        assert info.get("user", None)
        assert info.get("timestamp", None)

    @staticmethod
    def create(info) -> CaptionEdit:
        logger.debug("DAO: Creating new caption edit object")
        CaptionEditDAO.create_precondition(info)
        language = info["language"]
        media_id = info["external_id"]
        user = info["user"]
        timestamp = info["timestamp"]
        try:
            CaptionEdit.query.filter_by(
                language=language, media_id=media_id, timestamp=timestamp
            ).one()
            raise CaptionEditDAOException(
                f"The caption edit with media_id {media_id}, "
                f"language {language} and timestamp {timestamp} already exists"
            )
        except NoResultFound:
            logger.info(
                f"Creating CaptionEdit for media_id {media_id} and language {language}"
            )
            supervision_status = str(
                info.get("supervision_status", SupervisionStatuses.UNKNOWN.value)
            )
            supervision_ratio = info.get("supervision_ratio", None)
            caption_edit = CaptionEdit(
                language=language,
                media_id=media_id,
                user=user,
                timestamp=timestamp,
                supervision_status=supervision_status,
                supervision_ratio=supervision_ratio,
            )
            logger.debug(f"CaptionEdit created: {caption_edit}")

            db.session.add(caption_edit)

            upload = Upload.query.filter_by(media_id=media_id).one()
            upload.captions_edits.append(caption_edit)
            db.session.commit()
            logger.debug(
                f"CaptionEdit {caption_edit.id} added to Upload: {upload.media_id}"
            )

            return caption_edit
