import logging

from app.extensions import db
from app.models.captions_edits import CaptionEdit
from app.models.upload_state_logs import StateLog
from app.models.uploads import Upload

logger = logging.getLogger("webapp.daos")


class StateLogDAOException(Exception):
    pass


class StateLogDAO:
    @staticmethod
    def get_by_id(edit_id: str) -> StateLog:
        element = CaptionEdit.query.get(edit_id)
        return element

    @staticmethod
    def get_by_media_id(media_id: int) -> CaptionEdit:
        element = (
            CaptionEdit.query.filter_by(media_id=media_id)
            .order_by(CaptionEdit.updated_on.desc())  # pylint: disable=no-member
            .first()
        )
        return element

    @staticmethod
    def get_language_last_edit(media_id: str, language: str) -> CaptionEdit:
        element = (
            CaptionEdit.query.filter_by(media_id=media_id, language=language)
            .order_by(CaptionEdit.timestamp)
            .first_or_none()
        )
        return element

    @staticmethod
    def create(upload_id: int, description: str) -> CaptionEdit:
        logger.debug("DAO: Creating new caption edit object")

        state_log = StateLog(
            description=description,
        )

        logger.debug(f"StateLog created: {state_log}")

        db.session.add(state_log)

        upload = Upload.query.get(upload_id)
        upload.state_logs.append(state_log)
        db.session.commit()
        logger.debug(f"StateLog {state_log.id} added to Upload: {upload.media_id}")

        return state_log
