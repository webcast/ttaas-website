import logging
from datetime import datetime

from flask_jwt_extended import create_access_token
from sqlalchemy.orm.exc import NoResultFound

from app.daos.accounts import AccountDAO
from app.extensions import db
from app.models.token_blacklist import ApiToken
from app.utils.blacklist_helpers import add_token_to_database, update_token_in_database

logger = logging.getLogger("webapp.daos")


class ApiTokenException(Exception):
    pass


class ApiTokenDAO:
    @staticmethod
    def get_all(account_id: int):
        account = AccountDAO.get_account_by_id(account_id)
        return ApiToken.query.filter_by(revoked=False, account_id=account.id).all()

    @staticmethod
    def create_precondition(account_id, info, is_admin):
        assert info.get("tokenName", None)
        assert info.get("scope", None)
        if not is_admin:
            assert AccountDAO.has_access_current_user_to_account(account_id)

    @staticmethod
    def create(account_id, info, is_admin=False):
        logger.debug("DAO: Creating API token")
        ApiTokenDAO.create_precondition(account_id, info, is_admin)
        token_name = info["tokenName"]
        try:
            ApiToken.query.filter_by(name=token_name.strip().lower()).one()
            raise ApiTokenException(f"The token with name {token_name} already exists")
        except NoResultFound:
            logger.info(f"Creating Token with name {token_name}")
            access_token = create_access_token(identity=token_name)
            db_token = add_token_to_database(
                token_name, access_token, info["scope"], account_id
            )
            return db_token

    @staticmethod
    # @cache.memoize(CACHE_TIMEOUT)
    def delete_by_id(token_id):
        token = ApiToken.query.get(token_id)
        if token:
            if AccountDAO.has_access_current_user_to_account(token.account.id):
                token.revoked = True
                db.session.commit()
                logger.info(f"Token with id {token_id} has been revoked")
                return True

        logger.warning(f"Unable to revoke the Token with id {token_id} (Not found)")
        return False

    @staticmethod
    # @cache.memoize(CACHE_TIMEOUT)
    def get_by_name(token_name):
        token = ApiToken.query.filter_by(name=token_name).one()
        return token

    @staticmethod
    # @cache.memoize(CACHE_TIMEOUT)
    def refresh_by_id(token_id):
        token = ApiToken.query.get(token_id)
        if token:
            logger.debug(token.account)
            if AccountDAO.has_access_current_user_to_account(token.account.id):
                updated_token = update_token_in_database(token)
                logger.info(f"Token with id {token_id} has been revoked")
                return updated_token

        logger.warning(f"Unable to revoke the Token with id {token_id} (Not found)")
        return None

    @staticmethod
    def update_last_used(token_id):
        token = ApiToken.query.get(token_id)
        if token:
            token.last_used = datetime.now()
            db.session.commit()
