import logging
import uuid
from datetime import datetime

from flask_login import current_user
from sqlalchemy.exc import ProgrammingError

from app.daos.state_log import StateLogDAO
from app.extensions import db
from app.models.accounts import Account
from app.models.cern_search_settings import CernSearchSettings
from app.models.uploads import RunningStates, TaskStates, Upload

logger = logging.getLogger("webapp.daos")


class UploadDAOException(Exception):
    pass


def generate_media_id(prefix) -> str:
    media_id = f"{prefix}_{uuid.uuid4()}"
    return media_id


class UploadDAO:
    @staticmethod
    def get_upload_by_id(accountId: int) -> Upload:
        upload = Upload.query.get(accountId)
        return upload

    @staticmethod
    def get_upload_by_media_id(media_id: str) -> Upload:
        upload = Upload.query.filter_by(media_id=media_id).one_or_none()
        return upload

    @staticmethod
    def get_upload_by_media_id_for_account(media_id: str, account_id: int) -> Upload:
        upload = Upload.query.filter_by(
            media_id=media_id, account_id=account_id
        ).one_or_none()
        return upload

    @staticmethod
    def get_upload_by_media_id_and_nonce(media_id: str, nonce: str) -> Upload:
        upload = Upload.query.filter_by(
            media_id=media_id,
            nonce=nonce,
            # ingest_id=request.form["upload_id"],
        ).one_or_none()
        return upload

    @staticmethod
    def get_uploads_for_account(account_id: int) -> list[Upload]:
        uploads = Upload.query.filter_by(account_id=account_id).all()
        return uploads

    @staticmethod
    def get_uploads_by_username() -> list[Upload]:
        accounts = Upload.query.filter_by(username=current_user.username).all()  # type: ignore
        return accounts

    @staticmethod
    def get_upload_by_account_id_and_checksum(account_id: int, checksum: str):
        # We don't want to consider uploads in error state
        logger.info(f"Looking for existing upload with checksum {checksum}")
        existing_uploads = (
            Upload.query.filter_by(media_file_checksum=checksum, account_id=account_id)
            .filter(
                Upload.state.not_in(
                    [
                        TaskStates.ERROR_PREPARING_UPLOAD,
                        TaskStates.ERROR_RUNNING_TRANSCRIPTIONS,
                        TaskStates.ERROR_RUNNING_TRANSLATIONS,
                        TaskStates.ERROR_SUBMITTING,
                        TaskStates.ERROR_UPLOADING,
                        TaskStates.ERROR_NOTIFIED,
                        TaskStates.ERROR_CALLBACK_FAILED,
                        TaskStates.ERROR_EMAIL_FAILED,
                        TaskStates.DELETED,
                    ]
                )
            )
            .all()
        )
        existing_upload = None
        if len(existing_uploads) > 1:
            logger.debug(
                f"Found {len(existing_uploads)} uploads for checksum {checksum}"
            )
            for upload in existing_uploads:
                logger.debug(
                    f"Deleting upload {upload.media_id} for checksum {checksum}"
                )
                db.session.delete(upload)
            db.session.commit()
        if len(existing_uploads) == 1:
            existing_upload = existing_uploads[0]
        return existing_upload

    @staticmethod
    def create_precondition(info):
        assert info.get("title", None)
        assert info.get("media_file_path", None)
        assert info.get("su_account", None)
        assert info.get("language", None)
        assert info.get("username", None)
        assert info.get("upload_type", None)
        assert info.get("notification_method", None)
        assert info.get("account_id", None)

    @staticmethod
    def create(info, media_id=None) -> Upload:
        upload = None
        try:
            logger.debug("DAO: Creating new upload")
            UploadDAO.create_precondition(info)

            title = info["title"]
            su_account = info["su_account"]
            if not media_id:
                media_id = generate_media_id(su_account)

            logger.debug(f"Getting account ID {info['account_id']} for {media_id}")
            account = Account.query.get(info["account_id"])

            logger.info(f"Creating Upload with title {title}")
            upload = Upload(
                comments=info["comments"],
                language=info["language"],
                media_file_path=info["media_file_path"],
                media_id=media_id,
                notification_email=info.get("notification_email", None),
                notification_url=info.get("notification_url", None),
                notification_method=info["notification_method"].upper(),
                title=title,
                upload_type=info["upload_type"],
                username=info["username"],
                reference_url=info.get("reference_url", ""),
                account_id=info["account_id"],
            )
            logger.debug(f"Upload created: {upload}")

            db.session.add(upload)

            media_datetime = info.get("media_datetime", None)
            logger.debug(
                f"Creating CernSearchSettings for upload {upload.media_id}. Datetime: {media_datetime}"
            )
            converted_datetime = datetime.now()
            if media_datetime:
                date_time_format = "%Y-%m-%d, %H:%M:%S"
                converted_datetime = datetime.strptime(media_datetime, date_time_format)

            cern_search_settings = CernSearchSettings(
                read_groups=(
                    info["acl_read_groups"] if info.get("acl_read_groups", None) else ""
                ),
                read_users=(
                    info["acl_read_users"] if info.get("acl_read_users", None) else ""
                ),
                is_public=info.get("transcription_is_public", False),
                media_datetime=converted_datetime,
            )
            logger.debug(f"Adding CernSearchSettings to upload {upload.media_id}")
            db.session.add(cern_search_settings)
            upload.cern_search_settings = cern_search_settings

            logger.debug(f"Adding upload {upload.media_id} to account {account.id}")
            account.uploads.append(upload)
            db.session.commit()

            StateLogDAO.create(upload.id, "Upload created")
        except ProgrammingError as error:
            logger.error(f"Unable to create the upload: {error}", exc_info=True)
            raise UploadDAOException("Unable to create the upload") from error

        return upload

    @staticmethod
    def delete_upload_by_id(upload_id: int, hard: bool = False):
        upload = Upload.query.get(upload_id)
        if hard:
            db.session.delete(upload)
            return True
        upload.state = TaskStates.DELETED
        db.session.commit()
        StateLogDAO.create(upload.id, "Deleted upload")
        return True

    @staticmethod
    def update_ingest_id(upload_id: int, ingest_id: str, new_state: TaskStates):
        upload = Upload.query.get(upload_id)
        upload.ingest_id = ingest_id
        upload.state = new_state
        db.session.commit()
        return True

    @staticmethod
    def update_running_state(upload_id: int, new_state: str):
        upload = Upload.query.get(upload_id)
        upload.running_state = new_state

        if new_state == RunningStates.TRANSLATION_IN_PROGRESS.value:
            upload.state = TaskStates.RUNNING_TRANSLATIONS
            StateLogDAO.create(upload.id, "Running translations")
        if new_state == RunningStates.TRANSCRIPTION_IN_PROGRESS.value:
            upload.state = TaskStates.RUNNING_TRANSCRIPTIONS
            StateLogDAO.create(upload.id, "Running transcriptions")
        if (
            new_state == RunningStates.COMPLETED.value
            or new_state == RunningStates.MEDIA_UPDATED.value
        ):
            logger.debug(f"Upload {upload.id} transcription is finished")
            if upload.state == TaskStates.RUNNING_TRANSLATIONS:
                upload.state = TaskStates.TRANSLATION_FINISHED
                StateLogDAO.create(upload.id, "Translation finished")
            else:
                upload.state = TaskStates.TRANSCRIPTION_FINISHED
                StateLogDAO.create(upload.id, "Transcription finished")
        if not new_state or int(new_state) >= 100:
            if new_state == RunningStates.ERROR_TRANSLATING_MEDIA.value:
                upload.state = TaskStates.ERROR_RUNNING_TRANSLATIONS
                StateLogDAO.create(upload.id, "Error running translations")
            else:
                upload.state = TaskStates.ERROR_RUNNING_TRANSCRIPTIONS
                StateLogDAO.create(upload.id, "Error running transcriptions")

        db.session.commit()
        return True

    @staticmethod
    def update_task_state(upload_id: int, new_state: TaskStates):
        upload = Upload.query.get(upload_id)
        upload.state = new_state
        db.session.commit()
        return True

    @staticmethod
    def update_checksum(upload_id: int, checksum: str):
        upload = Upload.query.get(upload_id)
        upload.media_file_checksum = checksum
        db.session.commit()
        return True
