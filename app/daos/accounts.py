import logging
from datetime import datetime

from flask_login import current_user

from app.extensions import db
from app.models.accounts import Account

logger = logging.getLogger("webapp.daos")


class ApiTokenException(Exception):
    pass


class AccountDAO:
    @staticmethod
    def get_account_by_id(accountId: int) -> Account:
        account = Account.query.get(accountId)
        AccountDAO.set_account_last_used(account.id)
        return account

    @staticmethod
    def get_accounts_for_groups(roles: list[str]) -> list[Account]:
        accounts = []
        for role in roles:
            temp_accounts = Account.query.filter_by(role=role).all()
            for account in temp_accounts:
                logger.debug(f"Found account {account} for role {role}")
                accounts.append(account)
        return accounts

    @staticmethod
    def get_current_user_accounts() -> list[Account]:
        accounts = AccountDAO.get_accounts_for_groups(current_user.roles)
        return accounts

    @staticmethod
    def has_access_current_user_to_account(account_id: int) -> bool:
        accounts = AccountDAO.get_current_user_accounts()
        logger.debug(f"Current user accounts: {accounts}")
        if account_id in [account.id for account in accounts]:
            return True
        logger.debug(f"Current user has no access to account {account_id}")
        return False

    @staticmethod
    def set_account_last_used(account_id: int) -> None:
        account = Account.query.get(account_id)
        account.last_used = datetime.now()
        db.session.commit()
