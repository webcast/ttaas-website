import logging

from app.daos.uploads import UploadDAO
from app.extensions import db
from app.models.captions_edits import CaptionEdit
from app.models.cern_search_settings import CernSearchStates

logger = logging.getLogger("webapp.daos_cern_search_settings")


class CernSearchSettingsDAOException(Exception):
    pass


class CernSearchSettingsDAO:
    @staticmethod
    def set_error_submitting(media_id: str) -> bool:
        upload = UploadDAO.get_upload_by_media_id(media_id)
        upload.cern_search_settings.cern_search_state = (
            CernSearchStates.ERROR_SUBMITTING
        )
        db.session.commit()
        return True

    @staticmethod
    def set_submitted(media_id: str) -> bool:
        upload = UploadDAO.get_upload_by_media_id(media_id)
        upload.cern_search_settings.cern_search_state = CernSearchStates.SUBMITTED
        db.session.commit()
        return True

    @staticmethod
    def get_by_media_id(media_id: str) -> CaptionEdit:
        element = (
            CaptionEdit.query.filter_by(media_id=media_id)
            .order_by(CaptionEdit.updated_on.desc())  # pylint: disable=no-member
            .first()
        )
        return element
