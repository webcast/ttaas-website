import logging
import re
from datetime import datetime

import click
import requests
from flask.cli import with_appcontext
from requests.exceptions import HTTPError

from app.daos.uploads import UploadDAO
from app.services.cern_search.cern_search_api_client import CernSearchApiClient
from app.services.cern_search.cern_search_service import CernSearchService

logger = logging.getLogger("webapp.search_service")


@click.command()
@click.option("--media", help="The media ID that matches the ASR external ID")
@click.option("--su_account", help="The account in the ASR system")
@click.option("--delete_old", help="Delete old records true|false")
@click.option("--language", help="Defaults to en, but could be fr", default="en")
@with_appcontext
def search_send_document(
    media: str, su_account: str, delete_old: str, language: str
) -> int:
    """
    Sends a document to the CERN Search engine

    :return: 0 if the script ends well
    :rtype: int
    """
    logger.info("Searhing on CERN Search engine")
    search_service = CernSearchService(logger=logger)
    result = search_service.handle_caption_edit(
        media_id=media,
        language=language,
        su_account=su_account,
        delete_old=True if delete_old == "true" else False,
    )
    # logger.info(f"Found {len(result)} files")
    logger.info(result)

    return 0


def _get_acl_fromCESApi(indico_id, CES_URL, CES_TOKEN):
    """Helper method to get ACL's from a particular document

    :return: a dictionary with ACLs, if the script ends well
    :rtype: dict
    """
    access = {}
    if "_" in indico_id:
        indico_id = indico_id.split("_")[1]
    response = requests.get(
        f"{CES_URL}?contribution_id={indico_id}",
        headers={
            "Authorization": f"Bearer {CES_TOKEN}",
        },
    )
    try:
        response.raise_for_status()
    except HTTPError:
        logger.debug(
            f"got exception: {response.json()} while working with event {indico_id}"
        )
    if response.status_code == 200:
        access["read"] = [
            f"group:{group}"
            for group in (
                response.json()["result"]["not_found"]["groups"]
                + response.json()["result"]["allowed"]["groups"]
            )
        ]
        if "read" in access.keys():
            access["read"].extend(
                [
                    f"user:{user}"
                    for user in (
                        response.json()["result"]["not_found"]["users"]
                        + response.json()["result"]["allowed"]["users"]
                    )
                ]
            )
        else:
            access["read"] = [
                f"user:{user}"
                for user in (
                    response.json()["result"]["not_found"]["users"]
                    + response.json()["result"]["allowed"]["users"]
                )
            ]
        # check for externals
        for dict in response.json()["result"]["allowed"]["externals"]:
            for v in dict.values():
                if "read" in access.keys():
                    access["read"].extend([f"user:{v}"])
                else:
                    access["read"] = [f"user:{v}"]

    return access


def _get_eventtime(indico_id, CES_URL_INDICO, CES_TOKEN):
    indico_contrib = None
    indico_subcontrib = None
    event_id = None
    if "_" in indico_id:
        indico_id = indico_id.split("_")[1]
    match = re.search(
        r"(?P<indicoevent>\d+)(?P<indicocontrib>c\d+)?(?P<indicosubcontrib>sc\d+)?",
        indico_id,
    )
    if match:
        event_id = match.groupdict().get("indicoevent", None)
        indico_contrib = match.groupdict().get("indicocontrib", None)
        indico_subcontrib = match.groupdict().get("indicosubcontrib", None)
    else:
        logger.debug(
            f"no match on indicoid: {indico_id} while trying to retrieve eventdate"
        )
    if indico_contrib:
        indico_contrib = indico_contrib.replace("c", "")
    if indico_subcontrib:
        indico_subcontrib = indico_subcontrib.replace("sc", "")

    response = requests.get(
        f"{CES_URL_INDICO}?indico_id={event_id}",
        headers={
            "Authorization": f"Bearer {CES_TOKEN}",
        },
    )
    try:
        response.raise_for_status()
        # print(json.dumps(response.json(), indent=4))
        if indico_contrib or indico_subcontrib:
            for c in response.json()["results"][0]["contributions"]:
                if c["id"] == indico_contrib:
                    return f'{c["startDate"]["date"]}T{c["startDate"]["time"]}Z'
            for s in response.json()["results"][0]["sessions"]:
                if s["session"]["id"] == indico_contrib:
                    return f'{s["startDate"]["date"]}T{s["startDate"]["time"]}Z'
        else:
            start_date = response.json()["results"][0]["startDate"]["date"]
            start_time = response.json()["results"][0]["startDate"]["time"]
            return f"{start_date}T{start_time}Z"
    except HTTPError:
        logger.debug(
            f"got exception: {response.json()} while working with event {indico_id}"
        )

    return datetime.utcnow().strftime("%Y-%m-%dT%H%M%S")


@click.command()
@click.option("--media", help="The media ID that matches the ASR external ID")
@click.option("--su_account", help="The account in the ASR system")
@click.option("--delete_old", help="Delete old records true|false")
@click.option("--language", help="Defaults to en, but could be fr", default="en")
@click.option(
    "--indicoid", help="Indico id of the event, so ACL's can be retrieved from CES"
)
@click.option("--url", help="URL to access the media")
@with_appcontext
def search_send_document_from_api(
    media: str, su_account: str, delete_old: str, language: str, indicoid: str, url: str
) -> int:
    """
    Sends a document to the CERN Search engine

    :return: 0 if the script ends well
    :rtype: int
    """
    results = None
    logger.info("Send document to CERN search")
    search_service = CernSearchService(logger=logger)
    vtt_file_content = search_service.download_vtt_file(media, language)
    # Parse the vtt file and Generate the json (array of docs) to send
    # logger.info(vtt_file_content)

    search_api = CernSearchApiClient("weblecture-service", logger=logger)
    eventtime = _get_eventtime(indicoid, search_api.ces_indico, search_api.ces_token)
    logger.info(f"Event time {eventtime} for indico {indicoid}")
    upload = UploadDAO.get_upload_by_media_id(media)
    parsed_vtt_array = search_service.parse_vtt_file(
        upload.title, vtt_file_content, media, language, su_account, url, eventtime
    )
    if su_account == "opencast" and indicoid:
        acls = _get_acl_fromCESApi(indicoid, search_api.url_ces, search_api.ces_token)
        results = []
        if acls:
            for doc in parsed_vtt_array:
                result = search_api.send_document(
                    doc,
                    acls.get("read", None),
                    acls.get("update", None),
                    acls.get("delete", None),
                )
                results.append(result)
    logger.info(results)

    return 0


@click.command()
@click.option("--media", help="The media ID that matches the ASR external ID")
@click.option("--su_account", help="The account in the ASR system")
@click.option("--delete", help="Delete old records true|false", is_flag=True)
@click.option("--language", help="Defaults to en, but could be fr", default="en")
@click.option("--mediaid", help="mediaid to be removed", required=True)
@click.option(
    "--readers", help="if given people that can read the document", multiple=True
)
@with_appcontext
def search_delete_document_from_api_by_mediaid(
    media: str, su_account: str, delete: bool, language: str, mediaid: str, readers: str
) -> int:
    """
    Deletes a document to the CERN Search engine

    :return: 0 if the script ends well
    :rtype: int
    """
    logger.info("Delete document to CERN search")

    search_api = CernSearchApiClient("weblecture-service", logger=logger)
    others = []
    others.append(f"ttpmediaid={mediaid}")
    if language:
        others.append(f"language={language}")
    if su_account:
        others.append(f"su_account={su_account}")

    search_results = search_api.search_documents("", readers=readers, arrothers=others)
    search_docs = search_results["hits"]["hits"]
    logger.info(f"we got {len(search_docs)} documents for this query")
    if delete:
        search_api.delete_documents(search_docs)

    return 0


@click.command()
@click.option("--media", help="The media ID that matches the ASR external ID")
@click.option("--su_account", help="The account in the ASR system")
@click.option("--language", help="Defaults to en, but could be fr", default="en")
@click.option("--mediaid", help="mediaid to be removed")
@click.option(
    "--readers", help="if given people that can read the document", multiple=True
)
@click.option("--query", help="text to look for", default="")
@click.option("--eventtime", help="time using elastic query languag")
@with_appcontext
def search_search_document_from_api(
    media: str,
    su_account: str,
    language: str,
    mediaid: str,
    readers: str,
    query: str,
    eventtime: str,
) -> int:
    """
    Deletes a document to the CERN Search engine

    :return: number of documents
    :rtype: int
    """
    logger.info("Delete document to CERN search")

    search_api = CernSearchApiClient("weblecture-service", logger=logger)
    others = []
    if mediaid:
        others.append(f"ttpmediaid={mediaid}")
    if language:
        others.append(f"language={language}")
    if su_account:
        others.append(f"su_account={su_account}")
    if eventtime:
        others.append(f"eventdate={eventtime}")

    docs = search_api.search_documents(query, readers=readers, arrothers=others)
    logger.info(f"we got {len(docs)} documents for this query")
    print(docs)
    return len(docs)


@click.command()
@click.option("--media", help="The media ID that matches the ASR external ID")
@with_appcontext
def search_search_document(media: str) -> int:
    """
    Searches for documents in the CERN Search engine matching the criteria.

    :return: 0 if the script ends well
    :rtype: int
    """
    logger.info("Searhing on CERN Search engine")
    search_service = CernSearchService(logger=logger)
    result = search_service.search_document_by_id(media)
    logger.info(result)
    logger.info(f"Found {len(result)} documents")
    return 0


@click.command()
@click.option("--media", help="The media ID that matches the ASR external ID")
@with_appcontext
def search_delete_document(media: str) -> int:
    """
    Deletes documents in the CERN Search engine matching the criteria.

    :return: 0 if the script ends well
    :rtype: int
    """
    logger.info("Deleting CERN Search engine")
    search_service = CernSearchService(logger=logger)
    # results = search_service.search_document_by_id(media)
    # logger.info(f"Found {len(results)} documents")
    # logger.info(f"Deleting {len(results)} documents")
    results = search_service.delete_documents_by_id(media)
    logger.info("Documents deleted")
    logger.info(f"Found {len(results)} documents after delete")
    return 0
