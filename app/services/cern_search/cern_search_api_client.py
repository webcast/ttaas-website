import logging
import re
from typing import Any, Dict, List, Optional

from flask import current_app
from requests import Session
from requests.exceptions import HTTPError


def escape_colons(value):
    return value.replace(":", r"\:")


class CernSearchApiClient:
    def __init__(
        self,
        owner,
        logger: Optional[logging.Logger] = None,
    ):
        base_uri = current_app.config["CERNSEARCH_INSTANCE"]
        self.token = current_app.config["CERNSEARCH_TOKEN"]
        self.url_weblecture_player = current_app.config["CERNSEARCH_BASIC_URL_PLAYER"]
        self.url_ces = current_app.config["CERNSEARCH_CES_URL"]
        self.ces_token = current_app.config["CERNSEARCH_CES_TOKEN"]
        self.ces_indico = current_app.config["CERNSEARCH_CES_URL_INDICO"]
        self.schema = current_app.config["CERNSEARCH_SCHEMA"]
        self.owner = owner
        self.base_uri = base_uri

        if logger:
            self.logger = logger
        else:
            self.logger = logging.getLogger("job.cern_search_api")

    @property
    def session(self):
        session = Session()
        session.headers = {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": f"Bearer {self.token}",
        }
        return session

    def _build_access(
        self, readers: list[str], updaters: list[str], deleters: list[str]
    ) -> dict:
        owner_group = f"group:{self.owner}"
        _access = {
            "owner": [owner_group],
            "read": [],
            "delete": [owner_group],
            "update": [owner_group],
        }
        if readers:
            _access["read"].extend(readers)  # type: ignore [attr-defined]
        if updaters:
            _access["update"].extend(updaters)  # type: ignore [attr-defined]
        if deleters:
            _access["delete"].extend(deleters)  # type: ignore [attr-defined]
        return _access

    def handle_response(self, resp, expected_codes=None, expects_json=True):
        if expected_codes is None:
            expected_codes = [200, 201, 204]
        self.logger.debug(f"Response code {resp.status_code}: {resp.text}")
        resp.raise_for_status()
        if resp.status_code not in expected_codes:
            raise HTTPError(f"Unexpected status code {resp.status_code}", response=resp)
        if expects_json:
            return resp.json()
        return resp

    def send_document(
        self,
        document_dict: dict,
        readers: list[str],
        updaters: list[str],
        deleters: list[str],
    ):
        result = self.make_send_document_request(
            document_dict, readers, updaters, deleters
        )
        return result

    def make_send_document_request(
        self,
        document_dict: dict,
        readers: list[str],
        updaters: list[str],
        deleters: list[str],
    ) -> bool:
        _access = self._build_access(readers, updaters, deleters)
        data = {"_access": _access, "$schema": self.schema, **document_dict}
        self.logger.debug(f"Sending document data: {data}")
        api_path = "api/records/"
        url = f"{self.base_uri}/{api_path}"
        response = self.session.post(url, json=data)
        # print('Response code {0}\n'.format(response.status_code))

        self.handle_response(response)
        return response.json()

    def search_documents(
        self,
        query,
        page_number=1,
        page_size=10,
        readers=None,
        arrothers=None,
        all=False,
    ) -> Dict[Any, Any]:
        """We can query a document via contents of its text but also on properties e.g.
        ttpmediaid=opencast_2130-219302-1, so arrothers is an array of key value pairs,
        split by a '=' readers is an array e.g.
        ['group:weblecture-service', 'user:rgaspar']
        """
        data = {}
        if query:
            if re.search(r"\s+", query):
                query = re.sub(r"\s+", " +", query).strip()
                query = "+" + query
            data["q"] = query
        if readers:
            data["access"] = readers
        if page_number:
            data["page"] = page_number
        data["size"] = page_size
        if arrothers:
            self.logger.debug(f"arrothers: {arrothers}")
            others_dict = {}
            for item in arrothers:
                if not item:
                    continue
                self.logger.debug(f"item: {item}")
                index, value = item.split("=")
                others_dict[index] = value

            others = " AND ".join(
                [f"{k}:{escape_colons(v)}" for k, v in others_dict.items()]
            )
            if "q" in data.keys():
                data["q"] += " AND " + others
            else:
                data["q"] = others

        self.logger.debug(f"Searching query: {data['q']}")
        self.logger.debug(f"Search parameters: {data}")

        total_hits = []
        total = 0
        self.logger.debug(f"Query: {data}")
        response = self.session.get(f"{self.base_uri}/api/records/?", params=data)
        self.handle_response(response)
        result = response.json()

        total = result["hits"]["total"]

        if all:
            total_hits = response.json()["hits"]["hits"]
            while "next" in response.links:
                matching_regex = re.search(
                    r"page=(\d+)", response.json()["links"]["next"]
                )
                if matching_regex:
                    data["page"] = matching_regex.group(1)
                    response = self.session.get(
                        f"{self.base_uri}/api/records/?", params=data
                    )
                    self.handle_response(response)
                    total_hits.extend(response.json()["hits"]["hits"])

        return {"total": total, "objects": result["hits"]["hits"]}

    def delete_documents(self, documents_to_delete: list[dict]) -> List[Dict[Any, Any]]:
        results = []
        for doc in documents_to_delete:
            self.logger.debug(f"Deleting document with id {doc['id']}")
            response = self.session.delete(f"{self.base_uri}/api/record/{doc['id']}")
            result = self.handle_response(response, expects_json=False)
            results.append(result)

        return results

    def search_documents_for_player(self, query, readers_arr, size, others_dict):
        """Retrieve a number of documents

        Args:
            query ([type]): for example (New York) and (New Mexico)
            readers_arr ([type]): [description]
            size ([type]): [description]
            others_dict ([type]): [description]
            howmany (int, optional): [description]. Defaults to -1 -> ALL

        Returns:
            [type]: [description]
        """
        # build data

        data = {}
        if query:
            if re.search(r"\s+", query):
                query = re.sub(r"\s+", " +", query).strip()
                query = "+" + query
            data["q"] = query  # urllib.parse.quote(query)
        if readers_arr:
            data["access"] = readers_arr
        if size:
            data["size"] = size
        if others_dict:
            # others = " AND ".join(
            #     [
            #         "{0}:{1}".format(k, v.replace(":", "\:"))
            #         for k, v in others_dict.items()
            #     ]
            # )
            others = " AND ".join(
                [f"{k}:{escape_colons(v)}" for k, v in others_dict.items()]
            )
            if "q" in data.keys():
                data["q"] += " AND " + others
            else:
                data["q"] = others
        print(data)
        total_hits = []
        response = self.session.get(f"{self.base_uri}/api/records/?", params=data)
        self.handle_response(response)
        # print(response.json())
        total_hits = response.json()["hits"]["hits"]
        # print(total_hits)
        while "next" in response.links:
            m = re.search(r"page=(\d+)", response.json()["links"]["next"])
            if m:
                data["page"] = m.group(1)
                response = self.session.get(
                    f"{self.base_uri}/api/records/?", params=data
                )
                self.handle_response(response)
                total_hits.extend(response.json()["hits"]["hits"])

        return total_hits
