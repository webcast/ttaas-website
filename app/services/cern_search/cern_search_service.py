import logging
import re
from datetime import datetime
from typing import Any, Dict, List, Optional
from urllib.parse import parse_qs, urlencode, urlparse, urlunparse

import sqlalchemy
from flask import current_app

from app.daos.captions_edits import CaptionEditDAO
from app.daos.cern_search_settings import CernSearchSettingsDAO
from app.daos.uploads import UploadDAO
from app.services.cern_search.cern_search_api_client import CernSearchApiClient
from app.services.mllp.api_client import ApiSpeechClientMLLP


class CernSearchService:
    def __init__(self, logger: Optional[logging.Logger] = None):
        """
        :param logger:
        :type logger: logging.Logger
        """
        if logger:
            self.logger = logger
        else:
            self.logger = logging.getLogger("job.cern_search_service")

    def download_vtt_file(self, media_id: str, language: str) -> str:
        """Download a VTT file from a URL.

        Args:
            media_id (str): The external id to download the file from

        Returns:
            str: The path to the downloaded file
        """
        self.logger.debug(
            f"Downloading file from MLLP (media_id: {media_id}, language: {language})"
        )
        tokens = media_id.split("_")
        if tokens:
            api_client = ApiSpeechClientMLLP(substitute_user=tokens[0])
        else:
            api_client = ApiSpeechClientMLLP()
        downloaded_file = api_client.get_media_download(media_id, language)
        self.logger.debug("Got downloaded VTT file")
        return downloaded_file

    def parse_vtt_file(
        self,
        title: str,
        file_content: str,
        media_id: str,
        language: str,
        su_account: str,
        url: Optional[str] = None,
        eventtime: Optional[datetime] = None,
    ) -> list[dict]:
        """Parses a VTT file and converts it to an array of dicts with the CERN Search expected format

        VTT format example:
        1
        00:00:02.160 --> 00:00:07.990
        Do not close your eyes
        why now ok good

        Args:
            file_content (str): VTT text that will be parsed
            media_id (str): External ID of the documents
            language (str): Language of the documents
            url (str): used for search on CERN Search index's

        Returns:
            list[dict]: A list of dicts with the CERN Search expected format
        """
        lines_tuples = re.findall(
            r"(?:\d{1,}\n)?(\d\d:\d\d:\d\d).\d{1,3} --> \d\d:\d\d:\d\d.\d{1,3}\n((?:.+\n)+)",
            file_content,
        )
        documents_list = []
        for line in lines_tuples:
            format_matched = re.search(r"(\d\d):(\d\d):(\d\d)", line[0])
            if format_matched:
                begin_hour = format_matched.group(1)
                begin_min = format_matched.group(2)
                begin_sec = format_matched.group(3)
                display_time = self._format_time(begin_hour, begin_min, begin_sec)
            else:
                continue
            data = {
                "content": line[1],
                "site": current_app.config["CERNSEARCH_BASIC_URL_PLAYER"],
            }

            if not eventtime:
                formatted_event_datetime = datetime.utcnow().strftime(
                    "%Y-%m-%dT%H:%M:%SZ"
                )
            else:
                formatted_event_datetime = eventtime.strftime("%Y-%m-%dT%H:%M:%SZ")

            document = {
                "title": title,
                "language": language,
                "ttpmediaid": media_id,
                "asrtype": "vtt",
                "begin_hour": begin_hour,
                "begin_min": begin_min,
                "begin_sec": begin_sec,
                "_data": data,
                "su_account": su_account,
                "eventdate": formatted_event_datetime,
                "url": f"{url}&time={display_time}",
            }
            self.logger.debug(f"Document: {document}")
            documents_list.append(document)
        return documents_list

    def get_media_referenceUrl(self, media_id: str) -> str:
        """ """
        try:
            upload = UploadDAO.get_upload_by_media_id(media_id)
            self.logger.info(
                f"We got reference URL: <{upload.reference_url}> for media_id {media_id}"
            )
            return upload.reference_url

        except sqlalchemy.exc.NoResultFound:
            self.logger.debug(f"{media_id} not found at TTaaS db")
            return None

    def get_media_acls(self, media_id: str) -> dict:
        """Get the media acls from a CaptionEdit object.

        Args:
            media_id (str): _description_

        Returns:
            dict: _description_
        """
        access = {"owner": [""], "read": [], "write": [], "update": []}
        try:
            caption_edit = CaptionEditDAO.get_by_media_id(media_id)
            cern_search_settings = caption_edit.upload.cern_search_settings
            groups = []
            users = []
            if cern_search_settings.read_groups.strip() != "":
                groups = [
                    f"group:{group.strip()}"
                    for group in cern_search_settings.read_groups.split(",")
                    if group.strip() != ""
                ]
            if cern_search_settings.read_users.strip() != "":
                users = [
                    f"user:{user.strip()}"
                    for user in cern_search_settings.read_users.split(",")
                    if user.strip() != ""
                ]
        except sqlalchemy.exc.NoResultFound:
            groups = []
            users = []

        access["read"] = groups + users
        access["owner"] = []
        access["write"] = []
        access["update"] = []

        return access

    def send_text_to_cern_search(
        self, media_id: str, documents: list[dict], acls: dict
    ) -> list[dict]:
        """Send text to CERN Search.

        Args:
            text (str): The text to send to CERN Search
        """
        self.logger.debug("Sending text to CERN Search")
        cern_search_api = CernSearchApiClient("weblecture-service", logger=self.logger)
        results = []
        try:
            # Send the document to CERN Search
            for doc in documents:
                result = cern_search_api.send_document(
                    doc,
                    acls.get("read", None),
                    acls.get("update", None),
                    acls.get("delete", None),
                )
                results.append(result)

            CernSearchSettingsDAO.set_submitted(media_id)
            return results
        except Exception as exc:
            CernSearchSettingsDAO.set_error_submitting(media_id)
            self.logger.error(
                f"Error sending document to CERN Search: {exc}", exc_info=True
            )
        return results

    def search_document_by_id(self, document_id: str, all=True) -> Dict[Any, Any]:
        """Search a document by id.

        Args:
            document_id (str): The id of the document to search

        Returns:
            dict: The document found
        """
        cern_search_api = CernSearchApiClient("weblecture-service", logger=self.logger)
        result = cern_search_api.search_documents(
            f"ttpmediaid:{document_id}",
            page_size=50,
            all=all,
        )

        return result

    def search_text_in_document(
        self, text: str, document_id: str, other_filters=[]
    ) -> Dict[Any, Any]:
        """Search a document by id.

        Args:
            document_id (str): The id of the document to search

        Returns:
            dict: The document found
        """
        others_array = other_filters + [f"ttpmediaid={document_id}"]

        cern_search_api = CernSearchApiClient("weblecture-service", logger=self.logger)
        result = cern_search_api.search_documents(
            text, page_size=50, arrothers=others_array
        )

        return result

    def delete_documents_by_id(self, document_id: str) -> List[Dict[Any, Any]]:
        """Search a document by id.

        Args:
            document_id (str): The id of the document to search

        Returns:
            dict: The document found
        """
        cern_search_api = CernSearchApiClient("weblecture-service", logger=self.logger)
        search_result = cern_search_api.search_documents(f"ttpmediaid:{document_id}")
        documents = search_result["objects"]

        delete_result = cern_search_api.delete_documents(documents)
        return delete_result

    def handle_caption_edit(
        self, media_id: str, language: str, su_account: str, url=None, delete_old=True
    ) -> bool:
        """Handle a caption edit.

        Args:
            caption (CaptionEdit): The caption edit to handle
        """
        self.logger.debug("Updating CERN Search caption document")
        # Download the vtt from MLLP
        vtt_file_content = self.download_vtt_file(media_id, language)
        # Parse the vtt file and Generate the json (array of docs) to send
        if not url:
            url = self.get_media_referenceUrl(media_id)

        upload = UploadDAO.get_upload_by_media_id(media_id)

        parsed_vtt_array = self.parse_vtt_file(
            upload.title,
            vtt_file_content,
            media_id,
            language,
            su_account,
            url,
            upload.cern_search_settings.media_datetime,
        )
        # Set the acls
        acls = self.get_media_acls(media_id)

        self.logger.debug(f"ACLS: {acls}")
        self.logger.debug(f"Parsed VTT: {parsed_vtt_array}")
        # Search the existings documents for media_id and language
        # Delete the document matching the above terms
        if delete_old:
            self.delete_documents_by_id(media_id)
        # Put parsed_vtt and acls together and Send the document to CERN Search
        results = self.send_text_to_cern_search(media_id, parsed_vtt_array, acls)
        self.logger.debug(f"Results: {results}")
        self.logger.info("Updated CERN Search caption document")

        return True

    def search_weblecture_documents(
        self, text: str, page_number, page_size, other_filters=[]
    ) -> Dict[Any, Any]:
        """Search weblecture documents.

        Args:
            text (str): The text that will be searched

        Returns:
            List[Dict[Any, Any]]: A list of documents found
        """
        cern_search_api = CernSearchApiClient("weblecture-service", logger=self.logger)
        self.logger.debug(f"Searching for {text} and {other_filters}")
        return cern_search_api.search_documents(
            text, page_number, page_size, arrothers=other_filters
        )

    def group_weblectures_by_ttpid(
        self, documents: List[Dict[Any, Any]]
    ) -> List[Dict[Any, Any]]:
        """Group weblectures by ttpid.

        Args:
            documents (List[Dict[Any, Any]]): The documents to group

        Returns:
            List[Dict[Any, Any]]: The documents grouped by ttpid
        """
        # grouped_documents = {}
        # for document in documents:
        #     self.logger.debug(f"Document: {document}")
        #     ttpmediaid = document["metadata"]["ttpmediaid"]
        #     if ttpmediaid not in grouped_documents:
        #         grouped_documents[ttpmediaid] = []
        #     grouped_documents[ttpmediaid].append(document)

        # Get grouped documents by ttpmediaid and language
        grouped_documents: Dict[Any, Any] = {}
        for document in documents:
            self.logger.debug(f"Document: {document}")
            ttpmediaid = document["metadata"]["ttpmediaid"]
            language = document["metadata"]["language"]
            key = f"{ttpmediaid}_{language}"
            if key not in grouped_documents:
                grouped_documents[key] = []
            grouped_documents[key].append(document)

        # Get only one document per ttpmediaid
        distinct_documents = []
        for ttpmediaid, extracted_documents in grouped_documents.items():
            distinct_documents.append(extracted_documents[0])

        sorted_by_date = sorted(
            distinct_documents, key=lambda k: k["metadata"]["eventdate"], reverse=True
        )

        for document in sorted_by_date:
            self.logger.debug(f"Document: {document['metadata']['title']}")

        return sorted_by_date

    def sort_times(self, item):
        m = re.search(r"(?:(\d\d)h)?(?:(\d\d)m)?(?:(\d\d)s)?", item)
        if m:
            return (
                int(m.groups(0)[0]) * 3600
                + int(m.groups(0)[1]) * 60
                + int(m.groups(0)[2])
            )
        else:
            return -1

    def remove_time_attributes(self, url: str):
        # Analizar la URL para obtener los componentes
        parsed_url = urlparse(url)

        # Obtener los parámetros de la cadena de consulta
        query_params = parse_qs(parsed_url.query)

        # Eliminar todos los atributos "time" de la cadena de consulta
        query_params = {
            key: value for key, value in query_params.items() if key != "time"
        }

        # Generar una nueva cadena de consulta sin los atributos "time"
        new_query_string = urlencode(query_params, doseq=True)

        # Crear la nueva URL con la cadena de consulta modificada
        new_url_components = list(parsed_url)
        new_url_components[4] = new_query_string
        new_url = urlunparse(new_url_components)

        return new_url

    def get_documents_ready_for_player(self, query, readers, size, ndocs, others):
        """Searching for documents that satisfy a criteria"""
        opensearch = CernSearchApiClient("weblecture-service", logger=self.logger)
        hits = self._get_hits(opensearch, query, readers, size, others)
        if ndocs > -1:
            selectedDocs = hits[:ndocs]
        else:
            selectedDocs = hits

        # A dictionary of arrays
        events = {}
        for doc in selectedDocs:
            # print(doc)
            # print(doc.keys())
            if doc["metadata"]["ttpmediaid"] in events:
                events[doc["metadata"]["ttpmediaid"]]["ttpmediaid"].append(
                    self._format_time(
                        doc["metadata"]["begin_hour"],
                        doc["metadata"]["begin_min"],
                        doc["metadata"]["begin_sec"],
                    )
                )
            else:
                events[doc["metadata"]["ttpmediaid"]] = {}
                events[doc["metadata"]["ttpmediaid"]]["metadata"] = {}
                events[doc["metadata"]["ttpmediaid"]]["ttpmediaid"] = [
                    self._format_time(
                        doc["metadata"]["begin_hour"],
                        doc["metadata"]["begin_min"],
                        doc["metadata"]["begin_sec"],
                    )
                ]
                events[doc["metadata"]["ttpmediaid"]]["metadata"]["title"] = doc[
                    "metadata"
                ]["title"]
                events[doc["metadata"]["ttpmediaid"]]["metadata"]["eventdate"] = doc[
                    "metadata"
                ]["eventdate"]
                events[doc["metadata"]["ttpmediaid"]]["metadata"]["language"] = doc[
                    "metadata"
                ]["language"]

                # remove the query string from the doc['metadata']['url']
                new_url = self.remove_time_attributes(doc["metadata"]["url"])

                events[doc["metadata"]["ttpmediaid"]]["metadata"]["url"] = new_url

                self.logger.debug(events[doc["metadata"]["ttpmediaid"]])

                # events[doc["metadata"]["ttpmediaid"]["metadata"]["title"]] = events[
                #     doc["metadata"]["title"]
                # ]
            # if 'url' not in events[doc['metadata']['uri']].keys():
            #    prefix,_ = (doc['metadata']['url']).split("time=")
            #    events[doc['metadata']['uri']]['url']=prefix

            ordered_times = sorted(
                set(events[doc["metadata"]["ttpmediaid"]]["ttpmediaid"]),
                reverse=False,
                key=self.sort_times,
            )
            events[doc["metadata"]["ttpmediaid"]]["ttpmediaid"] = ordered_times
        # ordered_values = []
        # for key in events:
        #     orderedValues = sorted(
        #         set(events[key]["ttpmediaid"]), reverse=False, key=sortingValue
        #     )

        #     self.logger.info(
        #         "{}:{}".format(  # noqa
        #             key, ["0s" if x == "" else x for x in orderedValues]
        #         )
        #     )
        return events
        # print("{}&time={}".format(events[key]['url'],','.join(orderedValues)))

    def _format_time(self, begin_hour, begin_min, begin_sec):
        """Format time to get a string like: 01h35m45s

        Args:
            begin_hour ([string]): [description]
            begin_min ([string]): [description]
            begin_sec ([string]): [description]

        Returns:
            [type]: [description]
        """
        display_time = ""
        if begin_hour and int(begin_hour) > 0:
            display_time = f"{begin_hour}h"
        if begin_min and int(begin_min) > 0:
            display_time += f"{begin_min}m"
        if begin_sec and int(begin_sec) > 0:
            display_time += f"{begin_sec}s"
        if not display_time:
            return "0s"
        return display_time

    def _get_hits(self, csearch: CernSearchApiClient, query, readers, size, others):
        others_dict = {}
        for item in others:
            index, value = item.split("=")
            others_dict[index] = value
        self.logger.debug(others_dict)
        return csearch.search_documents_for_player(query, readers, size, others_dict)
