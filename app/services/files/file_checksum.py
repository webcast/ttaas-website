import hashlib


def get_file_checksum(file_path: str) -> str:
    """
    Get the checksum of a file.

    :param file_path: The path to the file.
    :return: The checksum of the file.
    """
    with open(file_path, "rb") as file:
        file_hash = hashlib.md5()
        while chunk := file.read(
            8192
        ):  # https://docs.python.org/3/whatsnew/3.8.html#assignment-expressions
            file_hash.update(chunk)
        return file_hash.hexdigest()
