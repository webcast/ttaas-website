import json
import logging

import click
from flask.cli import with_appcontext

from app.models.uploads import MetadataRcodes
from app.services.mllp.api_client import ApiSpeechClientMLLP

logger = logging.getLogger("webapp.mllp_service")


@click.command()
@with_appcontext
def get_media_list():
    """
    Get the information of a video

    :return: 0 if the script ends well
    :rtype: int
    """
    logger.info("Getting list of audio and video files")
    mllp_api = ApiSpeechClientMLLP()
    result = mllp_api.get_media_list()
    logger.info(f"Found {len(result)} files")
    logger.info(result)

    return 0


@click.command()
@with_appcontext
def get_systems():
    """
    Get a list of all available Speech Recognition,
    Machine Translation, and Text-To-Speech Systems
    that can be applied to transcribe, translate, and synthesize a media file.

    :return: 0 if the script ends well
    :rtype: int
    """
    logger.info("Getting list of available transcriptions systems")
    mllp_api = ApiSpeechClientMLLP()
    result = mllp_api.get_systems_list()
    logger.info(f"Found {len(result)} systems")
    logger.info(result)

    return 0


@click.command()
@click.option("--media", help="The media ID to retrieve the information from")
@with_appcontext
def get_media_langs(media: str) -> int:
    """Get a list of all subtitle and audiotrack languages available for a given media ID.

    :return: 0 if the script ends well
    :rtype: int
    """
    logger.info("Getting available langs of a media file")
    mllp_api = ApiSpeechClientMLLP()
    result = mllp_api.get_media_langs(media)
    logger.info(result)

    return 0


@click.command()
@click.option("--media", help="The media ID to retrieve the information from")
@with_appcontext
def get_media_metadata(media: str) -> int:
    """Get metadata and media file locations for a given media ID.

    :return: 0 if the script ends well
    :rtype: int
    """
    logger.info("Getting metadata of a media file")
    mllp_api = ApiSpeechClientMLLP()
    result = mllp_api.get_media_metadata(media)
    result_json = json.loads(result)
    if result_json and result_json["rcode"] == MetadataRcodes.NO_EXISTS.value:
        logger.info("Media does not exist")
    logger.info(result)

    return 0


@click.command()
@click.option("--media", help="The media ID to retrieve the information from")
@with_appcontext
def get_upload_status(media: str) -> int:
    """Check the current status of a specific upload ID.

    :return: 0 if the script ends well
    :rtype: int
    """
    logger.info("Getting status of a media file")
    mllp_api = ApiSpeechClientMLLP()
    result = mllp_api.get_upload_status(media)
    logger.info(result)

    return 0


@click.command()
@click.option("--media", help="The media ID to retrieve the information from")
@with_appcontext
def get_upload_list(media: str) -> int:
    """Get a list of all user's uploads.

    :return: 0 if the script ends well
    :rtype: int
    """
    logger.info("Getting status of a media file")
    mllp_api = ApiSpeechClientMLLP()
    result = mllp_api.get_upload_list(media)
    logger.info(result)

    return 0


@click.command()
@click.option("--media", help="The media ID to delete")
@with_appcontext
def delete_media(media: str) -> int:
    """Delete a media by its ID

    :return: 0 if the script ends well
    :rtype: int
    """
    logger.info("Deleting a media file")
    mllp_api = ApiSpeechClientMLLP()
    result = mllp_api.delete_media(media)
    logger.info(result)

    return 0
