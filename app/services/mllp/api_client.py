import enum
import logging
from typing import Any, Dict, Optional, Union

import tlp_client
from flask import current_app

from app.api.api_config import (
    ALLOWED_DOCUMENT_FILES,
    ALLOWED_SLIDES_FILES,
    ALLOWED_SUBTITLE_FILES,
)
from app.models.uploads import AvailableLanguages

logger = logging.getLogger("webapp.mllp_service")


class ApiTypeMLLP:
    API_SPEECH = "libtlp.TLPClient.API_SPEECH"
    API_TEXT = "libtlp.TLPClient.API_TEXT"
    API_WEB = "libtlp.TLPClient.API_WEB"


class ApiMLLPMetadataStatus(enum.Enum):
    EXIST = "0"
    DOESNT_EXIST = "1"


class ApiSpeechClientMLLP:
    def __init__(
        self,
        api_user: Optional[str] = None,
        api_key: Optional[str] = None,
        substitute_user: Optional[str] = None,
    ):
        self.api_base = current_app.config["MLLP_CONFIG"]["API_BASE"]
        self.editor_base = current_app.config["MLLP_CONFIG"]["EDITOR_BASE"]
        self.api_user = current_app.config["MLLP_CONFIG"]["API_USER"]
        self.api_secret = current_app.config["MLLP_CONFIG"]["API_SECRET"]

        if api_user and api_key:
            self.api_user = api_user
            self.api_secret = api_key

        self.tlp = tlp_client.TLPSpeechClient(
            self.api_base, self.editor_base, self.api_user, self.api_secret
        )

        self.substitute_user = substitute_user
        if substitute_user == current_app.config["MLLP_ADMIN_ACCOUNT"]:
            self.substitute_user = None

    def get_media_list(self) -> list[dict]:
        """Get a list of all the media files of a user

        Args:
            substitute_user ([str|None], optional): The su (substitute user) option
            can be used by Admin users to be identified as another existing API user
            at the TLP Web Service.. Defaults to None.

        Response example:
        ```json
        [
          {
          "duration": 167,
          "id": "ttp-cern-9NgHd6f8",
          "language": "en",
          "title": "LIVE from ISR - Injection of the first pilot beams into the LHC"
        },
        ...
        ]
        ```
        Returns:
            list[dict]: A list of all the media files of a user
        """
        # cern_certificate_bundle = "/etc/ssl/certs/CERN-bundle.pem"
        logger.debug(f"SU: {self.substitute_user}")
        self.tlp.api_list(su=self.substitute_user)
        media_list = self.tlp.get_printable_response_data()
        return media_list

    def get_media_full_list(
        self,
        limit: int,
        page: int,
        title: Optional[str] = None,
        external_id: Optional[str] = None,
        sort_by: Optional[str] = None,
        date_from: Optional[str] = None,
        date_to: Optional[str] = None,
    ) -> Union[str, bytes]:
        """Get a list of all the media files of a user with their details

        Args:
            limit (int): The number of media files to return.
            page (int): The page number to return.
            substitute_user ([str|None], optional): The su (substitute user) option
            can be used by Admin users to be identified as another existing API user
            at the TLP Web Service.. Defaults to None.

        Response example:
        ```json
        [
          {
          "duration": 167,
          "id": "ttp-cern-9NgHd6f8",
          "language": "en",
          "title": "LIVE from ISR - Injection of the first pilot beams into the LHC"
        },
        ...
        ]
        ```
        Returns:
            list[dict]: A list of all the media files of a user
        """
        # cern_certificate_bundle = "/etc/ssl/certs/CERN-bundle.pem"
        logger.debug(f"SU: {self.substitute_user}")
        self.tlp.api_list_fullinfo(
            limit,
            page,
            external_id=external_id,
            title=title,
            sort=sort_by,
            date_from=date_from,
            date_to=date_to,
            su=self.substitute_user,
        )
        media_list = self.tlp.get_printable_response_data()
        return media_list

    def get_systems_list(self) -> list[dict]:
        """Get a list of all available Speech Recognition, Machine Translation,
        and Text-To-Speech Systems that can be applied to transcribe, translate,
        and synthesize a media file.

        Args:
            substitute_user (str, optional): The su (substitute user) option
            can be used by Admin users to be identified as another existing API user
            at the TLP Web Service.. Defaults to None.

        Response example:
        ```json
        [
          {
            "lang": "en",
            "topic_adaptation_time": null,
            "rtf": null,
            "description": "Adapt-1L-id CERN English ASR System [2020-12-21]",
            "topic_adaptation": false,
            "id": 150,
            "name": "CERN-Adapted English ASR System (Adapt-1L-id)"
        },
        ]
        ```

        Returns:
            list[dict]: [description]
        """
        self.tlp.api_systems(su=self.substitute_user)
        sub_list = self.tlp.get_printable_response_data()
        return sub_list

    def get_media_langs(self, media_id: str) -> Union[str, Any]:
        """Get a list of all subtitle and audiotrack languages available
        for a given media ID.

        Args:
            media_id (str): The parameter of the Media ID.
            substitute_user (str, optional): The su (substitute user) option
            can be used by Admin users to be identified as another existing
            API user at the TLP Web Service. Defaults to None.

        Response example:
        ```json
          {
          "media_lang": "en",
          "langs": [
              {
                  "audiotracks": [],
                  "system_time": null,
                  "sup_status": 0,
                  "sup_ratio": 0.0,
                  "rev_rtf": 0.0,
                  "system_rtf": null,
                  "lang_code": "es",
                  "rev_time": 0,
                  "lang_name": "Espa\u00f1ol"
              },
              {
                  "audiotracks": [],
                  "system_time": null,
                  "sup_status": 0,
                  "sup_ratio": 0.0,
                  "rev_rtf": 0.0,
                  "system_rtf": null,
                  "lang_code": "fr",
                  "rev_time": 0,
                  "lang_name": "Fran\u00e7ais"
              },
              {
                  "audiotracks": [],
                  "system_time": null,
                  "sup_status": 0,
                  "sup_ratio": 0.0,
                  "rev_rtf": 0.0,
                  "system_rtf": null,
                  "lang_code": "en",
                  "rev_time": 0,
                  "lang_name": "English"
              }
          ],
          "rcode_description": "Language list available.",
          "rcode": 0,
          "subs_locked": false
        }
        ```

        Returns:
            list[dict]: [description]
        """
        self.tlp.api_langs(media_id, su=self.substitute_user)
        sub_list = self.tlp.get_printable_response_data()
        return sub_list

    def get_media_metadata(self, media_id: str) -> Union[str, Any]:
        """Get metadata and media file locations for a given media ID.

        Args:
            media_id (str): The parameter of the Media ID.
            substitute_user (str, optional): The su (substitute user) option
            can be used by Admin users to be identified as another existing
            API user at the TLP Web Service. Defaults to None.

        Response example:
        ```json
            {
            "rcode_description": "Media list and info available.",
            "audiotracks": [],
            "media": [
                {
                    "is_url": true,
                    "type_code": 6,
                    "media_format": "pcm",
                    "location": "https://ttp.mllp.upv.es/data/8/87dae0/18ec4ba1.pcm"
                },
                {
                    "is_url": true,
                    "type_code": 3,
                    "media_format": "jpg",
                    "location": "https://ttp.mllp.upv.es/data/8/8710941cf.jpg"
                },
                {
                    "is_url": true,
                    "type_code": 0,
                    "media_format": "mp4",
                    "location": "https://ttp.mllp.upv.es/data/1f8d26ab01a36.mp4"
                }
            ],
            "mediainfo": {
                "duration": 167,
                "category": null,
                "speakers": [
                    {
                        "name": "CERN"
                    }
                ],
                "language": "en",
                "title": "LIVE from ISR - Injection of the first pilot beams into the LHC"
            },
            "rcode": 0
        }
        ```

        Returns:
            list[dict]: [description]
        """
        self.tlp.api_metadata(media_id, su=self.substitute_user)
        sub_list = self.tlp.get_printable_response_data()
        return sub_list

    def get_upload_status(self, media_id: str) -> Union[str, Any]:
        """Check the current status of a specific upload ID.

        Args:
            media_id (str): The parameter of the Media ID.
            substitute_user (str, optional): The su (substitute user) option
            can be used by Admin users to be identified as another existing
            API user at the TLP Web Service. Defaults to None.

        Response example:
        ```json
        {
            "rcode" : <int> ,
            "rcode_description" : <str> ,
            "status_code" : <int> ,
            "status_info" : <str> ,
            "error_code" : <int> ,
            "uploaded_on" : <str> ,
            "last_update" : <str> ,
            "estimated_time_complete" : <str>
        }
        ```

        Returns:
            dict: [description]
        """
        self.tlp.api_status(media_id, su=self.substitute_user)
        data = self.tlp.get_printable_response_data()
        return data

    def get_upload_list(self, media_id: Optional[str] = None) -> Union[str, Any]:
        """Get a list of all user's uploads.

        Args:
            media_id (str, optional): The parameter of the Media ID.
            substitute_user (str, optional): The su (substitute user) option
            can be used by Admin users to be identified as another existing
            API user at the TLP Web Service. Defaults to None.

        Response example:
        ```json
        [
            {
                "id": <str>,
                "object_id": <str>,
                "status_code": <int>,
                "uploaded_on": <str>,
                "last_update": <str>
            },
            ...
        ]
        ```

        Returns:
            list[dict]: [description]
        """
        self.tlp.api_uploadslist(object_id=media_id, su=self.substitute_user)
        sub_list = self.tlp.get_printable_response_data()
        return sub_list

    def get_media_download(self, media_id: str, lang: str) -> str:
        """Download current subtitles file for a given media ID and language.

        Args:
            media_id (str): The parameter of the Media ID.
            lang (str): The parameter of the language.
            substitute_user (str, optional): The su (substitute user) option
            can be used by Admin users to be identified as another existing
            API user at the TLP Web Service. Defaults to None.

        Response example:
        ```json
            {

        }
        ```

        Returns:
            list[dict]: [description]
        """
        # def api_get(self, oid, lang, form=None, seldata=None, segfilt=None,
        # session_id=None, pending_revs=None, vhash=None,
        # su=None, use_req_key=False, req_key_lifetime=1):
        self.tlp.api_get(media_id, lang, form="vtt", su=self.substitute_user)
        file = self.tlp.get_printable_response_data()
        return file

    def get_tlp_client(self) -> tlp_client.TLPSpeechClient:
        return self.tlp

    def create_and_upload_media_package(
        self,
        title: str,
        language: str,
        media_id: str,
        media_file_path: str,
        nonce: str,
    ) -> str:
        logger.info(f"Creating media package for {media_id}")
        self.create_media_package(title, language, media_id, media_file_path, nonce)
        logger.info(f"Creating media package for {media_id} -> OK")
        logger.info(f"Ingesting media package for {media_id}")
        ingest_id = self.ingest_media_package(media_id)
        logger.info(f"Ingesting media package for {media_id} -> OK")
        return ingest_id

    def set_manifest_subtitles_requests(self, language: str, media_id: str):
        english_lang = AvailableLanguages.EN.value
        english_asr_id = current_app.config["MLLP_ENGLISH_ASR_ID"]

        french_lang = AvailableLanguages.FR.value
        french_asr_id = current_app.config["MLLP_FRENCH_ASR_ID"]

        if language == english_lang:
            logger.info(
                f"Language for {media_id} is english. Requesting subtitles in english."
            )
            self.tlp.manifest_add_subtitles_request(english_lang, english_asr_id)

            if current_app.config["MLLP_REQUEST_TRANSLATION"]:
                logger.info(
                    f"Language for {media_id} is english. Requesting translation to french."
                )
                self.tlp.manifest_add_subtitles_request(french_lang)
        elif language == french_lang:
            logger.info(
                f"Language for {media_id} is french. Requesting subtitles in french."
            )
            self.tlp.manifest_add_subtitles_request(french_lang, french_asr_id)
            if current_app.config["MLLP_REQUEST_TRANSLATION"]:
                logger.info(
                    f"Language for {media_id} is french. Requesting translation to english."
                )
                self.tlp.manifest_add_subtitles_request(english_lang)

    def create_media_package(
        self,
        title: str,
        language: str,
        media_id: str,
        media_file_path: str,
        nonce: str,
    ):
        # Create the media package
        self.tlp.manifest_init(media_id)
        # Ingest to MLLP
        # Add some metadata (media language and title are mandatory)
        self.tlp.manifest_set_metadata(language=language, title=title)
        # Add video/audio file (can be a public URL/URI, also)
        self.tlp.manifest_set_main_media_file(media_file_path)
        # Enable Ingest Service's Test Mode, since we are doing a test:
        test_mode = current_app.config["MLLP_TEST_INGEST"]
        self.tlp.manifest_set_options(test_mode=test_mode)
        #
        self.set_manifest_subtitles_requests(language, media_id)
        # Set the callback URL for update notifications
        self.tlp.manifest_add_callback_function(
            current_app.config["MLLP_INGEST_CALLBACK_URL"],
            params={"media_id": media_id, "nonce": nonce},
        )

    def ingest_media_package(self, media_id: str) -> str:
        # Generate Media Package File and upload it via the /ingest/new interface:
        logger.info(f"Calling TLP ingest_new for media_id: {media_id} ...")
        self.tlp.api_ingest_new(su=self.substitute_user)
        logger.info(f"Calling TLP ingest_new for media_id: {media_id} -> OK")
        # Why to print the response? I just want the upload ID!
        upload_id = self.tlp.ret_data["id"]
        logger.debug(f"Uploaded with upload ID: {upload_id}")
        return upload_id

    def get_document_type_from_extension(self, file_extension: str):
        if file_extension in ALLOWED_DOCUMENT_FILES:
            return self.tlp.FTYPE_CODE_DOCUMENT
        if file_extension in ALLOWED_SUBTITLE_FILES:
            return self.tlp.FTYPE_CODE_SUBS
        if file_extension in ALLOWED_SLIDES_FILES:
            return self.tlp.FTYPE_CODE_SLIDES
        return None

    def create_translation_media_package(
        self,
        language: str,
        existing_media_id: str,
        nonce: str,
    ):
        # Create the media package

        self.tlp.manifest_init(existing_media_id)
        # Ingest to MLLP
        # Add some metadata (media language and title are mandatory)
        self.tlp.manifest_set_metadata(language=language)
        # Enable Ingest Service's Test Mode, since we are doing a test:
        test_mode = current_app.config["MLLP_TEST_INGEST"]
        self.tlp.manifest_set_options(test_mode=test_mode)
        #
        destination_language = self.set_manifest_translation_request(
            language, existing_media_id
        )
        # Set the callback URL for update notifications
        self.tlp.manifest_add_callback_function(
            current_app.config["MLLP_INGEST_CALLBACK_URL"],
            params={"media_id": existing_media_id, "nonce": nonce},
        )
        return destination_language

    def set_manifest_translation_request(self, language: str, media_id: str):
        english_lang = AvailableLanguages.EN.value
        french_lang = AvailableLanguages.FR.value

        logger.debug(
            f"Setting manifest translation request for media_id: {media_id} to language: {language}"
        )

        if language == english_lang:
            if current_app.config["MLLP_REQUEST_TRANSLATION"]:
                logger.info(
                    f"Language for {media_id} is english. Requesting translation to french."
                )
                self.tlp.manifest_add_subtitles_request(french_lang)
                destination_language = french_lang
        elif language == french_lang:
            if current_app.config["MLLP_REQUEST_TRANSLATION"]:
                logger.info(
                    f"Language for {media_id} is french. Requesting translation to english."
                )
                self.tlp.manifest_add_subtitles_request(english_lang)
                destination_language = english_lang
        if not destination_language:
            logger.warning(
                f"Translation is disabled in the "
                f"system ({current_app.config['MLLP_REQUEST_TRANSLATION']}) or language is not supported ({language})"
            )
        return destination_language

    def request_translation(
        self, existing_media_id: str, language: str, nonce: str
    ) -> Dict[str, Any]:
        # Generate Media Package File and upload it via the /ingest/new interface:
        logger.info(
            f"Calling TLP ingest_update for media_id: {existing_media_id} and language {language} ..."
        )
        destination_language = self.create_translation_media_package(
            existing_media_id=existing_media_id, language=language, nonce=nonce
        )
        self.tlp.api_ingest_update(su=self.substitute_user)
        logger.info(
            f"Calling TLP ingest_updated for media_id: {existing_media_id} -> OK"
        )
        # Why to print the response? I just want the upload ID!
        upload_id = self.tlp.ret_data["id"]
        logger.debug(f"Translation request made with upload ID: {upload_id}")
        return {
            "media_id": existing_media_id,
            "language_from": language,
            "language_to": destination_language,
            "upload_id": upload_id,
        }

    def get_editor_urls(self, media_id: str, first_name: str, last_name: str) -> dict:
        edit_url = self.tlp.generate_player_url(
            oid=media_id,
            author_id=self.api_user,
            author_conf=100,
            author_name=f"{first_name} {last_name}",
        )
        view_url = self.tlp.generate_player_url(
            oid=media_id,
            author_id=None,
            author_conf=100,
            author_name=f"{first_name} {last_name}",
        )
        return {"editUrl": edit_url, "viewUrl": view_url}

    def delete_media(self, media_id: str) -> str:
        # Generate Media Package File and upload it via the /ingest/new interface:
        logger.info(f"Calling TLP delete for media_id: {media_id} ...")
        self.tlp.api_ingest_delete(media_id, mode="hard", su=self.substitute_user)
        logger.info(f"Calling TLP delete for media_id: {media_id} -> OK")
        # Why to print the response? I just want the upload ID!
        upload_id = self.tlp.ret_data["id"]
        logger.debug(f"Deleted upload ID: {upload_id}")
        return upload_id
