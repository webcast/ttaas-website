import logging
from urllib.parse import parse_qs, urlparse

import requests
from requests import Response


class UrlCallbackPost:
    def __init__(self, logger: logging.Logger = None):
        """
        :param contribution:
        :param logger:
        :type logger: logging.Logger
        """
        if logger:
            self.logger = logger
        else:
            self.logger = logging.getLogger("job.mail_service")

    def extract_query_params_from_string(self, url: str) -> dict:
        """Extract the query parameters from a string.

        Args:
            url (str): _description_

        Returns:
            dict: Dict with all the query parameters
        """
        parsed_url = urlparse(url)
        captured_value = parse_qs(parsed_url.query)
        return captured_value

    def make_post_request(self, url: str, data: dict) -> Response:
        """Make a POST request to the given URL and data.

        Args:
            url (str): The URL to send the request to
            data (dict): The data to send with the request

        Returns:
            Response: The response from the request
        """
        seconds_timeout = 4
        response = requests.post(url, json=data, timeout=seconds_timeout)
        return response

    def make_callback_request(
        self, url: str, callback_type="unknown", extra_data=None
    ) -> Response:
        """Make a callback request to an URL and append a callback type.

        Args:
            url (str): The URL to send the request to
            callback_type (str, optional): The type of the callback. Defaults to "unknown".
            extra_data (dict, optional): Extra data to send with the request. Defaults to {}.
        Returns:
            Response: The response from the request
        """
        self.logger.debug(f"Sending callback request to {url}")

        data = self.extract_query_params_from_string(url)

        extra_items = extra_data if extra_data else {}

        for key, value in extra_items.items():
            data[key] = value

        data["callbackType"] = callback_type

        self.logger.debug(f"Callback data: {data}")

        response = self.make_post_request(url, data)
        return response
