import logging

from flask import current_app
from requests import HTTPError
from zoom_python_client.client_components.meeting_livestreams.meeting_livestreams_component import (
    LiveStreamDict,
)

from app.services.zoom.zoom_service import ZoomService
from app.services.zoom_rtmp_live.asr_rest_api import ASRRestApi

logger = logging.getLogger("webapp.zoom_rtmp_live_service")


class ZoomRtmpLiveServiceException(Exception):
    pass


class ZoomMeetingNotStartedException(ZoomRtmpLiveServiceException):
    pass


class ZoomRtmpLiveService:
    def get_zoom_meeting_status(self, meeting_id) -> str:
        zoom_api_client = ZoomService.get_instance()
        zoom_meeting = zoom_api_client.meetings.get_meeting(meeting_id)

        if not zoom_meeting:
            raise ZoomRtmpLiveServiceException(f"Unknown Zoom  meeting {meeting_id}")

        if zoom_meeting and zoom_meeting["status"] not in ["waiting", "started"]:
            raise ZoomRtmpLiveServiceException(
                f"Unknown status for meeting id {zoom_meeting['status']}"
            )

        if zoom_meeting and zoom_meeting["status"] == "waiting":
            raise ZoomMeetingNotStartedException("Meeting is not started")
        return zoom_meeting["status"]

    def get_zoom_meeting_token(self, meeting_id) -> str:
        zoom_api_client = ZoomService.get_instance()

        meeting_token_result = zoom_api_client.meetings.get_meeting_token(meeting_id)
        meeting_token = meeting_token_result["token"]
        return meeting_token

    def set_zoom_meeting_live_stream_settings(self, meeting_id) -> bool:
        zoom_api_client = ZoomService.get_instance()
        # SET Live streaming on the meeting
        stream_data: LiveStreamDict = {
            "stream_url": current_app.config["LIVESTREAMING_SERVER"],
            "stream_key": meeting_id,
            "page_url": current_app.config["LIVESTREAMING_URL"],
            "resolution": current_app.config["LIVESTREAMING_RESOLUTION"],
        }
        result = zoom_api_client.meeting_livestreams.update_livestream(
            meeting_id, stream_data
        )
        logger.info(f"Live streaming configuration set for meeting id: {meeting_id}")

        return result

    def start_meeting_live_streaming(self, meeting_id):
        zoom_api_client = ZoomService.get_instance()

        logger.info(f"Starting live stream for meeting id: {meeting_id}")
        result = zoom_api_client.meeting_livestreams.update_livestream_status(
            meeting_id, "start"
        )
        return result

    def connect_to_live_asr(self, meeting_id, channel=None, password=None):
        """Sets a live asr session for a Zoom meeting if active."""
        # meeting_status = self.get_zoom_meeting_status(meeting_id)

        try:
            logger.debug("Obtaining stream token from Zoom")
            meeting_token = self.get_zoom_meeting_token(meeting_id)
            logger.debug(
                f"Stream token obtained: {meeting_token[:10]}...{meeting_token[-10:]}"
            )
        except HTTPError as error:
            logger.error(f"Error obtaining stream token from Zoom: {error}")
            raise ZoomRtmpLiveServiceException(
                f"Error obtaining stream token from Zoom: {error}"
            ) from error

        logger.debug("Setting ASR live stream on the RTMP server")
        if meeting_token:
            # Send stream to ASR server
            asr_rest_api = ASRRestApi(
                current_app.config["LIVESTREAMING_SERVER"],
                current_app.config["LIVESTREAMING_RESTAPI"],
                current_app.config["LIVESTREAMING_USER"],
                current_app.config["LIVESTREAMING_APIKEY"],
                current_app.config["LIVESTREAMING_SYSTEMID"],
                logger,
            )

            response = asr_rest_api.start(
                meeting_id, meeting_token, channel=channel, password=password
            )
            logger.debug(response.json())

            logger.info(f"live streaming started for meeting id: {meeting_id}")

            return True
        return False

    def list_streaming_sessions(self):
        """0 success, 1 Error"""
        logger.info("Get streams on the system if any")

        asr_rest_api = ASRRestApi(
            current_app.config["LIVESTREAMING_SERVER"],
            current_app.config["LIVESTREAMING_RESTAPI"],
            current_app.config["LIVESTREAMING_USER"],
            current_app.config["LIVESTREAMING_APIKEY"],
            current_app.config["LIVESTREAMING_SYSTEMID"],
            logger,
        )
        response = asr_rest_api.list()
        logger.debug(response.json())
        result = response.json()
        return result

    def kill_streaming_session(self, session_id):
        logger.info(f"Killing stream {session_id}")

        # Send stream to ASR server
        asr_rest_api = ASRRestApi(
            current_app.config["LIVESTREAMING_SERVER"],
            current_app.config["LIVESTREAMING_RESTAPI"],
            current_app.config["LIVESTREAMING_USER"],
            current_app.config["LIVESTREAMING_APIKEY"],
            current_app.config["LIVESTREAMING_SYSTEMID"],
            logger,
        )
        response = asr_rest_api.killSession(session_id)
        logger.debug(response.json())
        return True
