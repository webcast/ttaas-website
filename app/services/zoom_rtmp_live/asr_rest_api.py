import json
import urllib.parse

import requests
from requests.exceptions import HTTPError


class ASRRestApi(object):
    def __init__(
        self,
        streamingserver,
        streamingapi,
        streaminguser,
        streamingkey,
        streamingsystemid,
        logger,
        streamingurl="https://cern.ch",
        streamingres="720p",
        streamingsegment=8,
    ):
        self.streamingserver = streamingserver
        self.streamingapi = streamingapi
        self.streaminguser = streaminguser
        self.streamingkey = streamingkey
        self.streamingsystemid = streamingsystemid
        self.streamingurl = streamingurl
        self.streamingres = streamingres
        self.streamingsegment = streamingsegment
        self.logger = logger

    def start(self, meetingid, cctoken, channel=None, password=None):
        """Gives response object if success  or raises an exception if HTTP response different from 200"""
        data = {
            "username": self.streaminguser,
            "api_key": self.streamingkey,
            "systemId": self.streamingsystemid,
            "segmentSize": self.streamingsegment,
            "rtmpUrl": f"zoom/{meetingid}",
            "zoomAPITokenCC": cctoken,
        }

        if channel and password:
            data["channel"] = channel
            data["password"] = password

        try:
            response = requests.post(
                f"{self.streamingapi}start",
                headers={
                    "Content-Type": "application/json;charset=utf-8",
                },
                data=json.dumps(data),
                verify=False,
            )
            response.raise_for_status()
        except HTTPError as err:
            self.logger.debug(err)
            raise
        else:
            return response

    def list(self):
        data = {
            "username": self.streaminguser,
            "api_key": self.streamingkey,
        }
        try:
            response = requests.get(
                f"{self.streamingapi}status",
                headers={
                    "Content-Type": "application/json;charset=utf-8",
                },
                params=urllib.parse.urlencode(data),
                verify=False,
            )
            response.raise_for_status()
        except HTTPError as err:
            self.logger.debug(err)
            raise
        else:
            return response

    def killSession(self, sessionid):
        data = {
            "username": self.streaminguser,
            "api_key": self.streamingkey,
            "id": sessionid,
        }
        try:
            response = requests.post(
                f"{self.streamingapi}stop",
                headers={
                    "Content-Type": "application/json;charset=utf-8",
                },
                data=json.dumps(data),
                verify=False,
            )
            response.raise_for_status()
        except HTTPError as err:
            self.logger.debug(err)
            raise
        else:
            return response
