import logging
from typing import Optional

from flask import current_app, render_template

from app.models.pre_uploads import PreUpload
from app.models.uploads import Upload
from app.services.email.mail_service import MailService


def get_mail_to(user_email: str) -> list[str]:
    """
    If it's a production instance, send mail to the creator
    of the event and inform webcast service as BCC.
    Otherwise, send an email to the MAIL_TO configuration parameter
    :return: A list with the MAIL_TO addresses
    :rtype: list[str]
    """
    if current_app.config["IS_DEV"]:
        mail_to = [user_email]
    else:
        mail_to = [user_email, current_app.config["SERVICE_MAIL"]]

    return mail_to


def populate_mail(params: dict, template: str, subject: str) -> tuple[str, str]:
    """
    Populate the email with the given params

    :return: A tuple with the subject and the email content
    :rtype: tuple[str,str]
    """
    prefix = ""
    if current_app.config.get("IS_DEV", False):
        prefix = "[TEST] "

    media_title = params["title"]
    subject = f'{prefix} [TTAAS Service] {subject} "{media_title}"'

    email_template = render_template(
        template,
        title=media_title,
        created_on=params["created_on"],
        media_id=params["media_id"],
        language=params.get("language", ""),
        completed_type=params.get("completed_type", ""),
    )

    return subject, email_template


class UploadEmailService:
    def __init__(self, logger: Optional[logging.Logger] = None):
        """
        :type contribution: app.models.events.IndicoEventContribution
        :param logger:
        :type logger:
        """
        if logger:
            self.logger = logger
        else:
            self.logger = logging.getLogger("job.upload_email")

    def send_upload_finished_notification(
        self, upload_id: int, completed_type: str
    ) -> bool:
        """
        Sends an email to the user when the upload is finished.

        :return: True|False
        :rtype: bool
        """

        upload = Upload.query.get(upload_id)
        template = "emails/upload_finished.html"

        params = {
            "title": upload.title,
            "media_id": upload.media_id,
            "language": upload.language,
            "created_on": upload.created_on,
            "completed_type": completed_type or "",
        }
        subject, message = populate_mail(params, template, f"{completed_type} finished")
        mail_service = MailService(logger=self.logger)

        mail_service.send_html_mail(
            current_app.config["MAIL_FROM"],
            get_mail_to(upload.notification_email),
            subject,
            message,
        )

        return True

    def send_captions_updated_notification(
        self, upload_id: int, language: str, timestamp: str
    ) -> bool:
        """
        Sends an email to the user when the captions are updated.

        :return: True|False
        :rtype: bool
        """

        upload = Upload.query.get(upload_id)
        template = "emails/captions_updated.html"

        params = {
            "title": upload.title,
            "media_id": upload.media_id,
            "language": language,
            "created_on": timestamp,
        }
        subject, message = populate_mail(params, template, "Captions updated")
        mail_service = MailService(logger=self.logger)

        mail_service.send_html_mail(
            current_app.config["MAIL_FROM"],
            get_mail_to(upload.notification_email),
            subject,
            message,
        )

        return True

    def send_pre_upload_error_notification(self, pre_upload_id: int) -> bool:
        """
        Sends an email to the user when the upload is finished.

        :return: True|False
        :rtype: bool
        """

        upload = PreUpload.query.get(pre_upload_id)
        template = "emails/upload_error.html"

        params = {
            "title": upload.title,
            "media_id": upload.media_id,
            "created_on": upload.created_on,
        }
        subject, message = populate_mail(params, template, "Error")
        mail_service = MailService(logger=self.logger)

        mail_service.send_html_mail(
            current_app.config["MAIL_FROM"],
            get_mail_to(upload.notification_email),
            subject,
            message,
        )

        return True

    def send_upload_error_notification(self, upload_id: int) -> bool:
        upload = Upload.query.get(upload_id)
        template = "emails/upload_error.html"

        params = {
            "title": upload.title,
            "media_id": upload.media_id,
            "created_on": upload.created_on,
        }
        subject, message = populate_mail(params, template, "Error")
        mail_service = MailService(logger=self.logger)

        mail_service.send_html_mail(
            current_app.config["MAIL_FROM"],
            get_mail_to(upload.notification_email),
            subject,
            message,
        )

        return True

    def send_pre_upload_already_exists_notification(self, pre_upload_id: int) -> bool:
        """
        Sends an email to the user when the upload already exists

        :return: True|False
        :rtype: bool
        """

        upload = PreUpload.query.get(pre_upload_id)
        template = "emails/pre_upload_already_exists.html"

        params = {
            "title": upload.title,
            "media_id": upload.media_id,
            "created_on": upload.created_on,
        }
        subject, message = populate_mail(params, template, "Error")
        mail_service = MailService(logger=self.logger)

        mail_service.send_html_mail(
            current_app.config["MAIL_FROM"],
            get_mail_to(upload.notification_email),
            subject,
            message,
        )

        return True
