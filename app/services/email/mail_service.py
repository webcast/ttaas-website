import logging
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from flask import current_app


class MailService:
    def __init__(self, logger: logging.Logger = None):
        """
        :param contribution:
        :param logger:
        :type logger: logging.Logger
        """
        if logger:
            self.logger = logger
        else:
            self.logger = logging.getLogger("job.mail_service")

    def send_html_mail(
        self, email_from: str, email_to: list[str], subject: str, html: str
    ) -> bool:
        """
        Send html formatted mail.

        :param email_from: Email address that will appear on the FROM
        :type email_from: str
        :param email_to: Email address to send the emait to
        :type email_to: list[str]
        :param subject: Subject of the email
        :type subject: str
        :param html: Content of the email
        :type html: str
        :return: True if the email was sent successfully
        :rtype: bool
        """
        self.logger.info("Sending EMAIL...")
        # Create message container - the correct MIME type is multipart/alternative.
        msg = MIMEMultipart("alternative")
        msg["Subject"] = subject
        msg["From"] = email_from
        # set only the real receipent of the message (without BCC etc)
        msg["To"] = ", ".join(email_to)

        # encode html message

        body = MIMEText(html, "html", "utf-8")
        msg.attach(body)
        sender = smtplib.SMTP(current_app.config["MAIL_HOSTNAME"])

        sender.sendmail(email_from, email_to, msg.as_string())
        sender.quit()
        return True
