import logging

from flask import current_app
from requests import Timeout
from zoom_python_client.utils.logger import setup_logs as zoom_setup_logs
from zoom_python_client.zoom_api_client import ZoomApiClient

from app.daos.api_tokens import ApiTokenException

logger = logging.getLogger("webapp.zoom_client")


class ZoomService:
    @classmethod
    def get_instance(cls) -> ZoomApiClient:
        log_level = logging.DEBUG if current_app.config["IS_DEV"] else logging.INFO
        zoom_setup_logs(log_level=log_level)
        client = ZoomApiClient(
            account_id=current_app.config["ZOOM_ACCOUNT_ID"],
            client_id=current_app.config["ZOOM_CLIENT_ID"],
            client_secret=current_app.config["ZOOM_CLIENT_SECRET"],
            use_path=current_app.config["ZOOM_TOKEN_PATH"],
        )
        return client

    @classmethod
    def get_zoom_meeting_title_password(cls, meeting_id: str) -> dict:
        data_result = {
            "password": None,
            "title": None,
        }
        try:
            zoom_client = ZoomService.get_instance()
            result = zoom_client.meetings.get_meeting(meeting_id)
            try:
                data_result["password"] = result["h323_password"]
                data_result["title"] = result["topic"]
                logger.debug(
                    f"Got password for meeting '{data_result['title']}' ({meeting_id})"
                )
            except KeyError:
                pass
            except Timeout as error:
                raise ApiTokenException(
                    f"Unable to connect. Unable to get the meeting details {str(error)}"
                ) from error
            return data_result
        except ApiTokenException as error:
            raise ApiTokenException(
                f"Unable to connect. Unable to connect to the Zoom API: {str(error)}"
            ) from error
