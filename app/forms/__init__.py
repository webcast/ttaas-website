from datetime import date

from flask_wtf import FlaskForm
from wtforms import SelectField, StringField
from wtforms.validators import DataRequired, InputRequired

from app.models.accounts import Account
from app.models.token_blacklist import ApiTokenResources


class MetricsForm(FlaskForm):
    first_year = 2022
    choices = range(first_year, date.today().year + 1)

    selected_year = SelectField(
        "Select the metrics for the following year",
        choices=choices,
        validators=[InputRequired()],
    )


class ApiKeyForm(FlaskForm):
    token_name = StringField("Token name", validators=[DataRequired()])
    choices = [(item.value, item.name) for item in ApiTokenResources]

    account = SelectField(
        "Select an account",
        validators=[InputRequired()],
    )

    scope = SelectField(
        "Select an scope",
        choices=choices,
        validators=[InputRequired()],
    )

    def __init__(self):
        super(ApiKeyForm, self).__init__()
        self.account.choices = [
            (account.id, account) for account in Account.query.all()
        ]
