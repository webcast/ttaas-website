import logging
from urllib.parse import urlencode

from flask import current_app, redirect, render_template, request, session, url_for
from flask_login import logout_user

from app.views.blueprints import authentication_blueprint

logger = logging.getLogger("webapp")


@authentication_blueprint.route("/logout/")
def logout():
    """View to logout the user

    Returns:
        Response: The flask response object
    """
    if not current_app.config["OIDC_LOGOUT_URL"]:
        logout_user()
        return redirect(url_for("authentication.login"))
    logger.debug(request.args)
    next_url = request.url_root + url_for("authentication.login")
    query = urlencode({"post_logout_redirect_uri": next_url})
    logout_user()
    return redirect(current_app.config["OIDC_LOGOUT_URL"] + "?" + query)


@authentication_blueprint.route("/login/")
@authentication_blueprint.route("/login/callback")
def login():
    """Displays the login page

    Returns:
        Response: The flask response object
    """
    session["next_url"] = request.args.get("next")
    return render_template("login.html")
