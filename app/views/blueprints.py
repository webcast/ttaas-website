from flask import Blueprint


def _blueprint_factory(partial_module_string, url_prefix):
    """
    Generates blueprint objects for view modules.

    :param partial_module_string:  String representing a view module without the absolute
    path (e.g. 'home.index' for pypi_portal.views.home.index).
    :param url_prefix: URL prefix passed to the blueprint.
    :return: Blueprint instance for a view module.
    """
    name = partial_module_string
    import_name = f"app.views.{partial_module_string}"
    blueprint = Blueprint(name, import_name, url_prefix=url_prefix)
    return blueprint


authentication_blueprint = _blueprint_factory("authentication", "")


all_blueprints = (authentication_blueprint,)
