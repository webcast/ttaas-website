from app.views.admin_protected_view import AdminProtectedView


class AccountListView(AdminProtectedView):
    # Template to be used on the list view
    # Searchable fields on the list view
    column_searchable_list = [
        "id",
        "role",
        "su_account",
        "account_name",
        "created_on",
        "last_used",
        "revoked",
        "revoked_on",
    ]
    column_default_sort = ("created_on", True)
    column_sortable_list = (
        "id",
        "role",
        "su_account",
        "account_name",
        "created_on",
        "last_used",
        "revoked",
        "revoked_on",
    )

    # # List of columns displayed on the list view
    column_list = [
        "id",
        "role",
        "su_account",
        "account_name",
        "created_on",
        "last_used",
        "revoked",
        "revoked_on",
    ]
