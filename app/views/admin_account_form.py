from app.views.admin_account_list import AccountListView
from app.views.admin_protected_view import AdminProtectedView


class AccountFormView(AdminProtectedView):
    form_excluded_columns = ["uploads", "api_tokens"]


class AccountModelView(AccountFormView, AccountListView):
    list_template = "admin/account_list.html"
