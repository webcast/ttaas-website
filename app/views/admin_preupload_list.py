from app.views.admin_protected_view import AdminProtectedView


class PreUploadListView(AdminProtectedView):
    # Template to be used on the list view
    # Searchable fields on the list view
    column_searchable_list = [
        "title",
        "media_id",
        "existing_media_id",
        "state",
        "username",
        "created_on",
        "updated_on",
    ]
    column_default_sort = ("created_on", True)
    column_sortable_list = (
        "title",
        "media_id",
        "existing_media_id",
        "state",
        "username",
        "created_on",
        "updated_on",
    )

    # # List of columns displayed on the list view
    column_list = [
        "title",
        "media_id",
        "existing_media_id",
        "state",
        "account",
        "username",
        "created_on",
        "updated_on",
    ]


class PreUploadModelView(PreUploadListView):
    pass
