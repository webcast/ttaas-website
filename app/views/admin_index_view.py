from datetime import date

import flask_admin as admin
from flask import current_app, flash, redirect, request, session, url_for
from flask_admin import expose
from flask_login import current_user

from app.forms import MetricsForm
from app.utils.metrics import get_metrics


class ProtectedAdminIndexView(admin.AdminIndexView):
    @expose(
        "/",
        methods=(
            "GET",
            "POST",
        ),
    )
    def index(self):

        if not current_user.is_authenticated:
            return redirect(url_for("authentication.login"))

        if session.get("next_url"):
            next_url = session.get("next_url")
            session.pop("next_url", None)
            return redirect(next_url)

        metrics_form = MetricsForm()
        selected_year = date.today().year

        if request.method == "POST" and metrics_form.validate_on_submit():
            selected_year = metrics_form.selected_year.data

            flash(f"Displaying stats for year {selected_year} ")

        metrics = get_metrics(year=selected_year)
        asr_instance = current_app.config["MLLP_CONFIG"]["API_BASE"]
        db_instance = current_app.config["DB_SERVICE_NAME"]
        search_instance = current_app.config["CERNSEARCH_INSTANCE"]
        translations_enabled = current_app.config["MLLP_REQUEST_TRANSLATION"]

        return self.render(
            "admin/index.html",
            metrics_form=metrics_form,
            metrics=metrics,
            selected_year=selected_year,
            asr_instance=asr_instance,
            db_instance=db_instance,
            search_instance=search_instance,
            translations_enabled=translations_enabled,
        )
