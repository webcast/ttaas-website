from app.views.admin_protected_view import AdminProtectedView


class UploadListView(AdminProtectedView):
    # Template to be used on the list view
    # Searchable fields on the list view
    column_searchable_list = [
        "account.su_account",
        "media_id",
        "title",
        "username",
        "created_on",
    ]
    column_default_sort = ("created_on", True)
    column_sortable_list = (
        "id",
        "title",
        "media_id",
        "state",
        "created_on",
        "updated_on",
        "upload_type",
        "username",
    )

    # # List of columns displayed on the list view
    column_list = [
        "id",
        "media_id",
        "title",
        "state",
        "account",
        "created_on",
        "updated_on",
        "upload_type",
        "username",
    ]
