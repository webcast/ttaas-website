from flask_admin.contrib.sqla.form import InlineOneToOneModelConverter
from flask_admin.model.form import InlineFormAdmin

from app.models.cern_search_settings import CernSearchSettings
from app.views.admin_protected_view import AdminProtectedView
from app.views.admin_upload_list import UploadListView


class MyInlineModelForm(InlineFormAdmin):
    form_columns = (
        "cern_search_state",
        "is_public",
        "read_groups",
        "read_users",
        "media_datetime",
    )
    inline_converter = InlineOneToOneModelConverter


class UploadFormView(AdminProtectedView):
    form_excluded_columns = ["uploads", "captions_edits", "state_logs"]
    inline_models = (MyInlineModelForm(CernSearchSettings),)


class UploadModelView(UploadFormView, UploadListView):
    edit_template = "admin/upload_edit.html"
    pass
