import React, { useEffect } from "react";
import Main from "Main";
import AuthService from "services/auth-service";

export default function MainAuth() {
  const [initialized, setInitialized] = React.useState(false);

  useEffect(() => {
    const onLoadCallback = () => {
      console.log("Keycloak initialized");
      setInitialized(true);
    };
    console.log("Initializing Keycloak...");
    AuthService.initKeycloak(onLoadCallback);
  }, []);
  return <Main initialized={initialized} />;
}
