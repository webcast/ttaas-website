import { useQuery } from "react-query";
import apiClient from "api/api-headers";
import AuthService from "services/auth-service";
import { IAccount } from "types/account";

export const fetchAccounts = async () => {
  const { data } = await apiClient.get<IAccount[]>(`/accounts/`);
  return data;
};

export function useAcounts(options = {}) {
  return useQuery<IAccount[], Error>("accounts", () => fetchAccounts(), {
    ...options,
    enabled: AuthService.isLoggedIn(),
  });
}
