import { useQuery } from "react-query";
import apiClient from "api/api-headers";
import { IMessage } from "types/message";

export const fetchMessages = async () => {
  const { data } = await apiClient.get<IMessage[]>(`/messages/`);
  return data;
};

export function useMessages(options = {}) {
  return useQuery<IMessage[], Error>("messages", () => fetchMessages(), {
    ...options,
  });
}
