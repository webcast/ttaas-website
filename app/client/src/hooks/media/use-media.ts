import { useMutation, useQuery } from "react-query";
import apiClient from "api/api-headers";

export const fetchMedia = async (
  accountId: number | undefined,
  perPage: number = 1,
  page: number = 1,
  title: string = "",
  sort: string = "date",
  dateFrom: string = "",
  dateTo: string = "",
  mediaId: string = "",
) => {
  let titleParam = "";
  if (title !== "") {
    titleParam = `&title=${title}`;
  }
  let fromParam = "";
  if (dateFrom !== "") {
    fromParam = `&from=${dateFrom}`;
  }
  let toParam = "";
  if (dateTo !== "") {
    toParam = `&to=${dateTo}`;
  }
  const sortParam = `&sort=${sort}`;
  let idParam = "";
  if (mediaId !== "") {
    idParam = `&external_id=${mediaId}`;
  }
  const { data } = await apiClient.get<any>(
    `/transcriptions/account/${accountId}/media/?limit=${perPage}&page=${page}${titleParam}${idParam}${sortParam}${fromParam}${toParam}`,
  );
  return data;
};

export const fetchMediaDetails = async (
  accountId: number | undefined,
  mediaId: string | undefined,
) => {
  const { data } = await apiClient.get<any>(
    `/transcriptions/account/${accountId}/media/${mediaId}/`,
  );
  return data;
};

export const fetchTranscriptionSystems = async (
  accountId: number | undefined,
) => {
  const { data } = await apiClient.get<any>(
    `/transcriptions/account/${accountId}/systems/`,
  );
  return data;
};

export const fetchMediaStatus = async (
  accountId: number | undefined,
  mediaId: string | undefined,
) => {
  const { data } = await apiClient.get<any>(
    `/transcriptions/account/${accountId}/media/${mediaId}/status/`,
  );
  return data;
};

export const fetchMediaLanguages = async (
  accountId: number | undefined,
  mediaId: string | undefined,
) => {
  const { data } = await apiClient.get<any>(
    `/transcriptions/account/${accountId}/media/${mediaId}/languages/`,
  );
  return data;
};

export const downloadTranscriptionFile = async (
  accountId: number | undefined,
  mediaId: string | undefined,
  language: string | undefined,
) => {
  const { data } = await apiClient.get<any>(
    `/transcriptions/account/${accountId}/media/${mediaId}/languages/${language}/`,
  );
  return data;
};

export const fetchEditorUrl = async (
  accountId: number | undefined,
  mediaId: string | undefined,
) => {
  const { data } = await apiClient.get<any>(
    `/transcriptions/account/${accountId}/media/${mediaId}/editor/`,
  );
  return data;
};

export function useYourMedia(
  accountId: number | undefined,
  perPage = 1,
  page = 1,
  title = "",
  sort = "date",
  dateFrom = "",
  dateTo = "",
  mediaId = "",
  options = {},
) {
  return useQuery<any, Error>(
    [
      "your-media",
      accountId,
      { perPage, page, title, sort, dateFrom, dateTo, mediaId },
    ],
    () =>
      fetchMedia(
        accountId,
        perPage,
        page,
        title,
        sort,
        dateFrom,
        dateTo,
        mediaId,
      ),
    {
      enabled: !!accountId,
      ...options,
    },
  );
}

export function useMediaDetails(
  accountId: number | undefined,
  mediaId: string | undefined,
  options = {},
) {
  return useQuery<any, Error>(
    ["your-media", mediaId],
    () => fetchMediaDetails(accountId, mediaId),
    {
      enabled: !!mediaId && !!accountId,
      ...options,
    },
  );
}

export function useMediaStatus(
  accountId: number | undefined,
  mediaId: string | undefined,
  options = {},
) {
  return useQuery<any, Error>(
    ["upload-status", mediaId],
    () => fetchMediaStatus(accountId, mediaId),
    {
      enabled: !!mediaId && !!accountId,
      ...options,
    },
  );
}

export function useMediaLanguages(
  accountId: number | undefined,
  mediaId: string | undefined,
  options = {},
) {
  return useQuery<any, Error>(
    ["media-languages", mediaId],
    () => fetchMediaLanguages(accountId, mediaId),
    {
      enabled: !!mediaId && !!accountId,
      ...options,
    },
  );
}

export function useTranscriptionsSystems(accountId: number, options = {}) {
  return useQuery<any, Error>(
    ["transcription-systems"],
    () => fetchTranscriptionSystems(accountId),
    {
      ...options,
    },
  );
}

interface DownloadTranscriptionProps {
  accountId: number | undefined;
  mediaId: string | undefined;
  language: string | undefined;
}

export function useDownloadTranscription(options = {}) {
  return useMutation(
    ({ accountId, mediaId, language }: DownloadTranscriptionProps) =>
      downloadTranscriptionFile(accountId, mediaId, language),
    {
      ...options,
    },
  );
}

export function useEditorUrl(
  accountId: number | undefined,
  mediaId: string | undefined,
  options = {},
) {
  return useQuery<any, Error>(
    ["editor-url", mediaId],
    () => fetchEditorUrl(accountId, mediaId),
    {
      enabled: !!mediaId && !!accountId,
      ...options,
    },
  );
}
