import { useMutation, useQuery } from "react-query";
import apiClient from "api/api-headers";
import queryClient from "api/query-client";

interface UpdateMutationProps {
  accountId: number | undefined;
  uploadId: number | undefined;
}

export const fetchUploads = async (
  accountId: number | undefined,
  perPage: number = 1,
  page: number = 1,
  sortBy: string = "created_on",
  direction: string = "desc",
) => {
  const { data } = await apiClient.get<any>(
    `/uploads/account/${accountId}/?limit=${perPage}&page=${page}&sortBy=${sortBy}&direction=${direction}`,
  );
  return data;
};

export const fetchOtherUploads = async (
  perPage: number = 1,
  page: number = 1,
  sortBy: string = "created_on",
  direction: string = "desc",
) => {
  const { data } = await apiClient.get<any>(
    `/uploads/user/?limit=${perPage}&page=${page}&sortBy=${sortBy}&direction=${direction}`,
  );
  return data;
};

export function useUploads(
  accountId: number | undefined,
  perPage = 1,
  page = 1,
  sortBy = "created_on",
  direction = "desc",
  options = {},
) {
  return useQuery<any, Error>(
    ["uploads", accountId, perPage, page, sortBy, direction],
    () => fetchUploads(accountId, perPage, page, sortBy, direction),
    {
      enabled: !!accountId,
      ...options,
    },
  );
}

export function useOtherUploads(
  perPage = 1,
  page = 1,
  sortBy = "created_on",
  direction = "desc",
  options = {},
) {
  return useQuery<any, Error>(
    ["uploads", perPage, page, sortBy, direction],
    () => fetchOtherUploads(perPage, page, sortBy, direction),
    {
      ...options,
    },
  );
}

export const fetchIsMediaFileAvaliable = async (
  uploadId: number | undefined,
  accountId: number | undefined,
) => {
  const { data } = await apiClient.get<any>(
    `/uploads/account/${accountId}/uploads/${uploadId}/media-file/`,
  );
  return data;
};

export function useIsMediaFileAvailable(
  uploadId: number | undefined,
  accountId: number | undefined,
  options = {},
) {
  return useQuery<any, Error>(
    ["available", uploadId],
    () => fetchIsMediaFileAvaliable(uploadId, accountId),
    {
      enabled: !!uploadId && !!accountId,
      ...options,
    },
  );
}

export const useRetryIngest = () => {
  return useMutation(
    ({ accountId, uploadId }: UpdateMutationProps) =>
      apiClient.post<any>(
        `/uploads/account/${accountId}/uploads/${uploadId}/retry-ingest/`,
      ),
    {
      onSuccess: () => {
        queryClient.invalidateQueries(["uploads"]);
      },
      onError: () => {
        // queryClient.invalidateQueries(["uploads"]);
      },
    },
  );
};
