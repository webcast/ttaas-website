import { useQuery } from "react-query";
import apiClient from "api/api-headers";

export const fetchPreUploads = async (
  accountId: number | undefined,
  perPage: number = 1,
  page: number = 1,
  sortBy: string = "created_on",
  direction: string = "desc",
) => {
  const { data } = await apiClient.get<any>(
    `/pre-uploads/account/${accountId}/?limit=${perPage}&page=${page}&sortBy=${sortBy}&direction=${direction}`,
  );
  return data;
};

export function usePreUploads(
  accountId: number | undefined,
  perPage = 1,
  page = 1,
  sortBy = "created_on",
  direction = "desc",
  options = {},
) {
  return useQuery<any, Error>(
    ["pre-uploads", accountId, perPage, page, sortBy, direction],
    () => fetchPreUploads(accountId, perPage, page, sortBy, direction),
    {
      enabled: !!accountId,
      ...options,
    },
  );
}
