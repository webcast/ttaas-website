import { useMutation, useQuery } from "react-query";
import apiClient from "api/api-headers";
import { ITokenFormFields } from "types/token";

export const fetchApiTokens = async (account: number) => {
  const { data } = await apiClient.get<any>(`/api-tokens/account/${account}/`);
  return data;
};

export function useApiTokens(account: number) {
  return useQuery<any, Error>("api-tokens", () => fetchApiTokens(account));
}

export const useAddApiToken = (options = {}) => {
  return useMutation(
    (newToken: ITokenFormFields) =>
      apiClient.post(`/api-tokens/account/${newToken.account}/`, newToken),
    {
      ...options,
    },
  );
};

export const useDeleteApiToken = (id: number, options = {}) => {
  return useMutation(() => apiClient.delete(`/api-tokens/${id}`), {
    ...options,
  });
};

export const useRefreshApiToken = (id: number, options = {}) => {
  return useMutation(() => apiClient.put(`/api-tokens/${id}`, {}), {
    ...options,
  });
};
