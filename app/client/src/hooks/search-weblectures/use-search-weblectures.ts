import { useQuery } from "react-query";
import apiClient from "api/api-headers";

export const fetchSearchWeblectures = async (
  perPage: number = 1,
  page: number = 1,
  title: string = "",
  // sort: string = "date",
  dateFrom: string = "",
  dateTo: string = "",
  text: string = "",
  language: string = "",
) => {
  const tempParams = [];
  if (title !== "") {
    tempParams.push(`title=${title}`);
  }
  if (dateFrom !== "") {
    tempParams.push(`from=${dateFrom}`);
  }
  if (dateTo !== "") {
    tempParams.push(`to=${dateTo}`);
  }
  // const sortParam = `&sort=${sort}`;
  if (text !== "") {
    tempParams.push(`text=${text}`);
  }
  if (language !== "") {
    tempParams.push(`language=${language}`);
  }

  const { data } = await apiClient.get<any>(
    `/search-weblectures/?limit=${perPage}&page=${page}&${tempParams.join(
      "&",
    )}`,
  );
  return data;
};

export function useSearchWeblectures(
  perPage = 1,
  page = 1,
  title = "",
  // sort = "date",
  dateFrom = "",
  dateTo = "",
  text = "",
  language = "en",
  options = {},
) {
  return useQuery<any, Error>(
    ["weblectures", { perPage, page, title, dateFrom, dateTo, text, language }],
    () =>
      fetchSearchWeblectures(
        perPage,
        page,
        title,
        // sort,
        dateFrom,
        dateTo,
        text,
        language,
      ),
    {
      enabled: !!text,
      ...options,
    },
  );
}

export const fetchSearchWeblectureDetails = async (
  ttpMediaId: string | undefined,
  text: string = "",
) => {
  const tempParams = [];

  if (text !== "") {
    tempParams.push(`text=${text}`);
  }

  const { data } = await apiClient.get<any>(
    `/search-weblectures/${ttpMediaId}?${tempParams.join("&")}`,
  );

  return data;
};

export function useSearchWeblectureDetails(
  ttpMediaId: string | undefined,
  text: string = "",
  options = {},
) {
  return useQuery<any, Error>(
    ["weblectures", ttpMediaId, { text }],
    () => fetchSearchWeblectureDetails(ttpMediaId, text),
    {
      enabled: !!ttpMediaId,
      ...options,
    },
  );
}
