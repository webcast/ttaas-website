import { useState } from "react";
import { AppContextType } from "contexts/AppContext";
import { IAccount } from "types/account";
import { IUser } from "types/user";

export function useAppContext(): AppContextType {
  const [user, setUser] = useState<IUser | null>(null);
  const [selectedAccount, setSelectedAccount] = useState<IAccount | null>(null);

  const setUserContext = (value: IUser | null) => {
    setUser(value);
  };
  const setSelectedAccountContext = (account: IAccount | null) => {
    setSelectedAccount(account);
  };

  return { user, selectedAccount, setUserContext, setSelectedAccountContext };
}

export default useAppContext;
