import { useMutation, useQuery } from "react-query";
import apiClient from "api/api-headers";

export const fetchIsMeetingLive = async (meetingId: string | undefined) => {
  const { data } = await apiClient.get<any>(
    `/zoom-rtmp-live/meeting/${meetingId}/`,
  );
  return data;
};

export function useIsZoomMeetingLive(
  meetingId: string | undefined,
  enableQuery: boolean,
  options = {},
) {
  return useQuery<any, Error>(
    ["isMeetingLive", meetingId],
    () => fetchIsMeetingLive(meetingId),
    {
      ...options,
      enabled: !!enableQuery && !!meetingId,
    },
  );
}

export const fetchLiveAsrSessionsCount = async () => {
  const { data } = await apiClient.get<any>(`/zoom-rtmp-live/sessions/count/`);
  return data;
};

export function useGetLiveAsrSessionsCount(options = {}) {
  return useQuery<any, Error>(
    ["liveSessionsCount"],
    () => fetchLiveAsrSessionsCount(),
    {
      ...options,
    },
  );
}

interface UpdateMutationProps {
  meetingId: string | undefined;
  channel?: string | undefined;
  password?: string | undefined;
}

export const useSetLiveStreamSettings = () => {
  return useMutation(({ meetingId }: UpdateMutationProps) =>
    apiClient.put<any>(`/zoom-rtmp-live/meeting/${meetingId}/`),
  );
};

export const useStartAsrSession = () => {
  return useMutation(
    ({ meetingId, channel, password }: UpdateMutationProps) => {
      let apiUrl = `/zoom-rtmp-live/sessions/${meetingId}/`;
      if (channel && channel !== "" && password && password !== "") {
        apiUrl = `/zoom-rtmp-live/sessions/${meetingId}/?channel=${channel}&password=${password}`;
      }

      return apiClient.post<any>(apiUrl);
    },
  );
};
