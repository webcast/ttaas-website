import { useQuery } from "react-query";
import apiClient from "../../api/api-headers";

export const fetchUser = async () => {
  const { data } = await apiClient.get<any>("/user/");
  return data;
};
export function useUser() {
  return useQuery<any, Error>("user", fetchUser);
}
