import { useUser } from "./user-user";
import { QueryClientWrapper, renderHook, waitFor } from "test-utils";

describe("useUser Hook tests", () => {
  it("Gets the user details", async () => {
    const { result } = renderHook(() => useUser(), {
      wrapper: QueryClientWrapper,
    });
    // If this returns allways null, it was due to the react-query version
    await waitFor(() => result.current.isSuccess);

    const user = result.current.data;
    expect(user).toHaveProperty("username");
    expect(user).toHaveProperty("lastName");
    expect(user).toHaveProperty("firstName");
    expect(user).toHaveProperty("email");
    expect(user).toHaveProperty("roles");
  });
});
