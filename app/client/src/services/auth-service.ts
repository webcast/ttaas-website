import keycloak from "keycloak";

const initKeycloak = (onLoadCallback: any) => {
  console.log("Service: Initializing keycloak...");
  keycloak.onReady = (authenticated: any) => {
    onLoadCallback(authenticated);
  };
  keycloak
    .init({
      onLoad: "login-required",
      silentCheckSsoRedirectUri: encodeURI(
        `${window.location.origin}/silent-check-sso.html`,
      ),
    })
    .then((authenticated: any) => {
      if (!authenticated) {
        console.log("User is not authenticated");
      }
    })
    .catch(console.error);
};

const doLogin = keycloak.login;

const doLogout = keycloak.logout;

const getToken = () => keycloak.token;

const isLoggedIn = () => !!keycloak.token;

const updateToken = (successCallback: any) =>
  keycloak.updateToken(30).then(successCallback).catch(doLogin);

const getUsername = () => {
  const tokenParsed = keycloak.tokenParsed as any;

  return tokenParsed?.preferred_username;
};

const getFirstName = () => {
  const tokenParsed = keycloak.tokenParsed as any;

  return tokenParsed?.given_name;
};

const getLastName = () => {
  const tokenParsed = keycloak.tokenParsed as any;

  return tokenParsed?.family_name;
};

const getEmail = () => {
  const tokenParsed = keycloak.tokenParsed as any;

  return tokenParsed?.email;
};

const hasRole = (roles: string[]) => {
  console.log("Service: Checking roles...", roles);
  const userHasRole = roles.some((role: any) => keycloak.hasResourceRole(role));
  console.log("Service: Has role?", userHasRole);
  return userHasRole;
};

export default {
  initKeycloak,
  doLogin,
  doLogout,
  isLoggedIn,
  getToken,
  updateToken,
  getUsername,
  getFirstName,
  getLastName,
  getEmail,
  hasRole,
};
