/* eslint-disable @typescript-eslint/no-explicit-any */
import React from "react";
import { QueryClientProvider, QueryClient } from "react-query";
import { MemoryRouter } from "react-router-dom";
import { render as testingLibraryRender } from "@testing-library/react";
import { AppContext } from "contexts/AppContext";

export async function asyncForEach(array: [], callback: any) {
  for (let index = 0; index < array.length; index += 1) {
    // eslint-disable-next-line no-await-in-loop
    await callback(array[index], index, array);
  }
}

export const createKeycloakStub = (
  initialized: boolean,
  authenticated: boolean,
) => ({
  initialized,
  keycloak: {
    authenticated,
  },
});

export const createAuthMockStub = (
  initialized: boolean,
  isAuthenticated: boolean,
) => ({
  initialized,
  isAuthenticated,
});

const tempQueryClient = new QueryClient({
  defaultOptions: {
    queries: {
      retry: false,
    },
  },
});

export function QueryClientWrapper({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <QueryClientProvider client={tempQueryClient}>
      {children}
    </QueryClientProvider>
  );
}

function renderWithQueryClient(ui: any) {
  return testingLibraryRender(ui, { wrapper: QueryClientWrapper });
}

function renderWithAllProviders(ui: any, { providerProps }: any) {
  function Wrapper({ children }: { children: React.ReactNode }) {
    return (
      <AppContext.Provider value={providerProps}>
        <MemoryRouter>
          <QueryClientProvider client={tempQueryClient}>
            {children}
          </QueryClientProvider>
        </MemoryRouter>
      </AppContext.Provider>
    );
  }
  return testingLibraryRender(ui, { wrapper: Wrapper });
}
// re-export everything
// eslint-disable-next-line import/no-extraneous-dependencies
export * from "@testing-library/react";

// override render method
export { renderWithQueryClient, renderWithAllProviders };
