import React, { useState } from "react";
import { toast } from "react-toastify";
import { ActionIcon, Button, Group, Modal, Text } from "@mantine/core";
import { Refresh } from "tabler-icons-react";
import MessageCopyToken from "../MessageCopyToken/MessageCopyToken";
import queryClient from "api/query-client";
import { useRefreshApiToken } from "hooks/api-tokens/use-api-tokens";

interface IProps {
  apiToken: any;
}

export default function DeleteAPIKeyForm({ apiToken }: IProps) {
  const [modalOpen, setModalOpen] = useState(false);
  const [token, setToken] = useState<string | boolean>(false);
  const { mutate: deleteToken, isLoading } = useRefreshApiToken(apiToken.id, {
    onSuccess: (response: any) => {
      queryClient.invalidateQueries("api-tokens");
      toast.success("Api token has been refreshed");
      setToken(response.data.accessToken);
    },
    onError: (error: Error) => {
      toast.error(`Unable to refresh the API token: ${error.message}`);
      setModalOpen(false);
    },
  });
  const handleModalClose = () => {
    setModalOpen(false);
  };

  const refreshApiTokenAction = async () => {
    deleteToken();
  };

  return (
    <div>
      <ActionIcon
        title="Refresh API token"
        color="orange"
        onClick={() => setModalOpen(true)}
      >
        <Refresh />
      </ActionIcon>
      <Modal
        opened={modalOpen}
        onClose={() => setModalOpen(false)}
        title="Refresh API token"
      >
        <div>
          <Group>
            {token && <MessageCopyToken token={token} />}
            <Text>
              Are you sure that you want to refresh the API token{" "}
              <strong>{apiToken.name}?</strong> This action cannot be undone.
              You will need to update the token wherever you are using it
            </Text>
            <Button
              onClick={handleModalClose}
              disabled={isLoading}
              color="gray"
            >
              Close
            </Button>
            <Button
              color="red"
              type="submit"
              loading={isLoading}
              onClick={refreshApiTokenAction}
            >
              Confirm refresh
            </Button>
          </Group>
        </div>
      </Modal>
    </div>
  );
}
