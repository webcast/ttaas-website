import React, { useEffect } from "react";
import { Alert, Group, Loader, Paper, Table, Title } from "@mantine/core";
import AddAPIKeyForm from "../AddAPIKeyForm/AddAPIKeyForm";
import DeleteAPIKeyForm from "../DeleteAPIKeyForm/DeleteAPIKeyForm";
import RefreshAPIKeyForm from "../RefreshAPIKeyForm/RefreshAPIKeyForm";
import { useApiTokens } from "hooks/api-tokens/use-api-tokens";
import { IAccount } from "types/account";

interface IProps {
  account: IAccount;
}

export default function ApiKeysTable({ account }: IProps) {
  const {
    data: apiTokens,
    error,
    isFetching,
    refetch,
  } = useApiTokens(account.id);

  useEffect(() => {
    refetch();
  }, [account.id, refetch]);

  return (
    <Group direction="column" grow>
      <Title order={4}>API Tokens</Title>
      <AddAPIKeyForm account={account} />
      <Paper shadow="xs" p="md">
        {error && (
          <Alert color="red" title="Unable to fetch API tokens">
            {error.message}
          </Alert>
        )}
        {isFetching && (
          <Alert color="blue" title="Loading">
            <Loader /> Loading API tokens
          </Alert>
        )}
        {apiTokens && apiTokens.length === 0 && (
          <Alert color="blue">You don&apos;t have any API tokens yet.</Alert>
        )}
        {apiTokens && apiTokens.length > 0 && (
          <Table>
            <thead>
              <tr>
                <th>Name</th>
                <th>Scope</th>
                <th>Last used</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              {apiTokens.map((apiToken: any) => (
                <tr key={apiToken.id}>
                  <td>
                    <strong>{apiToken.name}</strong>
                  </td>
                  <td>{apiToken.resource}</td>
                  <td>
                    {apiToken.lastUsed ? apiToken.lastUsed : "Never used"}
                  </td>
                  <td>
                    <Group>
                      <RefreshAPIKeyForm apiToken={apiToken} />
                      <DeleteAPIKeyForm apiToken={apiToken} />
                    </Group>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        )}
      </Paper>
    </Group>
  );
}
