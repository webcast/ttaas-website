import React, { useState } from "react";
import { toast } from "react-toastify";
import {
  Button,
  Group,
  Modal,
  Select,
  TextInput,
  Text,
  Space,
  Badge,
} from "@mantine/core";
import { useForm } from "@mantine/form";
import { Plus } from "tabler-icons-react";
import MessageCopyToken from "../MessageCopyToken/MessageCopyToken";
import queryClient from "api/query-client";
import { useAddApiToken } from "hooks/api-tokens/use-api-tokens";
import { IAccount } from "types/account";
import { ITokenFormFields } from "types/token";

interface IProps {
  account: IAccount;
}

export default function AddAPIKeyForm({ account }: IProps) {
  const [modalOpen, setModalOpen] = useState(false);
  const [token, setToken] = useState<string | boolean>(false);
  const form = useForm({
    initialValues: {
      tokenName: "",
      scope: "all" as const,
      account: 0,
    },
  });

  const handleModalClose = () => {
    setModalOpen(false);
  };
  const {
    mutate: addApiToken,
    isLoading,
    data: addResponse,
  } = useAddApiToken({
    onSuccess: (response: any) => {
      queryClient.invalidateQueries("api-tokens");
      toast.success("Api token has been created");
      setToken(response.data.accessToken);
    },
    onError: (error: Error) => {
      console.log(addResponse);
      toast.error(`Unable to add the API token: ${error.message}`);
    },
  });

  const onSubmit = async (data: ITokenFormFields) => {
    const formData = data;
    formData.account = account.id;
    setToken(false);
    addApiToken(formData);
    console.log(data);
  };

  const scopesOptions = [
    {
      id: "all",
      key: "all",
      label: "All",
      value: "all",
    },
  ];

  // useEffect(() => {
  //   resetForm({ account: account.accountName });
  // }, [account.accountName, resetForm]);

  return (
    <div>
      <Group position="left">
        <Button
          onClick={() => setModalOpen(true)}
          color="green"
          leftIcon={<Plus />}
        >
          Add a new API token
        </Button>
      </Group>
      <Modal
        opened={modalOpen}
        onClose={() => setModalOpen(false)}
        title="Add a new API token"
      >
        <form onSubmit={form.onSubmit(onSubmit)}>
          {token && <MessageCopyToken token={token} />}
          <Space h="md" />
          <TextInput
            required
            label="Token Name"
            placeholder="The identifier of the token"
            {...form.getInputProps("tokenName")}
          />
          <Space h="md" />
          <Select
            {...form.getInputProps("scope")}
            label="Scope"
            placeholder="Used features"
            data={scopesOptions}
            required
          />
          <Space h="md" />
          <Text size="sm">
            Selected account: <Badge>{account.accountName}</Badge>
          </Text>
          <Space h="md" />
          <Group>
            <Button
              onClick={handleModalClose}
              disabled={isLoading}
              color="gray"
            >
              Close
            </Button>
            <Button color="green" type="submit" loading={isLoading}>
              Submit
            </Button>
          </Group>
        </form>
      </Modal>
    </div>
  );
}
