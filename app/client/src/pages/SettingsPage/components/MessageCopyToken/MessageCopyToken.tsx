import React from "react";
import { toast } from "react-toastify";
import { Alert, Group, TextInput, Text, Button } from "@mantine/core";
import { useClipboard } from "@mantine/hooks";
import { AlertTriangle, Check, Clipboard } from "tabler-icons-react";

type Props = {
  token: string | boolean;
};

export default function MessageCopyToken({ token }: Props) {
  const clipboard = useClipboard({ timeout: 500 });

  function copyTextToClipboard(entryText: string) {
    clipboard.copy(entryText);
    toast.success("Copied to clipboard");
  }

  return (
    <Alert color="green" title="Your access token" icon={<AlertTriangle />}>
      <Group>
        <Text>
          Your access token has been regenerated. Please copy the following
          token.
          <strong>You won&apos;t be able to see it again</strong>:
        </Text>
        {/* <Group position="left" grow> */}

        {/* </Group> */}
      </Group>
      <TextInput
        style={{ marginTop: "1em", marginBottom: "1em" }}
        type="text"
        value={token.toString()}
      />
      <Button
        variant="light"
        color="green"
        compact
        title="Copy to clipboard"
        leftIcon={clipboard.copied ? <Check color="teal" /> : <Clipboard />}
        onClick={() => copyTextToClipboard(token.toString())}
      >
        Copy to clipboard
      </Button>
    </Alert>
  );
}
