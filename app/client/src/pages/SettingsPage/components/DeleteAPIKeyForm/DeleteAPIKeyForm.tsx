import React, { useState } from "react";
import { toast } from "react-toastify";
import { ActionIcon, Button, Group, Modal, Space, Text } from "@mantine/core";
import { Trash } from "tabler-icons-react";
import queryClient from "api/query-client";
import { useDeleteApiToken } from "hooks/api-tokens/use-api-tokens";

interface IProps {
  apiToken: any;
}

export default function DeleteAPIKeyForm({ apiToken }: IProps) {
  const [modalOpen, setModalOpen] = useState(false);
  const { mutate: deleteToken, isLoading } = useDeleteApiToken(apiToken.id, {
    onSuccess: () => {
      queryClient.invalidateQueries("api-tokens");
      toast.success("Api token has been deleted");
      setModalOpen(false);
    },
    onError: (error: Error) => {
      toast.error(`Unable to delete the API token: ${error.message}`);
      setModalOpen(false);
    },
  });
  const handleModalClose = () => {
    setModalOpen(false);
  };

  const deleteApiTokenAction = async () => {
    deleteToken();
  };

  return (
    <div>
      <ActionIcon
        color="red"
        size="lg"
        radius="md"
        title="Delete API token"
        onClick={() => setModalOpen(true)}
      >
        <Trash name="delete" />
      </ActionIcon>
      <Modal
        opened={modalOpen}
        onClose={() => setModalOpen(false)}
        size="small"
        title="Delete API token"
      >
        <div>
          <Text size="sm">
            Are you sure that you want to delete the API token{" "}
            <strong>{apiToken.name}?</strong> This action cannot be undone.
          </Text>
          <Space h="md" />

          <Group>
            <Button
              onClick={handleModalClose}
              disabled={isLoading}
              color="gray"
            >
              Close
            </Button>
            <Button
              color="red"
              type="submit"
              loading={isLoading}
              onClick={deleteApiTokenAction}
            >
              Confirm delete
            </Button>
          </Group>
        </div>
      </Modal>
    </div>
  );
}
