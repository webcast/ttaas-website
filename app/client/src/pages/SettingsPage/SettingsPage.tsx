import React, { useContext } from "react";
import { Alert, Grid } from "@mantine/core";
import ApiKeysTable from "./components/ApiKeysTable/ApiKeysTable";
import { Footer } from "components/Footer/Footer";
import PageTitle from "components/PageTitle/PageTitle";
import { AppContext } from "contexts/AppContext";

export default function YourFilesPage() {
  const { selectedAccount } = useContext(AppContext);

  return (
    <Grid columns={12}>
      <Grid.Col span={12}>
        <PageTitle header="Settings" />
      </Grid.Col>
      <Grid.Col span={12}>
        {selectedAccount && !selectedAccount.isGuestAccount ? (
          <ApiKeysTable account={selectedAccount} />
        ) : (
          <Alert color="blue" title="Please, select an account">
            You must select an account to use this feature.
          </Alert>
        )}
        <Footer />
      </Grid.Col>
    </Grid>
  );
}
