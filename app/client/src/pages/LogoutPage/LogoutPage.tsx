import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { Center, Grid, Paper, Text, Title } from "@mantine/core";
import { Footer } from "components/Footer/Footer";
import routes from "routes";
import AuthService from "services/auth-service";

export default function LogoutPage() {
  const navigate = useNavigate();
  console.log("loading logout page");

  useEffect(() => {
    AuthService.doLogout({
      redirectUri: `${encodeURI(window.location.href)}/`,
    });
  }, []);

  useEffect(() => {
    if (!AuthService.isLoggedIn()) {
      navigate(`/${routes.loginRoute}`);
    }
  }, [navigate]);

  return (
    <Center style={{ width: 400, height: 200, margin: "20px auto" }}>
      <Grid columns={12}>
        <Grid.Col span={12}>
          <Paper shadow="xs" p="md">
            <Title order={3}>
              Transcription and translation service website
            </Title>
            <Text>Logging out...</Text>
          </Paper>
          <Footer />
        </Grid.Col>
      </Grid>
    </Center>
  );
}
