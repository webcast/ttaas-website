import React from "react";
import {
  Group,
  Grid,
  Image,
  Card,
  Text,
  useMantineTheme,
  Paper,
  Anchor,
} from "@mantine/core";
import { Footer } from "components/Footer/Footer";
import PageTitle from "components/PageTitle/PageTitle";

export default function HomePage() {
  const theme = useMantineTheme();
  const secondaryColor =
    theme.colorScheme === "dark" ? theme.colors.dark[1] : theme.colors.gray[7];
  return (
    <Grid columns={12}>
      <PageTitle header="Home" />
      <Grid.Col span={12}>
        <Group direction="column" grow>
          <Group spacing="xl" grow>
            <Card shadow="sm" p="lg">
              <Card.Section>
                <Image
                  src="/images/undraw_speech_to_text_re_8mtf.svg"
                  alt="Traduction system"
                  height={200}
                />
              </Card.Section>
              <Text
                weight={500}
                align="center"
                style={{ marginBottom: 5, marginTop: theme.spacing.sm }}
              >
                Transcribe
              </Text>
              <Text
                size="sm"
                style={{ color: secondaryColor, lineHeight: 1.5 }}
                align="center"
              >
                Transcribe your media automatically by just uploading it.
              </Text>
            </Card>

            <Card shadow="sm" p="lg">
              <Card.Section>
                <Image
                  src="/images/undraw_around_the_world_re_rb1p.svg"
                  alt="Traduction system"
                  height={200}
                />
              </Card.Section>
              <Text
                weight={500}
                align="center"
                style={{ marginBottom: 5, marginTop: theme.spacing.sm }}
              >
                Translate
              </Text>
              <Text
                size="sm"
                style={{ color: secondaryColor, lineHeight: 1.5 }}
                align="center"
              >
                Translate your media automatically to several languages.
              </Text>
            </Card>

            <Card shadow="sm" p="lg">
              <Card.Section>
                <Image
                  src="/images/undraw_web_browsing_p-77-h.svg"
                  alt="Traduction system"
                  height={200}
                />
              </Card.Section>
              <Text
                weight={500}
                align="center"
                style={{ marginBottom: 5, marginTop: theme.spacing.sm }}
              >
                Boost accesibility
              </Text>
              <Text
                size="sm"
                style={{ color: secondaryColor, lineHeight: 1.5 }}
                align="center"
              >
                Boost the accesibility of your media by adding captions and
                transcriptions.
              </Text>
            </Card>
          </Group>
          <Paper shadow="sm" p="lg">
            <Text weight={500}>Documentation</Text>
            <Group direction="row">
              <Image
                src="/images/undraw_road_to_knowledge_m8s0.svg"
                alt="Documentation"
                height={40}
              />
              <Text
                size="sm"
                style={{ color: secondaryColor, lineHeight: 1.5 }}
              >
                Documentation of the project and user guides{" "}
                <Anchor
                  href="https://ttaas.docs.cern.ch/"
                  title="Transcription and translation service documentation"
                  target="_blank"
                >
                  can be found here
                </Anchor>
                .
              </Text>
            </Group>
          </Paper>
          <Footer />
        </Group>
      </Grid.Col>
    </Grid>
  );
}
