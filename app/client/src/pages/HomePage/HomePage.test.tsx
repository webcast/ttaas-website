import React from "react";
import HomePage from "./HomePage";
import { renderWithAllProviders, screen, waitFor } from "test-utils";

vi.mock("hooks/app-context/use-app-context", () => ({
  useAppContext: () => {
    return {
      user: { uid: "username" },
      selectedAccount: { accountName: "AccountA", isGuestAccount: false },
    };
  },
}));

describe("HomePage tests", () => {
  it("renders expected texts", async () => {
    const providerProps = {
      user: {
        uid: "username",
        username: "username",
        firstName: "firstName",
        lastName: "lastName",
        email: "email",
        roles: ["account1", "account2"],
      },
      selectedAccount: {
        id: 1,
        accountName: "AccountA",
        isGuestAccount: false,
      },
      setUserContext: vi.fn(),
      setSelectedAccountContext: vi.fn(),
    };

    renderWithAllProviders(<HomePage />, { providerProps });
    await waitFor(() => {
      const element = screen.getByText(/Home/i);
      expect(element).toBeInTheDocument();
    });
  });
});
