import React, { useContext, useEffect } from "react";
import { useParams } from "react-router-dom";
import { Alert, Grid, Paper, Space } from "@mantine/core";
import MediaEditorUrlsList from "./components/MediaEditorUrlsList/MediaEditorUrlsList";
import MediaMainDetails from "./components/MediaMainDetails/MediaMainDetails";
import TranscriptionsDownloadList from "./components/TranscriptionsDownloadList/TranscriptionsDownloadList";
import LoadingGrid from "components/LoadingGrid/LoadingGrid";
import PageTitle from "components/PageTitle/PageTitle";
import { AppContext } from "contexts/AppContext";
import { useMediaDetails, useMediaLanguages } from "hooks/media/use-media";

export default function YourFilesDetailsPage() {
  const { mediaId } = useParams();
  const { selectedAccount } = useContext(AppContext);
  const [image, setImage] = React.useState<any | undefined>(undefined);
  const [loaded, setLoaded] = React.useState<boolean>(false);
  const [mediaVideo, setMediaVideo] = React.useState<any | undefined>(
    undefined,
  );
  const [mediaPcm, setMediaPcm] = React.useState<any | undefined>(undefined);
  const [mediaInfo, setMediaInfo] = React.useState<any | undefined>(undefined);

  const [audioTracks, setAudioTracks] = React.useState<any | undefined>(
    undefined,
  );
  const { data, isLoading, error } = useMediaDetails(
    selectedAccount?.id,
    mediaId,
  );
  const { data: mediaLanguages } = useMediaLanguages(
    selectedAccount?.id,
    mediaId,
  );

  useEffect(() => {
    if (data) {
      console.log(data);
      console.log("receivedData", data);

      if (data.mediainfo) {
        setMediaInfo(data.mediainfo);
      }

      if (data.audioTracks) {
        setAudioTracks(data.audioTracks);
      }

      if (data.media) {
        data.media.forEach((media: any) => {
          if (media.media_format === "jpg") {
            setImage(media);
          }
          if (media.media_format === "mp4") {
            setMediaVideo(media);
          }
          if (media.media_format === "pcm") {
            setMediaPcm(media);
          }
        });
      }
      setLoaded(true);
    }
  }, [data, selectedAccount?.id]);

  if (isLoading) {
    return <LoadingGrid />;
  }

  return (
    <Grid columns={12}>
      <Grid.Col span={12}>
        {data && data.mediainfo && (
          <PageTitle
            withBackButton
            header={`Your media: ${data.mediainfo.title} (${data.mediainfo.language})`}
          />
        )}
      </Grid.Col>
      <Grid.Col span={12}>
        <Paper shadow="xs" p="md">
          {error && (
            <Alert color="red" title="Unable to fetch media details">
              Error: {error.message}
            </Alert>
          )}
          {loaded && selectedAccount && !selectedAccount.isGuestAccount && (
            <>
              <MediaMainDetails
                image={image}
                mediaInfo={mediaInfo}
                mediaVideo={mediaVideo}
                mediaPcm={mediaPcm}
                audioTracks={audioTracks}
              />
              <Space h="md" />
              {mediaLanguages && (
                <TranscriptionsDownloadList
                  mediaId={mediaId}
                  langs={mediaLanguages.langs}
                />
              )}
              <Space h="md" />
              <MediaEditorUrlsList mediaId={mediaId} />
            </>
          )}
        </Paper>
      </Grid.Col>
    </Grid>
  );
}
