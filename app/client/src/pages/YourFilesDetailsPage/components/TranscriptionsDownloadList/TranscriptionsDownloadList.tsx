import React, { useContext } from "react";
import { toast } from "react-toastify";
import { Button, Group, Title, Text } from "@mantine/core";
import fileDownload from "js-file-download";
import { AppContext } from "contexts/AppContext";
import { useDownloadTranscription } from "hooks/media/use-media";
import { getFormattedDateFromFormat } from "utils/date-time";

interface LanguageProps {
  lang_name: string;
  lang_code: string;
}

interface Props {
  langs: LanguageProps[];
  mediaId: string | undefined;
}

export default function TranscriptionsDownloadList({ mediaId, langs }: Props) {
  const { selectedAccount } = useContext(AppContext);
  const { mutateAsync: download } = useDownloadTranscription(
    selectedAccount?.id,
  );
  const downloadTranscriptionFile = async (language: string) => {
    if (mediaId) {
      try {
        const receivedData = await download({
          accountId: selectedAccount?.id,
          mediaId,
          language,
        });
        fileDownload(receivedData, `${mediaId}_${language}.vtt`);
      } catch (error) {
        console.error(error);
        toast.error("Unable to download the transcription's file");
      }
    }
  };

  return (
    <Group direction="column">
      <Title order={3}>Transcriptions</Title>
      <Group direction="row">
        <Text>Downloads: </Text>
        {langs &&
          langs.map((language: any) => (
            <div key={language.lang_code}>
              <Button
                compact
                onClick={() => downloadTranscriptionFile(language.lang_code)}
                title={`Download ${language.lang_name} transcription`}
              >
                {language.lang_name}
              </Button>{" "}
              {language.last_updated && (
                <Text size="sm">
                  (Last updated:{" "}
                  {getFormattedDateFromFormat(language.last_updated)})
                </Text>
              )}
            </div>
          ))}
      </Group>
    </Group>
  );
}
