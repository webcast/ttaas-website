import React, { useContext } from "react";
import { Alert, Anchor, Group, List, Title } from "@mantine/core";
import { Edit, Eye } from "tabler-icons-react";
import LoadingGrid from "components/LoadingGrid/LoadingGrid";
import { AppContext } from "contexts/AppContext";
import { useEditorUrl } from "hooks/media/use-media";

interface Props {
  mediaId: string | undefined;
}

export default function MediaEditorUrlsList({ mediaId }: Props) {
  const { selectedAccount } = useContext(AppContext);
  const { data, isLoading, error } = useEditorUrl(selectedAccount?.id, mediaId);

  if (isLoading) {
    return <LoadingGrid />;
  }

  if (error) {
    return (
      <Alert color="red" title="Unable to fetch">
        {error.message}
      </Alert>
    );
  }

  return (
    <Group direction="column">
      <Title order={3}>Editor</Title>
      <List spacing="xs" size="sm">
        <List.Item icon={<Edit />}>
          <Anchor
            href={data.editUrl}
            title="Read and write view"
            target="_blank"
          >
            Edit the transcription
          </Anchor>
        </List.Item>
        <List.Item icon={<Eye />}>
          <Anchor href={data.viewUrl} title="Read only view" target="_blank">
            View the transcription
          </Anchor>
        </List.Item>
      </List>
    </Group>
  );
}
