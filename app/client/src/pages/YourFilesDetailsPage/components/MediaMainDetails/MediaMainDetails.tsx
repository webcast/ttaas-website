import React from "react";
import { Anchor, Group, Image, List } from "@mantine/core";
import { Clock, Language, Link, Tag, Users } from "tabler-icons-react";

interface ImageProps {
  location: string;
}

interface MediaInfoProps {
  language: string;
  category: string;
  duration: string;
  speakers: string[];
}

interface MediaVideoProps {
  is_url: string;
  location: string;
}

interface MediaMainDetailsProps {
  image: ImageProps;
  mediaInfo: MediaInfoProps;
  mediaVideo: MediaVideoProps;
  mediaPcm: MediaVideoProps;
  audioTracks: MediaVideoProps[];
}

export default function MediaMainDetails({
  image,
  mediaInfo,
  mediaVideo,
  mediaPcm,
  audioTracks,
}: MediaMainDetailsProps) {
  return (
    <Group>
      {image && image.location && (
        <Image src={image.location} radius="md" width={250} height={141} />
      )}
      <List>
        <List.Item icon={<Language />} title="Language">
          <strong>Language:</strong> {mediaInfo.language}
        </List.Item>
        {mediaInfo.category && (
          <List.Item icon={<Tag />} title="Category">
            <strong>Category:</strong> {mediaInfo.category}
          </List.Item>
        )}
        <List.Item icon={<Clock />} title="Duration">
          <strong>Duration:</strong> {mediaInfo.duration}
        </List.Item>
        <List.Item title="Speakers" icon={<Users />}>
          <strong>Speakers:</strong>{" "}
          {mediaInfo.speakers.map((speaker: any) => speaker.name).join(", ")}
        </List.Item>
      </List>
      <List>
        {mediaVideo && mediaVideo.is_url && (
          <List.Item icon={<Link href={mediaVideo.location} />}>
            <Anchor href={mediaVideo.location}>Original media URL</Anchor>
          </List.Item>
        )}
        <List>
          {mediaPcm && mediaPcm.is_url && (
            <List.Item icon={<Link href={mediaVideo.location} />}>
              <Anchor href={mediaPcm.location}>PCM URL</Anchor>
            </List.Item>
          )}

          {audioTracks &&
            audioTracks.map((track: any) => (
              <List.Item key={track.media_format}>
                {track.media_format}{" "}
                <Anchor href={track.location}>Attachment URL</Anchor>
              </List.Item>
            ))}
        </List>
      </List>
    </Group>
  );
}
