import React, { useContext } from "react";
import {
  Grid,
  Paper,
  Title,
  Text,
  ThemeIcon,
  Group,
  Anchor,
} from "@mantine/core";
import { Help } from "tabler-icons-react";
import UploadForm from "./components/UploadForm/UploadForm";
import { Footer } from "components/Footer/Footer";
import PageTitle from "components/PageTitle/PageTitle";
import { AppContext } from "contexts/AppContext";

export default function UploadFormPage() {
  const { selectedAccount } = useContext(AppContext);

  return (
    <Grid columns={12}>
      <Grid.Col span={12}>
        <PageTitle header="Upload media" />
      </Grid.Col>
      <Grid.Col span={8}>
        <Paper shadow="xs" p="md">
          {selectedAccount && !selectedAccount.isGuestAccount && <UploadForm />}
        </Paper>
        <Footer />
      </Grid.Col>
      <Grid.Col span={4}>
        <Paper shadow="xs" p="md">
          <Group>
            <Group direction="row">
              <ThemeIcon color="blue">
                <Help />
              </ThemeIcon>
              <Title order={4}>Help</Title>
            </Group>

            <Text size="sm">
              Use this form to upload media to obtain a transcription.
            </Text>
            <Text size="sm">
              Please, refer to{" "}
              <Anchor
                href="https://ttaas.docs.cern.ch/"
                title="Transcription and translation service documentation"
                target="_blank"
              >
                our documentation
              </Anchor>{" "}
              if you need assistance.
            </Text>
          </Group>
        </Paper>
      </Grid.Col>
    </Grid>
  );
}
