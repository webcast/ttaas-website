/* eslint-disable no-nested-ternary */
/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { useContext, useState } from "react";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";
import {
  Box,
  Checkbox,
  Group,
  MantineTheme,
  TextInput,
  Text,
  useMantineTheme,
  Space,
  Select,
  Textarea,
  Button,
  Modal,
  List,
  ActionIcon,
  ThemeIcon,
  Anchor,
  Accordion,
} from "@mantine/core";
import { TimeInput } from "@mantine/dates";
import { Dropzone, DropzoneStatus } from "@mantine/dropzone";
import { useForm as useMantineForm } from "@mantine/form";
import ProgressBar from "@ramonak/react-progress-bar";
import dayjs from "dayjs";
import { Video, Trash, Check, Clock } from "tabler-icons-react";
import UploadService from "../FileUploadService/FileUploadService";
import config from "config";
import { AppContext } from "contexts/AppContext";
import routes from "routes";
import AuthService from "services/auth-service";

export interface IFormFields {
  title: string;
  language: string;
  mediaFile: File | null;
  isPublic: boolean;
  termsOfService: boolean;
  comments: string;
  notificationEmail: string;
  username: string;
  startTime: Date | null;
  endTime: Date | null;
}

function getIconColor(status: DropzoneStatus, theme: MantineTheme) {
  return status.accepted
    ? theme.colors[theme.primaryColor][theme.colorScheme === "dark" ? 4 : 6]
    : status.rejected
    ? theme.colors.red[theme.colorScheme === "dark" ? 4 : 6]
    : theme.colorScheme === "dark"
    ? theme.colors.dark[0]
    : theme.colors.gray[7];
}

export const dropzoneChildren = (
  status: DropzoneStatus,
  theme: MantineTheme,
) => (
  <Group
    position="center"
    spacing="xl"
    style={{ minHeight: 220, pointerEvents: "none" }}
  >
    <Video style={{ color: getIconColor(status, theme) }} size={80} />

    <div>
      <Text size="xl" inline>
        Drag a video here or click to select file
      </Text>
      <Text size="sm" color="dimmed" inline mt={7}>
        Supported formats: mp4, mov, m4v, ogv,wmv,avi, mpg, flv, ogg, wav,mp3,
        oga, flac, aac.
      </Text>
      <Text size="sm" color="dimmed" inline mt={7}>
        <strong>File should not exceed: 2GB and a duration of 4 hours</strong>
      </Text>
    </div>
  </Group>
);

export default function UploadForm() {
  const theme = useMantineTheme();
  const [opened, setOpened] = useState(false);
  const { selectedAccount } = useContext(AppContext);
  const [isLoading, setIsLoading] = useState(false);

  const form = useMantineForm({
    initialValues: {
      title: "",
      termsOfService: false,
      language: "",
      comments: "",
      isPublic: false,
      mediaFile: null,
      notificationEmail: AuthService.getEmail(),
      username: AuthService.getUsername(),
      startTime: null,
      endTime: null,
    },

    validate: {
      termsOfService: (value: boolean | string | null) => {
        return value ? null : "Please accept terms of the service";
      },
      title: (value: boolean | string | null) => {
        return value ? null : "Please enter a title";
      },
      notificationEmail: (value: boolean | string | null) => {
        if (!value) {
          return "Please enter an email";
        }
        return /^\S+@\S+$/.test(value.toString()) ? null : "Invalid email";
      },
      language: (value: boolean | string | null) => {
        return value ? null : "Please select a language";
      },
      mediaFile: (value: any) => {
        return value ? null : "Please select a video file";
      },
      endTime: (value, values) => {
        if (
          values.startTime &&
          values.endTime &&
          dayjs(values.startTime).format("HH:mm:ss") !==
            dayjs(values.endTime).format("HH:mm:ss")
        ) {
          return values.startTime >= values.endTime
            ? "End time should be greater than start time"
            : null;
        }
        return null;
      },
    },
  });
  const [selectedMediaFile, setSelectedMediaFile] = useState<any>(null);
  const [uploadedFiles, setUploadedFiles] = useState<any[]>([]);
  const [displayProgressBar, setDisplayProgressBar] = useState<boolean>(false);
  const [progress, setProgress] = useState(0);

  const systemsOptions = [
    {
      id: "en",
      key: "en",
      label: "English",
      value: "en",
    },
    {
      id: "fr",
      key: "fr",
      label: "French",
      value: "fr",
    },
  ];

  const selectFile = (files: any) => {
    form.clearFieldError("mediaFile");
    form.setFieldValue("mediaFile", files[0]);
    setSelectedMediaFile(files[0]);
  };

  const upload = (formData: FormData) => {
    setProgress(0);
    setDisplayProgressBar(true);
    setIsLoading(true);

    UploadService.upload(selectedAccount?.id, formData, (event: any) => {
      setProgress(Math.round((100 * event.loaded) / event.total));
    })
      .then(() => {
        setUploadedFiles([selectedMediaFile]);
        toast.success("Form submitted successfully");
        setSelectedMediaFile(null);
        form.reset();
        setOpened(true);
      })
      .catch((error) => {
        setProgress(0);
        toast.error(`Unable to submit the form: ${error.message}`);
        setDisplayProgressBar(false);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  const onSubmit = (values: IFormFields) => {
    const formData = new FormData();
    console.log(values);
    setUploadedFiles([]);

    if (!AuthService.getUsername()) {
      return;
    }
    const username: string = AuthService.getUsername() as string;

    formData.append("title", values.title);
    formData.append("language", values.language);
    formData.append("isPublic", values.isPublic.toString());
    formData.append("termsOfService", values.termsOfService.toString());
    formData.append("mediaFile", values.mediaFile as any);
    formData.append("comments", values.comments);
    formData.append("notificationEmail", values.notificationEmail);
    formData.append("username", username);
    if (values.startTime) {
      formData.append("startTime", dayjs(values.startTime).format("HH:mm:ss"));
    }
    if (values.endTime) {
      formData.append("endTime", dayjs(values.endTime).format("HH:mm:ss"));
    }

    upload(formData);
  };

  const deleteMediaFile = () => {
    setSelectedMediaFile(null);
    form.setFieldValue("mediaFile", null);
  };

  if (!AuthService.getUsername()) {
    return <Box>User is not set. Please, reload the page.</Box>;
  }

  return (
    <Box>
      <form onSubmit={form.onSubmit(onSubmit)}>
        <TextInput
          required
          label="Media title"
          placeholder="How you identify your media"
          {...form.getInputProps("title")}
          disabled={isLoading}
        />
        <Space h="md" />
        <label htmlFor="mediaFile" className="mantine-TextInput-label">
          Media File
          <span
            className="mantine-TextInput-required"
            style={{ color: "#f03e3e" }}
          >
            {" "}
            *
          </span>
        </label>
        <Dropzone
          {...form.getInputProps("mediaFile")}
          onDrop={selectFile}
          onReject={() => toast.error("Please, select a valid file.")}
          maxSize={config.MEDIA_MAX_SIZE}
          accept={config.MEDIA_MIMES}
          multiple={false}
          disabled={isLoading}
        >
          {(status) => dropzoneChildren(status, theme)}
        </Dropzone>
        {form.errors && form.errors.mediaFile && (
          <Text color="red">{form.errors.mediaFile}</Text>
        )}
        <Space h="md" />
        {selectedMediaFile && (
          <List>
            <List.Item
              icon={
                <ActionIcon onClick={deleteMediaFile} disabled={isLoading}>
                  <Trash color="red" />
                </ActionIcon>
              }
            >
              {selectedMediaFile.name}
            </List.Item>
          </List>
        )}
        <Space h="md" />
        <Select
          {...form.getInputProps("language")}
          label="Language"
          placeholder="Language of the media"
          data={systemsOptions}
          required
          disabled={isLoading}
        />
        <Space h="md" />
        <TextInput
          required
          label="Notification email"
          placeholder="Email used to notify the media owner"
          {...form.getInputProps("notificationEmail")}
          disabled={isLoading}
        />
        <Space h="md" />
        <Accordion icon={<Clock />} defaultValue="customization">
          <Accordion.Item label="Start and end times (optional)">
            <Text>
              Set the start and end times if you only want transcription of a
              selected fragment. Leave empty to transcribe the whole media.
            </Text>
            <Button
              mt="md"
              size="xs"
              variant="default"
              onClick={() => {
                form.setFieldValue("startTime", null);
                form.setFieldValue("endTime", null);
              }}
            >
              Reset times
            </Button>
            <TimeInput
              mt="md"
              label="Start time"
              description="Format: HH:MM:SS"
              withSeconds
              format="24"
              {...form.getInputProps("startTime")}
            />

            <Space h="md" />
            <TimeInput
              label="End time"
              description="Format: HH:MM:SS"
              withSeconds
              format="24"
              {...form.getInputProps("endTime")}
            />
          </Accordion.Item>
        </Accordion>

        <Space h="md" />
        <Textarea
          {...form.getInputProps("comments")}
          placeholder="Comments"
          label="Your comments"
          disabled={isLoading}
        />
        <Checkbox
          mt="md"
          label="The media is public and I want the transcription to be Indexed in CERN Search"
          {...form.getInputProps("isPublic", { type: "checkbox" })}
          disabled={isLoading}
        />
        <Checkbox
          mt="md"
          label="I read and agree to the Terms and Conditions"
          {...form.getInputProps("termsOfService", { type: "checkbox" })}
          disabled={isLoading}
          required
        />
        <Group position="left" mt="md">
          <Button
            type="submit"
            loading={isLoading}
            disabled={JSON.stringify(form.errors) !== "{}"}
          >
            Submit
          </Button>
        </Group>
        <Space h="md" />
        {displayProgressBar && (
          <div className="progress">
            <ProgressBar
              completed={progress}
              bgColor="#007eff"
              height="30px"
              labelColor="#ffffff"
            />
          </div>
        )}
      </form>
      <Modal
        centered
        opened={opened}
        onClose={() => setOpened(false)}
        title={
          <Group direction="row">
            <ThemeIcon color="green">
              <Check />
            </ThemeIcon>
            <Text>Media has been uploaded successfully</Text>
          </Group>
        }
        overlayColor={
          theme.colorScheme === "dark"
            ? theme.colors.green[9]
            : theme.colors.green[1]
        }
      >
        <Group direction="column">
          <Text size="sm">
            Media has been uploaded successfully and will be soon transcribed.
          </Text>
          <Text size="sm">
            You will receive an email on{" "}
            <strong> {AuthService.getEmail()}</strong> when the transcription is
            ready.
          </Text>
          <Text size="sm">
            In the meantime you can check the status of the transcription in the{" "}
            <Anchor
              component={Link}
              to={routes.getRouteWithBase(routes.uploadsRoute)}
            >
              uploads page
            </Anchor>
            .
          </Text>
          {uploadedFiles.length > 0 && (
            <Text size="sm">
              <div className="card-header">List of uploaded files</div>
              <List size="sm">
                {uploadedFiles.map((file: any) => (
                  <List.Item className="list-group-item" key={file.name}>
                    {file.name}
                  </List.Item>
                ))}
              </List>
            </Text>
          )}
          <Button color="gray" onClick={() => setOpened(false)}>
            Close
          </Button>
        </Group>
      </Modal>
    </Box>
  );
}
