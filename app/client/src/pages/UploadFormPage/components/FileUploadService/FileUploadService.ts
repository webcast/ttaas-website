import apiClient from "api/api-headers";

const upload = (
  accountId: number | undefined,
  formData: FormData,
  onUploadProgress: any,
) => {
  if (!accountId) {
    throw Error("accountId is required");
  }

  return apiClient.post(`/uploads/account/${accountId}/ingest/`, formData, {
    headers: { "Content-Type": "multipart/form-data" },
    onUploadProgress,
  });
};

export default {
  upload,
};
