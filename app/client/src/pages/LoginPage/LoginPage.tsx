import React from "react";
import { Button, Center, Grid, Paper, Title } from "@mantine/core";
import { Footer } from "components/Footer/Footer";
import AuthService from "services/auth-service";

export default function LoginPage() {
  console.log("loading login page");
  return (
    <Center style={{ width: 400, height: 200, margin: "20px auto" }}>
      <Grid columns={12}>
        <Grid.Col span={12}>
          <Paper shadow="xs" p="md">
            <Title order={3}>
              Transcription and translation service website
            </Title>
            <Button
              onClick={() => {
                AuthService.doLogin({
                  redirectUri: encodeURI(window.location.href),
                });
              }}
            >
              Login
            </Button>
          </Paper>
          <Footer />
        </Grid.Col>
      </Grid>
    </Center>
  );
}
