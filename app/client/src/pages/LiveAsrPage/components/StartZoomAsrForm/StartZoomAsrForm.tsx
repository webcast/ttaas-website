/* eslint-disable react/destructuring-assignment */
/* eslint-disable no-nested-ternary */
/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { useState } from "react";
import { toast } from "react-toastify";
import {
  Box,
  Group,
  TextInput,
  Space,
  Button,
  List,
  Text,
  ThemeIcon,
  Loader,
} from "@mantine/core";
import { useForm as useMantineForm } from "@mantine/form";
import { Check, X } from "tabler-icons-react";
import {
  useGetLiveAsrSessionsCount,
  useStartAsrSession,
} from "hooks/zoom-live-asr/use-zoom-live-asr";
import AuthService from "services/auth-service";

export interface IFormFields {
  zoomMeetingId: string;
}

export default function StartZoomAsrForm() {
  const [submitDisabled, setSubmitDisabled] = useState(false);

  const {
    data: sessionsData,
    isFetching: sessionsIsFetching,
    error: sessionsError,
    isRefetching: sessionsIsRefetching,
    refetch: refetchSessions,
  } = useGetLiveAsrSessionsCount();

  const {
    data: response,
    error,
    isLoading,
    mutate: startAsrSessionForMeeting,
    reset,
  } = useStartAsrSession();

  const form = useMantineForm({
    initialValues: {
      zoomMeetingId: "",
    },

    validate: {
      zoomMeetingId: (value: boolean | string | null) => {
        return value ? null : "Please enter the Zoom meeting ID";
      },
    },
  });

  const onSubmit = (values: IFormFields) => {
    console.log("onSubmit", values);
    setSubmitDisabled(true);

    if (
      sessionsData &&
      sessionsData.activeSessions < sessionsData.maxSessions
    ) {
      startAsrSessionForMeeting({ meetingId: values.zoomMeetingId });
    } else {
      toast.error("There are no available sessions");
    }
  };

  const clearData = () => {
    reset();
    refetchSessions();
    if (
      sessionsData &&
      sessionsData.activeSessions < sessionsData.maxSessions
    ) {
      setSubmitDisabled(false);
    }
  };

  if (!AuthService.getUsername()) {
    return <Box>User is not set. Please, reload the page.</Box>;
  }

  return (
    <Box>
      <form onSubmit={form.onSubmit(onSubmit)}>
        <TextInput
          required
          label="Zoom meeting ID"
          placeholder="The Zoom meeting where you want to enable the transcription"
          {...form.getInputProps("zoomMeetingId")}
        />
        <Space h="md" />
        <List spacing="xs" size="sm" center>
          {sessionsError && (
            <List.Item
              icon={
                <ThemeIcon color="red" size={24} radius="xl">
                  <X size="1rem" />
                </ThemeIcon>
              }
            >
              Something went wrong checking resources
            </List.Item>
          )}

          {(sessionsIsFetching || sessionsIsRefetching) && (
            <List.Item icon={<Loader size="sm" />}>
              Checking if there are resources...
            </List.Item>
          )}

          {!sessionsIsFetching &&
            !sessionsIsRefetching &&
            sessionsData &&
            sessionsData.activeSessions < sessionsData.maxSessions && (
              <List.Item
                icon={
                  <ThemeIcon color="green" size={24} radius="xl">
                    <Check size="1rem" />
                  </ThemeIcon>
                }
              >
                Resources available for Live ASR (Used:{" "}
                {sessionsData.activeSessions} /{sessionsData.maxSessions})
              </List.Item>
            )}

          {!sessionsIsFetching &&
            !sessionsIsRefetching &&
            sessionsData &&
            sessionsData.activeSessions >= sessionsData.maxSessions && (
              <List.Item
                icon={
                  <ThemeIcon color="red" size={24} radius="xl">
                    <X size="1rem" />
                  </ThemeIcon>
                }
              >
                There are no resources left (Used: {sessionsData.activeSessions}{" "}
                /{sessionsData.maxSessions})
              </List.Item>
            )}

          {error && (
            <List.Item
              icon={
                <ThemeIcon color="red" size={24} radius="xl">
                  <X size="1rem" />
                </ThemeIcon>
              }
            >
              Unknown error
            </List.Item>
          )}

          {response && response.data && response.data.error && (
            <List.Item
              icon={
                <ThemeIcon color="red" size={24} radius="xl">
                  <X size="1rem" />
                </ThemeIcon>
              }
            >
              {response.data.message}
            </List.Item>
          )}

          {isLoading && (
            <List.Item icon={<Loader size="sm" />}>Loading...</List.Item>
          )}

          {response && response.data && response.data.asrStarted && (
            <List.Item
              icon={
                <ThemeIcon color="green" size={24} radius="xl">
                  <Check size="1rem" />
                </ThemeIcon>
              }
            >
              Live ASR Zoom session started
            </List.Item>
          )}

          <Space h="md" />
          {response && response.data && response.data.asrStarted && (
            <Text size="sm">
              Zoom captions have been enabled. Please, go to your meeting
              <strong>
                &quot;Captions&quot; -&gt; &quot;Set up manual captioner&quot;
                and enable them
              </strong>
              . Otherwise, the default Zoom ones will be displayed.
            </Text>
          )}
        </List>
        <Group position="left" mt="md">
          <Button
            type="submit"
            loading={isLoading}
            disabled={
              !sessionsData ||
              sessionsData.activeSessions >= sessionsData.maxSessions ||
              submitDisabled
            }
          >
            Start transcription
          </Button>
          <Button
            disabled={JSON.stringify(form.errors) !== "{}"}
            onClick={clearData}
            color="gray"
          >
            Reset form
          </Button>
        </Group>
      </form>
    </Box>
  );
}
