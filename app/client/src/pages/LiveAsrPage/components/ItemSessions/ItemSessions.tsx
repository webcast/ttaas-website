import React, { useEffect } from "react";
import { List, ThemeIcon } from "@mantine/core";
import { Check, Loader, X } from "tabler-icons-react";
import { useGetLiveAsrSessionsCount } from "hooks/zoom-live-asr/use-zoom-live-asr";

interface ItemSessionsProps {
  onSessionsLoaded: (result: boolean) => void;
}

export function ItemSessions({ onSessionsLoaded }: ItemSessionsProps) {
  const {
    data: sessionsData,
    isFetching: sessionsIsFetching,
    error: sessionsError,
    isRefetching: sessionsIsRefetching,
  } = useGetLiveAsrSessionsCount();

  const [activeSessions, setActiveSessions] = React.useState(0);
  const [maxSessions, setMaxSessions] = React.useState(0);

  useEffect(() => {
    if (sessionsData) {
      // eslint-disable-next-line react/destructuring-assignment
      setActiveSessions(sessionsData.activeSessions);
      // eslint-disable-next-line react/destructuring-assignment
      setMaxSessions(sessionsData.maxSessions);
      if (sessionsData.activeSessions <= sessionsData.maxSessions) {
        onSessionsLoaded(true);
      }
    }
    if (sessionsError) {
      onSessionsLoaded(false);
    }
  }, [onSessionsLoaded, sessionsData, sessionsError]);

  if (sessionsError) {
    return (
      <List.Item
        icon={
          <ThemeIcon color="red" size={24} radius="xl">
            <X size="1rem" />
          </ThemeIcon>
        }
      >
        Something went wrong checking resources
      </List.Item>
    );
  }

  if (sessionsIsFetching || sessionsIsRefetching) {
    return (
      <List.Item
        icon={
          <ThemeIcon color="gray" size={24} radius="xl">
            <Loader size="1rem" />
          </ThemeIcon>
        }
      >
        Checking if there are resources...
      </List.Item>
    );
  }

  if (sessionsData) {
    if (activeSessions < maxSessions) {
      return (
        <List.Item
          icon={
            <ThemeIcon color="green" size={24} radius="xl">
              <Check size="1rem" />
            </ThemeIcon>
          }
        >
          Resources available for Live ASR ({activeSessions} /{maxSessions})
        </List.Item>
      );
    }
    if (activeSessions >= maxSessions) {
      return (
        <List.Item
          icon={
            <ThemeIcon color="red" size={24} radius="xl">
              <X size="1rem" />
            </ThemeIcon>
          }
        >
          There are no resources left ({activeSessions} /{maxSessions})
        </List.Item>
      );
    }
  }
  return null;
}

export default ItemSessions;
