import React, { useEffect } from "react";
import { List, ThemeIcon } from "@mantine/core";
import { Check, Loader, X } from "tabler-icons-react";
import { useSetLiveStreamSettings } from "hooks/zoom-live-asr/use-zoom-live-asr";

interface ISettingsProps {
  onSettingsLoaded: (result: boolean) => void;
  enableQuery: boolean;
  zoomMeetingId: string;
}

export function ItemSettings({
  onSettingsLoaded,
  enableQuery,
  zoomMeetingId,
}: ISettingsProps) {
  const {
    data: settingsData,
    error: settingsError,
    isLoading: settingsLoading,
    mutate: updateLiveStreamSettings,
  } = useSetLiveStreamSettings();

  useEffect(() => {
    if (enableQuery) {
      console.log(
        `Updating live stream settings with Zoom ID: ${zoomMeetingId}`,
      );
      setTimeout(() => {
        updateLiveStreamSettings({
          meetingId: zoomMeetingId,
          channel: undefined,
          password: undefined,
        });
      }, 1000);
    }
  }, [enableQuery, updateLiveStreamSettings, zoomMeetingId]);

  useEffect(() => {
    if (settingsData && settingsData.data.started) {
      onSettingsLoaded(true);
    }
    if (settingsError) {
      onSettingsLoaded(false);
    }
  }, [onSettingsLoaded, settingsData, settingsError]);

  if (settingsError) {
    return (
      <List.Item
        icon={
          <ThemeIcon color="red" size={24} radius="xl">
            <X size="1rem" />
          </ThemeIcon>
        }
      >
        Unable to start the live stream
      </List.Item>
    );
  }

  if (settingsLoading) {
    return (
      <List.Item
        icon={
          <ThemeIcon color="gray" size={24} radius="xl">
            <Loader size="1rem" />
          </ThemeIcon>
        }
      >
        Setting up the meeting live stream...
      </List.Item>
    );
  }

  if (settingsData && settingsData.data.started) {
    return (
      <List.Item
        icon={
          <ThemeIcon color="green" size={24} radius="xl">
            <Check size="1rem" />
          </ThemeIcon>
        }
      >
        Live stream configured and started
      </List.Item>
    );
  }

  if (settingsData && !settingsData.data.started) {
    return (
      <List.Item
        icon={
          <ThemeIcon color="green" size={24} radius="xl">
            <Check size="1rem" />
          </ThemeIcon>
        }
      >
        Live stream configured and started
      </List.Item>
    );
  }

  return null;
}

export default ItemSettings;
