import React, { useEffect } from "react";
import { List, ThemeIcon } from "@mantine/core";
import { Check, Loader, X } from "tabler-icons-react";
import { useIsZoomMeetingLive } from "hooks/zoom-live-asr/use-zoom-live-asr";

interface ItemMeetingLiveProps {
  enableQuery: boolean;
  zoomMeetingId: string;
  onMeetingIsLive: (isLive: boolean) => void;
}

export function ItemMeetingLiveComponent({
  enableQuery,
  zoomMeetingId,
  onMeetingIsLive,
}: ItemMeetingLiveProps) {
  const { data, isFetching, isRefetching, error } = useIsZoomMeetingLive(
    zoomMeetingId,
    enableQuery,
  );
  useEffect(() => {
    if (data) {
      if (data.isLive) {
        console.log("Meeting is live");
        onMeetingIsLive(true);
      } else {
        console.log("Meeting is not live");
        onMeetingIsLive(false);
      }
    }
  }, [data, onMeetingIsLive]);

  useEffect(() => {
    if (error) {
      console.log("Error checking if meeting is live");
      onMeetingIsLive(false);
    }
  }, [error, onMeetingIsLive]);

  if (error) {
    return (
      <List.Item
        icon={
          <ThemeIcon color="red" size={24} radius="xl">
            <X size="1rem" />
          </ThemeIcon>
        }
      >
        Something went wrong checking if meeting is live
      </List.Item>
    );
  }

  if (isFetching || isRefetching) {
    return (
      <List.Item
        icon={
          <ThemeIcon color="gray" size={24} radius="xl">
            <Loader size="1rem" />
          </ThemeIcon>
        }
      >
        Checking if meeting is live...
      </List.Item>
    );
  }

  if (data && data.isLive) {
    return (
      <List.Item
        icon={
          <ThemeIcon color="green" size={24} radius="xl">
            <Check size="1rem" />
          </ThemeIcon>
        }
      >
        Meeting is live
      </List.Item>
    );
  }

  if (data && !data.isLive) {
    return (
      <List.Item
        icon={
          <ThemeIcon color="red" size={24} radius="xl">
            <X size="1rem" />
          </ThemeIcon>
        }
      >
        Meeting is not live
      </List.Item>
    );
  }
  return null;
}

export default ItemMeetingLiveComponent;
