import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import {
  Alert,
  Anchor,
  Box,
  Button,
  Grid,
  Group,
  List,
  Paper,
  TextInput,
} from "@mantine/core";
import { useForm } from "@mantine/form";
import {
  Clock,
  Language,
  Link as AnchorIcon,
  PlayerPlay,
  InfoCircle,
} from "tabler-icons-react";
import PageTitle from "components/PageTitle/PageTitle";
import { useQueryParameters } from "hooks/query-params/use-query-params";
import { useSearchWeblectureDetails } from "hooks/search-weblectures/use-search-weblectures";

interface ISearchDetailsProps {
  text: string;
}

export default function SearchDetailsPage() {
  const { searchId } = useParams();
  const params = useQueryParameters();
  const navigate = useNavigate();

  const [text, setText] = useState<string>(params.get("text") || "");

  const searchForm = useForm<ISearchDetailsProps>({
    initialValues: {
      // sort,
      // title,
      // dateFrom: dateFromString !== "" ? new Date(dateFromString) : null,
      // dateTo: dateToString !== "" ? new Date(dateToString) : null,
      // page: 1,
      text: params.get("text") || "",
      // limit: 10,
    },
  });

  const { data, isLoading } = useSearchWeblectureDetails(searchId, text);

  const submitSearchForm = (values: ISearchDetailsProps) => {
    // const titleParam = "";
    // if (values.title !== "") {
    //   //   titleParam = `&title=${values.title}`;
    //   setTitle(values.title);
    // }
    let textParam = "";
    if (values.text !== "") {
      textParam = `text=${values.text}`;
      setText(values.text);
    }

    navigate(`?${textParam}`);
  };

  useEffect(() => {
    setText(params.get("text") || "");
  }, [params]);

  return (
    <Grid columns={12}>
      <Grid.Col span={12}>
        <PageTitle
          withBackButton
          header={`Searching weblecture: ${data?.title || ""}`}
          subHeader={searchId}
        />
      </Grid.Col>
      <Grid.Col span={12}>
        <Group grow direction="column">
          <Paper shadow="xs" p="md">
            <Box>
              <form
                onSubmit={searchForm.onSubmit((values) =>
                  submitSearchForm(values),
                )}
              >
                <Grid columns={24}>
                  <Grid.Col span={12}>
                    <TextInput
                      label="Text"
                      placeholder="Text that appears on the transcription"
                      {...searchForm.getInputProps("text")}
                    />
                  </Grid.Col>
                </Grid>

                <Group>
                  <Group mt="xl">
                    <Button type="submit" loading={isLoading}>
                      Search
                    </Button>
                  </Group>
                </Group>
              </form>
            </Box>
          </Paper>
          <List>
            <List.Item icon={<Clock />} title="Results">
              Showing {data ? data.total : ""} results for search{" "}
              <strong>&quot;{text}&quot;</strong>
            </List.Item>
          </List>

          {data && !data.url && (
            <Paper shadow="xs" p="md">
              <Alert color="red">
                <strong>
                  No texts found in the transcription for search &quot;{text}
                  &quot;
                </strong>
              </Alert>
            </Paper>
          )}

          {data && data.url && (
            <Paper shadow="xs" p="md">
              <Group grow direction="column">
                <Alert color="blue" icon={<InfoCircle />}>
                  Showing the first 50 results. If you need more precise
                  results, please fine-tune your search.
                </Alert>
                <List>
                  <List.Item icon={<PlayerPlay />} title="Title">
                    <strong>Title:</strong> {data ? data.title : ""}
                  </List.Item>
                  <List.Item icon={<Language />} title="Language: ">
                    <strong>Language:</strong> {data ? data.language : ""}
                  </List.Item>
                  <List.Item icon={<Clock />} title="Date">
                    <strong>Date:</strong> {data ? data.eventdate : ""}
                  </List.Item>
                  <List.Item title="URL" icon={<AnchorIcon />}>
                    <strong>URL: </strong>{" "}
                    <Anchor href={data ? data.url : ""}>
                      Open weblecture player
                    </Anchor>
                  </List.Item>
                </List>
              </Group>
            </Paper>
          )}
        </Group>
      </Grid.Col>
    </Grid>
  );
}
