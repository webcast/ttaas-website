import React, { useEffect } from "react";
import { Button } from "@mantine/core";
import {
  useIsMediaFileAvailable,
  useRetryIngest,
} from "hooks/uploads/use-uploads";

interface Props {
  uploadId: number;
  accountId: number | undefined;
}

export default function RetryIngestButton({ uploadId, accountId }: Props) {
  const [disabled, setDisabled] = React.useState(true);
  const { data, error, isFetching } = useIsMediaFileAvailable(
    uploadId,
    accountId,
  );
  const { mutate: retryUploadIngest } = useRetryIngest();

  useEffect(() => {
    if (error) {
      setDisabled(true);
    }

    if (data) {
      if (data.exists) {
        setDisabled(false);
      } else {
        setDisabled(true);
      }
    }
  }, [data, error]);

  const retryIngest = () => {
    retryUploadIngest({ uploadId, accountId });
  };

  console.log("RetryIngestButton", { data, error, isFetching, disabled });

  return (
    <Button
      loading={isFetching}
      size="xs"
      disabled={disabled}
      onClick={retryIngest}
    >
      Retry
    </Button>
  );
}
