import React, { useContext } from "react";
import { Grid, Paper, Tabs } from "@mantine/core";
import { Clock, Upload } from "tabler-icons-react";
import PreUploadList from "./components/PreUploadList/PreUploadList";
import UploadList from "./components/UploadList/UploadList";
import { Footer } from "components/Footer/Footer";
import PageTitle from "components/PageTitle/PageTitle";
import { AppContext } from "contexts/AppContext";

export default function UploadListPage() {
  const { selectedAccount } = useContext(AppContext);

  return (
    <Grid columns={12}>
      <Grid.Col span={12}>
        <PageTitle header="Uploads" />
      </Grid.Col>
      <Grid.Col span={12}>
        <Paper shadow="xs" p="md">
          {selectedAccount && !selectedAccount.isGuestAccount && (
            <Tabs>
              <Tabs.Tab label="Uploads" icon={<Upload size={14} />}>
                <UploadList />
              </Tabs.Tab>
              <Tabs.Tab label="Pending uploads" icon={<Clock size={14} />}>
                <PreUploadList />
              </Tabs.Tab>
            </Tabs>
          )}
        </Paper>
        <Footer />
      </Grid.Col>
    </Grid>
  );
}
