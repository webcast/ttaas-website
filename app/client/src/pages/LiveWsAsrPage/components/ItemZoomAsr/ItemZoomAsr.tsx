import React, { useEffect } from "react";
import { List, ThemeIcon } from "@mantine/core";
import { Check, X, Loader } from "tabler-icons-react";
import { useStartAsrSession } from "hooks/zoom-live-asr/use-zoom-live-asr";

interface ItemZoomAsrProps {
  onAsrLoaded: (result: boolean) => void;
  enableQuery: boolean;
  zoomMeetingId: string;
}

export function ItemZoomAsr({
  onAsrLoaded,
  enableQuery,
  zoomMeetingId,
}: ItemZoomAsrProps) {
  const {
    data: asrData,
    error: asrError,
    isLoading: asrLoading,
    mutate: startAsrSessionForMeeting,
  } = useStartAsrSession();

  React.useEffect(() => {
    if (enableQuery) {
      setTimeout(() => {
        startAsrSessionForMeeting({
          meetingId: zoomMeetingId,
        });
      }, 1000);
    }
  }, [enableQuery, startAsrSessionForMeeting, zoomMeetingId]);

  useEffect(() => {
    if (asrData && asrData.data.started) {
      onAsrLoaded(true);
    }
    if (asrError) {
      onAsrLoaded(false);
    }
  }, [onAsrLoaded, asrData, asrError]);

  if (asrError) {
    return (
      <List.Item
        icon={
          <ThemeIcon color="red" size={24} radius="xl">
            <X size="1rem" />
          </ThemeIcon>
        }
      >
        Unable to start Live ASR Zoom session{" "}
      </List.Item>
    );
  }

  if (asrLoading) {
    return (
      <List.Item
        icon={
          <ThemeIcon color="gray" size={24} radius="xl">
            <Loader size="1rem" />
          </ThemeIcon>
        }
      >
        Starting Live ASR Zoom session...
      </List.Item>
    );
  }

  if (asrData && asrData.data.started) {
    return (
      <List.Item
        icon={
          <ThemeIcon color="green" size={24} radius="xl">
            <Check size="1rem" />
          </ThemeIcon>
        }
      >
        Live ASR Zoom session started
      </List.Item>
    );
  }

  if (asrData && !asrData.data.started) {
    return (
      <List.Item
        icon={
          <ThemeIcon color="green" size={24} radius="xl">
            <Check size="1rem" />
          </ThemeIcon>
        }
      >
        Live ASR Zoom session not started
      </List.Item>
    );
  }
  return null;
}

export default ItemZoomAsr;
