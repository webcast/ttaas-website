/* eslint-disable react/destructuring-assignment */
/* eslint-disable no-nested-ternary */
/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { useState } from "react";
import { toast } from "react-toastify";
import {
  Box,
  Group,
  TextInput,
  Space,
  Button,
  List,
  Text,
  ThemeIcon,
  Loader,
  Anchor,
} from "@mantine/core";
import { useForm as useMantineForm } from "@mantine/form";
import { Check, X } from "tabler-icons-react";
import {
  useGetLiveAsrSessionsCount,
  useStartAsrSession,
} from "hooks/zoom-live-asr/use-zoom-live-asr";
import AuthService from "services/auth-service";

export interface IFormFields {
  zoomMeetingId: string;
  channel: string;
  password: string;
}

export default function StartZoomAsrForm() {
  const [submitDisabled, setSubmitDisabled] = useState(false);

  const {
    data: sessionsData,
    isFetching: sessionsIsFetching,
    error: sessionsError,
    isRefetching: sessionsIsRefetching,
    refetch: refetchSessions,
  } = useGetLiveAsrSessionsCount();

  const {
    data: response,
    error,
    isLoading,
    mutate: startAsrSessionForMeeting,
    reset,
  } = useStartAsrSession();

  const form = useMantineForm({
    initialValues: {
      zoomMeetingId: "",
      channel: "",
      password: "",
    },

    validate: {
      zoomMeetingId: (value: boolean | string | null) => {
        return value ? null : "Please enter the Zoom meeting ID";
      },
      channel: (value: boolean | string | null) => {
        return value ? null : "Please enter the channel";
      },
      password: (value: boolean | string | null) => {
        return value ? null : "Please enter the password";
      },
    },
  });

  const onSubmit = (values: IFormFields) => {
    console.log("onSubmit", values);
    setSubmitDisabled(true);

    if (
      sessionsData &&
      sessionsData.activeSessions < sessionsData.maxSessions
    ) {
      startAsrSessionForMeeting({
        meetingId: values.zoomMeetingId,
        channel: values.channel,
        password: values.password,
      });
    } else {
      toast.error("There are no available sessions");
    }
  };

  const clearData = () => {
    reset();
    refetchSessions();
    if (
      sessionsData &&
      sessionsData.activeSessions < sessionsData.maxSessions
    ) {
      setSubmitDisabled(false);
    }
  };

  if (!AuthService.getUsername()) {
    return <Box>User is not set. Please, reload the page.</Box>;
  }

  return (
    <Box>
      <form onSubmit={form.onSubmit(onSubmit)}>
        <TextInput
          required
          label="Zoom meeting ID"
          placeholder="The Zoom meeting where you want to enable the transcription"
          {...form.getInputProps("zoomMeetingId")}
        />
        <Space h="md" />
        <TextInput
          required
          label="Channel"
          placeholder="The channel for the transcription"
          {...form.getInputProps("channel")}
        />
        <Space h="md" />
        <TextInput
          required
          label="Password"
          placeholder="The password for the channel"
          {...form.getInputProps("password")}
        />
        <Space h="md" />
        <List spacing="xs" size="sm" center>
          {sessionsError && (
            <List.Item
              icon={
                <ThemeIcon color="red" size={24} radius="xl">
                  <X size="1rem" />
                </ThemeIcon>
              }
            >
              Something went wrong checking resources
            </List.Item>
          )}

          {(sessionsIsFetching || sessionsIsRefetching) && (
            <List.Item icon={<Loader size="sm" />}>
              Checking if there are resources...
            </List.Item>
          )}

          {!sessionsIsFetching &&
            !sessionsIsRefetching &&
            sessionsData &&
            sessionsData.activeSessions < sessionsData.maxSessions && (
              <List.Item
                icon={
                  <ThemeIcon color="green" size={24} radius="xl">
                    <Check size="1rem" />
                  </ThemeIcon>
                }
              >
                Resources available for Live ASR (Used:{" "}
                {sessionsData.activeSessions} /{sessionsData.maxSessions})
              </List.Item>
            )}

          {!sessionsIsFetching &&
            !sessionsIsRefetching &&
            sessionsData &&
            sessionsData.activeSessions >= sessionsData.maxSessions && (
              <List.Item
                icon={
                  <ThemeIcon color="red" size={24} radius="xl">
                    <X size="1rem" />
                  </ThemeIcon>
                }
              >
                There are no resources left (Used: {sessionsData.activeSessions}{" "}
                /{sessionsData.maxSessions})
              </List.Item>
            )}

          {error && (
            <List.Item
              icon={
                <ThemeIcon color="red" size={24} radius="xl">
                  <X size="1rem" />
                </ThemeIcon>
              }
            >
              Unknown error
            </List.Item>
          )}

          {response && response.data && response.data.error && (
            <List.Item
              icon={
                <ThemeIcon color="red" size={24} radius="xl">
                  <X size="1rem" />
                </ThemeIcon>
              }
            >
              {response.data.message}
            </List.Item>
          )}

          {isLoading && (
            <List.Item icon={<Loader size="sm" />}>Loading...</List.Item>
          )}

          {response && response.data && response.data.asrStarted && (
            <List.Item
              icon={
                <ThemeIcon color="green" size={24} radius="xl">
                  <Check size="1rem" />
                </ThemeIcon>
              }
            >
              Live ASR Zoom WS session started
            </List.Item>
          )}

          <Space h="md" />
          {response && response.data && response.data.asrStarted && (
            <Text size="sm">
              Zoom captions have been enabled. Your live transcriptions are
              available in the following URL{" "}
              <Anchor
                href={`https://transcription-live.app.cern.ch/?room=${form.values.channel}&pass=${form.values.password}`}
                target="_blank"
                title="Transcription's page"
              >{`https://transcription-live.app.cern.ch/?room=${form.values.channel}&pass=${form.values.password}`}</Anchor>
              .
            </Text>
          )}
        </List>
        <Group position="left" mt="md">
          <Button
            type="submit"
            loading={isLoading}
            disabled={
              !sessionsData ||
              sessionsData.activeSessions >= sessionsData.maxSessions ||
              submitDisabled
            }
          >
            Start transcription
          </Button>
          <Button
            disabled={JSON.stringify(form.errors) !== "{}"}
            onClick={clearData}
            color="gray"
          >
            Reset form
          </Button>
        </Group>
      </form>
    </Box>
  );
}
