import React from "react";
import { Grid } from "@mantine/core";
import WeblecturesList from "./components/WeblecturesList/WeblecturesList";
import PageTitle from "components/PageTitle/PageTitle";

export default function SearchWeblecturesPage() {
  return (
    <Grid columns={12}>
      <Grid.Col span={12}>
        <PageTitle header="Search public weblectures" />
      </Grid.Col>
      <Grid.Col span={12}>
        <WeblecturesList />
      </Grid.Col>
    </Grid>
  );
}
