import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import {
  Alert,
  Anchor,
  Box,
  Button,
  Grid,
  Group,
  Paper,
  Table,
  Text,
  TextInput,
} from "@mantine/core";
import { DatePicker } from "@mantine/dates";
import { useForm } from "@mantine/form";
import dayjs from "dayjs";
import { useQueryParameters } from "hooks/query-params/use-query-params";
import { useSearchWeblectures } from "hooks/search-weblectures/use-search-weblectures";
import { ISearchWeblectureFormValues } from "types/search-weblecture";
import { getFormattedDate } from "utils/date-time";

// enum SearchSortValues {
//   TITLE = "title",
//   TITLE_ASC = "title:asc",
//   TITLE_DESC = "title:desc",
//   EXTERNAL_ID = "external_id",
//   EXTERNAL_DI_ASC = "external_id:asc",
//   EXTERNAL_ID_DESC = "external_id:desc",
//   DATE = "date",
//   DATE_ASC = "date:asc",
//   DATE_DESC = "date:desc",
// }

export default function WeblecturesList() {
  const navigate = useNavigate();
  const params = useQueryParameters();
  const [perPage] = useState<number>(Number(params.get("limit")) || 20);
  const [pageNumber, setPageNumber] = useState<number>(
    Number(params.get("page")) || 1,
  );
  const [title, setTitle] = useState<string>(params.get("title") || "");
  const [text, setText] = useState<string>(params.get("text") || "");

  // const [sort, setSort] = useState<string>(
  //   params.get("sort") || SearchSortValues.DATE,
  // );
  const [dateFromString, setDateFromString] = useState<string>(
    params.get("from") || "",
  );
  const [dateToString, setDateToString] = useState<string>(
    params.get("to") || "",
  );

  const searchForm = useForm<ISearchWeblectureFormValues>({
    initialValues: {
      // sort,
      title,
      dateFrom: dateFromString !== "" ? new Date(dateFromString) : null,
      dateTo: dateToString !== "" ? new Date(dateToString) : null,
      page: 1,
      text: params.get("text") || "",
      // limit: 10,
    },
  });

  useEffect(() => {
    setPageNumber(Number(params.get("page")));
    setTitle(params.get("title") || "");
    setText(params.get("text") || "");
    // setSort(params.get("sort") || SearchSortValues.DATE);
    if (params.get("from") !== "") {
      setDateFromString(params.get("from") || "");
    }
    if (params.get("to") !== "") {
      setDateToString(params.get("to") || "");
    }
  }, [params]);

  // const [total, setTotal] = useState<number>(0);
  const [uploads, setUploads] = useState<any[]>([]);

  const { data, isLoading, error } = useSearchWeblectures(
    perPage || 1,
    pageNumber || 1,
    title || "",
    // sort || "",
    dateFromString || "",
    dateToString || "",
    text || "",
  );

  // const changePage = (newPage: number) => {
  //   const queryParams = buildQueryParams(newPage);

  //   navigate(queryParams);
  // };

  const submitSearchForm = (values: ISearchWeblectureFormValues) => {
    let titleParam = "";
    if (values.title !== "") {
      titleParam = `&title=${values.title}`;
    }
    let textParam = "";
    if (values.text !== "") {
      textParam = `&text=${values.text}`;
    }
    // let sortParam = "";
    // if (values.sort !== null) {
    //   sortParam = `&sort=${values.sort}`;
    // }
    // let idParam = "";
    // if (values.mediaId !== null) {
    //   idParam = `&external_id=${values.mediaId}`;
    // }
    let fromParam = "";
    if (values.dateFrom !== null) {
      fromParam = `&from=${dayjs(values.dateFrom).format("YYYY-MM-DD")}`;
      setDateFromString(dayjs(values.dateFrom).format("YYYY-MM-DD"));
    }
    let toParam = "";
    if (values.dateTo !== null) {
      toParam = `&to=${dayjs(values.dateTo).format("YYYY-MM-DD")}`;
      setDateToString(dayjs(values.dateTo).format("YYYY-MM-DD"));
    }
    navigate(
      `?page=${values.page}${titleParam}${fromParam}${toParam}${textParam}`,
    );
  };

  useEffect(() => {
    if (data) {
      setUploads(data.objects);
      // const tempTotal =
      //   data.total / perPage < 1 ? 1 : Math.ceil(data.total / perPage);
      // setTotal(tempTotal);
    }
  }, [perPage, pageNumber, title, data]);

  return (
    <Group grow direction="column">
      <Box>
        <form
          onSubmit={searchForm.onSubmit((values) => submitSearchForm(values))}
        >
          <Grid columns={24}>
            <Grid.Col span={12}>
              <TextInput
                label="Text"
                placeholder="Text that appears on the transcription"
                {...searchForm.getInputProps("text")}
              />
            </Grid.Col>
          </Grid>

          <Group>
            <Group mt="xl">
              <Text size="md">Filters</Text>
            </Group>
            <TextInput
              label="Title"
              placeholder="Title of the media"
              {...searchForm.getInputProps("title")}
            />
            {/* <Select
              label="Sort by"
              placeholder="Pick one"
              {...searchForm.getInputProps("sort")}
              data={Object.values(SearchSortValues).map((value) => ({
                label: value,
                value,
              }))}
            /> */}
            <DatePicker
              placeholder="Pick date"
              label="From"
              inputFormat="YYYY-MM-DD"
              labelFormat="YYYY-MM-DD"
              {...searchForm.getInputProps("dateFrom")}
            />
            <DatePicker
              placeholder="Pick date"
              label="To"
              inputFormat="YYYY-MM-DD"
              labelFormat="YYYY-MM-DD"
              {...searchForm.getInputProps("dateTo")}
            />

            <Group mt="xl">
              <Button type="submit" loading={isLoading}>
                Search
              </Button>
            </Group>
          </Group>
        </form>
      </Box>
      <Group grow direction="column">
        {isLoading && <Alert color="blue">Loading...</Alert>}
        {error && (
          <Alert color="red" title="Unable to fetch uploads">
            Error: {error.message}
          </Alert>
        )}
        {uploads.length === 0 && (
          <Alert color="blue">You don&apos;t have any uploads yet.</Alert>
        )}
        {uploads.length > 0 && (
          <Paper shadow="xs" p="md">
            <Table>
              <thead>
                <tr>
                  {/* <th>Id</th> */}
                  <th>Date</th>
                  <th>Title</th>
                  <th>Hits</th>
                  <th />
                </tr>
              </thead>
              <tbody>
                {uploads.map((upload: any) => (
                  <tr key={upload.ttpmediaid}>
                    <td>{getFormattedDate(upload.event_date)}</td>
                    <td>
                      <Anchor href={upload.url} target="_blank">
                        {upload.title} ({upload.language})
                      </Anchor>
                    </td>
                    <td>{upload.times}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
            {/* <Pagination page={pageNumber} onChange={changePage} total={total} /> */}
          </Paper>
        )}
      </Group>
    </Group>
  );
}
