import React, { useEffect } from "react";
import { Image } from "@mantine/core";

interface IMedia {
  location: string;
}

interface Props {
  mediaList: IMedia[];
}

export default function MediaImage({ mediaList }: Props) {
  const [imageLocation, setImageLocation] = React.useState<string>("");

  useEffect(() => {
    /* eslint no-plusplus: ["error", { "allowForLoopAfterthoughts": true }] */
    for (let i = 0; i < mediaList.length; i++) {
      if (
        mediaList[i].location.includes("jpg") ||
        mediaList[i].location.includes("png") ||
        mediaList[i].location.includes("jpeg")
      ) {
        setImageLocation(mediaList[i].location);
      }
    }
  }, [mediaList]);

  return <Image src={imageLocation} height={160} alt="Media image" />;
}
