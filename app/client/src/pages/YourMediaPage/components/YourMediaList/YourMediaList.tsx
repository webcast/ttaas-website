import React, { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import {
  Alert,
  Box,
  Button,
  Card,
  Group,
  Pagination,
  Select,
  Text,
  TextInput,
} from "@mantine/core";
import { DatePicker } from "@mantine/dates";
import { useForm } from "@mantine/form";
import dayjs from "dayjs";
import { Calendar, Language } from "tabler-icons-react";
import MediaImage from "../MediaImage/MediaImage";
import LoadingGrid from "components/LoadingGrid/LoadingGrid";
import { AppContext } from "contexts/AppContext";
import { useYourMedia } from "hooks/media/use-media";
import { useQueryParameters } from "hooks/query-params/use-query-params";
import { ISearchFormValues } from "types/search-form";

enum SearchSortValues {
  TITLE = "title",
  TITLE_ASC = "title:asc",
  TITLE_DESC = "title:desc",
  EXTERNAL_ID = "external_id",
  EXTERNAL_DI_ASC = "external_id:asc",
  EXTERNAL_ID_DESC = "external_id:desc",
  DATE = "date",
  DATE_ASC = "date:asc",
  DATE_DESC = "date:desc",
}

export default function YourMediaList() {
  const { selectedAccount } = useContext(AppContext);

  const navigate = useNavigate();
  const params = useQueryParameters();
  const [perPage] = useState<number>(Number(params.get("limit")) || 10);
  const [pageNumber, setPageNumber] = useState<number>(
    Number(params.get("page")) || 1,
  );
  const [title, setTitle] = useState<string>(params.get("title") || "");
  const [sort, setSort] = useState<string>(
    params.get("sort") || SearchSortValues.DATE,
  );
  const [mediaId, setMediaId] = useState<string>(
    params.get("external_id") || "",
  );
  const [dateFromString, setDateFromString] = useState<string>(
    params.get("from") || "",
  );
  // const [dateFrom, setDateFrom] = useState<Date | null>(null);
  const [dateToString, setDateToString] = useState<string>(
    params.get("to") || "",
  );
  // const [dateTo, setDateTo] = useState<Date | null>(null);

  const searchForm = useForm<ISearchFormValues>({
    initialValues: {
      mediaId,
      sort,
      title,
      dateFrom: dateFromString !== "" ? new Date(dateFromString) : null,
      dateTo: dateToString !== "" ? new Date(dateToString) : null,
      page: "1",
      // limit: 10,
    },
  });

  useEffect(() => {
    setPageNumber(Number(params.get("page")));
    setTitle(params.get("title") || "");
    setMediaId(params.get("external_id") || "");
    setSort(params.get("sort") || SearchSortValues.DATE);
    if (params.get("from") !== "") {
      setDateFromString(params.get("from") || "");
    }
    if (params.get("to") !== "") {
      setDateToString(params.get("to") || "");
    }
  }, [params]);

  const [total, setTotal] = useState<number>(0);
  const [uploads, setUploads] = useState<any[]>([]);

  const { data, isLoading, error, refetch } = useYourMedia(
    selectedAccount?.id,
    perPage || 1,
    pageNumber || 1,
    title || "",
    sort || "",
    dateFromString || "",
    dateToString || "",
    mediaId || "",
  );

  const changePage = (newPage: number) => {
    let titleParam = "";
    if (title !== "") {
      titleParam = `&title=${title}`;
    }
    let sortParam = "";
    if (sort !== null) {
      sortParam = `&sort=${sort}`;
    }
    let fromParam = "";
    if (dateFromString !== null) {
      fromParam = `&from=${dateFromString}`;
    }
    let toParam = "";
    if (dateToString !== null) {
      toParam = `&to=${dateToString}`;
    }
    navigate(`?page=${newPage}${titleParam}${sortParam}${fromParam}${toParam}`);
  };

  const submitSearchForm = (values: ISearchFormValues) => {
    let titleParam = "";
    if (values.title !== "") {
      titleParam = `&title=${values.title}`;
    }
    let sortParam = "";
    if (values.sort !== null) {
      sortParam = `&sort=${values.sort}`;
    }
    let idParam = "";
    if (values.mediaId !== null) {
      idParam = `&external_id=${values.mediaId}`;
    }
    let fromParam = "";
    if (values.dateFrom !== null) {
      fromParam = `&from=${dayjs(values.dateFrom).format("YYYY-MM-DD")}`;
      setDateFromString(dayjs(values.dateFrom).format("YYYY-MM-DD"));
    }
    let toParam = "";
    if (values.dateTo !== null) {
      toParam = `&to=${dayjs(values.dateTo).format("YYYY-MM-DD")}`;
      setDateToString(dayjs(values.dateTo).format("YYYY-MM-DD"));
    }
    navigate(
      `?page=${values.page}${idParam}${titleParam}${sortParam}${fromParam}${toParam}`,
    );
  };

  useEffect(() => {
    refetch();
  }, [selectedAccount?.id, refetch]);

  useEffect(() => {
    if (data) {
      setUploads(data.objects);
      const tempTotal =
        data.total / perPage < 1 ? 1 : Math.ceil(data.total / perPage);
      setTotal(tempTotal);
    }
  }, [perPage, pageNumber, title, data]);

  if (isLoading) {
    return <LoadingGrid />;
  }

  return (
    <Group grow direction="column">
      {error && (
        <Alert color="red" title="Unable to fetch media files">
          Error: {error.message}
        </Alert>
      )}
      {uploads.length === 0 && (
        <Alert color="blue">You don&apos;t have any media files yet.</Alert>
      )}
      <Box>
        <form
          onSubmit={searchForm.onSubmit((values) => submitSearchForm(values))}
        >
          <Group>
            <Group mt="xl">
              <Text size="md">Filters</Text>
            </Group>
            <TextInput
              label="Media ID"
              placeholder="ID of the media"
              {...searchForm.getInputProps("mediaId")}
            />
            <TextInput
              label="Title"
              placeholder="Title of the media"
              {...searchForm.getInputProps("title")}
            />
            <Select
              label="Sort by"
              placeholder="Pick one"
              {...searchForm.getInputProps("sort")}
              data={Object.values(SearchSortValues).map((value) => ({
                label: value,
                value,
              }))}
            />
            <DatePicker
              placeholder="Pick date"
              label="From"
              inputFormat="DD/MM/YYYY"
              labelFormat="DD/MM/YYYY"
              {...searchForm.getInputProps("dateFrom")}
            />
            <DatePicker
              placeholder="Pick date"
              label="To"
              inputFormat="DD/MM/YYYY"
              labelFormat="DD/MM/YYYY"
              {...searchForm.getInputProps("dateTo")}
            />

            <Group mt="xl">
              <Button type="submit">Search</Button>
            </Group>
          </Group>
        </form>
      </Box>
      <Group>
        {uploads.length > 0 &&
          uploads.map((file: any) => (
            <Card
              style={{ width: 270, cursor: "pointer" }}
              key={file.id}
              shadow="sm"
              p="xl"
              onClick={() => navigate(file.id)}
            >
              <Card.Section>
                <MediaImage mediaList={file.media} />
              </Card.Section>
              <Text weight={500} size="lg">
                {file.metadata.title}
              </Text>

              <Group position="apart">
                <Text size="sm">
                  <Calendar /> {file.metadata.date}
                </Text>
                <Text size="sm">
                  <Language /> {file.metadata.language}
                </Text>
              </Group>
            </Card>
          ))}
      </Group>
      <Pagination page={pageNumber} onChange={changePage} total={total} />
    </Group>
  );
}
