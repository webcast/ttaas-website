import React, { useContext } from "react";
import { Grid } from "@mantine/core";
import YourMediaList from "./components/YourMediaList/YourMediaList";
import PageTitle from "components/PageTitle/PageTitle";
import { AppContext } from "contexts/AppContext";

export default function YourMediaPage() {
  const { selectedAccount } = useContext(AppContext);

  return (
    <Grid columns={12}>
      <Grid.Col span={12}>
        <PageTitle header="Home" />
      </Grid.Col>
      <Grid.Col span={12}>
        {selectedAccount && !selectedAccount.isGuestAccount && (
          <YourMediaList />
        )}
      </Grid.Col>
    </Grid>
  );
}
