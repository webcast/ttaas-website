import React from "react";
import { Grid, Paper } from "@mantine/core";
import OtherUploadsList from "./components/OtherUploadsList/OtherUploadsList";
import { Footer } from "components/Footer/Footer";
import PageTitle from "components/PageTitle/PageTitle";

export default function UploadsOnOtherServicesPage() {
  return (
    <Grid columns={12}>
      <Grid.Col span={12}>
        <PageTitle header="Uploads assigned to you" />
      </Grid.Col>
      <Grid.Col span={12}>
        <Paper shadow="xs" p="md">
          <OtherUploadsList />
        </Paper>
        <Footer />
      </Grid.Col>
    </Grid>
  );
}
