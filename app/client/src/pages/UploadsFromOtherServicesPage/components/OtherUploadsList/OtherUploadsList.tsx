import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Alert, Badge, Group, Pagination, Table } from "@mantine/core";
import LoadingGrid from "components/LoadingGrid/LoadingGrid";
import { useQueryParameters } from "hooks/query-params/use-query-params";
import { useOtherUploads } from "hooks/uploads/use-uploads";
import { getFormattedDate } from "utils/date-time";

function TaskStateTag({ taskState }: { taskState: string }) {
  if (taskState.includes("Error")) {
    return <Badge color="red">{taskState}</Badge>;
  }
  if (
    taskState.includes("Waiting") ||
    taskState.includes("Uploading") ||
    taskState.includes("Running")
  ) {
    return <Badge color="orange">{taskState}</Badge>;
  }
  if (taskState.includes("Completed")) {
    return <Badge color="green">{taskState}</Badge>;
  }
  return <Badge color="blue">{taskState}</Badge>;
}

export default function UploadList() {
  const navigate = useNavigate();
  const params = useQueryParameters();
  const [perPage] = useState<number>(Number(params.get("limit")) || 10);
  const [pageNumber, setPageNumber] = useState<number>(
    Number(params.get("page")) || 1,
  );
  const [sortBy, setSortBy] = useState<string>(
    params.get("sortBy") || "created_on",
  );
  const [direction, setDirection] = useState<string>(
    params.get("direction") || "desc",
  );

  useEffect(() => {
    const pageNum = Number(params.get("page"));
    const sorting = params.get("sortBy");
    const direct = params.get("direction");
    setPageNumber(pageNum);
    setSortBy(sorting || "created_on");
    setDirection(direct || "desc");
  }, [params]);

  const [total, setTotal] = useState<number>(0);
  const [uploads, setUploads] = useState<any[]>([]);

  const { data, isLoading, error } = useOtherUploads(
    perPage || 1,
    pageNumber || 1,
    sortBy,
    direction,
  );
  const changePage = (newPage: number) => {
    navigate(`?page=${newPage}&sortBy=${sortBy}&direction=${direction}`);
  };

  const changeSortBy = (newSort: string) => {
    const direct = direction === "asc" ? "desc" : "asc";
    navigate(`?page=${pageNumber}&sortBy=${newSort}&direction=${direct}`);
  };

  useEffect(() => {
    if (data) {
      setUploads(data.objects);
      const tempTotal =
        data.total / perPage < 1 ? 1 : Math.ceil(data.total / perPage);
      setTotal(tempTotal);
    }
  }, [perPage, pageNumber, sortBy, direction, data]);

  if (isLoading) {
    return <LoadingGrid />;
  }

  return (
    <Group grow direction="column">
      {error && (
        <Alert color="red" title="Unable to fetch uploads">
          Error: {error.message}
        </Alert>
      )}
      {uploads.length === 0 && (
        <Alert color="blue">
          You don&apos;t have any uploads assigned to your username yet.
        </Alert>
      )}
      {uploads.length > 0 && (
        <>
          <Table>
            <thead>
              <tr>
                <th
                  style={{ cursor: "pointer" }}
                  onClick={() => changeSortBy("id")}
                >
                  Id
                </th>
                <th
                  style={{ cursor: "pointer" }}
                  onClick={() => changeSortBy("created_on")}
                >
                  Created
                </th>
                <th
                  style={{ cursor: "pointer" }}
                  onClick={() => changeSortBy("title")}
                >
                  Title
                </th>
                <th
                  style={{ cursor: "pointer" }}
                  onClick={() => changeSortBy("state")}
                >
                  State
                </th>
                <th>Account</th>
                <th
                  style={{ cursor: "pointer" }}
                  onClick={() => changeSortBy("updated_on")}
                >
                  Last update
                </th>
              </tr>
            </thead>
            <tbody>
              {uploads.map((upload: any) => (
                <tr key={upload.mediaId}>
                  <td>{upload.id}</td>
                  <td>{getFormattedDate(upload.createdOn)}</td>
                  <td>{upload.title}</td>
                  <td>
                    <TaskStateTag taskState={upload.state} />
                  </td>
                  <td>{upload.account}</td>
                  <td>{getFormattedDate(upload.updatedOn)}</td>
                </tr>
              ))}
            </tbody>
          </Table>
          <Pagination page={pageNumber} onChange={changePage} total={total} />
        </>
      )}
    </Group>
  );
}
