import { IConfig } from "./types/config";

const mediaMaxSize = 2 * 1024 * 1024 * 1024;
const attachmentMaxSize = 50 * 1024 * 1024;
const supportedVideoMediaMimeTypes = [
  "video/mp4",
  "video/x-m4v",
  "video/quicktime",
  "video/ogg",
  "video/mpeg",
  "video/webm",
  "video/x-msvideo",
  "video/x-ms-wmv",
  "video/x-flv",
  "audio/x-wav",
  "audio/mpeg",
  "audio/ogg",
  "audio/aac",
  "audio/x-flac",
  "application/ogg",
];
const supportedAttachmentMimeTypes = [
  "application/pdf", // pdf
  "application/msword", // doc
  "application/vnd.openxmlformats-officedocument.wordprocessingml.document", // docx
  "text/plain", // txt, srt, trs?
  "application/vnd.ms-powerpoint", // ppt
  "application/vnd.openxmlformats-officedocument.presentationml.presentation", // pptx
  "application/ttml+xml", // dfxp
];

const dev: IConfig = {
  MEDIA_MAX_SIZE: mediaMaxSize, // 2GB
  MEDIA_MIMES: supportedVideoMediaMimeTypes,
  ATTACHMENT_MIMES: supportedAttachmentMimeTypes,
  ATTACHMENT_MAX_SIZE: attachmentMaxSize, // 50MB
  LIVE_ASR_ACCOUNT_ID: import.meta.env.VITE_LIVE_ASR_ACCOUNT_ID
    ? +import.meta.env.VITE_LIVE_ASR_ACCOUNT_ID
    : 2,
  API: {
    ENDPOINT: "http://localhost:5000",
  },
  IS_DEV_INSTALL: true,
  OIDC: {
    CLIENT_ID: import.meta.env.VITE_OIDC_CLIENT_ID
      ? import.meta.env.VITE_OIDC_CLIENT_ID
      : "mbp_rene",
    CLIENT_URL: import.meta.env.VITE_OIDC_CLIENT_URL
      ? import.meta.env.VITE_OIDC_CLIENT_URL
      : "http://localhost:3000",
  },
  SENTRY_DSN: import.meta.env.VITE_SENTRY_DSN
    ? import.meta.env.VITE_SENTRY_DSN
    : "",
  SENTRY_ENV: import.meta.env.VITE_SENTRY_ENV
    ? import.meta.env.VITE_SENTRY_ENV
    : "",
};

const prod: IConfig = {
  MEDIA_MAX_SIZE: mediaMaxSize, // 2GB
  MEDIA_MIMES: supportedVideoMediaMimeTypes,
  ATTACHMENT_MIMES: supportedAttachmentMimeTypes,
  ATTACHMENT_MAX_SIZE: attachmentMaxSize, // 50MB
  LIVE_ASR_ACCOUNT_ID: import.meta.env.VITE_LIVE_ASR_ACCOUNT_ID
    ? +import.meta.env.VITE_LIVE_ASR_ACCOUNT_ID
    : 11,
  API: {
    ENDPOINT: import.meta.env.VITE_API_ENDPOINT
      ? import.meta.env.VITE_API_ENDPOINT
      : "<TODO>",
  },
  IS_DEV_INSTALL: import.meta.env.VITE_IS_DEV_INSTALL === "true",
  OIDC: {
    CLIENT_ID: import.meta.env.VITE_OIDC_CLIENT_ID
      ? import.meta.env.VITE_OIDC_CLIENT_ID
      : "mbp_rene",
    CLIENT_URL: import.meta.env.VITE_OIDC_CLIENT_URL
      ? import.meta.env.VITE_OIDC_CLIENT_URL
      : "http://localhost:3000",
  },
  SENTRY_DSN: import.meta.env.VITE_SENTRY_DSN
    ? import.meta.env.VITE_SENTRY_DSN
    : "",
  SENTRY_ENV: import.meta.env.VITE_SENTRY_ENV
    ? import.meta.env.VITE_SENTRY_ENV
    : "",
};

const test: IConfig = {
  MEDIA_MAX_SIZE: mediaMaxSize, // 2GB
  MEDIA_MIMES: supportedVideoMediaMimeTypes,
  ATTACHMENT_MIMES: supportedAttachmentMimeTypes,
  ATTACHMENT_MAX_SIZE: attachmentMaxSize, // 50MB
  LIVE_ASR_ACCOUNT_ID: import.meta.env.VITE_LIVE_ASR_ACCOUNT_ID
    ? +import.meta.env.VITE_LIVE_ASR_ACCOUNT_ID
    : 11,
  API: {
    ENDPOINT: "http://127.0.0.1:8080",
  },
  IS_DEV_INSTALL: true,
  OIDC: {
    CLIENT_ID: "mbp_rene",
    CLIENT_URL: import.meta.env.VITE_OIDC_CLIENT_URL
      ? import.meta.env.VITE_OIDC_CLIENT_URL
      : "http://localhost:3000",
  },
  SENTRY_DSN: "12345",
  SENTRY_ENV: "",
};

let tempConfig = test;

if (import.meta.env.PROD === true) {
  tempConfig = prod;
}

if (import.meta.env.DEV === true) {
  tempConfig = dev;
}
const config = tempConfig;

export default config;
