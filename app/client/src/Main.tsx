import React from "react";
import { QueryClientProvider } from "react-query";
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import { Center } from "@mantine/core";
import { ReactQueryDevtools } from "react-query/devtools";
import queryClient from "./api/query-client";
import App from "./App";
import HomePage from "./pages/HomePage/HomePage";
import LoadingGrid from "components/LoadingGrid/LoadingGrid";
import { AppContext } from "contexts/AppContext";
import { useAppContext } from "hooks/app-context/use-app-context";
import ZoomLiveAsrPage from "pages/LiveAsrPage/ZoomLiveAsrPage";
import WsLiveAsrPage from "pages/LiveWsAsrPage/WsLiveAsrPage";
import LoginPage from "pages/LoginPage/LoginPage";
import LogoutPage from "pages/LogoutPage/LogoutPage";
import SearchDetailsPage from "pages/SearchDetailsPage/SearchDetailsPage";
import SearchWeblecturesPage from "pages/SearchWeblecturesPage/SearchWeblecturesPage";
import SettingsPage from "pages/SettingsPage/SettingsPage";
import UploadFormPage from "pages/UploadFormPage/UploadFormPage";
import UploadListPage from "pages/UploadListPage/UploadListPage";
import UploadsFromOtherServicesPage from "pages/UploadsFromOtherServicesPage/UploadsFromOtherServicesPage";
import YourFilesDetailsPage from "pages/YourFilesDetailsPage/YourFilesDetailsPage";
import YourFilesPage from "pages/YourMediaPage/YourMediaPage";
import routes from "routes";

interface Props {
  initialized: boolean;
}
export default function Main({ initialized }: Props) {
  const appContext = useAppContext();

  if (!initialized) {
    // console.log(initialized);
    return (
      <Center style={{ width: "100vw", height: "100vh" }}>
        <LoadingGrid />
      </Center>
    );
  }

  return (
    <AppContext.Provider value={appContext}>
      <QueryClientProvider client={queryClient}>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<Navigate to={routes.appBaseRoute} />} />
            <Route path={routes.loginRoute} element={<LoginPage />} />
            <Route path={routes.logoutRoute} element={<LogoutPage />} />
            <Route path={routes.appBaseRoute} element={<App />}>
              <Route path="" element={<Navigate to={routes.homeRoute} />} />
              <Route path={routes.homeRoute} element={<HomePage />} />
              <Route path={routes.yourFilesRoute} element={<YourFilesPage />} />
              <Route
                path={routes.yourFilesDetailesRoute}
                element={<YourFilesDetailsPage />}
              />
              <Route
                path={routes.otherUploadsRoute}
                element={<UploadsFromOtherServicesPage />}
              />
              <Route
                path={routes.uploadFormRoute}
                element={<UploadFormPage />}
              />
              <Route path={routes.uploadsRoute} element={<UploadListPage />} />
              <Route path={routes.settingsRoute} element={<SettingsPage />} />
              <Route path={routes.zoomLiveAsr} element={<ZoomLiveAsrPage />} />
              <Route path={routes.wsLiveAsr} element={<WsLiveAsrPage />} />
              <Route
                path={routes.searchWeblectures}
                element={<SearchWeblecturesPage />}
              />
              <Route
                path={routes.searchWeblecturesDetails}
                element={<SearchDetailsPage />}
              />
            </Route>
          </Routes>
        </BrowserRouter>
        <ReactQueryDevtools initialIsOpen={false} />
      </QueryClientProvider>
    </AppContext.Provider>
  );
}
