/* eslint-disable import/no-extraneous-dependencies */
import { rest } from "msw";
import accountMock from "./responses/account";
import messagesMock from "./responses/messages";
import userMock from "./responses/user";

export const handlers = [
  rest.get("/api/private/v1/user/", (req, res, ctx) => {
    // If authenticated, return a mocked user details
    console.log("userMock", userMock);
    return res(ctx.status(200), ctx.json(userMock));
  }),
  rest.get("/api/private/v1/accounts/", (req, res, ctx) => {
    // If authenticated, return a mocked user details
    return res(ctx.status(200), ctx.json(accountMock));
  }),
  rest.get("/api/private/v1/messages/", (req, res, ctx) => {
    // If authenticated, return a mocked user details
    return res(ctx.status(200), ctx.json(messagesMock));
  }),
];

export default handlers;
