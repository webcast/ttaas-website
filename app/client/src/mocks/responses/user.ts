import { IUser } from "types/user";

const userMock: IUser = {
  username: "username",
  firstName: "first",
  lastName: "last",
  email: "email@cernemail.ch",
  roles: ["group1", "group2"],
};

export default userMock;
