import { IAccount } from "types/account";

const accountMock: IAccount[] = [
  { id: 1, accountName: "AccountA", isGuestAccount: false },
  { id: 2, accountName: "AccountB", isGuestAccount: false },
];

export default accountMock;
