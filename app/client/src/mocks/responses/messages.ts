import { IMessage } from "types/message";

const messagesMock: IMessage[] = [
  {
    id: 1,
    body: "Hello World",
    createdOn: "2019-01-01T00:00:00.000Z",
    updatedOn: "2019-01-01T00:00:00.000Z",
    category: "danger",
    title: "Message 1",
  },
];

export default messagesMock;
