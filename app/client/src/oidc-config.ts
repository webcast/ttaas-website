import { User } from "oidc-client-ts";
import config from "config";

const onSigninCallback = (_user: User | void): void => {
  if (_user) {
    localStorage.setItem("token", _user.access_token);
  }
  window.history.replaceState({}, document.title, window.location.pathname);
};

const onRemoveUser = (): void => {
  localStorage.removeItem("token");
};

const oidcConfig = {
  authority: "https://auth.cern.ch/auth/realms/cern",
  client_id: config.OIDC.CLIENT_ID,
  redirect_uri: config.OIDC.CLIENT_URL,
  post_logout_redirect_uri: `${config.OIDC.CLIENT_URL}/app-logout`,
  onSigninCallback,
  onRemoveUser,
  // ...
};

export default oidcConfig;
