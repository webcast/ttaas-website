interface IAPI {
  ENDPOINT: string;
}

interface IOIDC {
  CLIENT_ID: string;
  CLIENT_URL: string;
}

export interface IConfig {
  MEDIA_MAX_SIZE: number;
  MEDIA_MIMES: string[];
  ATTACHMENT_MAX_SIZE: number;
  ATTACHMENT_MIMES: string[];
  API: IAPI;
  IS_DEV_INSTALL: boolean;
  OIDC: IOIDC;
  SENTRY_DSN: string;
  SENTRY_ENV: string;
  LIVE_ASR_ACCOUNT_ID: number;
}
