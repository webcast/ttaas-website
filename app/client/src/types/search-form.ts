export interface ISearchFormValues {
  page: string;
  sort: string;
  title: string;
  dateFrom: Date | null;
  dateTo: Date | null;
  mediaId: string;
}
