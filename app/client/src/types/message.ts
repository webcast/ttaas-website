export interface IMessage {
  id: number;
  title: string;
  body: string;
  category: string;
  createdOn: string;
  updatedOn: string;
}
