export interface ISearchWeblectureFormValues {
  page: number;
  // sort: string;
  title: string;
  dateFrom: Date | null;
  dateTo: Date | null;
  text: string;
}
