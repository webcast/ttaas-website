export interface IAccount {
  id: number;
  accountName: string;
  isGuestAccount: boolean;
}
