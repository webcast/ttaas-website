export type ApiScopes = "all";

export interface ITokenFormFields {
  tokenName: string;
  scope: ApiScopes;
  account: number;
}
