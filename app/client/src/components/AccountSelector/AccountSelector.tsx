import React, { useContext, useEffect, useState } from "react";
import { toast } from "react-toastify";
import { Select, SelectItem } from "@mantine/core";
import { AppContext } from "contexts/AppContext";
import { useAcounts } from "hooks/accounts/use-accounts";
import { IAccount } from "types/account";

export default function AccountSelector() {
  const { selectedAccount, setSelectedAccountContext } = useContext(AppContext);
  const [accountsOptions, setAccountsOptions] = useState<SelectItem[]>([]);

  const getAccountsOptions = (response: any): SelectItem[] => {
    if (response) {
      return response.map((account: IAccount) => ({
        value: account.id.toString(),
        label: account.accountName,
      }));
    }
    return [];
  };
  const { data: accounts, isLoading: accountsLoading } = useAcounts();

  const getAccountById = (value: string): IAccount | null => {
    if (accounts) {
      return accounts.filter((option) => option.id.toString() === value)[0];
    }
    return null;
  };

  useEffect(() => {
    if (accounts) {
      const options = getAccountsOptions(accounts);
      setAccountsOptions(options);
    }
  }, [accounts]);

  const selectAccount = (value: string | null) => {
    if (value) {
      const tempAccount = getAccountById(value);
      setSelectedAccountContext(tempAccount);
      // setSelectedAccountId(value);
      toast.success(`Account ${tempAccount?.accountName} selected`);
    }
  };

  return (
    <Select
      label="Select an account"
      placeholder="Account"
      disabled={accountsLoading}
      data={accountsOptions}
      value={selectedAccount?.id.toString()}
      onChange={selectAccount}
    />
  );
}
