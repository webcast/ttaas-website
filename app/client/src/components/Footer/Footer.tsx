import React, { useState } from "react";
import { Anchor, List, Modal, Text, Title } from "@mantine/core";
import { PACKAGE_VERSION } from "version";

export function Footer() {
  const [opened, setOpened] = useState(false);
  return (
    <Text align="center" size="sm" style={{ marginTop: 20 }}>
      Version {PACKAGE_VERSION} - CERN Transcription and Translation Service -
      2022 - {new Date().getFullYear()} -{" "}
      <Anchor onClick={() => setOpened(true)}>Credits</Anchor>
      <Modal
        opened={opened}
        onClose={() => setOpened(false)}
        title={<Title order={3}>Credits</Title>}
      >
        <Title order={4} style={{ marginBottom: 10 }}>
          Images and resources
        </Title>
        <List>
          <List.Item>
            <Anchor
              href="https://www.flaticon.com/free-icons/transcription"
              title="transcription icons"
              target="_blank"
              rel="noopener noreferrer"
            >
              Transcription icon created by Freepik - Flaticon
            </Anchor>
          </List.Item>
          <List.Item>
            {" "}
            <Anchor
              href="https://undraw.co/search"
              title="transcription icons"
              target="_blank"
              rel="noopener noreferrer"
            >
              Illustrations by undraw
            </Anchor>
          </List.Item>
        </List>
      </Modal>
    </Text>
  );
}

export default Footer;
