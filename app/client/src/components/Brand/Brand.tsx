import React, { useContext } from "react";
import {
  Badge,
  Box,
  Burger,
  Group,
  MediaQuery,
  Text,
  useMantineColorScheme,
} from "@mantine/core";
import Logo from "components/Logo/Logo";
import { AppContext } from "contexts/AppContext";

export default function Brand({
  opened,
  setOpened,
}: {
  opened: boolean;
  setOpened: (opened: boolean) => void;
}) {
  const { colorScheme } = useMantineColorScheme();
  const { selectedAccount } = useContext(AppContext);

  return (
    <Box
      sx={(theme) => ({
        paddingLeft: theme.spacing.xs,
        paddingRight: theme.spacing.xs,
        paddingBottom: theme.spacing.lg,
        borderBottom: `1px solid ${
          theme.colorScheme === "dark"
            ? theme.colors.dark[4]
            : theme.colors.gray[2]
        }`,
      })}
    >
      <Group position="apart">
        <Group>
          {selectedAccount && (
            <MediaQuery largerThan="sm" styles={{ display: "none" }}>
              <Burger
                opened={opened}
                onClick={() => setOpened(!opened)}
                size="sm"
                // color={theme.colors.gray[6]}
                mr="xl"
              />
            </MediaQuery>
          )}
          <Logo colorScheme={colorScheme} />
        </Group>

        <Group>
          <MediaQuery smallerThan="sm" styles={{ display: "none" }}>
            <Text size="xs">
              {selectedAccount ? (
                <>
                  Selected account: <Badge>{selectedAccount.accountName}</Badge>
                </>
              ) : (
                "No account selected"
              )}
            </Text>
          </MediaQuery>
        </Group>
      </Group>
    </Box>
  );
}
