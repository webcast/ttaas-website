import React from "react";
import { Anchor, Group, Header, Text } from "@mantine/core";
import { User } from "tabler-icons-react";
import AuthService from "services/auth-service";

export default function TopMenu() {
  const linkStyle = { color: "#ccc", padding: "6px 8px", cursor: "pointer" };

  const logout = () => {
    AuthService.doLogout({
      redirectUri: `${encodeURI(window.location.href)}/`,
    });
  };

  const login = () => {
    AuthService.doLogin({
      redirectUri: encodeURI(window.location.href),
    });
  };
  return (
    <Header
      height={50}
      p="xs"
      style={{ color: "#fff", backgroundColor: "#222" }}
      className="cern-toolbar"
    >
      <Group position="apart">
        <Group>
          <Text size="sm">
            <strong>CERN</strong>
          </Text>
          <Text size="xs">Accelerating Science</Text>
        </Group>
        <Group>
          {AuthService.isLoggedIn() ? (
            <>
              <Text size="xs">
                <User name="user" size={15} /> Logged in as:{" "}
                <strong
                  title={`User: ${AuthService.getFirstName()} ${AuthService.getLastName()}`}
                >
                  {AuthService.getFirstName()} {AuthService.getLastName()} (
                  {AuthService.getUsername()})
                </strong>
              </Text>
              <Anchor
                size="xs"
                title="Logout"
                onClick={logout}
                style={linkStyle}
              >
                Logout
              </Anchor>
            </>
          ) : (
            <Group>
              <Text size="xs" onClick={login} style={linkStyle}>
                Login
              </Text>
            </Group>
          )}
          <Anchor
            size="xs"
            title="CERN Directory"
            href="https://cern.ch/directory"
            target="_blank"
            style={linkStyle}
          >
            Directory
          </Anchor>
        </Group>
      </Group>
    </Header>
  );
}

TopMenu.defaultProps = {
  user: undefined,
};
