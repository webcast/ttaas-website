import React from "react";
import { screen } from "@testing-library/react";
import TopMenu from "./TopMenu";
import {
  asyncForEach,
  createAuthMockStub,
  renderWithAllProviders,
} from "test-utils";

const mockAuthStub = createAuthMockStub(true, true);

vi.mock("react-oidc-context", () => {
  const originalModule = jest.requireActual("react-oidc-context");
  return {
    ...originalModule,
    useAuth: () => ({ ...mockAuthStub, signinRedirect: vi.fn() }),
  };
});

vi.mock("react-oidc-context");

describe("TopMenu tests", () => {
  it("renders expected texts", async () => {
    const providerProps = {
      user: {
        uid: "username",
        username: "username",
        firstName: "firstName",
        lastName: "lastName",
        email: "email",
        roles: ["account1", "account2"],
      },
      selectedAccount: {
        id: 1,
        accountName: "AccountA",
        isGuestAccount: false,
      },
      setUserContext: vi.fn(),
      setSelectedAccountContext: vi.fn(),
    };
    renderWithAllProviders(<TopMenu />, {
      providerProps,
    });
    const texts = [/CERN/i, /Accelerating Science/i, /Directory/i];

    await asyncForEach(texts, async (text: string) => {
      const items = await screen.findByText(text);
      expect(items).toBeInTheDocument();
    });
  });
});

describe("TopMenu not logged in tests", () => {
  it("renders expected texts", async () => {
    const providerProps = {
      user: {
        uid: "username",
        username: "username",
        firstName: "firstName",
        lastName: "lastName",
        email: "email",
        roles: ["account1", "account2"],
      },
      selectedAccount: {
        id: 1,
        accountName: "AccountA",
        isGuestAccount: false,
      },
      setUserContext: vi.fn(),
      setSelectedAccountContext: vi.fn(),
    };
    renderWithAllProviders(<TopMenu />, {
      providerProps,
    });
    const texts = [/CERN/i, /Accelerating Science/i, /Directory/i];

    await asyncForEach(texts, async (text: string) => {
      const items = await screen.findByText(text);
      expect(items).toBeInTheDocument();
    });
  });
});
