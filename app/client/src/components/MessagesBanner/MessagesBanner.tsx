import React, { useEffect } from "react";
import { Alert, Group } from "@mantine/core";
import { AlertCircle } from "tabler-icons-react";
import { useMessages } from "hooks/messages/use-messages";
import { IMessage } from "types/message";

export default function MessagesBanner() {
  const [messagesFormatted, setMessagesFormatted] = React.useState<IMessage[]>(
    [],
  );

  const getMessageColor = (messageCategory: string) => {
    switch (messageCategory) {
      case "error":
        return "red";
      case "warning":
        return "yellow";
      case "info":
        return "blue";
      default:
        return "green";
    }
  };

  const { data: messages, error } = useMessages();

  useEffect(() => {
    if (messages) {
      let tempMessages: IMessage[] = [];
      messages.forEach((result: IMessage) => {
        const tempResult = result;
        tempResult.category = getMessageColor(result.category);
        tempMessages = [...tempMessages, tempResult];
      });
      setMessagesFormatted(tempMessages);
    }
  }, [messages]);

  if (error) {
    return <Alert color="red">{error.message}</Alert>;
  }
  if (messagesFormatted && messagesFormatted.length > 0) {
    return (
      <Group style={{ marginBottom: 20 }} grow>
        {messagesFormatted.map((message: IMessage) => (
          <Alert
            icon={<AlertCircle size={16} />}
            title={message.title}
            color={message.category}
            key={message.id}
          >
            {message && message.body}
          </Alert>
        ))}
      </Group>
    );
  }

  return null;
}
