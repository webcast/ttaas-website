import React from "react";
import { useNavigate } from "react-router-dom";
import { Group, Title, Text, Badge, ActionIcon } from "@mantine/core";
import { ArrowLeft } from "tabler-icons-react";

interface PageTitleProps {
  withBackButton?: boolean;
  header: string;
  subHeader?: string;
}

function PageTitle({ withBackButton, header, subHeader }: PageTitleProps) {
  const navigate = useNavigate();
  const navigateBack = () => {
    navigate(-1);
  };

  return (
    <Group direction="column" grow>
      <Title
        order={1}
        sx={(theme) => ({
          color:
            theme.colorScheme === "dark"
              ? theme.colors.dark[2]
              : theme.colors.gray[9],
        })}
      >
        {withBackButton && (
          <ActionIcon style={{ display: "inline" }} onClick={navigateBack}>
            <ArrowLeft />
          </ActionIcon>
        )}{" "}
        {header}
      </Title>
      <Text
        size="sm"
        sx={(theme) => ({
          color:
            theme.colorScheme === "dark"
              ? theme.colors.dark[3]
              : theme.colors.gray[7],
        })}
      >
        {subHeader && <Badge>{subHeader}</Badge>}
      </Text>
    </Group>
  );
}

PageTitle.defaultProps = {
  withBackButton: false,
  subHeader: null,
};

export default PageTitle;
