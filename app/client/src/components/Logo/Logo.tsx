import React from "react";
import { Group, ColorScheme, Text, Image, Badge } from "@mantine/core";
import config from "config";
import { PACKAGE_VERSION } from "version";
// import routes from "routes";

export default function Logo({ colorScheme }: { colorScheme: ColorScheme }) {
  return (
    <Group>
      <Image width={30} height={30} src="/transcription.png" alt="CERN logo" />
      <Text color={colorScheme === "dark" ? "#fff" : "#000"}>
        <strong>Transcription and Translation Service</strong>{" "}
      </Text>
      <Badge variant="outline">{PACKAGE_VERSION}</Badge>
      {config.IS_DEV_INSTALL && <Badge color="orange">DEV Install</Badge>}
    </Group>
  );
}
