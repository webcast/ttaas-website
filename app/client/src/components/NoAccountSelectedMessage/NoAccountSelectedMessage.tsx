import React from "react";
import { Grid, Alert } from "@mantine/core";
import { Id } from "tabler-icons-react";

export default function NoAccountSelectedMessage() {
  return (
    <Grid columns={12}>
      <Grid.Col span={12}>
        <Alert color="blue" title="No account selected" icon={<Id />}>
          Please, select an account to continue.
        </Alert>
      </Grid.Col>
    </Grid>
  );
}
