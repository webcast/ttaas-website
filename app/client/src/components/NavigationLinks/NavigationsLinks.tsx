import React, { useContext } from "react";
import {
  Home,
  Files,
  FileUpload,
  FileInfo,
  Settings,
  Upload,
  VideoPlus,
  Search,
  Video,
} from "tabler-icons-react";
import NavigationLink from "./NavigationLink";
import config from "config";
import { AppContext } from "contexts/AppContext";
import routes from "routes";

const data = [
  {
    icon: <Home size={16} />,
    color: "blue",
    label: "Home",
    route: routes.homeRoute,
    showToGuest: true,
  },
  {
    icon: <Files size={16} />,
    color: "teal",
    label: "Your Media",
    route: routes.yourFilesRoute,
    showToGuest: false,
  },
  {
    icon: <Upload size={16} />,
    color: "violet",
    label: "Upload media",
    route: routes.uploadFormRoute,
    showToGuest: false,
  },
  {
    icon: <FileUpload size={16} />,
    color: "orange",
    label: "Your uploads",
    route: routes.uploadsRoute,
    showToGuest: false,
  },
  {
    icon: <FileInfo size={16} />,
    color: "grape",
    label: "Uploads assigned to you",
    route: routes.otherUploadsRoute,
    showToGuest: true,
  },
  {
    icon: <Search size={16} />,
    color: "red",
    label: "Search public weblectures",
    route: routes.searchWeblectures,
    showToGuest: true,
  },
  {
    icon: <Settings size={16} />,
    color: "red",
    label: "Settings",
    route: routes.settingsRoute,
    showToGuest: false,
  },
];

const liveAsrLink = {
  icon: <VideoPlus size={16} />,
  color: "blue",
  label: "Zoom Live ASR",
  route: routes.zoomLiveAsr,
  showToGuest: false,
};

const liveWsAsrLink = {
  icon: <Video size={16} />,
  color: "blue",
  label: "WS Live ASR",
  route: routes.wsLiveAsr,
  showToGuest: false,
};

export default function NavigationLinks() {
  const { selectedAccount } = useContext(AppContext);

  let links = [];
  if (selectedAccount && selectedAccount.isGuestAccount) {
    const ages = data.filter((link) => {
      return link.showToGuest === true;
    });
    links = ages.map((link) => (
      <NavigationLink {...link} key={link.label} to={link.route} />
    ));
  } else if (
    selectedAccount &&
    selectedAccount.id === config.LIVE_ASR_ACCOUNT_ID
  ) {
    links = [
      <NavigationLink
        {...liveAsrLink}
        key={liveAsrLink.label}
        to={liveAsrLink.route}
      />,
      <NavigationLink
        {...liveWsAsrLink}
        key={liveWsAsrLink.label}
        to={liveWsAsrLink.route}
      />,
    ];
  } else {
    links = data.map((link) => (
      <NavigationLink {...link} key={link.label} to={link.route} />
    ));
  }
  return <div>{links}</div>;
}
