export const initialZoomLiveAsrState = {
  enableLiveQuery: false,
  enableSetSettingsQuery: false,
  enableAsrQuery: false,
  meetingIsLive: false,
  settingsOk: false,
  asrOk: false,
};

export function zoomLiveAsrReducer(state: any, action: any) {
  console.log(action);
  switch (action.type) {
    case "set_meeting_live":
      return {
        ...state,
        meetingIsLive: true,
        enableSettingsQuery: true,
        enableLiveQuery: false,
      };
    case "set_meeting_not_live":
      return {
        ...state,
        meetingIsLive: false,
        enableSettingsQuery: false,
        enableLiveQuery: false,
      };
    case "set_settings_ok":
      return {
        ...state,
        settingsOk: action.payload,
        enableSettingsQuery: false,
      };
    case "set_asr_ok":
      return { ...state, asrOk: action.payload, enableAsrQuery: false };
    case "disable_live_query":
      return { ...state, enableLiveQuery: false };
    case "disable_settings_query":
      return { ...state, enableSettingsQuery: false };
    case "disable_asr_query":
      return { ...state, enableAsrQuery: false };
    case "disable_all_queries":
      return initialZoomLiveAsrState;
    case "enable_live_query":
      // eslint-disable-next-line no-case-declarations
      const result = {
        ...state,
        enableLiveQuery: true,
        enableSettingsQuery: false,
        enableAsrQuery: false,
      };
      console.log(result);
      return result;
    case "enable_settings_query":
      console.log("enable_settings_query");
      return {
        ...state,
        enableLiveQuery: false,
        enableSettingsQuery: true,
        enableAsrQuery: false,
      };
    case "enable_asr_query":
      return {
        ...state,
        enableLiveQuery: false,
        enableSettingsQuery: false,
        enableAsrQuery: true,
      };
    default:
      throw new Error();
  }
}
export default zoomLiveAsrReducer;
