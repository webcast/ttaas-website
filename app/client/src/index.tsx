import React from "react";
import * as Sentry from "@sentry/react";
import ReactDOM from "react-dom/client";
import "react-toastify/dist/ReactToastify.css";
import "./index.css";
import reportWebVitals from "./reportWebVitals";
import setupInterceptors from "api/axios-interceptors";
import config from "config";
import MainAuth from "MainAuth";

if (process.env.NODE_ENV === "production") {
  Sentry.init({
    dsn: config.SENTRY_DSN,
    environment: config.SENTRY_ENV,
    // Performance Monitoring
    tracesSampleRate: 1.0, //  Capture 100% of the transactions
  });
}

// if (process.env.NODE_ENV !== "production") {
//   // eslint-disable-next-line global-require
//   const { default: axe } = require("@axe-core/react");
//   axe(React, ReactDOM, 1000);
// }

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement,
);

setupInterceptors();

root.render(
  <React.StrictMode>
    <MainAuth />
  </React.StrictMode>,
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
