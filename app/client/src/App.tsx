import React, { useContext, useState } from "react";
import "./App.css";
import { Outlet } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import {
  AppShell,
  Navbar,
  Header,
  ActionIcon,
  ColorScheme,
  MantineProvider,
  ColorSchemeProvider,
  Group,
  Anchor,
  Container,
  Text,
  Paper,
} from "@mantine/core";
import { Sun, MoonStars, QuestionMark } from "tabler-icons-react";
import AccountSelector from "components/AccountSelector/AccountSelector";
import Brand from "components/Brand/Brand";
import { Footer } from "components/Footer/Footer";
import MessagesBanner from "components/MessagesBanner/MessagesBanner";
import NavigationLinks from "components/NavigationLinks/NavigationsLinks";
import NoAccountSelectedMessage from "components/NoAccountSelectedMessage/NoAccountSelectedMessage";
import PageTitle from "components/PageTitle/PageTitle";
import TopMenu from "components/TopMenu/TopMenu";
import { AppContext } from "contexts/AppContext";

function App() {
  const { selectedAccount } = useContext(AppContext);

  const [colorScheme, setColorScheme] = useState<ColorScheme>("light");
  const toggleColorScheme = (value?: ColorScheme) =>
    setColorScheme(value || (colorScheme === "dark" ? "light" : "dark"));
  const [opened, setOpened] = useState(false);

  return (
    <ColorSchemeProvider
      colorScheme={colorScheme}
      toggleColorScheme={toggleColorScheme}
    >
      <MantineProvider theme={{ colorScheme }}>
        <AppShell
          navbarOffsetBreakpoint="sm"
          padding="md"
          navbar={
            selectedAccount ? (
              <Navbar
                hiddenBreakpoint="sm"
                hidden={!opened}
                width={{ sm: 300, lg: 400 }}
                height="calc(100vh - 105px)"
                p="md"
              >
                <Navbar.Section mt="md">
                  <AccountSelector />
                </Navbar.Section>
                <Navbar.Section grow mt="md">
                  <NavigationLinks />
                </Navbar.Section>
                <Navbar.Section>
                  <Group>
                    <ActionIcon
                      variant="default"
                      onClick={() => toggleColorScheme()}
                      size={30}
                    >
                      {colorScheme === "dark" ? (
                        <Sun size={16} />
                      ) : (
                        <MoonStars size={16} />
                      )}
                    </ActionIcon>
                    <Anchor
                      href="/api/public/v1/"
                      target="_blank"
                      title="API documentation"
                    >
                      API
                    </Anchor>
                  </Group>
                </Navbar.Section>
              </Navbar>
            ) : undefined
          }
          header={
            <>
              <TopMenu />
              <Header height={55} p="xs">
                <Brand opened={opened} setOpened={setOpened} />
              </Header>
            </>
          }
          styles={(theme) => ({
            main: {
              backgroundColor:
                theme.colorScheme === "dark"
                  ? theme.colors.dark[8]
                  : theme.colors.gray[0],
            },
          })}
        >
          <MessagesBanner />
          {selectedAccount ? (
            <Outlet />
          ) : (
            <Container size="xs" px="xs">
              <PageTitle header="Account selection" subHeader="" />
              <Paper shadow="sm" p="lg">
                <Group grow direction="column">
                  <NoAccountSelectedMessage />
                  <AccountSelector />
                  <Text size="sm">
                    <QuestionMark /> Don&apos;t have an account yet? Open a
                    ticket to request it.
                  </Text>
                </Group>
              </Paper>
              <Footer />
            </Container>
          )}
          <ToastContainer
            position="top-center"
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            pauseOnHover
          />
        </AppShell>
      </MantineProvider>
    </ColorSchemeProvider>
  );
}

export default App;
