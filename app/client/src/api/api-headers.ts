import axios from "axios";

const apiClient = axios.create({
  baseURL: `/api/private/v1`,
});

export default apiClient;
