import React from "react";
import { screen } from "@testing-library/react";
import App from "./App";
import { asyncForEach, renderWithAllProviders } from "test-utils";

describe("App tests", () => {
  it("renders expected texts", async () => {
    const providerProps = {
      user: {
        uid: "username",
        username: "username",
        firstName: "firstName",
        lastName: "lastName",
        email: "email",
        roles: ["account1", "account2"],
      },
      selectedAccount: {
        id: 1,
        accountName: "AccountA",
        isGuestAccount: false,
      },
      setUserContext: vi.fn(),
      setSelectedAccountContext: vi.fn(),
    };
    renderWithAllProviders(<App />, { providerProps });
    const element = screen.getByText(/Transcription and Translation Service/i);
    expect(element).toBeInTheDocument();
    const texts = [
      /Your media/i,
      /Home/i,
      /Uploads assigned to you/i,
      /Settings/i,
      /Your Uploads/i,
      /Upload media/i,
    ];

    await asyncForEach(texts, async (text: string) => {
      const items = await screen.findAllByText(text);
      expect(items).toHaveLength(1);
    });

    expect(element).toBeInTheDocument();
  });
});
