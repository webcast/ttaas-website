import { DateTime } from "luxon";

export const getFormattedDate = (date: string) => {
  return DateTime.fromISO(date).toFormat("dd/MM/yyyy HH:mm");
};

export const getFormattedDateFromFormat = (date: string) => {
  console.log(date);
  const newDate = date.split(".")[0];
  const newFormat = DateTime.fromFormat(
    newDate,
    "yyyy-MM-dd HH:mm:ss",
  ).toFormat("dd/MM/yyyy HH:mm:ss");
  console.log(newFormat);
  return newFormat;
};

export default {
  getFormattedDate,
};
