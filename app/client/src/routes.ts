const appBaseRoute = "/app/";
const homeRoute = "home";
const yourFilesRoute = "your-media";
const uploadFormRoute = "new-upload";
const yourFilesDetailesRoute = "your-media/:mediaId";
const otherUploadsRoute = "other-uploads";
const uploadsRoute = "uploads";
const settingsRoute = "settings";
const creditsRoute = "credits";
const loginRoute = "app-login";
const logoutRoute = "app-logout";
const zoomLiveAsr = "zoom-live-asr";
const wsLiveAsr = "ws-live-asr";
const searchWeblectures = "search-weblectures";
const searchWeblecturesDetails = "search-weblectures/:searchId";

const getRouteWithBase = (route: string) => `/${appBaseRoute}/${route}`;

export default {
  appBaseRoute,
  homeRoute,
  yourFilesRoute,
  yourFilesDetailesRoute,
  otherUploadsRoute,
  uploadsRoute,
  settingsRoute,
  uploadFormRoute,
  creditsRoute,
  loginRoute,
  logoutRoute,
  zoomLiveAsr,
  wsLiveAsr,
  searchWeblectures,
  searchWeblecturesDetails,
  getRouteWithBase,
};
