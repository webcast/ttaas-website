import React from "react";
import { IAccount } from "types/account";
import { IUser } from "types/user";

export interface AppContextType {
  user: IUser | null;
  selectedAccount: IAccount | null;
  setUserContext: any;
  setSelectedAccountContext: any;
}

export const AppContext = React.createContext<AppContextType>({
  selectedAccount: null,
  user: null,
  setUserContext: () => {},
  setSelectedAccountContext: () => {},
});

export default AppContext;
