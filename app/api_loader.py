from typing import Tuple

from flask import Blueprint

from app.api.internal.v1.load_api import (
    load_api_namespaces as load_api_internal_namespaces,
)
from app.api.private.v1.load_api import (
    load_api_namespaces as load_api_private_namespaces,
)
from app.api.public.v1.load_api import load_api_namespaces as load_api_public_namespaces


def load_api_private_blueprints() -> Tuple[Blueprint]:
    """Loads all the blueprints, adding the development ones if needed

    Returns:
        list[Blueprint]: All the blueprints of the API
    """
    all_blueprints = (load_api_private_namespaces(),)

    return all_blueprints


def load_api_public_blueprints() -> Tuple[Blueprint]:
    """Loads all the blueprints, adding the development ones if needed

    Returns:
        list[Blueprint]: All the blueprints of the API
    """
    all_blueprints = (load_api_public_namespaces(),)

    return all_blueprints


def load_api_internal_blueprints() -> Tuple[Blueprint]:
    """Loads all the blueprints, adding the development ones if needed

    Returns:
        list[Blueprint]: All the blueprints of the API
    """
    all_blueprints = (load_api_internal_namespaces(),)

    return all_blueprints
