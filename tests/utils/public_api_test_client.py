# import os
from _pytest.monkeypatch import MonkeyPatch

from app.extensions import db
from app.models.accounts import Account
from tests.utils.base_test_client import BaseTestCase

# _basedir = os.path.abspath(os.path.dirname(__file__))


class PubliApiBaseTestCase(BaseTestCase):
    def setUp(self):
        super().setUp()
        self.monkeypatch = MonkeyPatch()

        account_one = Account(
            su_account="TEST_ACCOUNT", role="TEST_ROLE", account_name="Test Account"
        )
        db.session.add(account_one)
        db.session.commit()

        # info = {"tokenName": "test_token", "scope": "all"}
        # api_token = ApiTokenDAO.create(account_id=account_one.id, info=info)
        # self.access_token = api_token.access_token
