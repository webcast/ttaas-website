class MockUser:
    username = "anacleto"
    first_name = "Anacleto"
    last_name = "Agente Secreto"
    email = "anacletor@cern.ch"
    roles = ["role1", "role2"]

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return 10
