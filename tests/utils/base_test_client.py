import os
from pathlib import Path

from _pytest.monkeypatch import MonkeyPatch
from flask_testing import TestCase

from app.app_factory import create_app
from app.extensions import db

# _basedir = os.path.abspath(os.path.dirname(__file__))


class BaseTestCase(TestCase):
    # SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(_basedir, 'test_app.db')
    def create_app(self):
        app = create_app("config/config.test.py")
        app.config["TESTING"] = True
        app.config["LIVESERVER_PORT"] = 8943
        # Default timeout is 5 seconds
        app.config["LIVESERVER_TIMEOUT"] = 10
        return app

    def setUp(self):
        db.create_all()
        self.monkeypatch = MonkeyPatch()
        # common.Session = db.session
        # self.session = common.Session()

    def tearDown(self):
        db.session.remove()
        # common.Session.remove()
        db.drop_all()

        # os.remove(upload.media_file_path)
        current_dir = os.path.abspath(os.path.dirname(__file__))
        path = Path(current_dir)
        project_dir = path.parent.absolute().parent
        test_uploads_folder = os.path.join(project_dir, "uploads-test")
        # Delete all files in the uploads-test folder
        for file in os.listdir(test_uploads_folder):
            file_path = os.path.join(test_uploads_folder, file)
            try:
                # Delete mp4 file
                if file_path.endswith(".mp4"):
                    os.remove(file_path)
            except Exception as error:
                print(error)
