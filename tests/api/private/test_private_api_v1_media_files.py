import logging
from unittest import mock

import responses
from flask import url_for

from app.extensions import db
from app.models.accounts import Account
from tests.mocks.mock_media_responses import (
    response_download_language_vtt,
    response_languages,
    response_media_details_mock,
    responses_media_list_mock,
)
from tests.utils.base_test_client import BaseTestCase
from tests.utils.current_user_mock import MockUser

logger = logging.getLogger("webapp")


class MediaFilesTest(BaseTestCase):
    @responses.activate
    @mock.patch("flask_login.utils._get_user")
    def test_list_media_files(self, current_user):
        user = MockUser()
        current_user.return_value = user
        self.app.config["LOGIN_DISABLED"] = True
        with self.client:
            account = Account(
                role="role1", account_name="account1", su_account="su_account1"
            )
            db.session.add(account)
            db.session.commit()

            api_url = self.app.config["MLLP_CONFIG"]["API_BASE"]
            responses.add(
                responses.POST,
                f"{api_url}/speech/list_fullinfo",
                json=responses_media_list_mock,
                status=200,
            )

            response = self.client.get(
                url_for(
                    "private_api.transcriptions_media_endpoint",
                    account_id="1",
                ),
            )

            self.assertEqual(response.status_code, 200)
            result = response.json
            print(result)
            assert result["total"] == 3
            assert len(result["objects"]) == 3
            assert result["objects"][0]["id"] == "ttp-cern-9NgHd6f8"
            assert result["objects"][1]["id"] == "ttp-cern-kflsjdr0843"
            assert result["objects"][2]["id"] == "ttp-cern-345345"

    @responses.activate
    @mock.patch("flask_login.utils._get_user")
    def test_media_files_details(self, current_user):
        user = MockUser()
        current_user.return_value = user
        self.app.config["LOGIN_DISABLED"] = True
        with self.client:
            account = Account(
                role="role1", account_name="account1", su_account="su_account1"
            )
            db.session.add(account)
            db.session.commit()

            api_url = self.app.config["MLLP_CONFIG"]["API_BASE"]
            responses.add(
                responses.POST,
                f"{api_url}/speech/metadata",
                json=response_media_details_mock,
                status=200,
            )
            response = self.client.get(
                url_for(
                    "private_api.transcriptions_media_details_endpoint",
                    account_id=1,
                    media_id=123,
                )
            )
            self.assertEqual(response.status_code, 200)
            result = response.json
            print(result)
            assert result["rcode_description"] == "Media list and info available."
            assert result["audiotracks"] == []
            assert result["media"][0]["is_url"] is True
            assert result["media"][0]["type_code"] == 6
            assert result["media"][0]["media_format"] == "pcm"
            assert result["mediainfo"] is not None

    @responses.activate
    @mock.patch("flask_login.utils._get_user")
    def test_media_files_languages(self, current_user):
        user = MockUser()
        current_user.return_value = user
        self.app.config["LOGIN_DISABLED"] = True
        with self.client:
            account = Account(
                role="role1", account_name="account1", su_account="su_account1"
            )
            db.session.add(account)
            db.session.commit()

            api_url = self.app.config["MLLP_CONFIG"]["API_BASE"]
            responses.add(
                responses.POST,
                f"{api_url}/speech/langs",
                json=response_languages,
                status=200,
            )
            response = self.client.get(
                url_for(
                    "private_api.transcriptions_languages_endpoint",
                    account_id=1,
                    media_id=123,
                )
            )
            self.assertEqual(response.status_code, 200)
            result = response.json
            print(result)
            assert len(result["langs"]) == 3
            assert result["media_lang"] == "en"
            assert result["rcode"] == 0
            assert result["rcode_description"] == "Language list available."

            assert result["langs"][1]["lang_name"] == "Fran\u00e7ais"
            assert result["langs"][2]["lang_code"] == "en"

    @responses.activate
    @mock.patch("flask_login.utils._get_user")
    def test_languages_download_endpoint(self, current_user):
        user = MockUser()
        current_user.return_value = user
        self.app.config["LOGIN_DISABLED"] = True
        with self.client:
            account = Account(
                role="role1", account_name="account1", su_account="su_account1"
            )
            db.session.add(account)
            db.session.commit()

            api_url = self.app.config["MLLP_CONFIG"]["API_BASE"]
            responses.add(
                responses.POST,
                f"{api_url}/speech/get",
                json=response_download_language_vtt,
                status=200,
            )
            response = self.client.get(
                url_for(
                    "private_api.transcriptions_languages_download_endpoint",
                    account_id=1,
                    media_id=123,
                    lang="en",
                )
            )
            self.assertEqual(response.status_code, 200)
            result = str(response.data)
            print(result)
            assert "WEBVTT" in result
            assert "Never drink liquid nitrogen" in result


class ApiPrivateEditor(BaseTestCase):
    # @responses.activate
    @mock.patch("flask_login.utils._get_user")
    def test_editor_links_endpoint(self, current_user):
        user = MockUser()
        current_user.return_value = user
        self.app.config["LOGIN_DISABLED"] = True
        with self.client:
            account = Account(
                role="role1", account_name="account1", su_account="su_account1"
            )
            db.session.add(account)
            db.session.commit()
            response = self.client.get(
                url_for(
                    "private_api.transcriptions_editor_links_endpoint",
                    account_id=1,
                    media_id="12345",
                )
            )
            self.assertEqual(response.status_code, 200)
            result = response.json
            print(result)
            assert "https://ttp.mllp.upv.es/player" in result["editUrl"]
            assert "https://ttp.mllp.upv.es/player" in result["viewUrl"]
