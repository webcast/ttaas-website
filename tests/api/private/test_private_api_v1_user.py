import logging

from flask import url_for

from tests.utils.base_test_client import BaseTestCase

logger = logging.getLogger("webapp")


class ApiUserTest(BaseTestCase):
    def setUp(self):
        super().setUp()

    def test_api_user(self):
        self.app.config["LOGIN_DISABLED"] = True
        response = self.client.get(url_for("private_api.user_user_endpoint"))
        self.assertEqual(response.status_code, 200)
        self.assertIn("username", response.data.decode("utf-8"))
        self.assertIn("firstName", response.data.decode("utf-8"))
        self.assertIn("lastName", response.data.decode("utf-8"))
        self.assertIn("email", response.data.decode("utf-8"))
        self.assertIn("roles", response.data.decode("utf-8"))

    def test_api_user_denied(self):
        self.app.config["LOGIN_DISABLED"] = False
        response = self.client.get(url_for("private_api.user_user_endpoint"))
        self.assertEqual(response.status_code, 401)
        self.assertIn("Authorization Denied", response.data.decode("utf-8"))
