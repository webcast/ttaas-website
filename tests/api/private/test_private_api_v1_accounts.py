import json
import logging
from unittest import mock

from flask import url_for

from app.extensions import db
from app.models.accounts import Account
from tests.utils.base_test_client import BaseTestCase
from tests.utils.current_user_mock import MockUser

logger = logging.getLogger("webapp")


class ApiAccountsTest(BaseTestCase):
    def setUp(self):
        super().setUp()

    def test_api_user_denied(self):
        self.app.config["LOGIN_DISABLED"] = False
        response = self.client.get(url_for("private_api.accounts_account_endpoint"))
        self.assertEqual(response.status_code, 401)
        self.assertIn("Authorization Denied", response.data.decode("utf-8"))

    @mock.patch("flask_login.utils._get_user")
    def test_api_no_accounts(self, current_user):
        self.app.config["LOGIN_DISABLED"] = True
        with self.client:
            user = MockUser()
            current_user.return_value = user
            response = self.client.get(url_for("private_api.accounts_account_endpoint"))
            self.assertEqual(response.status_code, 200)
            self.assertEqual(
                [{"accountName": "Guest", "id": 0, "isGuestAccount": True}],
                json.loads(response.data),
            )

    @mock.patch("flask_login.utils._get_user")
    def test_api_with_accounts(self, current_user):
        self.app.config["LOGIN_DISABLED"] = True

        account = Account(
            role="role1", account_name="account1", su_account="su_account1"
        )
        db.session.add(account)
        db.session.commit()

        with self.client:
            user = MockUser()
            current_user.return_value = user
            self.assertEqual(len(Account.query.all()), 1)

            response = self.client.get(url_for("private_api.accounts_account_endpoint"))
            self.assertEqual(response.status_code, 200)
            self.assertEqual(
                [
                    {"accountName": "Guest", "id": 0, "isGuestAccount": True},
                    {"accountName": "account1", "id": 1, "isGuestAccount": False},
                ],
                json.loads(response.data),
            )
