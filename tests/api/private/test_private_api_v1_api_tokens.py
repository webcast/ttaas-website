import json
import logging
from unittest import mock

from flask import url_for

from app.daos.api_tokens import ApiTokenDAO
from app.extensions import db
from app.models.accounts import Account
from app.models.token_blacklist import ApiToken, ApiTokenResources
from tests.utils.base_test_client import BaseTestCase
from tests.utils.current_user_mock import MockUser

logger = logging.getLogger("webapp")


class ApiApitokensTest(BaseTestCase):
    def test_api_user_denied(self):
        self.app.config["LOGIN_DISABLED"] = False
        response = self.client.get(
            url_for("private_api.api-tokens_api_token_list_endpoint", account_id=1)
        )
        self.assertEqual(response.status_code, 401)
        self.assertIn("Authorization Denied", response.data.decode("utf-8"))

    @mock.patch("flask_login.utils._get_user")
    def test_api_no_tokens(self, current_user):
        self.app.config["LOGIN_DISABLED"] = True
        account = Account(
            role="role1", account_name="account1", su_account="su_account1"
        )
        db.session.add(account)
        db.session.commit()
        with self.client:
            user = MockUser()
            current_user.return_value = user
            response = self.client.get(
                url_for(
                    "private_api.api-tokens_api_token_list_endpoint", account_id="1"
                ),
            )
            self.assertEqual(response.status_code, 200)
            self.assertEqual([], json.loads(response.data))

    @mock.patch("flask_login.utils._get_user")
    def test_api_with_tokens(self, current_user):
        user = MockUser()
        current_user.return_value = user
        self.app.config["LOGIN_DISABLED"] = True
        with self.client:
            account = Account(
                role="role1", account_name="account1", su_account="su_account1"
            )
            db.session.add(account)
            db.session.commit()
            token = ApiTokenDAO.create(
                account.id,
                {
                    "tokenName": "token1",
                    "scope": ApiTokenResources.ALL,
                },
            )
            db.session.add(token)
            db.session.commit()

            response = self.client.get(
                url_for(
                    "private_api.api-tokens_api_token_list_endpoint", account_id="1"
                )
            )
            json_data = json.loads(response.data)
            self.assertEqual(response.status_code, 200)
            self.assertEqual(1, len(json_data))
            self.assertEqual(token.id, int(json_data[0]["id"]))
            self.assertEqual(token.name, json_data[0]["name"])
            self.assertEqual(None, json_data[0]["lastUsed"])
            with self.assertRaises(KeyError):
                json_data[0]["accessToken"]

    @mock.patch("flask_login.utils._get_user")
    def test_create_api_token(self, current_user):
        user = MockUser()
        current_user.return_value = user
        self.app.config["LOGIN_DISABLED"] = True
        with self.client:
            account = Account(
                role="role1", account_name="account1", su_account="su_account1"
            )
            db.session.add(account)
            db.session.commit()

            data = {"tokenName": "TEST token name", "scope": "all"}

            response = self.client.post(
                url_for(
                    "private_api.api-tokens_api_token_list_endpoint",
                    account_id="1",
                ),
                json=data,
            )

            json_data = json.loads(response.data)
            self.assertEqual(response.status_code, 201)
            self.assertEqual(json_data["id"], "1")
            self.assertEqual(json_data["name"], "TEST token name")
            self.assertEqual(json_data["resource"], "all")
            self.assertEqual(json_data["lastUsed"], None)
            assert json_data["accessToken"] is not None

    @mock.patch("flask_login.utils._get_user")
    def test_refresh_api_token(self, current_user):
        user = MockUser()
        current_user.return_value = user
        self.app.config["LOGIN_DISABLED"] = True
        with self.client:
            account = Account(
                role="role1", account_name="account1", su_account="su_account1"
            )
            db.session.add(account)
            db.session.commit()

            token = ApiTokenDAO.create(
                account.id,
                {
                    "tokenName": "token1",
                    "scope": ApiTokenResources.ALL,
                },
            )
            db.session.add(token)
            db.session.commit()

            response = self.client.put(
                url_for(
                    "private_api.api-tokens_api_token_details_endpoint",
                    token_id="1",
                ),
            )

            json_data = json.loads(response.data)
            print(json_data)
            self.assertEqual(response.status_code, 200)
            self.assertEqual(json_data["id"], "1")
            self.assertEqual(json_data["name"], "token1")
            self.assertEqual(json_data["resource"], "all")
            self.assertEqual(json_data["lastUsed"], None)
            assert json_data["accessToken"] is not None

    @mock.patch("flask_login.utils._get_user")
    def test_delete_api_token(self, current_user):
        user = MockUser()
        current_user.return_value = user
        self.app.config["LOGIN_DISABLED"] = True
        with self.client:
            account = Account(
                role="role1", account_name="account1", su_account="su_account1"
            )
            db.session.add(account)
            db.session.commit()

            token = ApiTokenDAO.create(
                account.id,
                {
                    "tokenName": "token1",
                    "scope": ApiTokenResources.ALL,
                },
            )
            db.session.add(token)
            db.session.commit()

            tokens = ApiToken.query.filter_by(revoked=False).all()
            assert len(tokens) == 1

            response = self.client.delete(
                url_for(
                    "private_api.api-tokens_api_token_details_endpoint",
                    token_id="1",
                ),
            )

            self.assertEqual(response.status_code, 204)

            tokens = ApiToken.query.filter_by(revoked=False).all()
            assert len(tokens) == 0
