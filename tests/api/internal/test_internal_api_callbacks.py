from flask import url_for

from app.api.internal.v1.namespaces.callbacks import AsrCallbackSource
from app.extensions import db
from app.models.accounts import Account
from app.models.captions_edits import SupervisionStatuses
from app.models.uploads import NotificationMethods, RunningStates
from app.tasks.email_notifications import send_single_caption_update_email
from app.tasks.transcriptions import update_cern_search_documents
from app.tasks.url_notifications import send_single_caption_callback
from tests.factories.cern_search_settings import CernSearchSettingsFactory
from tests.factories.upload import UploadFactory
from tests.utils.base_test_client import BaseTestCase


class ApiInternalCallbacks(BaseTestCase):
    def setUp(self):
        super().setUp()

        def task_mock(*args, **kwargs):
            return "OK"

        self.monkeypatch.setattr(update_cern_search_documents, "apply_async", task_mock)
        self.monkeypatch.setattr(
            send_single_caption_update_email, "apply_async", task_mock
        )
        self.monkeypatch.setattr(send_single_caption_callback, "apply_async", task_mock)
        self.monkeypatch.setattr(
            send_single_caption_update_email, "apply_async", task_mock
        )

    def test_callbacks_ingest_endpoint(self):
        self.app.config["LOGIN_DISABLED"] = True
        with self.client:
            account = Account(
                role="role1", account_name="account1", su_account="su_account1"
            )
            db.session.add(account)
            db.session.commit()

            upload = UploadFactory()
            upload.account_id = account.id
            db.session.commit()

            data = {
                "source": AsrCallbackSource.INGEST.value,
                "external_id": upload.media_id,
                "internal_status_code": RunningStates.COMPLETED.value,
            }

            response = self.client.post(
                url_for("internal_api.callbacks_callback_endpoint"), data=data
            )
            self.assertEqual(response.status_code, 200)

    def test_callbacks_ingest_not_found_endpoint(self):
        self.app.config["LOGIN_DISABLED"] = True
        with self.client:
            account = Account(
                role="role1", account_name="account1", su_account="su_account1"
            )
            db.session.add(account)
            db.session.commit()

            upload = UploadFactory()
            upload.account_id = account.id
            db.session.commit()

            data = {
                "source": AsrCallbackSource.INGEST.value,
                "external_id": "12345",
                "internal_status_code": RunningStates.COMPLETED.value,
            }

            response = self.client.post(
                url_for("internal_api.callbacks_callback_endpoint"), data=data
            )
            self.assertEqual(response.status_code, 400)

    def test_callbacks_postprocessor_email_endpoint(self):
        self.app.config["LOGIN_DISABLED"] = True
        with self.client:
            account = Account(
                role="role1", account_name="account1", su_account="su_account1"
            )
            db.session.add(account)
            db.session.commit()

            upload = UploadFactory()
            upload.account_id = account.id
            upload.notification_method = NotificationMethods.EMAIL
            db.session.commit()

            cern_search_settings = CernSearchSettingsFactory()
            upload.cern_search_settings = cern_search_settings
            db.session.add(cern_search_settings)
            db.session.commit()

            data = {
                "source": AsrCallbackSource.POST_PROCESSOR.value,
                "external_id": upload.media_id,
                "language": "en",
                "user": "test_user",
                "timestamp": "2022-10-19 08:51:33",
                "supervision_status": SupervisionStatuses.FULLY_HUMAN.value,
                "supervision_ratio": 4,
            }

            response = self.client.post(
                url_for("internal_api.callbacks_callback_endpoint"), data=data
            )
            self.assertEqual(response.status_code, 200)

    def test_callbacks_postprocessor_callback_endpoint(self):
        self.app.config["LOGIN_DISABLED"] = True
        with self.client:
            account = Account(
                role="role1", account_name="account1", su_account="su_account1"
            )
            db.session.add(account)
            db.session.commit()

            upload = UploadFactory()
            upload.account_id = account.id
            upload.notification_method = NotificationMethods.CALLBACK
            db.session.commit()

            cern_search_settings = CernSearchSettingsFactory()
            upload.cern_search_settings = cern_search_settings
            db.session.add(cern_search_settings)
            db.session.commit()

            data = {
                "source": AsrCallbackSource.POST_PROCESSOR.value,
                "external_id": upload.media_id,
                "language": "en",
                "user": "test_user",
                "timestamp": "2022-10-19 08:51:33",
                "supervision_status": SupervisionStatuses.FULLY_HUMAN.value,
                "supervision_ratio": 4,
            }

            response = self.client.post(
                url_for("internal_api.callbacks_callback_endpoint"), data=data
            )
            self.assertEqual(response.status_code, 200)

    def test_callbacks_first_ingest_endpoint(self):
        self.app.config["LOGIN_DISABLED"] = True
        with self.client:
            account = Account(
                role="role1", account_name="account1", su_account="su_account1"
            )
            db.session.add(account)
            db.session.commit()

            upload = UploadFactory()
            upload.account_id = account.id
            db.session.commit()

            data = {
                "source": AsrCallbackSource.INGEST.value,
                "media_id": upload.media_id,
                "internal_status_code": RunningStates.COMPLETED.value,
                "nonce": upload.nonce,
                "upload_id": upload.ingest_id,
                "language": "en",
                "external_id": upload.media_id,
                "user": "test_user",
                "timestamp": "2022-10-19 08:51:33",
                "supervision_status": SupervisionStatuses.FULLY_AUTOMATIC.value,
            }

            response = self.client.post(
                url_for("internal_api.callbacks_callback_ingest_endpoint"), data=data
            )
            self.assertEqual(response.status_code, 200)

    def test_callbacks_first_ingest_not_found_endpoint(self):
        self.app.config["LOGIN_DISABLED"] = True
        with self.client:
            account = Account(
                role="role1", account_name="account1", su_account="su_account1"
            )
            db.session.add(account)
            db.session.commit()

            upload = UploadFactory()
            upload.account_id = account.id
            db.session.commit()

            data = {
                "source": AsrCallbackSource.INGEST.value,
                "media_id": upload.media_id,
                "internal_status_code": RunningStates.COMPLETED.value,
                "nonce": "1234",
                "upload_id": upload.ingest_id,
            }

            response = self.client.post(
                url_for("internal_api.callbacks_callback_ingest_endpoint"), data=data
            )
            self.assertEqual(response.status_code, 400)

    def test_callbacks_first_ingest_wrong_source_endpoint(self):
        self.app.config["LOGIN_DISABLED"] = True
        with self.client:
            account = Account(
                role="role1", account_name="account1", su_account="su_account1"
            )
            db.session.add(account)
            db.session.commit()

            upload = UploadFactory()
            upload.account_id = account.id
            db.session.commit()

            data = {
                "source": AsrCallbackSource.POST_PROCESSOR.value,
                "media_id": upload.media_id,
                "internal_status_code": RunningStates.COMPLETED.value,
                "nonce": upload.nonce,
                "upload_id": upload.ingest_id,
            }

            response = self.client.post(
                url_for("internal_api.callbacks_callback_ingest_endpoint"), data=data
            )
            self.assertEqual(response.status_code, 400)
