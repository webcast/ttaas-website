import io
import logging

from flask import url_for

from app.models.pre_uploads import PreUpload
from app.models.uploads import TaskStates, Upload
from app.tasks.ingest import ingest_task
from tests.factories.upload import UploadFactory
from tests.utils.public_api_test_client import PubliApiBaseTestCase

logger = logging.getLogger("webapp")


class ApiPublicUploadsTestEmpty(PubliApiBaseTestCase):
    def test_list_uploads(self):
        self.app.config["LOGIN_DISABLED"] = True
        response = self.client.get(url_for("public_api.uploads_uploads_endpoint"))
        self.assertEqual(response.status_code, 200)
        result = response.json
        assert result["total"] == 0
        assert result["objects"] == []


class ApiPublicUploadsTestFull(PubliApiBaseTestCase):
    def test_list_uploads(self):
        upload = UploadFactory()
        self.assertEqual([upload], Upload.query.all())

        self.app.config["LOGIN_DISABLED"] = True
        response = self.client.get(url_for("public_api.uploads_uploads_endpoint"))
        self.assertEqual(response.status_code, 200)
        result = response.json
        assert result["total"] == 1
        assert len(result["objects"]) == 1
        assert result["objects"][0]["id"] == upload.id
        assert result["objects"][0]["mediaId"] == upload.media_id
        assert result["objects"][0]["title"] == upload.title
        assert result["objects"][0]["uploadType"] == upload.upload_type.value
        assert result["objects"][0]["notificationEmail"] == upload.notification_email
        assert result["objects"][0]["comments"] == upload.comments


class ApiPublicUploadsTestDetails(PubliApiBaseTestCase):
    def test_details_uploads(self):
        upload = UploadFactory()
        self.assertEqual([upload], Upload.query.all())

        self.app.config["LOGIN_DISABLED"] = True
        response = self.client.get(
            url_for(
                "public_api.uploads_upload_details_endpoint", media_id=upload.media_id
            ),
        )
        self.assertEqual(response.status_code, 200)
        result = response.json

        print(result)
        assert result["id"] == 1
        assert result["mediaId"] == upload.media_id
        assert result["existingMediaId"] is None
        assert result["language"] == upload.language.value
        assert result["title"] == upload.title
        assert result["state"] == upload.state.value
        assert result["comments"] == upload.comments
        assert result["notificationEmail"] == upload.notification_email
        assert result["uploadType"] == upload.upload_type.value
        assert result["createdOn"] is not None
        assert result["updatedOn"] is not None
        assert result["searchIsPublic"]


class ApiPublicUploadsTestIngest(PubliApiBaseTestCase):
    def test_ingest_upload(self):
        def ingest_task_mock(*args, **kwargs):
            return "OK"

        self.monkeypatch.setattr(ingest_task, "apply_async", ingest_task_mock)

        self.app.config["LOGIN_DISABLED"] = True

        media_name = "fake-media-stream.mp4"

        data = {
            "comments": "These are the comments of the upload",
            "language": "en",
            "notificationEmail": "test-test12345@cern.ch",
            "notificationMethod": "EMAIL",
            "title": "This is a test upload",
            "username": "test_user",
            "mediaFile": (io.BytesIO(b"some random data"), media_name),
        }
        logger.debug("data")
        logger.debug(data)
        response = self.client.post(
            url_for(
                "public_api.uploads_ingest_endpoint",
            ),
            data=data,
            headers={"Content-Type": "multipart/form-data"},
        )
        self.assertEqual(response.status_code, 201)
        result = response.json

        assert result["id"] is not None
        assert result["mediaId"] is not None
        assert result["existingMediaId"] is None
        assert result["state"] == TaskStates.CREATED.value
        assert result["createdOn"] is not None
        assert result["updatedOn"] is not None
        assert result["username"] == "test_user"
        assert result["title"] == "This is a test upload"

        pre_upload = PreUpload.query.get(result["id"])
        assert pre_upload is not None
        assert result["mediaId"] == pre_upload.media_id
