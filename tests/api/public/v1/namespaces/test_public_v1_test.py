import logging

from flask import url_for

from tests.utils.public_api_test_client import PubliApiBaseTestCase

logger = logging.getLogger("webapp")


class ApiPublicTestTest(PubliApiBaseTestCase):
    def test_api_user(self):
        self.app.config["LOGIN_DISABLED"] = True
        response = self.client.get(url_for("public_api.test_test_endpoint"))
        self.assertEqual(response.status_code, 200)
        self.assertIn("result", response.data.decode("utf-8"))

    def test_api_user_denied_no_header(self):
        response = self.client.get(url_for("public_api.test_test_endpoint"))
        self.assertEqual(response.status_code, 401)
        self.assertIn("Missing Authorization Header", response.data.decode("utf-8"))
