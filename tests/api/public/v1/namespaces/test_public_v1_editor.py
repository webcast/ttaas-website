import logging

from flask import url_for

from tests.utils.public_api_test_client import PubliApiBaseTestCase

logger = logging.getLogger("webapp")


class ApiPublicEditor(PubliApiBaseTestCase):
    def test_editor_links_endpoint(self):
        self.app.config["LOGIN_DISABLED"] = True
        response = self.client.get(
            url_for("public_api.editor_editor_links_endpoint", media_id="12345")
        )
        self.assertEqual(response.status_code, 200)
        result = response.json
        assert "https://ttp.mllp.upv.es/player" in result["editUrl"]
        assert "https://ttp.mllp.upv.es/player" in result["viewUrl"]
