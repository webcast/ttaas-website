import logging

import responses
from flask import url_for

from tests.mocks.mock_media_responses import (
    response_download_language_vtt,
    response_languages,
    response_media_details_mock,
    responses_media_list_mock,
)
from tests.utils.public_api_test_client import PubliApiBaseTestCase

logger = logging.getLogger("webapp")


class ApiPublicMediaFiles(PubliApiBaseTestCase):
    @responses.activate
    def test_media_endpoint(self):
        self.app.config["LOGIN_DISABLED"] = True
        api_url = self.app.config["MLLP_CONFIG"]["API_BASE"]
        responses.add(
            responses.POST,
            f"{api_url}/speech/list_fullinfo",
            json=responses_media_list_mock,
            status=200,
        )
        response = self.client.get(
            url_for("public_api.media-files_media_list_endpoint")
        )
        self.assertEqual(response.status_code, 200)
        result = response.json
        print(result)
        assert result["total"] == 3
        assert len(result["objects"]) == 3
        assert result["objects"][0]["id"] == "ttp-cern-9NgHd6f8"
        assert result["objects"][1]["id"] == "ttp-cern-kflsjdr0843"
        assert result["objects"][2]["id"] == "ttp-cern-345345"

    @responses.activate
    def test_media_details_endpoint(self):
        self.app.config["LOGIN_DISABLED"] = True
        api_url = self.app.config["MLLP_CONFIG"]["API_BASE"]
        responses.add(
            responses.POST,
            f"{api_url}/speech/metadata",
            json=response_media_details_mock,
            status=200,
        )
        response = self.client.get(
            url_for("public_api.media-files_media_details_endpoint", media_id=123)
        )
        self.assertEqual(response.status_code, 200)
        result = response.json
        print(result)
        assert len(result) == 6
        assert result["rcode_description"] == "Media list and info available."
        assert result["audiotracks"] == []
        assert result["media"][0]["is_url"] is True
        assert result["media"][0]["type_code"] == 6
        assert result["media"][0]["media_format"] == "pcm"
        assert result["mediainfo"] is not None


class ApiPublicMediaFilesLanguages(PubliApiBaseTestCase):
    @responses.activate
    def test_languages_endpoint(self):
        self.app.config["LOGIN_DISABLED"] = True
        api_url = self.app.config["MLLP_CONFIG"]["API_BASE"]
        responses.add(
            responses.POST,
            f"{api_url}/speech/langs",
            json=response_languages,
            status=200,
        )
        response = self.client.get(
            url_for("public_api.media-files_languages_endpoint", media_id=123)
        )
        self.assertEqual(response.status_code, 200)
        result = response.json
        assert len(result["langs"]) == 3
        assert result["media_lang"] == "en"
        assert result["rcode"] == 0
        assert result["rcode_description"] == "Language list available."

        assert result["langs"][1]["lang_name"] == "Fran\u00e7ais"
        assert result["langs"][2]["lang_code"] == "en"

    @responses.activate
    def test_languages_download_endpoint(self):
        self.app.config["LOGIN_DISABLED"] = True
        api_url = self.app.config["MLLP_CONFIG"]["API_BASE"]
        responses.add(
            responses.POST,
            f"{api_url}/speech/get",
            json=response_download_language_vtt,
            status=200,
        )
        response = self.client.get(
            url_for(
                "public_api.media-files_languages_download_endpoint",
                media_id=123,
                lang="en",
            )
        )
        self.assertEqual(response.status_code, 200)
        result = str(response.data)
        print(result)
        assert "WEBVTT" in result
        assert "Never drink liquid nitrogen" in result


class ApiPublicMediaFilesChecksum(PubliApiBaseTestCase):
    @responses.activate
    def test_checksum_endpoint(self):
        self.app.config["LOGIN_DISABLED"] = True
        api_url = self.app.config["MLLP_CONFIG"]["API_BASE"]
        responses.add(
            responses.POST,
            f"{api_url}/speech/get",
            json=response_download_language_vtt,
            status=200,
        )
        response = self.client.get(
            url_for(
                "public_api.media-files_media_file_checksum_exists_endpoint",
                checksum="1234567",
            )
        )
        self.assertEqual(response.status_code, 200)
        result = response.json
        assert result["already_exists"] is False
