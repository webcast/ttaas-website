import json

import responses

from app.services.mllp.api_client import ApiSpeechClientMLLP
from tests.mocks.mock_responses import (
    responses_mock_get_media_langs,
    responses_mock_get_media_metadata,
    responses_mock_get_upload_list,
    responses_mock_get_upload_status,
    responses_mock_list_media,
    responses_mock_list_systems,
)
from tests.utils.base_test_client import BaseTestCase


class ServiceMllpApiClientTest(BaseTestCase):
    @responses.activate
    def test_get_media_list(self):
        api_url = self.app.config["MLLP_CONFIG"]["API_BASE"]
        responses.add(
            responses.POST,
            f"{api_url}/speech/list",
            json=responses_mock_list_media,
            status=200,
        )

        client = ApiSpeechClientMLLP()
        result = client.get_media_list()
        self.assertEqual(json.loads(result), responses_mock_list_media)

    @responses.activate
    def test_get_systems_list(self):
        api_url = self.app.config["MLLP_CONFIG"]["API_BASE"]
        responses.add(
            responses.POST,
            f"{api_url}/speech/systems",
            json=responses_mock_list_systems,
            status=200,
        )

        client = ApiSpeechClientMLLP()
        result = client.get_systems_list()
        self.assertEqual(json.loads(result), responses_mock_list_systems)

    @responses.activate
    def test_get_media_langs(self):
        api_url = self.app.config["MLLP_CONFIG"]["API_BASE"]
        responses.add(
            responses.POST,
            f"{api_url}/speech/langs",
            json=responses_mock_get_media_langs,
            status=200,
        )

        client = ApiSpeechClientMLLP()
        result = client.get_media_langs("1")
        self.assertEqual(json.loads(result), responses_mock_get_media_langs)

    @responses.activate
    def test_get_media_metadata(self):
        api_url = self.app.config["MLLP_CONFIG"]["API_BASE"]
        responses.add(
            responses.POST,
            f"{api_url}/speech/metadata",
            json=responses_mock_get_media_metadata,
            status=200,
        )

        client = ApiSpeechClientMLLP()
        result = client.get_media_metadata("1")
        self.assertEqual(json.loads(result), responses_mock_get_media_metadata)

    @responses.activate
    def test_get_upload_status(self):
        api_url = self.app.config["MLLP_CONFIG"]["API_BASE"]
        responses.add(
            responses.POST,
            f"{api_url}/speech/status",
            json=responses_mock_get_upload_status,
            status=200,
        )

        client = ApiSpeechClientMLLP()
        result = client.get_upload_status("1")
        self.assertEqual(json.loads(result), responses_mock_get_upload_status)

    @responses.activate
    def test_get_uploads_list(self):
        api_url = self.app.config["MLLP_CONFIG"]["API_BASE"]
        responses.add(
            responses.POST,
            f"{api_url}/speech/uploadslist",
            json=responses_mock_get_upload_list,
            status=200,
        )

        client = ApiSpeechClientMLLP()
        result = client.get_upload_list("1")
        self.assertEqual(json.loads(result), responses_mock_get_upload_list)
