import random

import factory

from app.daos.uploads import generate_media_id
from app.extensions import db
from app.models.upload_states import (
    AvailableLanguages,
    NotificationMethods,
    UploadTypes,
)
from app.models.uploads import Upload
from tests.factories.cern_search_settings import CernSearchSettingsFactory


class UploadFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = Upload

        # Use the not-so-global scoped_session
        # Warning: DO NOT USE common.Session()!
        sqlalchemy_session = db.session

    title = factory.Faker("company")
    username = factory.Faker("user_name")
    language = random.choice([AvailableLanguages.EN, AvailableLanguages.FR])
    media_file_path = "/dev/null"
    account_id = 1
    upload_type = random.choice([UploadTypes.API, UploadTypes.WEB])
    notification_method = random.choice(
        [
            NotificationMethods.CALLBACK,
            NotificationMethods.EMAIL,
            NotificationMethods.NONE,
        ]
    )
    notification_email = factory.Faker("email")
    media_id = generate_media_id("test_account")
    comments = factory.Faker("paragraph", nb_sentences=5)
    ingest_id = factory.Faker("pystr")
    nonce = factory.Faker("pystr")

    cern_search_settings = factory.SubFactory(CernSearchSettingsFactory)
