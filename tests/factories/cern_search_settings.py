import factory

from app.extensions import db
from app.models.cern_search_settings import CernSearchSettings


class CernSearchSettingsFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = CernSearchSettings

        # Use the not-so-global scoped_session
        # Warning: DO NOT USE common.Session()!
        sqlalchemy_session = db.session

    is_public = True
