responses_mock_list_media = [
    {
        "duration": 167,
        "id": "ttp-cern-9NgHd6f8",
        "language": "en",
        "title": "LIVE from ISR - Injection of the first pilot beams into the LHC",
    },
    {
        "duration": 168,
        "id": "ttp-cern-9NgHd6f9",
        "language": "fr",
        "title": "Direct des ISR - LHC",
    },
]

responses_mock_list_systems = [
    {
        "lang": "en",
        "topic_adaptation_time": None,
        "rtf": None,
        "description": "Adapt-1L-id CERN English ASR System [2020-12-21]",
        "topic_adaptation": None,
        "id": 150,
        "name": "CERN-Adapted English ASR System (Adapt-1L-id)",
    },
    {
        "lang": "fr",
        "topic_adaptation_time": None,
        "rtf": None,
        "description": "CERN French ASR System [2020-12-21]",
        "topic_adaptation": None,
        "id": 151,
        "name": "CERN-Adapted French ASR System (Adapt-1L-id)",
    },
]


responses_mock_get_media_langs = {
    "media_lang": "en",
    "langs": [
        {
            "audiotracks": [],
            "system_time": None,
            "sup_status": 0,
            "sup_ratio": 0.0,
            "rev_rtf": 0.0,
            "system_rtf": None,
            "lang_code": "es",
            "rev_time": 0,
            "lang_name": "Espa\u00f1ol",
        },
        {
            "audiotracks": [],
            "system_time": None,
            "sup_status": 0,
            "sup_ratio": 0.0,
            "rev_rtf": 0.0,
            "system_rtf": None,
            "lang_code": "fr",
            "rev_time": 0,
            "lang_name": "Fran\u00e7ais",
        },
        {
            "audiotracks": [],
            "system_time": None,
            "sup_status": 0,
            "sup_ratio": 0.0,
            "rev_rtf": 0.0,
            "system_rtf": None,
            "lang_code": "en",
            "rev_time": 0,
            "lang_name": "English",
        },
    ],
    "rcode_description": "Language list available.",
    "rcode": 0,
    "subs_locked": False,
}

responses_mock_get_media_metadata = {
    "rcode_description": "Media list and info available.",
    "audiotracks": [],
    "media": [
        {
            "is_url": True,
            "type_code": 6,
            "media_format": "pcm",
            "location": "https://ttp.mllp.upv.es/data/8/876069312e03ec70671e249616fea2d276cfdbd5dc5c9c5acea22cea3197dae0/37a2021157cabe5f1ccc34cc18ec4ba1.pcm",  # noqa
        },
        {
            "is_url": True,
            "type_code": 3,
            "media_format": "jpg",
            "location": "https://ttp.mllp.upv.es/data/8/876069312e03ec70671e249616fea2d276cfdbd5dc5c9c5acea22cea3197dae0/d3b6a31d2dcd94155634acab110941cf.jpg",  # noqa
        },
        {
            "is_url": True,
            "type_code": 0,
            "media_format": "mp4",
            "location": "https://ttp.mllp.upv.es/data/8/876069312e03ec70671e249616fea2d276cfdbd5dc5c9c5acea22cea3197dae0/8fbdf63d9863aacbfa31f8d26ab01a36.mp4",  # noqa
        },
    ],
    "mediainfo": {
        "duration": 167,
        "category": None,
        "speakers": [{"name": "CERN"}],
        "language": "en",
        "title": "LIVE from ISR - Injection of the first pilot beams into the LHC",
    },
    "rcode": 0,
}

responses_mock_get_upload_status = {
    "rcode": 1,
    "rcode_description": "Media uploading",
    "status_code": 200,
    "status_info": "Found",
    "error_code": 0,
    "uploaded_on": "2020-12-21T12:00:00",
    "last_update": "2020-12-21T12:00:00",
    "estimated_time_complete": "10 minutes",
}

responses_mock_get_upload_list = [
    {
        "id": "ttp-cern-9NgHd6f9",
        "object_id": "ttp-cern-9NgHd6f9",
        "status_code": 200,
        "uploaded_on": "2020-12-21T12:00:00",
        "last_update": "2020-12-21T12:00:00",
    },
    {
        "id": "ttp-cern-9NgHd6f0",
        "object_id": "ttp-cern-9NgHd6f0",
        "status_code": 200,
        "uploaded_on": "2020-12-21T13:00:00",
        "last_update": "2020-12-21T13:00:00",
    },
]
