responses_media_list_mock = {
    "total": 3,
    "objects": [
        {
            "duration": 167,
            "id": "ttp-cern-9NgHd6f8",
            "language": "en",
            "title": "LIVE from ISR - Injection of the first pilot beams into the LHC",
        },
        {
            "duration": 167,
            "id": "ttp-cern-kflsjdr0843",
            "language": "en",
            "title": "LIVE 2 - Injection of the second",
        },
        {
            "duration": 16,
            "id": "ttp-cern-345345",
            "language": "fr",
            "title": "LIVE 3 - All injected",
        },
    ],
}

response_media_details_mock = {
    "rcode_description": "Media list and info available.",
    "audiotracks": [],
    "media": [
        {
            "is_url": True,
            "type_code": 6,
            "media_format": "pcm",
            "location": "https://ttp.mllp.upv.es/data/8/876069312e03ec70671e249616fea2d276cfdbd5dc5c9c5acea22cea3197dae0/37a2021157cabe5f1ccc34cc18ec4ba1.pcm",  # noqa
        },
        {
            "is_url": True,
            "type_code": 3,
            "media_format": "jpg",
            "location": "https://ttp.mllp.upv.es/data/8/876069312e03ec70671e249616fea2d276cfdbd5dc5c9c5acea22cea3197dae0/d3b6a31d2dcd94155634acab110941cf.jpg",  # noqa
        },
        {
            "is_url": True,
            "type_code": 0,
            "media_format": "mp4",
            "location": "https://ttp.mllp.upv.es/data/8/876069312e03ec70671e249616fea2d276cfdbd5dc5c9c5acea22cea3197dae0/8fbdf63d9863aacbfa31f8d26ab01a36.mp4",  # noqa
        },
    ],
    "mediainfo": {
        "duration": 167,
        "category": None,
        "speakers": [{"name": "CERN"}],
        "language": "en",
        "title": "LIVE from ISR - Injection of the first pilot beams into the LHC",
    },
    "rcode": 0,
}

response_languages = {
    "media_lang": "en",
    "langs": [
        {
            "audiotracks": [],
            "system_time": None,
            "sup_status": 0,
            "sup_ratio": 0.0,
            "rev_rtf": 0.0,
            "system_rtf": None,
            "lang_code": "es",
            "rev_time": 0,
            "lang_name": "Espa\u00f1ol",
        },
        {
            "audiotracks": [],
            "system_time": None,
            "sup_status": 0,
            "sup_ratio": 0.0,
            "rev_rtf": 0.0,
            "system_rtf": None,
            "lang_code": "fr",
            "rev_time": 0,
            "lang_name": "Fran\u00e7ais",
        },
        {
            "audiotracks": [],
            "system_time": None,
            "sup_status": 0,
            "sup_ratio": 0.0,
            "rev_rtf": 0.0,
            "system_rtf": None,
            "lang_code": "en",
            "rev_time": 0,
            "lang_name": "English",
        },
    ],
    "rcode_description": "Language list available.",
    "rcode": 0,
    "subs_locked": False,
}

response_download_language_vtt = """
WEBVTT

00:01.000 --> 00:04.000
- Never drink liquid nitrogen.

00:05.000 --> 00:09.000
- It will perforate your stomach.
- You could die.
"""
