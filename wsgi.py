import urllib3

from app.app_factory import create_app

urllib3.disable_warnings()

app = create_app()
