# Deployment

> ⚠️ Secrets and service accounts are not restored. You need to create them manually. These include:
>
> - Secret `gitlab-registry-auth`: To access the Gitlab Registry
> - Service Account `gitlabci-deployer`: To give Gitlab access to deploy in Openshift

## Add the Openshift deploy key to Gitlab

> To grant access to the private Gitlab repository

```
oc login paas.cern.ch
oc project (ttaas|ttaas-qa|ttaas-test) # 2 options
oc get secrets/sshdeploykey -o go-template='{{index .metadata.annotations "cern.ch/ssh-public-key"}}{{println}}'
```

## Deploy a Docker image

1. Run the following commands in Openshift

```bash
oc create serviceaccount gitlabci-deployer
```

```bash
oc policy add-role-to-user registry-editor -z gitlabci-deployer
```

```bash
oc policy add-role-to-user view -z gitlabci-deployer
```

```bash
oc serviceaccounts get-token gitlabci-deployer
```

2. Paste the generated token in Gitlab `Settings/CI/CD/Variables` as `IMAGE_IMPORT_TOKEN_<PROD|QA|TEST>`.

### If you are using Gitlab CI's registry

3. Generate a deploy token in Gitlab. This will generate a username and a password that will be added to Openshift later.
   1. Needed rights for: `read_registry`
   2. The script is located on [/okd4/grant-registry-access.sh](/okd4/grant-registry-access.sh). Change the corresponging username/password.

### If you are using registry.cern.ch

On paas.cern.ch create a pull secret with the name `registry.cern.ch` in order to pull the image from the registry.

Add the pull secret to the `gitlabci-deployer` service account and to the `default` service account.

The username and password must be the ones of the robot account created in registry.cern.ch.

- Imagestream being updated from Gitlab when `redeploy` stage finishes

To restore a backup of the elements in Openshift:

## Restore the config maps:

```bash
oc create -f okd4/configmap.yaml
```

Update the values in the configmap to match the correct ones.

The secrets can be generated using python:

```python
import os
os.urandom(24)
```

## Create the Redis pod for caching:

In OKD4 select the template and deploy it (`Add to project` -> `Select from project`) and select redis persistent.

## Set the API endpoint in Gitlab

Create the `API_ENDPOINT_TEST`, `API_ENDPOINT_QA` and `API_ENDPOINT_PROD` variables in Gitlab CI. These should point to the domain the application will be on (ex: https://ttaas-test.web.cern.ch)

## Create the storage volumes

1. PersistentVolumeClaims:
   1. `celerybeat-schedule`: 8GiB - cephfs-no-backup
   2. `uploads`: 50GiB (QA and Test) - cephfs-no-backup

## Restore the QA application (deployment configs, image streams, services and routes)

⚠️ Beware that you will need to change the project name (ttaas|ttaas-qa|ttaas-test)

```bash
oc create -f okd4/project.yaml
```

- By default, the ImageStream will take the `qa` Docker image, go to the ImageStream and change `qa` with `prod`.
- The route will point to https://localhost. Change this to the hostname of your application.

## Restore the celery tasks deployments

⚠️ Beware that you will need to change the project name (ttaas|ttaas-qa|ttaas-test)

```bash
oc create -f okd4/celery-tasks.yaml
```

## OKD4 useful commands

```bash
oc debug <POD_NAME>
```
