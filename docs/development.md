# Development

## Requirements

- Python 3.9.10
- Node v17.5.0

## Technologies

- Flask
- React

## Local install

### Database

You can install a database of your choice in your computer.

- MySQL: https://dev.mysql.com/downloads/mysql/

### Dependencies

### Python dependencies using Poetry

Poetry will take all the information on the `pyproject.toml` file and will install all its dependencoies.

You can install Poetry using the following command:

**Linux or Mac:**
```bash
curl -sSL https://install.python-poetry.org | python3 -
```

**Windows:**
```powershell
(Invoke-WebRequest -Uri https://install.python-poetry.org -UseBasicParsing).Content | py -
```

Source: https://python-poetry.org/docs/#installing-with-the-official-installer

#### Install dependencies and create the virtual environment

```bash
poetry install
```

Activate the virtual env:
```bash
source .venv/bin/activate
```

### Node and React dependencies

Install the project dependencies for Javascript:

```bash
cd app/client
yarn
```

### Install the pre-commit Git hook

```bash
pre-commit install
```

### Configuration file

Rename `app/config/config.py.sample` to `app/config/config.py` and populate it with the correct values (Database, paths, etc).

### Migrate the database

Run the migrations on the database

```bash
FLASK_APP=wsgi.py flask db upgrade
```

## Run

### Webapp

```bash
FLASK_APP=wsgi.py FLASK_DEBUG=true flask run
```

Another option is to use Visual Studio Code for running and debugging. In this case just go to the Visual Studio Code menu and select "Run" and "Run without debugging" or "Start debugging".

### Celery and Redis

#### Redis

Redis is required to run celery. It can be installed with the following commands depending on your platform:

- Ubuntu: `sudo apt-get install redis-server`
- Mac: `brew install redis`

Then run the server:

```bash
redis-server
```

#### Celery

> Don't forget to set up the correct setting in the `config.py` file.

```bash
celery -A celery_app:celery worker -Q default --loglevel=info -P solo
```

Another option is to use Visual Studio Code for running and debugging. In this case just go to the Visual Studio Code menu and select "Run" and "Run without debugging" or "Start debugging".

## Other Available commands

Get a list of the available commands running the following:

```bash
FLASK_APP=wsgi.py flask --help
```

## Links to libs and other docs

- Mantine (UI): https://mantine.dev/
- Paas Docs: https://paas.docs.cern.ch/
