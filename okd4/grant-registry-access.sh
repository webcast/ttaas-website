# On Gitlab: Settings -> Repository Settings -> Deploy Tokens
# This will be used to grant access to Openshift to the Gitlab Docker registry
# You must be logged in to your project in Openshift (oc login openshift.cern.ch && oc project <WATHEVER>)
# DO NOT put quotes at the beginning and end
user=<REPLACE_BY_GTILAB_TOKEN_USER>
token=<REPLACE_BY_GTILAB_TOKEN_PASS>

# Generate the secret (make sure to `oc login` into your Openshift project first)
auth=${user}:${token}
auth_encoded=$(printf "%s" $auth | base64)
dockercfg=$(echo "{\"auths\": {\"gitlab-registry.cern.ch\": {\"auth\": \"${auth_encoded}\"}, \"gitlab.cern.ch\": {\"auth\": \"${auth_encoded}\"}}}")
oc create secret generic gitlab-registry-auth --from-literal=.dockerconfigjson="${dockercfg}" --type=kubernetes.io/dockerconfigjson
## After this, assign the registry pull secret to the service account running your application.
## Please note that, by default, applications are run by the 'default' service account.
## If that is not your case, adjust the service account name in the command as appropriate:
oc secrets link default gitlab-registry-auth --for=pull
