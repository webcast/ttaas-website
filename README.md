# Transcription and Translation Service Website

<p align="center"><img src="transcription.png" alt="Transcription logo" height="200"></p>

This application contains the frontend and the API to connect to the Transcription and Translation Service.

## Development

Please refer to [Development docs](/docs/development.md)

## Attributions

- <a href="https://www.flaticon.com/free-icons/transcription" title="transcription icons">Transcription icon created by Freepik - Flaticon</a>
- Illustrations by https://undraw.co/search
